//
//  SceneDelegate.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/25/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import FBSDKCoreKit
import FBSDKLoginKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    let googleAPIKey = "AIzaSyBELqZDkQbiHu4_vZvGKBRHof3wuudIMNc"
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
       guard let windowScene = (scene as? UIWindowScene) else { return }
        
        GMSServices.provideAPIKey(googleAPIKey)
        GMSPlacesClient.provideAPIKey(googleAPIKey)
        
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.default
        
        UITabBar.appearance().tintColor = UIColor(hexString:"FD7B5E")
        let vc = ISCFPTJobsLaunchViewController()
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
       
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Not called under iOS 12 - See AppDelegate applicationDidBecomeActive
        
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Not called under iOS 12 - See AppDelegate applicationWillResignActive
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Not called under iOS 12 - See AppDelegate applicationWillEnterForeground
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Not called under iOS 12 - See AppDelegate applicationDidEnterBackground
    }
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else { return };
        ApplicationDelegate.shared.application( UIApplication.shared, open: url, sourceApplication: nil, annotation: [UIApplication.OpenURLOptionsKey.annotation] )
        
    }
}
