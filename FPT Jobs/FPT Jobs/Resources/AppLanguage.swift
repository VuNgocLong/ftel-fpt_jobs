//
//  AppLanguage.swift
//  ISCCamera
//
//  Created by Long Vu on 9/11/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

struct AppLanguage {
    
    static var unauthorizedMessage =  Locale.preferredLanguages[0].contains("vi") ? "Hết thời gian đăng nhập":"Time login expire"
    static var appAlert =  Locale.preferredLanguages[0].contains("vi") ? "Thông Báo":"Alert"
    static var commonAppName = "FPT Jobs"
    static var commonAppSetting =  Locale.preferredLanguages[0].contains("vi") ? "Cài đặt":"Setting"
    static var errorNetworkNoInternet =  Locale.preferredLanguages[0].contains("vi") ? "Không có kết nối mạng. Vui lòng kiểm tra mạng và thử lại!":"No network connection. Please check the network and try again!"
    static var errorNetworkInCorrectTypeResponse  =  Locale.preferredLanguages[0].contains("vi") ? "Dữ liệu không hợp lệ!":"Data invalid"
    static var errorNetworkFailureConnectionApi =  Locale.preferredLanguages[0].contains("vi") ? "Kết nối thất bại!":"Connect fail"
    static var commonConfirmTitle =  Locale.preferredLanguages[0].contains("vi") ? "Xác nhận":"Confirm"
    static var commonOkTitle =  Locale.preferredLanguages[0].contains("vi") ? "Đồng ý":"OK"
    static var commonCancelTitle  =  Locale.preferredLanguages[0].contains("vi") ? "Huỷ":"Cancel"
    static var jobsTabTitle =  Locale.preferredLanguages[0].contains("vi") ? "Việc làm":"Jobs"
    static var newsTabTitle =  Locale.preferredLanguages[0].contains("vi") ? "Tin tức":"News"
    static var eventTabTitle =  Locale.preferredLanguages[0].contains("vi") ? "Sự kiện":"Event"
    static var newsShareTitle =  Locale.preferredLanguages[0].contains("vi") ? "Tin tức và chia sẻ":"News and Sharing"
    static var biometricTabTitle = Locale.preferredLanguages[0].contains("vi") ? "Sinh trắc học":"Biometrics"
    static var contactTabTitle = Locale.preferredLanguages[0].contains("vi") ? "Liên hệ":"Contact"
    static var jobsNotiTitle =  Locale.preferredLanguages[0].contains("vi") ? "Thông báo":"Notification"
    static var searchContentPlaceHolder = Locale.preferredLanguages[0].contains("vi") ? "Gõ từ khoá bạn cần tìm...":"Enter keyword..."
    static var connecting = Locale.preferredLanguages[0].contains("vi") ? "Kết nối......":"Connecting......"
    static var jobsDetailTitle = Locale.preferredLanguages[0].contains("vi") ? "Tin tuyển dụng chi tiết":"Job Details"
    static var jobsSalary = Locale.preferredLanguages[0].contains("vi") ? "Thoả thuận":"Deal"
    static var nullEmail = Locale.preferredLanguages[0].contains("vi") ? "Email không được để trống":"Email is not empty"
    static var nullIDCard = Locale.preferredLanguages[0].contains("vi") ? "CMND không được để trống":"ID card is not empty"
    static var nullHeight = Locale.preferredLanguages[0].contains("vi") ? "Chiều cao không được để trống":"Height is not empty"
    static var nullWeight = Locale.preferredLanguages[0].contains("vi") ? "Cân nặng không được để trống":"Weight is not empty"
    static var salaryInvalid = Locale.preferredLanguages[0].contains("vi") ? "Tiền lương không đúng định dạng":"Salary invalid"
    static var nullResidentAddress = Locale.preferredLanguages[0].contains("vi") ? "Địa chỉ nơi ở hiện tại không được để trống":"Resident Address is not empty"
    static var nullResidentProvince = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn tỉnh thành nơi ở hiện tại":"Please select resident province"
    static var nullResidentDistrict = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn quận huyện nơi ở hiện tại":"Please select resident district"
    static var nullBirthDayDistrict = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn quận huyện quê quán":"Please select home town district"
    static var nullBirthDayProvince = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn tỉnh thành quê quán":"Please select home town province"
    static var nullName = Locale.preferredLanguages[0].contains("vi") ? "Tên hiển thị không được để trống":"Fullname is not empty"
    static var nullPassword = Locale.preferredLanguages[0].contains("vi") ? "Mật khẩu không được để trống":"Password is not empty"
    static var nullOTP = Locale.preferredLanguages[0].contains("vi") ? "Mã code không được để trống":"Code is not empty"
    static var nullRePassword = Locale.preferredLanguages[0].contains("vi") ? "Mật khẩu xác nhận không được để trống":"Password confirm is not empty"
    static var emailInvalid = Locale.preferredLanguages[0].contains("vi") ? "Email không đúng định dạng":"Email invalid"
    static var passwordInvalid = Locale.preferredLanguages[0].contains("vi") ? "Độ dài mật khẩu từ 8 ký tự":"Password length from 8 characters"
    static var repassInvalid = Locale.preferredLanguages[0].contains("vi") ? "Mật khẩu xác nhận không trùng khớp":"Password confirm invalid"
    static var termsOfService = Locale.preferredLanguages[0].contains("vi") ? "Điều khoản dịch vụ":"Tems of service"
    static var privatePolicy = Locale.preferredLanguages[0].contains("vi") ? "Chính sách bảo mật":"Privacy policy"
    static var questionOfService = Locale.preferredLanguages[0].contains("vi") ? "Câu hỏi thường gặp":"Frequently questions"
    static var userProfile = Locale.preferredLanguages[0].contains("vi") ? "Thông tin cá nhân":"Profile"
    static var forgetPasswordAlert = Locale.preferredLanguages[0].contains("vi") ? "Xin vui lòng check email để lấy mã reset":"Please check email"
    static var registerAccountSuccess = Locale.preferredLanguages[0].contains("vi") ? "Đăng ký tài khoản thành công":"Register account success"
    static var updateAccountSuccess = Locale.preferredLanguages[0].contains("vi") ? "Cập nhật tài khoản thành công":"Update account success"
    static var updateAccountFail = Locale.preferredLanguages[0].contains("vi") ? "Cập nhật tài khoản thất bại":"Update account fail"
    static var updatePasswordSuccess = Locale.preferredLanguages[0].contains("vi") ? "Đổi mật khẩu thành công":"Change password success"
    static var examTabTitle = Locale.preferredLanguages[0].contains("vi") ? "Thi tuyển online":"Examination online"
    
    static var EMAIL_OR_PASSWORD_INCORRECT =  Locale.preferredLanguages[0].contains("vi") ? "Tài khoản hoặc mật khẩu không chính xác":"User or password incorrect"

    static var jobsApplyList = Locale.preferredLanguages[0].contains("vi") ? "Danh sách các vị trí ứng tuyển":"Applied positions"
    static var jobsFollowList = Locale.preferredLanguages[0].contains("vi") ? "Danh sách các vị trí theo dõi":"Following list"
    static var confirmLogout = Locale.preferredLanguages[0].contains("vi") ? "Bạn có muốn đăng xuất không?":"Confirm logging out?"
    static var sendForgetPasswordSuccess = Locale.preferredLanguages[0].contains("vi") ? "Reset tài khoản thành công":"Reset account success"
    static var updateAvatarFail = Locale.preferredLanguages[0].contains("vi") ? "Cập nhật ảnh đại diện thất bại":"Update avatar fail"

    static var nullKenhTuyenDung = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn kênh tuyển dụng":"Please select recruitment source"
    static var nullSex = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn giới tính":"Please select sex"
    static var nullBirthday = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn ngày sinh":"Please select birthday"
    static var nullWorkProvice = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn khu vực bạn muốn làm việc":"Please select desired workplace location"
    static var nullQualificationType = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn hệ":"Please select qualification"
    static var nullSchoolName = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn trường":"Please select school name"
    static var nullMajors = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn chuyên ngành":"Please select major"
    static var nullSkills = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn kỹ năng":"Please select skill"
    static var nullPersonInfomation = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng điền kinh nghiệm làm việc":"Please enter work experience"
    static var nullYearsOfExperience = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng chọn số năm làm việc":"Please select years of experience"
    static var nullFullName = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng điền họ và tên":"Please enter fullname"
    static var nullPhone = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng điền số điện thoại":"Please enter phone"
    static var lengthSkill = Locale.preferredLanguages[0].contains("vi") ? "Chỉ được chọn 3 kỹ năng":"Only choose 3 skills"
    static var lengthMajor = Locale.preferredLanguages[0].contains("vi") ? "Chỉ được chọn 3 chuyên ngành":"Only choose 3 major"

    static var jobsStatusAccept = Locale.preferredLanguages[0].contains("vi") ? "Đã duyệt":"Accept"
    static var jobsStatusUnAccept = Locale.preferredLanguages[0].contains("vi") ? "Chưa duyệt":"Not accept"
    static var jobsStatusTesting = Locale.preferredLanguages[0].contains("vi") ? "Tham gia thi tuyển":"Start Exam"
    static var jobsStatusInterview = Locale.preferredLanguages[0].contains("vi") ? "Tham gia phỏng vấn":"Start interview"
    static var jobsStatusInterviewOnline = Locale.preferredLanguages[0].contains("vi") ? "Tham gia phỏng vấn Online":"Start interview online"
    static var jobsContinuteInterviewOnline = Locale.preferredLanguages[0].contains("vi") ? "Tiếp tục phỏng vấn Online":"Continue interview online"
    static var jobsConfirmInterview = Locale.preferredLanguages[0].contains("vi") ? "Xác nhận phỏng vấn":"Accept interview"
    static var jobsDeplayInterview = Locale.preferredLanguages[0].contains("vi") ? "Lùi lịch phỏng vấn":"Postpone job admission schedule"
    static var jobsUnConfirmInterview = Locale.preferredLanguages[0].contains("vi") ? "Từ chối lịch phỏng vấn":"Reject interview"
    static var jobsConfirm = Locale.preferredLanguages[0].contains("vi") ? "Xác nhận nhận việc":"Confirm taking the job"
    static var jobsDeplayConfirm = Locale.preferredLanguages[0].contains("vi") ? "Hoãn nhận việc":"Postpone job admission schedule"
    static var jobsUnConfirm = Locale.preferredLanguages[0].contains("vi") ? "Từ chối nhận việc":"Reject job"
    static var jobsInterViewOnline = Locale.preferredLanguages[0].contains("vi") ? "Phỏng vấn Online":"Interview online"
    
    static var jobsApplySuccessExamTypeMessage =  Locale.preferredLanguages[0].contains("vi") ? "Bạn đã ứng tuyển thành công tại FPT Telecom. Bạn có tối đa 3 ngày để hoàn thành quá trình thi tuyển trực tuyến. Sau khoảng thời gian này, tài khoản ứng tuyển của bạn sẽ bị vô hiệu hóa.":"You just successfully applied for the job at FPT Telecom. You have 3 days to complete the online examination or your account will be removed from our system."
    static var jobsApplySuccessNotExamTypeMessage =  Locale.preferredLanguages[0].contains("vi") ? "Bạn đã ứng tuyển thành công tại FPT Telecom. Chúng tôi sẽ xem xét CV của bạn, và liên hệ lại với ứng viên đạt vòng này trong vòng 20 ngày kể từ thời điểm hiện tại.":"You just successfully applied to the job at FPT Telecom.We will give your CV a look and contact you if you meet our reqirements within 20 days from this moment."
    static var jobsApplyFailNotEnoughInfoMessage =  Locale.preferredLanguages[0].contains("vi") ? "Bạn phải cập nhật đủ thông tin cá nhân mới được ứng tuyển,bạn có muốn cập nhật thông tin cá nhân ngay bây giờ không ?":"You have to update your profile to be able to apply for the job, would you like to update now?"
    static var jobsApplyFailNoNotEnoughInfoMessage =  Locale.preferredLanguages[0].contains("vi") ? "Tin tuyển dụng đã ứng tuyển, Bạn vui lòng ứng tuyển lại sau 3 tháng":"You have already applied to this job, please try again after three months"
    static var jobsApplyFailUnauthorizedMessage =  Locale.preferredLanguages[0].contains("vi") ? "Bạn cần phải đăng nhập hoặc đăng ký thông tin mới được ứng tuyển, bạn có muốn đăng nhập hoặc đăng ký ngay không ?":"You have to login or register to be able to apply for the job, would you like to login or register now?"
    
    static var acceptJobsInterviewSucces =  Locale.preferredLanguages[0].contains("vi") ? "Xác nhận phỏng vấn thành công":"Confirm interview successful"
    static var acceptJobsInterviewFail =  Locale.preferredLanguages[0].contains("vi") ? "Xác nhận phỏng vấn thất bại":"Confirm interview fail"
    static var delayJobsInterviewSucces =  Locale.preferredLanguages[0].contains("vi") ? "Lùi lịch phỏng vấn thành công":"delay interview successful"
    static var delayJobsInterviewFail =  Locale.preferredLanguages[0].contains("vi") ? "Lùi lịch phỏng vấn thất bại":"delay interview fail"
    
    static var CheckExamExpire =  Locale.preferredLanguages[0].contains("vi") ? "Đã quá hạn 3 ngày kể từ ngày ứng tuyển. Bài thi đã bị hủy":"You didn't take the exam in three days from the day you applied for the job, so the exam has been canceled"
    static var CheckExamFinish =  Locale.preferredLanguages[0].contains("vi") ? "Bài thi đã kết thúc":"You have been finished the exam"
    static var CheckExamNotExist =  Locale.preferredLanguages[0].contains("vi") ? "Không lấy được bài thi, vui lòng thử lại sau":"Could not get the exam, please try again"
    static var getTimeExamAlert =  Locale.preferredLanguages[0].contains("vi") ? "Quá trình thi tuyển bao gồm bài thi ({0} phút) và phỏng vấn online ({1} phút). Nếu không hoàn thành/không đạt bạn sẽ bị giới hạn ứng tuyển vị trí này trong vòng 3 tháng tới. Vui lòng lựa chọn thiết bị. có camera để thực hiện thi tuyển. Bạn đã sẵn sàng thi?":"The recruitment examination consists of 1 test ({0} minutes long) and an online interview ({1} minutes long). If you fail to complete/pass this examination you will be restricted from applying for this position for the next 3 months.\n Please select a device with a camera to start the examination.\nAre you ready?"
    
    static var examQuestionNumber = Locale.preferredLanguages[0].contains("vi") ? "Câu hỏi số":"Question number"
    static var examQuestionFinish = Locale.preferredLanguages[0].contains("vi") ? "Bạn có muốn kết thúc bài thi?":"Do you want to end the exam?"
    
    static var deleteJobFollowSucces = Locale.preferredLanguages[0].contains("vi") ? "Xoá theo dõi công việc thành công":"Delete job following is success?"
    static var confirmJobFollowSucces = Locale.preferredLanguages[0].contains("vi") ? "Xác nhận theo dõi công việc thành công":"Confirm job following is success?"
    
    
    static var btnJobFollow = Locale.preferredLanguages[0].contains("vi") ? "Theo dõi":"Follow"
    static var btnEditobFollow = Locale.preferredLanguages[0].contains("vi") ? "CHỈNH SỬA THEO DÕI":"EDIT FOLLOWING LIST"
    static var jobFollowConfigExist = Locale.preferredLanguages[0].contains("vi") ? "Công việc đã đang theo dõi":"Job is following"
    
    static var uploadImageFail = Locale.preferredLanguages[0].contains("vi") ? "Upload hình ảnh thất bại":"Upload image fail"
    
    
    //Message alert
    static var messageUnConfirmInterviewSuccess = Locale.preferredLanguages[0].contains("vi") ? "Từ chối lịch phỏng vấn thành công":"Reject interview success"
    static var messageBiometricsConfirm = Locale.preferredLanguages[0].contains("vi") ? "Bạn phải up đủ ảnh mặt trước và mặt sau CMND":"You must upload enough photos of the front and back of the your identity"
}

