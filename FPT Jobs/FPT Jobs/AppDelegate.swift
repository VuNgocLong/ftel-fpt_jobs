//
//  AppDelegate.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/24/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import FBSDKLoginKit
import Alamofire
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate  {
    
    var window: UIWindow?
    var timer:Timer?
    let googleAPIKey = "AIzaSyBELqZDkQbiHu4_vZvGKBRHof3wuudIMNc"
    var arrURL : [ISCGetExamVideoQuestionOutput] = []
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        GMSPlacesClient.provideAPIKey(googleAPIKey)
        GMSServices.provideAPIKey(googleAPIKey)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        window = UIWindow.init(frame: UIScreen.main.bounds)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.default
        createNotification(application)
        
        UITabBar.appearance().tintColor = UIColor(hexString:"FD7B5E")
        UserDefaults.standard.removeObject(forKey: Common.keyToken)
        let vc = ISCFPTJobsLaunchViewController()
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
        SignalRManager.sharedInstance.signalRConnect()
        self.getToken()
        GIDSignIn.sharedInstance().clientID = Common.googleClientID
        UserDefaults.standard.removeObject(forKey: Common.keyUserLiveChat)
        
        
        return true
    }
    
    func checkUploadFile() {
        let objFile: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserInterView)
        if objFile != nil {
            if let array = objFile?["array"] as? [[String: Any]] {
                if array.count > 0 {
                    for item in array {
                        let obj = parseModel(dict: item)
                        if obj.ReturnInt != 1 {
                            arrURL.append(obj)
                            uploadVideo(obj: obj)
                        }
                    }
                }
            }
            if arrURL.count > 0 {
                let dict : [String:Any] = [
                    "array" : arrURL
                ]
                UserDefaults.standard.set(dictionary: dict, forKey: Common.keyUserInterView)
            }else {
                UserDefaults.standard.removeObject(forKey: Common.keyUserInterView)
            }
        }
    }

    @objc func getToken() {
        if timer != nil {
             timer!.invalidate()
             timer = nil
        }
        DispatchQueue.main.async {
            guard NetworkState().isConnected else {
              let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                      message: AppLanguage.errorNetworkNoInternet,
                                                      preferredStyle: .alert)
              alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                 self.getToken()
              })
                self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
               return
           }
        }
        getKongToken({ (token) in
            Common.shared.saveKongToken(token: token)
            self.timer = Timer.scheduledTimer(timeInterval: 1800, target: self, selector: #selector(self.getToken), userInfo: nil, repeats: true)
        }) { (error) in
//            self.getToken()
        }
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        Common.DEVICE_TOKEN = token
        print(token)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if application.applicationState == .active {
            let item : JSON = (userInfo["aps"] as? JSON)!
            let alert : JSON = (item["alert"] as? JSON)!
            let body : String = (alert["body"] as? String)!
            let title : String = (alert["title"] as? String)!
            let alertController = UIAlertController(title: title,
                                                    message: body,
                                                    preferredStyle: .alert)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
            let when = DispatchTime.now() + 3.0
            DispatchQueue.main.asyncAfter(deadline: when){
                alertController.dismiss(animated: true, completion: nil)
            }
        } else if application.applicationState == .background {
            
        } else if application.applicationState == .inactive {
            
        }
    }
    
    func createNotification(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        SignalRManager.sharedInstance.signalRConnect()
        self.getToken()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        SignalRManager.sharedInstance.signalRConnect()
        self.getToken()
    }
    

    func applicationDidEnterBackground(_ application: UIApplication) {
        if timer != nil {
             timer!.invalidate()
             timer = nil
        }
    }

    @discardableResult
    func grabStoryboard() -> Bool {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        if #available(iOS 13.0, *){
            if let scene = UIApplication.shared.connectedScenes.first{
                guard let windowScene = (scene as? UIWindowScene) else { return false }
                print(">>> windowScene: \(windowScene)")
                let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                window.windowScene = windowScene
                window.rootViewController = vc
                window.makeKeyAndVisible()
                self.window = window
            }
        } else {
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        return true
    }
    
    func grabLogin() -> Bool {
        let vc = ISCLoginViewController()
        if #available(iOS 13.0, *){
            if let scene = UIApplication.shared.connectedScenes.first{
                guard let windowScene = (scene as? UIWindowScene) else { return false }
                print(">>> windowScene: \(windowScene)")
                let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                window.windowScene = windowScene
                window.rootViewController = vc
                window.makeKeyAndVisible()
                self.window = window
            }
        } else {
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if GoogleAuth.isValidatedWithUrl(url: url as NSURL) {
            return GIDSignIn.sharedInstance()?.handle(url) ?? false
        }
        // url from facebook
        else if FacebookAuth.isValidatedWithUrl(url: url as NSURL) {
            return ApplicationDelegate.shared.application(app, open: url, options: options)
        }
        // application hasn't supported this url yet
        else {
              return false
        }
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        SignalRManager.sharedInstance.onLeavingPage()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension AppDelegate {
    func parseModel(dict : [String:Any]) -> ISCGetExamVideoQuestionOutput {
        var StartTime:String = ""
        var EndTime:String = ""
        var InterviewerCode:String = ""
        var JobCode:String = ""
        var Status:Int = 0
        var ExamLength:Int = 0
        var TotalQuestionaireNumber:Int = 0
        var ExamQuestionId:Int = 0
        var ExamVideoResultId:Int = 0
        var OrderNo:Int = 0
        var QuestionnaireCode:String = ""
        var Questionnaire:String = ""
        var QuestionnaireEn:String = ""
        var TimeLength:Int?
        var StartTimeQuestion:String = ""
        var EndTimeQuestion:String = ""
        var VideoUrl:String = ""
        var FileName:String = ""
        var ReturnInt:Int = 0
        var FileIndex:Int = 0
        if let startTime = dict["StartTime"] as? String {
            StartTime = startTime
        }
        if let endTime = dict["EndTime"] as? String {
            EndTime = endTime
        }
        if let interviewerCode = dict["InterviewerCode"] as? String {
            InterviewerCode = interviewerCode
        }
        if let jobCode = dict["JobCode"] as? String {
            JobCode = jobCode
        }
        if let status = dict["Status"] as? Int {
            Status = status
        }
        if let examLength = dict["ExamLength"] as? Int {
            ExamLength = examLength
        }
        if let totalQuestionaireNumber = dict["TotalQuestionaireNumber"] as? Int {
            TotalQuestionaireNumber = totalQuestionaireNumber
        }
        if let examQuestionId = dict["ExamQuestionId"] as? Int {
            ExamQuestionId = examQuestionId
        }
        if let examVideoResultId = dict["ExamVideoResultId"] as? Int {
            ExamVideoResultId = examVideoResultId
        }
        if let orderNo = dict["OrderNo"] as? Int {
            OrderNo = orderNo
        }
        if let questionnaireCode = dict["QuestionnaireCode"] as? String {
            QuestionnaireCode = questionnaireCode
        }
        if let questionnaire = dict["Questionnaire"] as? String {
            Questionnaire = questionnaire
        }
        if let questionnaireEn = dict["QuestionnaireEn"] as? String {
            QuestionnaireEn = questionnaireEn
        }
        if let timeLength = dict["TimeLength"] as? Int {
            TimeLength = timeLength
        }
        if let startTimeQuestion = dict["StartTimeQuestion"] as? String {
            StartTimeQuestion = startTimeQuestion
        }
        if let endTimeQuestion = dict["EndTimeQuestion"] as? String {
            EndTimeQuestion = endTimeQuestion
        }
        if let videoUrl = dict["VideoUrl"] as? String {
            VideoUrl = videoUrl
        }
        if let fileName = dict["FileName"] as? String {
            FileName = fileName
        }
        if let returnInt = dict["ReturnInt"] as? Int {
            ReturnInt = returnInt
        }
        if let fileIndex = dict["FileIndex"] as? Int {
            FileIndex = fileIndex
        }
        return ISCGetExamVideoQuestionOutput(StartTime: StartTime, EndTime: EndTime, InterviewerCode: InterviewerCode, JobCode: JobCode, Status: Status, ExamLength: ExamLength, TotalQuestionaireNumber: TotalQuestionaireNumber, ExamQuestionId: ExamQuestionId, ExamVideoResultId: ExamVideoResultId, OrderNo: OrderNo, QuestionnaireCode: QuestionnaireCode, Questionnaire: Questionnaire, QuestionnaireEn: QuestionnaireEn, TimeLength: TimeLength, StartTimeQuestion: StartTimeQuestion, EndTimeQuestion: EndTimeQuestion, VideoUrl: VideoUrl, FileName: FileName, ReturnInt: ReturnInt, FileIndex: FileIndex)
    }
    
    func uploadVideo(obj : ISCGetExamVideoQuestionOutput){
        var startTimeQuestion : String = ""
        var startTimeQuestionURL : String = ""
        if obj.StartTimeQuestion != nil {
            let years = obj.StartTimeQuestion?.components(separatedBy: "-")
            if years?.count ?? 0 > 1 {
                let year : Int = Int(years![0]) ?? 0
                if year < 2000 {
                    let dateStr = Common.shared.getTodayString()
                    startTimeQuestionURL = dateStr.replacingOccurrences(of: " ", with: "T")
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                    let date = dateFormatter.date(from: dateStr)
                    dateFormatter.dateFormat = "M/d/yyyy h:mm:ss a"
                    let Date12 = dateFormatter.string(from: date!)
                    startTimeQuestion = Date12
                }else {
                    startTimeQuestion = obj.StartTimeQuestion?.replacingOccurrences(of: "T", with: " ") ?? ""
                    if startTimeQuestion != "" {
                        let dateFormatter = DateFormatter()
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                        let date = dateFormatter.date(from: startTimeQuestion)
                        dateFormatter.dateFormat = "M/d/yyyy h:mm:ss a"
                        let Date12 = dateFormatter.string(from: date!)
                        startTimeQuestion = Date12
                    }
                }
            }

        }

        let tokenExamVideo : String = startTimeQuestion + (obj.InterviewerCode ?? "") + "\(obj.ExamQuestionId ?? 0)" + "\(obj.TimeLength ?? 0)" + "!@$v@22cdf|"
        startTimeQuestionURL = encodeURIComponent(s: startTimeQuestionURL)
        let urlString = (ISCInterviewConfig.PostRecordedAudioVideoAsync + "?jwt=\(Common.shared.getKongToken())").replacingOccurrences(of: "\"", with: "")
        let param : String = "&InterviewCode=\(obj.InterviewerCode ?? "")&questionCode=\(obj.QuestionnaireCode ?? "")&jobCode=\(obj.JobCode ?? "")&examQuestionId=\(obj.ExamQuestionId ?? 0)&startTime=\(encodeURIComponent(s: obj.StartTime ?? ""))&tokenExamVideo=\(tokenExamVideo.MD5)&examLength=\(obj.ExamLength ?? 0)&startTimeQuestion=\(startTimeQuestionURL)&timeLength=\(obj.TimeLength ?? 0)&isIOS=true&iosFileName=\(obj.FileName ?? "")"
        let parameters: Parameters = [:]
        do {
            let urlFile = URL(fileURLWithPath: obj.VideoUrl ?? "")
            let videoData = try Data(contentsOf: urlFile, options: .mappedIfSafe)
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key,value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                multipartFormData.append(videoData, withName: "file_1", fileName: "\(obj.FileName ?? "").mp4", mimeType: "video/mp4")},
                 usingThreshold:UInt64.init(),
                 to: urlString + param,
                 method:.post,
                 headers: ["Authorization" : "Bearer \(UserDefaults.standard.getValue(key: Common.keyAccessToken))"],
                 encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response)
                            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                                if let ReturnInt = dict["ReturnInt"] as? Int {
                                    if ReturnInt == 1 {
                                        print("Upload : thành công............")
                                        self.updateStatusFileUpload(obj : obj)
                                    }
                                }
                            }
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        break
                    }
            })
        } catch let error as NSError {
            print(error)
        }catch {
            // Catch any other errors
        }
    }
        
        func updateStatusFileUpload(obj : ISCGetExamVideoQuestionOutput){
            var index : Int = 0
             for item in arrURL {
                if obj.FileIndex == item.FileIndex && obj.ExamQuestionId == item.ExamQuestionId {
                    var dataResult = item
                    dataResult.ReturnInt = 1
                    arrURL[index] = dataResult
                    let fileManager: FileManager = FileManager()
                    if fileManager.isDeletableFile(atPath: item.VideoUrl ?? "") {
                        _ = try? fileManager.removeItem(atPath: item.VideoUrl ?? "")
                        print("Upload : xoá file local............")
                    }
                    return
                }
                index += 1
             }
        }
}
extension UIApplication {
    
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = base as? UINavigationController, navigationController.viewControllers.count > 0 {
            return topViewController(navigationController.visibleViewController)
        }
        
        if let tabBarController = base as? UITabBarController {
            if let selected = tabBarController.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presentedViewController = base?.presentedViewController {
            return topViewController(presentedViewController)
        }
        
        return base
    }
    
}
