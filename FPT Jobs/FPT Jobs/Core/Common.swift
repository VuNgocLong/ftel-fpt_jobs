//
//  Common.swift
//  ISCCamera
//
//  Created by PHAM CHI HIEU on 5/31/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class Common {
    static let shared = Common()
    static var numberInbox:Int = 0
    static var DEVICE_TOKEN = ""
    static let authorizationKong = "Basic bHVhbnR2M0BmcHQuY29tLnZuOjEyMzQ1Ng=="
    static let googleClientID:String = "619995557385-645q5tpqgbm1b1om9seqm1re661ogjev.apps.googleusercontent.com"
    
    static let keyToken:String = "token"
    static let keyAccessToken:String = "access_token"
    static let keyUserProfile:String = "user_profile"
    static let keyUserLiveChat:String = "user_live_chat"
    static let keyUserCMND:String = "user_cmnd"
    static let keyUserInterView:String = "file_interview"
    static let bearTokenKey:String = "bear_token"
    static let key_avatar:String = "img_avatar"
    
    var deviceUIID : String {
        return AppKeyChain.shared().imei()
    }
    func getKongToken() -> String {
        return UserDefaults.standard.getValue(key: Common.keyToken)
    }
    func saveKongToken(token:String) {
        UserDefaults.standard.setValue(value: token, key: Common.keyToken)
    }
    func getAccessToken() -> String {
        return UserDefaults.standard.getValue(key: Common.keyAccessToken)
    }
    func saveAccessToken(token:String) {
        UserDefaults.standard.setValue(value: token, key: Common.keyAccessToken)
    }
    func setBearToken(dict: Dictionary<String,Any>) {
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dict)
        userDefaults.set(encodedData, forKey: Common.bearTokenKey)
        userDefaults.synchronize()
    }
    func getBearToken() -> BearToken {
        if isKeyPresentInUserDefaults(key: Common.bearTokenKey) {
            let userDefaults = UserDefaults.standard
            let decoded  = userDefaults.object(forKey: Common.bearTokenKey) as! Data
            let dict = (NSKeyedUnarchiver.unarchiveObject(with: decoded) as? Dictionary<String,Any>)!
            return BearToken(dict)
        }
        return BearToken(Dictionary<String, Any>())
    }
    func getImgAvatar() -> NSData {
        return UserDefaults.standard.object(forKey: Common.key_avatar) as! NSData
    }
    func saveImgAvatar(imgURL:NSData) {
        UserDefaults.standard.set(imgURL, forKey: Common.key_avatar)
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func checkEmail(str : String) -> Bool {
        let stringTest = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        let result = stringTest.evaluate(with: str)
        return result
    }
    
    func replaceStringCharacter(str : String) -> String{
        var strResult = str.lowercased()
        strResult = strResult.replacingOccurrences(of: "à", with: "a").replacingOccurrences(of: "á", with: "a").replacingOccurrences(of: "ạ", with: "a").replacingOccurrences(of: "ả", with: "a").replacingOccurrences(of: "ã", with: "a").replacingOccurrences(of: "â", with: "a").replacingOccurrences(of: "ầ", with: "a").replacingOccurrences(of: "ấ", with: "a").replacingOccurrences(of: "ậ", with: "a").replacingOccurrences(of: "ẩ", with: "a").replacingOccurrences(of: "ẫ", with: "a").replacingOccurrences(of: "ă", with: "a").replacingOccurrences(of: "ằ", with: "a").replacingOccurrences(of: "ắ", with: "a").replacingOccurrences(of: "ẳ", with: "a").replacingOccurrences(of: "ẵ", with: "a")
        
        strResult = strResult.replacingOccurrences(of: "è", with: "e").replacingOccurrences(of: "é", with: "e").replacingOccurrences(of: "ẹ", with: "e").replacingOccurrences(of: "ẻ", with: "e").replacingOccurrences(of: "ẽ", with: "e").replacingOccurrences(of: "ê", with: "e").replacingOccurrences(of: "ề", with: "e").replacingOccurrences(of: "ế", with: "e").replacingOccurrences(of: "ệ", with: "e").replacingOccurrences(of: "ể", with: "e").replacingOccurrences(of: "ễ", with: "e")
        
        strResult = strResult.replacingOccurrences(of: "ì", with: "i").replacingOccurrences(of: "í", with: "i").replacingOccurrences(of: "ị", with: "i").replacingOccurrences(of: "ỉ", with: "i").replacingOccurrences(of: "ĩ", with: "i")
        
        strResult = strResult.replacingOccurrences(of: "ò", with: "o").replacingOccurrences(of: "ó", with: "o").replacingOccurrences(of: "ọ", with: "o").replacingOccurrences(of: "ỏ", with: "o").replacingOccurrences(of: "õ", with: "o").replacingOccurrences(of: "ô", with: "o").replacingOccurrences(of: "ồ", with: "o").replacingOccurrences(of: "ố", with: "o").replacingOccurrences(of: "ộ", with: "o").replacingOccurrences(of: "ổ", with: "o").replacingOccurrences(of: "ỗ", with: "o").replacingOccurrences(of: "ơ", with: "o").replacingOccurrences(of: "ờ", with: "o").replacingOccurrences(of: "ớ", with: "o").replacingOccurrences(of: "ợ", with: "o").replacingOccurrences(of: "ở", with: "o").replacingOccurrences(of: "ỡ", with: "o")
        
        strResult = strResult.replacingOccurrences(of: "ù", with: "u").replacingOccurrences(of: "ú", with: "u").replacingOccurrences(of: "ụ", with: "u").replacingOccurrences(of: "ủ", with: "u").replacingOccurrences(of: "ũ", with: "u").replacingOccurrences(of: "ư", with: "u").replacingOccurrences(of: "ừ", with: "u").replacingOccurrences(of: "ứ", with: "u").replacingOccurrences(of: "ự", with: "u").replacingOccurrences(of: "ử", with: "u").replacingOccurrences(of: "ữ", with: "u")
        
        strResult = strResult.replacingOccurrences(of: "ỳ", with: "y").replacingOccurrences(of: "ý", with: "y").replacingOccurrences(of: "ỵ", with: "y").replacingOccurrences(of: "ỷ", with: "y").replacingOccurrences(of: "ỹ", with: "y")
        
        strResult = strResult.replacingOccurrences(of: "đ", with: "d")
        strResult = strResult.replacingOccurrences(of: "-", with: "")
        strResult = strResult.replacingOccurrences(of: "  ", with: "-")
        strResult = strResult.replacingOccurrences(of: " ", with: "-")
        strResult = strResult.replacingOccurrences(of: ",", with: "-")
        strResult = strResult.replacingOccurrences(of: ".", with: "-")
        strResult = strResult.replacingOccurrences(of: "(", with: "")
        strResult = strResult.replacingOccurrences(of: ")", with: "")
        
        return strResult
    }
    
    //MARK -- list array data
    var arayRecruitment : [SelectOption] {
       return [
            SelectOption(Key: 1, Value: "FPTJobs.com"),
            SelectOption(Key: 2, Value: "Tuyendung.fpt.com.vn"),
            SelectOption(Key: 3, Value: "Timviecnhanh.com"),
            SelectOption(Key: 4, Value: "Careerlink.vn"),
            SelectOption(Key: 5, Value: "Vieclam24h.vn"),
            SelectOption(Key: 6, Value: "Vietnamworks.com"),
            SelectOption(Key: 7, Value: "Linkedin"),
            SelectOption(Key: 8, Value: "Facebook và mạng xã hội khác"),
            SelectOption(Key: 9, Value: "Mywork.com.vn"),
            SelectOption(Key: 11, Value: "Tuyendung.com.vn"),
            SelectOption(Key: 12, Value: "Jobstreet.vn"),
            SelectOption(Key: 13, Value: "Itviec.com"),
            SelectOption(Key: 14, Value: "Careerbuilder.vn"),
            SelectOption(Key: 15, Value: "YBOX"),
            SelectOption(Key: 16, Value: "TOPCV"),
            SelectOption(Key: 17, Value: "Job offer email"),
            SelectOption(Key: 19, Value: "Banderoles, posters, leaflets..."),
            SelectOption(Key: 20, Value: "Newspapers"),
            SelectOption(Key: 21, Value: "Job fairs"),
            SelectOption(Key: 22, Value: "Schools"),
            SelectOption(Key: 23, Value: "Recruitment events"),
            SelectOption(Key: 24, Value: "Friends\'/Relatives\' recommendation"),
            SelectOption(Key: 25, Value: "Internal recommendation"),
            SelectOption(Key: 26, Value: "From unit/department"),
            SelectOption(Key: 27, Value: "Local recruitment websites"),
            SelectOption(Key: 28, Value: "From other candidate"),
            SelectOption(Key: 29, Value: "Other (specify)"),
        ]
    }
    
    var arayQualification : [SelectOption] {
       return [
            SelectOption(Key: 2, Value: Locale.preferredLanguages[0].contains("vi") ? "Tiến sĩ":"Doctor's Degree"),
            SelectOption(Key: 3, Value: Locale.preferredLanguages[0].contains("vi") ? "Thạc sĩ":"Master's Degree"),
            SelectOption(Key: 4, Value: Locale.preferredLanguages[0].contains("vi") ? "Đại học":"Bachelor's Degree"),
            SelectOption(Key: 5, Value: Locale.preferredLanguages[0].contains("vi") ? "Cao đẳng":"Associate's Degree"),
            SelectOption(Key: 6, Value: Locale.preferredLanguages[0].contains("vi") ? "Trung cấp":"Vocational certificate"),
            SelectOption(Key: 7, Value: Locale.preferredLanguages[0].contains("vi") ? "Trung học Phổ thông":"High School graduation"),
        ]
    }
    
    var arayExperience : [SelectOption] {
       return [
            SelectOption(Key: 0, Value: Locale.preferredLanguages[0].contains("vi") ? "Chưa có kinh nghiệm":"No experience"),
            SelectOption(Key: 1, Value: Locale.preferredLanguages[0].contains("vi") ? "0 - 1 năm kinh nghiệm":"0 - 1 years of experience"),
            SelectOption(Key: 2, Value: Locale.preferredLanguages[0].contains("vi") ? "1 - 2 năm kinh nghiệm":"1 - 2 years of experience"),
            SelectOption(Key: 3, Value: Locale.preferredLanguages[0].contains("vi") ? "2 - 3 năm kinh nghiệm":"2 - 3 years of experience"),
            SelectOption(Key: 4, Value: Locale.preferredLanguages[0].contains("vi") ? "3 - 4 năm kinh nghiệm":"3 - 4 years of experience"),
            SelectOption(Key: 5, Value: Locale.preferredLanguages[0].contains("vi") ? "4 - 5 năm kinh nghiệm":"4 - 5 years of experience"),
            SelectOption(Key: 6, Value: Locale.preferredLanguages[0].contains("vi") ? "Trên 5 năm kinh nghiệm":"Over 5 years of experience"),
        ]
    }
    
    var arayGender : [SelectOption] {
       return [
            SelectOption(Key: 1, Value: Locale.preferredLanguages[0].contains("vi") ? "Nam":"Male"),
            SelectOption(Key: 2, Value: Locale.preferredLanguages[0].contains("vi") ? "Nữ":"Female"),
            SelectOption(Key: 3, Value: Locale.preferredLanguages[0].contains("vi") ? "Khác":"Other"),
        ]
    }
    
    var arayMarital : [SelectOptionString] {
       return [
            SelectOptionString(Key: "0", Value: Locale.preferredLanguages[0].contains("vi") ? "Khác":"Other"),
            SelectOptionString(Key: "S", Value: Locale.preferredLanguages[0].contains("vi") ? "Độc thân":"Single"),
            SelectOptionString(Key: "M", Value: Locale.preferredLanguages[0].contains("vi") ? "Đã kết hôn":"Married"),
        ]
    }
    
    func strStatusJobsApply(ProcessStatus : Int) -> String {
        var strReturn : String = ""
        switch ProcessStatus {
        case 0:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ xét duyệt":"Waiting for approvement"
            break
        case 1:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Tham gia thi tuyển":"Doing Exam"
            break
        case 2:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Tham gia thi tuyển":"Doing Exam"
            break
        case 3:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Tham gia phỏng vấn":"Accept Interview"
            break
        case 4:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Không đạt thi tuyển":"Did not pass the exam"
            break
        case 5:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Không tham gia phỏng vấn":"Did not accept the interview"
            break
        case 6:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Tham gia phỏng vấn":"Accept Interview"
            break
        case 7:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ xét duyệt":"Waiting for approvement"
            break
        case 8:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ xếp lịch phỏng vấn":"Waitting for setup interview schedule"
            break
        case 10:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ xác nhận lịch phỏng vấn online":"Waitting for accept online interview schedule"
            break
        case 11:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Từ chối phỏng vấn online":"Declined interview online"
            break
        case 12:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ phỏng vấn online":"Waitting for interview online"
            break
        case 13:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Hoãn lịch phỏng vấn online":"Delayed interview online"
            break
        case 14:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Không tham gia phỏng vấn online":"Did not present to interview online"
            break
        case 15:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ kết quả phỏng vấn online":"Waitting for result of interview online"
            break
        case 16:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ xếp lịch phỏng vấn trực tiếp":"Waitting for scheduling an face to face interview"
            break
        case 17:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ xác nhận lịch phỏng vấn trực tiếp":"Waitting for face to face interview acceptation"
            break
        case 18:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Từ chối phỏng trực tiếp":"Declined face to face interview"
            break
        case 19:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ phỏng vấn trực tiếp":"Waitting for face to face interview"
            break
        case 20:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Hoãn phỏng trực tiếp":"Delayed face to face interview"
            break
        case 22:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Chờ kết quả phỏng vấn trực tiếp":"Waitting for result of face to face interview"
            break
        case 24:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Đạt":"Pass"
            break
        case 25:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Không đạt":"Failed"
            break
        case 26:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Đã mời nhận việc":"Job invitation sent"
            break
        case 27:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Không nhận việc":"Declined to take the job"
            break
        case 28:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Đã nhận việc":"Taken the job"
            break
        case 29:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Hoãn nhận việc":"Delayed to take the job"
            break
        case 30:
            strReturn = Locale.preferredLanguages[0].contains("vi") ? "Đang phỏng vấn online":"Currently Interview online"
            break
        default:
            break
        }
        return strReturn
    }

    func convertDateTimeFormaterToString(_ dateStr: String, type : String) -> String
    {
        var strDate : String = ""
        if dateStr != "" {
            switch type {
            case "dd/MM/yyyy HH:mm":
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateStr)
                dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
                strDate =  dateFormatter.string(from: date!)
                break
            case "d/M/yyyy HH:mm:ss":
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateStr)
                dateFormatter.dateFormat = "d/M/yyyy HH:mm:ss"
                strDate =  dateFormatter.string(from: date!)
                break
            case "dd/MM/yyyy":
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateStr)
                dateFormatter.dateFormat = "dd/MM/yyyy"
                strDate =  dateFormatter.string(from: date!)
                break
            case "HH:mm dd/MM/yyyy":
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateStr)
                dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
                strDate =  dateFormatter.string(from: date!)
                break
            case "dd.MM.yyyy":
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateStr)
                dateFormatter.dateFormat = "dd.MM.yyyy"
                strDate =  dateFormatter.string(from: date!)
                break
            default:
                break
            }
        }
        
        return strDate
    }
    func getTodayString() -> String{
            let date = Date()
            let calender = Calendar.current
            let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)

            let year = components.year
            let month = components.month ?? 0 < 10 ? "0\(components.month ?? 0)" : "\(components.month ?? 0)"
            let day = components.day ?? 0 < 10 ? "0\(components.day ?? 0)" : "\(components.day ?? 0)"
            let hour = components.hour ?? 0 < 10 ? "0\(components.hour ?? 0)" : "\(components.hour ?? 0)"
            let minute = components.minute ?? 0 < 10 ? "0\(components.minute ?? 0)" : "\(components.minute ?? 0)"
            let second = components.second ?? 0 < 10 ? "0\(components.second ?? 0)" : "\(components.second ?? 0)"

            let today_string = String(year!) + "-" + String(month) + "-" + String(day) + " " + String(hour)  + ":" + String(minute) + ":" +  String(second)

            return today_string
    }
    
    func getTodayNoTimeString() -> String{
           let date = Date()
           let calender = Calendar.current
           let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)

           let year = components.year
           let month = components.month ?? 0 < 10 ? "0\(components.month ?? 0)" : "\(components.month ?? 0)"
           let day = components.day ?? 0 < 10 ? "0\(components.day ?? 0)" : "\(components.day ?? 0)"
           let today_string = String(year!) + "-" + String(month) + "-" + String(day) 

           return today_string
    }
    
    func MD5(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)

        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
    func checkValidKongToken () -> Bool {
        if Common.shared.isKeyPresentInUserDefaults(key: Common.keyAccessToken) {
            let kongToken = Common.shared.getBearToken()
            if kongToken.access_token == ""{
                return false
            }else  if kongToken.DateReponse == "" {
                return false
            }else {
                do {
                    let date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    let result = dateFormatter.string(from: date)
                    let minuts = try calculatorToken(kongToken.DateReponse, result) as! Int
                    if minuts < 0 {
                        return false
                    }
                    if minuts > 60 {
                        return false
                    }else {
                        return true
                    }
                }catch {
                    return false
                }
            }
        }
        return false
    }
    
    func calculatorToken(_ startDate : String, _ endDate : String) throws -> Any {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let firstDate = dateFormatter.date(from: startDate)
        let secondDate = dateFormatter.date(from: endDate)
        let distanceBetweenDates: TimeInterval? = secondDate!.timeIntervalSince(firstDate!)
        let secondsInAnMinuts: Double = 60
        let minutsBetweenDates = Int((distanceBetweenDates! / secondsInAnMinuts))
        return minutsBetweenDates
    }
}


func getCurrentDate() -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let result = formatter.string(from: date)
    return result
}


func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

func convertIntoJSONString(arrayObject: [Any]) -> String? {
    
    do {
        let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
        if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
            return jsonString as String
        }
        
    } catch let error as NSError {
        print("Array convertIntoJSON - \(error.description)")
    }
    return nil
}

func encodeURIComponent(s : String) -> String{
    var result:String = s.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    result = result.replacingOccurrences(of: "\\+",with:"%20")
                .replacingOccurrences(of: "\\%21",with: "!")
                .replacingOccurrences(of: "\\%27",with: "'")
                .replacingOccurrences(of: "\\%28",with: "(")
                .replacingOccurrences(of: "\\%29",with: ")")
                .replacingOccurrences(of: "\\%7E",with: "~")
                .replacingOccurrences(of: ":", with: "%3A")
                .replacingOccurrences(of: "/", with: "-")
    return result;
}

extension Bundle {
    var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    var bundleName: String? {
        return infoDictionary?["CFBundleName"] as? String
    }
}

struct SelectOption: Decodable {
    var Key:Int?
    var Value:String?
}

struct SelectOptionString: Decodable {
    var Key:String?
    var Value:String?
}
struct BearToken {
    var dictionary : Dictionary<String,Any>
    init(_ dict: Dictionary<String,Any>) {
        self.dictionary = dict;
    }
    var access_token : String {
        if let result = self.dictionary["access_token"] as? String {
            return result
        }
        return ""
    }
    var refresh_token : String {
        if let result = self.dictionary["refresh_token"] as? String {
            return result
        }
        return ""
    }
    var DateReponse : String {
        if let result = self.dictionary["DateReponse"] as? String {
            return result
        }
        return ""
    }
    var username : String {
        if let result = self.dictionary["username"] as? String {
            return result
        }
        return ""
    }
    var password : String {
        if let result = self.dictionary["password"] as? String {
            return result
        }
        return ""
    }
}


