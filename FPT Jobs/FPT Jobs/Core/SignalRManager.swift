//
//  SignalRManager.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/14/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
import SignalRSwift

public enum SignalRManagerState {
    case disconnected
    case connecting
    case connected
    case reconnecting
}

public enum SignalRVideoAuthenState {
    case success
    case authfailed
}
public enum CallingState {
    case incomingcall
    case callaccepted
    case callcanceled
    case cantacceptcall
    case callended
}
protocol SignalRVideoCallDelegate: class {
    func didRegisterUser(_ status: SignalRVideoAuthenState, _ connectionID : String)
    func didDisconnectedUser(_ connectionID : String)
    func didRecieveCalling(_ status: CallingState,_ connections : [String],_ time : Double)
    func didReceiveOffer(_ sdp: String, _ connectionID : String)
    func didRecieveAnswer(_ sdp: String, _ connectionID : String)
    func didRecieveICECandidate(candidate: String, sdpMLineIndex: Int32, sdpMid: String?, isCandidate : Bool, _ conectionID : String)
    func didChangeState(_ status: SignalRManagerState)
}

protocol SignalRChatDelegate: class {
    func didChangeState(_ status: SignalRManagerState)
    func didOutsideChatboxCreate(_ model: OutsideChatboxCreateModels)
    func didLoadAllMessages(_ arrayMessage: [OnPersonalMessageMdels])
    func didPersonalMessage(_ message: OnPersonalMessageMdels)
}

class SignalRManager : NSObject {
    static let sharedInstance = SignalRManager()
    weak var delegate: SignalRVideoCallDelegate?
    weak var delegateChat: SignalRChatDelegate?
    var chatHub: HubProxy!
    var videoHub: HubProxy!
    var connection: HubConnection!
    private var state: SignalRManagerState = .connecting {
        didSet {
            self.delegate?.didChangeState(state)
            self.delegateChat?.didChangeState(state)
        }
    }
    override init() {
        super.init()

        connection = HubConnection(withUrl: "https://inside.fptjobs.com/staging")
        chatHub = self.connection.createHubProxy(hubName: "chathub")
        videoHub = self.connection.createHubProxy(hubName: "VideoCallHub")
        connection.started = { [unowned self] in
            self.state = .connected
        }

        connection.reconnecting = { [unowned self] in
            self.state = .reconnecting
        }

        connection.reconnected = { [unowned self] in
            self.state = .connected
        }

        connection.closed = { [unowned self] in
            self.state = .disconnected
        }

        connection.connectionSlow = {
            print("Connection slow...")
        }

        connection.error = { [unowned self] error in
            let anError = error as NSError
            if anError.code == NSURLErrorTimedOut {
                self.connection.start()
            }
        }
        
        _ = chatHub.on(eventName: "onOutsideChatboxCreate") { (args) in
            if args.count > 0 {
                if let data = args[0] as? Dictionary<String,Any> {
                   UserDefaults.standard.set(dictionary: data, forKey: Common.keyUserLiveChat)
                    let model  = OutsideChatboxCreateModels(dict: data)
                    self.delegateChat?.didOutsideChatboxCreate(model)
                }
            }
        }
        
        _ = chatHub.on(eventName: "onLoadAllMessages") { (args) in
            if args.count > 1 {
                if let Messages = args[1] as? [Dictionary<String,Any>] {
                    let arrMessage = Messages.compactMap({OnPersonalMessageMdels(dict: $0)})
                    if arrMessage.count > 0 {
                        self.delegateChat?.didLoadAllMessages(arrMessage)
                    }
                }
            }
        }
        
        _ = chatHub.on(eventName: "onPersonalMessage") { (args) in
            if args.count > 1 {
                if let data = args[1] as? Dictionary<String,Any> {
                    self.delegateChat?.didPersonalMessage(OnPersonalMessageMdels(dict: data))
                }
            }
        }

        _ = videoHub.on(eventName: "authfailed") { (args) in
            self.delegate?.didRegisterUser(SignalRVideoAuthenState.authfailed, "")
        }
        _ = videoHub.on(eventName: "regissuccess") { (args) in
            var connectionID = ""
            if args.count > 0 {
                connectionID = args[0] as? String ?? ""
            }
            self.delegate?.didRegisterUser(SignalRVideoAuthenState.success, connectionID)
        }
        _ = videoHub.on(eventName: "userdisconnected") { (args) in
            var connectionID = ""
            if args.count > 1 {
                connectionID = args[1] as? String ?? ""
            }
            self.delegate?.didDisconnectedUser(connectionID)
        }
        _ = videoHub.on(eventName: "incomingcall") { (args) in
            self.delegate?.didRecieveCalling(.incomingcall, [], 0)
        }
        _ = videoHub.on(eventName: "callaccepted") { (args) in
            let arrIP : [String] = args[0] as! [String]
            var time : Double = 0
            if args.count > 2 {
                time = args[2] as! Double
            }
            self.delegate?.didRecieveCalling(.callaccepted, arrIP, time)
        }
        _ = videoHub.on(eventName: "callcanceled") { (args) in
               self.delegate?.didRecieveCalling(.callcanceled, [], 0)
           }
        _ = videoHub.on(eventName: "cantacceptcall") { (args) in
            self.delegate?.didRecieveCalling(.cantacceptcall, [], 0)
        }
        _ = videoHub.on(eventName: "cantacceptcall") { (args) in
            self.delegate?.didRecieveCalling(.cantacceptcall, [], 0)
        }
        _ = videoHub.on(eventName: "callended") { (args) in
            self.delegate?.didRecieveCalling(.callended, [], 0)
        }
        _ = videoHub.on(eventName: "onnewdata") { (args) in
            if args.count > 1 {
                let connectionID = args[0] as? String
                if let obj = args[1] as? String {
                    print("ONNEWDATA : \(obj)")
                    let dict = convertToDictionary(text: obj)
                    
                    if let dicSdp = dict?["sdp"] as? [String: Any ]{
                        
                        if let sdp = dicSdp["sdp"] as? String {
                             if let type = dicSdp["type"] as? String {
                                 if type == "answer" {
                                    self.delegate?.didRecieveAnswer(sdp, connectionID ?? "")
                                 }else {
                                    self.delegate?.didReceiveOffer(sdp, connectionID ?? "")
                                }
                             }
                        }
                    }
                    if let dicCandidate = dict?["candidate"] as? [String: Any ]{
                        var _candidate : String = ""
                        var _sdpMid : String = ""
                        var _isCandidate : Bool = false
                        var _sdpMLineIndex : Int = 0
                        if let candidate = dicCandidate["candidate"] as? String{
                             _candidate = candidate
                        }
                        if let sdpMid = dicCandidate["sdpMid"] as? String {
                            _sdpMid = sdpMid
                        }
                        if let sdpMLineIndex = dicCandidate["sdpMLineIndex"] as? Int {
                            _sdpMLineIndex = sdpMLineIndex
                        }
                        if let isCandidate = dict?["isCandidate"] as? Bool {
                           _isCandidate = isCandidate
                        }
                        self.delegate?.didRecieveICECandidate(candidate: _candidate, sdpMLineIndex: Int32(_sdpMLineIndex), sdpMid: _sdpMid, isCandidate: _isCandidate, connectionID ?? "")
                    }
                }
            }
        }
    }
    
    func signalRConnect(){
        switch connection.state {
        case .connecting:
            connection.start()
            self.state = .connecting
            break
            
        case .connected:
            self.state = .connected
            break
            
        case .reconnecting:
            self.state = .reconnecting
            break
            
        case .disconnected:
             connection.start()
            self.state = .disconnected
            break
        }
    }
    func signalRDisconect(){
        connection.disconnect()
    }
    func registerUserAuto(){
        var GroupName : String = ""
        var GroupId : String = "0"
        var isLogin : Bool = false
        let userProfile: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if userProfile != nil {
            if let InterviewerCode = userProfile?["InterviewerCode"] as? String {
                GroupId = InterviewerCode
                isLogin = true
            }
            if let Email = userProfile?["Email"] as? String {
                GroupName = Email
            }
            
        }else {
            let userLiveChat: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserLiveChat)
            if userLiveChat != nil {
                let userLiveChat = OutsideChatboxCreateModels(dict: userLiveChat!)
                GroupName = userLiveChat.GroupName
            }
        }
        if GroupName != "" {
            if let hub = chatHub{
                hub.invoke(method: "regisUser", withArgs: [GroupId, GroupName, GroupName, "", isLogin])
            }
        }
    }
    func registerUser(groupId : String, email : String, isLogin : Bool){
        if let hub = self.chatHub{
            hub.invoke(method: "regisUser", withArgs: [groupId, email, email, "", isLogin])
        }
    }
    func onReadAllMessages(groupId : String) {
        if let hub = self.chatHub{
            hub.invoke(method: "onReadAllMessages", withArgs: [groupId])
        }
    }
    
    func onMessageFromOutside(groupId : String, message : String){
        if let hub = self.chatHub{
            hub.invoke(method: "OnMessageFromOutside", withArgs: [groupId, message, 1])
        }
        
    }
    
    func regisUser(_ token : String){
        videoHub.invoke(method: "regisUser", withArgs: [token])
    }
    
    func accept(){
        videoHub.invoke(method: "accept", withArgs: [])
    }
    
    func decLine(){
        videoHub.invoke(method: "decline", withArgs: [])
    }
    
    func endCall(){
        videoHub.invoke(method: "endcall", withArgs: [])
    }
    
    func onLeavingPage(){
        videoHub.invoke(method: "onLeavingPage", withArgs: [])
    }
    
    func sendOffer(_ sdp: String, _ ip : String) {
        videoHub.invoke(method: "senddata", withArgs: [sdp,ip])
    }
    
    func sendAnswer(_ sdp: String, _ ip : String) {
        videoHub.invoke(method: "senddata", withArgs: [sdp,ip])
    }
    
    func sendICECandidate(_ candidate: String, _ ip : String) {
        videoHub.invoke(method: "senddata", withArgs: [candidate,ip])
    }
    
    
}
