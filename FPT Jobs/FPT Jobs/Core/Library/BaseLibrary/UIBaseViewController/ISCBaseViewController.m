//
//  ISCBaseViewController.m
//  ISCCamera
//
//  Created by Pham Chi Hieu on 11/17/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import "ISCBaseViewController.h"
#import <UIKit/UIKit.h>



@implementation UIColor (HexString)

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end

@interface ISCBaseViewController (){
    UIView *loadingView;
}

@end

@implementation ISCBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)showLoading{
    loadingView = [[UIView alloc]initWithFrame: [[UIScreen mainScreen]bounds]];
    loadingView.backgroundColor = [UIColor clearColor];
    UIWindow *window = [[[UIApplication sharedApplication]delegate]window];
    [window addSubview:loadingView];
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(loadingView.frame.origin.x, loadingView.frame.origin.y, loadingView.frame.size.width, loadingView.frame.size.height)];
    backgroundView.backgroundColor = [UIColor blackColor];
    [backgroundView setAlpha: 0.3];
    [loadingView addSubview:backgroundView];
    
    UIView *indicatorView = [[UIView alloc]initWithFrame:CGRectMake((loadingView.frame.size.width - 180) / 2, (loadingView.frame.size.height - 50) / 2, 180, 50)];
    indicatorView.backgroundColor = [UIColor whiteColor];
    [indicatorView setAlpha:1.0];
    indicatorView.layer.cornerRadius = 10;
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.frame = CGRectMake(0, 0, 50, 50);
    [activityView startAnimating];
    
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, 180, 50)];
    textLabel.textColor = [UIColor grayColor];
    NSString *preferredLanguage = [[NSLocale preferredLanguages] firstObject];
    if ([preferredLanguage containsString:@"vi"]){
        textLabel.text = @"Kết nối......";
    }else {
        textLabel.text = @"Connecting......";
    }
    
    [indicatorView addSubview:activityView];
    [indicatorView addSubview:textLabel];
    [loadingView addSubview:indicatorView];
}
- (void)hideLoading{
    for (UIView *view in loadingView.subviews)
    {
        [view removeFromSuperview];
    }
    [loadingView removeFromSuperview];
//    @objc func stopIndicator( _ vc: UIViewController, _ loadingView: UIView , completionHandler: @escaping (_ success: Bool) -> ())
//    {
//        DispatchQueue.main.async {
//            for view in loadingView.subviews{
//                view.removeFromSuperview()
//            }
//            loadingView.removeFromSuperview()
//            completionHandler(true)
//        }
//    }
}
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
