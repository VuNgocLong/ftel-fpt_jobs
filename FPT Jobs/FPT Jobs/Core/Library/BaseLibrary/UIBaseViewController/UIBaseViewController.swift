//
//  UIBaseViewController.swift
//  ISCCamera
//
//  Created by Long Vu on 5/6/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SDWebImage
import SignalRSwift

class UIBaseViewController: UIViewController {

    var isSearchViewController:Bool = false
    var isRootViewController:Bool = true
    var isShowChat:Bool = true
    var isSearch:Bool = false
    var backButtonTitle = ""
    var largeTitle = ""
    let v = UIView(frame: UIScreen.main.bounds)
    let heightScaleFactor = UIDevice.current.heightScaleFactor
    var tintButtonColor = UIColor.init(hexString: "#cdcccc")
    var tintColor = UIColor.init(hexString: "#cdcccc")
    var titleOnly:Bool = false
    var searchTft:UIAutoSizeTextField?
//    var chatBtn : UIAutoSizeChatButton?
    var totalNoti:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        self.hideKeyboardWhenTappedAround()
        
    }
    
    @objc func tapLive() {
        let vc = ISCLiveChatViewController()
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControlerCustom(vc, AppLanguage.jobsTabTitle)
    }
   
     @objc func buttonTapped(sender: AnyObject) {
        let vc = ISCLiveChatViewController()
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControlerCustom(vc, AppLanguage.jobsTabTitle)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(tapLive), name: NSNotification.Name(rawValue: "TAP_LIVE"), object: nil)
        if !isRootViewController {
            initNavController()
        }else {
            loadNotiCount()
        }

        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#FFFFFF")
        self.tabBarController?.tabBar.items?[0].title = AppLanguage.jobsTabTitle
        self.tabBarController?.tabBar.items?[1].title = AppLanguage.newsTabTitle
//        self.tabBarController?.tabBar.items?[2].title = AppLanguage.biometricTabTitle
        self.tabBarController?.tabBar.items?[2].title = AppLanguage.contactTabTitle

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
          
    func loadNotiCount(){
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
       if saved != nil {
           if let ID = saved!["ID"] as? Int {
                self.getCountNoti(ISCNotiListInput(ApplicantId: ID, PageIndex: 0, PageSize: 10), { (data) in
                    self.totalNoti = data.TotalRecords ?? 0
                    self.initNavController()
                }) { (error) in
                    
                }
           }
        }else {
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        for tempView in view.subviews {
            if tempView is UIAutoSizeChatButton {
                tempView.removeFromSuperview()
            }
        }
        if isShowChat {
            
//            let sizeBtn = UIScreen.main.bounds.size.height/812 * 60
//            chatBtn = UIAutoSizeChatButton(frame: CGRect(x: UIScreen.main.bounds.size.width * 0.9 - sizeBtn, y: UIScreen.main.bounds.size.height * 0.8 - (self.tabBarController?.tabBar.frame.size.height ?? 0) , width: sizeBtn, height: sizeBtn))
//            chatBtn?.setImage(UIImage(named: "ico-chat-buble"), for: .normal)
//            chatBtn?.addTarget(self, action:#selector(self.buttonMoved(sender:event:)), for:.touchDragInside)
//            chatBtn?.addTarget(self, action:#selector(self.buttonMoved(sender:event:)), for: .touchDragOutside)
//
//            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.buttonTapped))
//            tapGesture.numberOfTapsRequired = 1
//            chatBtn?.addGestureRecognizer(tapGesture)
// 
//            self.view.addSubview(chatBtn!)
        }
    }
    
    
//    @objc func buttonMoved(sender: AnyObject, event: UIEvent) {
//        let ylimit = UIScreen.main.bounds.size.height * 0.8 - (self.tabBarController?.tabBar.frame.size.height ?? 0)
//        guard let control = sender as? UIControl else { return }
//        guard let touches = event.allTouches else { return }
//        guard let touch = touches.first else { return }
//
//        let prev = touch.previousLocation(in: control)
//        let p = touch.location(in: control)
//        var center = control.center
//        center.x += p.x - prev.x
//        center.y += p.y - prev.y
//        if center.y >= ylimit + 20 {
//            return
//        }
//        print(center.y)
//        control.center = center
//    }
    
   
    
    // MARK: - Inits
    func initNavController() {
      if !isRootViewController {
        let backImage = UIImage(named: "icon-back")!
        let barButton = UIBarButtonItem(image: backImage, target: self, action: #selector(backAction), iconColor: tintButtonColor, textColor: tintColor)
        self.navigationController?.title = backButtonTitle
        self.navigationItem.leftBarButtonItems = [barButton]
        let imageAvatar = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        imageAvatar.image = UIImage(named: "user_avatar")
        if Common.shared.isKeyPresentInUserDefaults(key: Common.key_avatar){
            let dataImg = Common.shared.getImgAvatar()
            imageAvatar.image = UIImage(data: dataImg as Data)
        }else {
            let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
            if saved != nil {
                if let PortraitImage = saved?["PortraitImage"] as? String {
                    if PortraitImage != "" {
                        SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(PortraitImage)"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                                if let img = image {
                                    imageAvatar.image = img
                                }
                            }
                        }
                    }
                }
        }
        
        let viewAvatar : UIView = UIView()
        viewAvatar.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        let avatarBtn: UIButton = UIButton(type: .custom)
        avatarBtn.addTarget(self, action: #selector(self.backAvatarAction), for: .touchUpInside)
        avatarBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        viewAvatar.layer.borderWidth = 3
        viewAvatar.layer.borderColor = UIColor.white.cgColor
        viewAvatar.layer.cornerRadius = viewAvatar.frame.size.width / 2
        viewAvatar.layer.masksToBounds = true
        viewAvatar.addSubview(avatarBtn)
        viewAvatar.addSubview(imageAvatar)
        let barBtn = UIBarButtonItem(customView: viewAvatar)
        self.navigationItem.rightBarButtonItem = barBtn
        
        }
        
        if isSearchViewController {
//            if !isShowChat {
//                return
//            }
            addSearchView()
            
        }
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#F27228")]
        self.navigationController?.navigationBar.isOpaque = false
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    @objc func menuAction() {
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuLoginViewController") as! ISCMenuLoginViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }else {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuViewController") as! ISCMenuViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }
        
    }
    @objc func backAvatarAction() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func rightButtonAction() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @objc func notiAction() {
        let storyboard = UIStoryboard(name: "Noti", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCNotificationListViewController") as! ISCNotificationListViewController
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, "")
    }
    @objc func searchTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCSearchViewController") as! ISCSearchViewController
        vc.Content = searchTft?.text ?? ""
        vc.hidesBottomBarWhenPushed = false
        self.pushViewControler(vc, "")
    }
    
    func addSearchView() {
        let navigationBarHeight = self.navigationController?.navigationBar.bounds.size.height ?? 10
        let searchView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.65, height: navigationBarHeight - 10))
        searchTft = UIAutoSizeTextField(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.65, height: navigationBarHeight - 20))
        searchView.addSubview(searchTft!)
       
        
        searchTft!.placeholder = AppLanguage.searchContentPlaceHolder
        searchTft!.borderStyle = .none
        searchTft!.font = UIFont.systemFont(ofSize: 13)
        searchView.layer.cornerRadius = (navigationBarHeight - 10)/2
        searchView.layer.borderColor = UIColor.init(hexString: "#cdcccc").cgColor
        searchView.layer.borderWidth = 1.0
            
        searchTft!.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 0))
        searchTft!.leftViewMode = UITextField.ViewMode.always
            
        let searchIconImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (navigationBarHeight - 20)*0.5 , height: (navigationBarHeight - 20)*0.5))
        let searchIconView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: (navigationBarHeight)*0.5, height: (navigationBarHeight - 20)*0.5))
        searchIconImageView.image = UIImage(named: "ico-search-navi")
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(searchTapped(tapGestureRecognizer:)))
        searchIconImageView.isUserInteractionEnabled = true
        searchIconImageView.addGestureRecognizer(tapGestureRecognizer)
        
        searchIconView.addSubview(searchIconImageView)
        searchTft!.rightView = searchIconView
        searchTft!.rightViewMode = UITextField.ViewMode.always
            
        searchTft!.center = searchView.center
        
        if !isSearch {
           let tapGestureRecognizerView = UITapGestureRecognizer(target: self, action: #selector(searchTapped(tapGestureRecognizer:)))
           let btnSearch = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.65, height: navigationBarHeight - 10))
           btnSearch.addGestureRecognizer(tapGestureRecognizerView)
           searchView.addSubview(btnSearch)
        }else {
            searchTft?.becomeFirstResponder()
        }
            
        self.navigationItem.titleView = searchView
        let imageAvatar = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        imageAvatar.image = UIImage(named: "user_avatar")
        
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if !isRootViewController {
            if Common.shared.isKeyPresentInUserDefaults(key: Common.key_avatar){
                let dataImg = Common.shared.getImgAvatar()
                imageAvatar.image = UIImage(data: dataImg as Data)
            }else {
                if saved != nil {
                    if let PortraitImage = saved?["PortraitImage"] as? String {
                        if PortraitImage != "" {
                            SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(PortraitImage )"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                                if let img = image {
                                    imageAvatar.image = img
                                }
                            }
                        }
                    }
                }
            }
            
            let viewAvatar : UIView = UIView()
            viewAvatar.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            let avatarBtn: UIButton = UIButton(type: .custom)
            avatarBtn.addTarget(self, action: #selector(self.backAvatarAction), for: .touchUpInside)
            avatarBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            viewAvatar.layer.borderWidth = 3
            viewAvatar.layer.borderColor = UIColor.white.cgColor
            viewAvatar.layer.cornerRadius = viewAvatar.frame.size.width / 2
            viewAvatar.layer.masksToBounds = true
            viewAvatar.addSubview(avatarBtn)
            viewAvatar.addSubview(imageAvatar)
            let barBtn = UIBarButtonItem(customView: viewAvatar)
            self.navigationItem.rightBarButtonItem = barBtn
        }else {
            if Common.shared.isKeyPresentInUserDefaults(key: Common.key_avatar){
                let dataImg = Common.shared.getImgAvatar()
                imageAvatar.image = UIImage(data: dataImg as Data)
            }else {
                if saved != nil {
                    if let PortraitImage = saved?["PortraitImage"] as? String {
                        if PortraitImage != "" {
                            SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(PortraitImage )"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                                if let img = image {
                                    imageAvatar.image = img
                                }
                            }
                        }
                    }
                }
            }
            
            let viewAvatar : UIView = UIView()
            viewAvatar.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            let avatarBtn: UIButton = UIButton(type: .custom)
            avatarBtn.addTarget(self, action: #selector(self.menuAction), for: .touchUpInside)
            avatarBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            viewAvatar.layer.borderWidth = 3
            viewAvatar.layer.borderColor = UIColor.white.cgColor
            viewAvatar.layer.cornerRadius = viewAvatar.frame.size.width / 2
            viewAvatar.layer.masksToBounds = true
            viewAvatar.addSubview(avatarBtn)
            viewAvatar.addSubview(imageAvatar)
            let barBtn = UIBarButtonItem(customView: viewAvatar)
            self.navigationItem.leftBarButtonItem = barBtn
            
            
            let notiImg = UIImage(named: totalNoti == 0 ? "icon-noti-1" : "icon-noti-2")!
            let button = UIBarButtonItem(image: notiImg, target: self, action: #selector(self.notiAction), size: self.navigationController?.navigationBar.bounds.size.height ?? 0 / 2)
            self.navigationItem.rightBarButtonItems = [button]
        }
    }
    
    func overrideRightButtonAction(_ action: Selector?) {
        SDWebImageManager.shared.loadImage(with: URL(string: "https://data.whicdn.com/images/299530921/original.png"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
            if let img = image {
                let button = UIBarButtonItem(image: img, target: self, action: action, size: self.navigationController?.navigationBar.bounds.size.height ?? 0 / 2)
                self.navigationItem.rightBarButtonItems = [button]
            }
        }
    }
    
    @objc
    func dismissKeyboard() {
        self.searchTft?.endEditing(true)
        self.view.endEditing(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIBaseViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}

extension UIBaseViewController {
    func showHUD(message:String) {
        ISCProgressHUD().loadingIndicator(self.tabBarController ?? self, self.v, message) { (complete) in }
    }
    func hideHUD() {
        ISCProgressHUD().stopIndicator(self.tabBarController ?? self, self.v) { (complete) in }
    }
    func showAlert(with title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)

        if actions.isEmpty {
            let action = UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel, handler: nil)
            alert.addAction(action)
        } else {
            actions.forEach(alert.addAction)
        }

        self.present(alert, animated: true, completion: nil)
    }
    func alertMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
extension UIImage {
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage ?? UIImage()
    }
}
extension UIBarButtonItem {
    convenience init(image :UIImage, target: Any?, action: Selector?, iconColor : UIColor, textColor : UIColor) {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.tintColor = iconColor
 
        button.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        if let target = target, let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        self.init(customView: button)
    }
    convenience init(image :UIImage, target: Any?, action: Selector?, size:CGFloat) {
           let button = UIButton(type: .custom)
        button.setImage(image.resizeImage(image: image, targetSize: CGSize(width: size - size*0.05, height: size - size*0.05)), for: .normal)
    
           button.frame = CGRect(x: 0, y: 0, width: size - size*0.05, height: size - size*0.05)
           if let target = target, let action = action {
               button.addTarget(target, action: action, for: .touchUpInside)
           }
        button.layer.cornerRadius = size/2
        button.layer.masksToBounds = true
           self.init(customView: button)
       }
}

extension UIViewController {
    func presentFullscreen(_ viewController:UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        let navigationController:UINavigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overFullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: animated, completion: completion)
    }
}

//SignalR
extension UIBaseViewController : SingleButtonDialogPresenter {

}
