//
//  ISCBaseViewController.h
//  ISCCamera
//
//  Created by Pham Chi Hieu on 11/17/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ISCBaseViewController : UIViewController
- (void)showLoading;
- (void)hideLoading;
@end

NS_ASSUME_NONNULL_END
