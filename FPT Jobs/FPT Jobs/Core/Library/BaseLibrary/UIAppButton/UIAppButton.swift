//
//  UIAppButton.swift
//  ISCCamera
//
//  Created by Long Vu on 5/6/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class UIAppButton: UIButton {
    let gradientLayer = CAGradientLayer()
    
    override func awakeFromNib() {
        setup()
    }
    
    private func setup() {
        setAutoSizeButton()
        setCornerRadius(for: 12)
        applyBtnGradient()
    }
    
    private func setAutoSizeButton() {
        guard let pointSize = self.titleLabel?.font.pointSize else {return}
        var fontSize = UIScreen.main.bounds.size.height/812 * pointSize
        if fontSize < 12 { fontSize = 12 }
        self.titleLabel?.font = self.titleLabel?.font.withSize(fontSize)
    }
    
    private func setCornerRadius(for value: CGFloat) {
        self.layer.cornerRadius = value
        self.clipsToBounds = true
    }
    
    func setLabelForButton(label: String) {
        self.titleLabel?.text = label
        self.titleLabel?.textColor = UIColor.white
    }
    
    func applyBtnGradient() {
        self.createGradientLayer(colorOne: UIColor.darkGrey, colorTwo: UIColor.charcoalGreyTwo)
    }
    
    func createGradientLayer(colorOne: UIColor, colorTwo: UIColor) {
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0,y: 0.0)
        gradientLayer.cornerRadius = 12.0
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = self.bounds
    }
}
