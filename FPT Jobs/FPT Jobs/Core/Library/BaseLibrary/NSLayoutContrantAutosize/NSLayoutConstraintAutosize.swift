//
//  NSLayoutConstraintAutosize.swift
//  ISCCamera
//
//  Created by Long Vu on 5/27/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class NSLayoutConstraintAutosize: NSLayoutConstraint {
    
    override var constant: CGFloat {
        set {
            super.constant = newValue
        }
        get {
            return UIScreen.main.bounds.size.height/812 * super.constant
        }
    }
}
