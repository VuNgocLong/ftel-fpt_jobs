//
//  ISCProgressHUD.swift
//  ISCCamera
//
//  Created by Long Vu on 5/8/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCProgressHUD {
    private var window: UIWindow!
    @objc func loadingIndicator( _ vc: UIViewController, _ loadingView: UIView, _ msg:String , completionHandler: @escaping (_ success: Bool) -> ())
    {
        loadingView.backgroundColor = UIColor.clear
        vc.view.addSubview(loadingView)
        
        var backgroundView = UIView()
        backgroundView = UIView(frame: CGRect(x: loadingView.frame.origin.x, y: loadingView.frame.origin.y, width: loadingView.frame.size.width, height: loadingView.frame.size.height))
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.3;
        loadingView.addSubview(backgroundView)
        
        var indicatorView = UIView()
        indicatorView = UIView(frame: CGRect(x: loadingView.frame.midX - 90, y: loadingView.frame.midY - 25, width: 180, height: 50))
        indicatorView.backgroundColor = UIColor.white
        indicatorView.alpha = 1.0
        indicatorView.layer.cornerRadius = 10
        
        let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 180, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = msg
        indicatorView.addSubview(activityView)
        indicatorView.addSubview(textLabel)
        loadingView.addSubview(indicatorView)
        completionHandler(true)
    }
    @objc func stopIndicator( _ vc: UIViewController, _ loadingView: UIView , completionHandler: @escaping (_ success: Bool) -> ())
    {
        for view in loadingView.subviews{
            view.removeFromSuperview()
        }
        loadingView.removeFromSuperview()
        completionHandler(true)
    }
}
