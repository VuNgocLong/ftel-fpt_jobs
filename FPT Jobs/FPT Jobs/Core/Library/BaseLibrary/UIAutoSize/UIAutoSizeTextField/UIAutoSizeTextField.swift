//
//  UIAutoSizeTextField.swift
//  ISCCamera
//
//  Created by Long Vu on 5/6/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

@IBDesignable
class UIAutoSizeTextField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        guard let pointSize = self.font?.pointSize else {return}
        var fontSize = UIScreen.main.bounds.size.height/812 * pointSize
        if fontSize < 12 { fontSize = 12 }
        self.font = self.font?.withSize(fontSize)
    }
}
