//
//  UIAutoSizeButton.swift
//  ISCCamera
//
//  Created by Long Vu on 5/6/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

@IBDesignable
class UIAutoSizeButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        guard let pointSize = self.titleLabel?.font.pointSize else {return}
        var fontSize = UIScreen.main.bounds.size.width/414 * pointSize
        if fontSize < 12 { fontSize = 12 }
        self.titleLabel?.font = self.titleLabel?.font.withSize(fontSize)
    }
}
class UIAutoSizeChatButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        guard let pointSize = self.titleLabel?.font.pointSize else {return}
        var fontSize = UIScreen.main.bounds.size.height/414 * pointSize
        if fontSize < 12 { fontSize = 12 }
        self.titleLabel?.font = self.titleLabel?.font.withSize(fontSize)
    }
}
