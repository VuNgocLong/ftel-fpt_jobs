//
//  UIAutoSizeLabel.swift
//  ISCCamera
//
//  Created by Long Vu on 4/23/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

@IBDesignable
class UIAutoSizeLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        var fontSize = UIScreen.main.bounds.size.height/812 * self.font.pointSize
        if fontSize < 12 { fontSize = 12 }
        self.font = self.font.withSize(fontSize)
    }
}

class UIAutoSizeLabelNomarl: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        var fontSize = UIScreen.main.bounds.size.height/812 * self.font.pointSize
        if fontSize < 12 { fontSize = 12 }
        self.font = self.font.withSize(fontSize)
       
    }
}

@IBDesignable
class UIAutoSizeLabelTitle: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        var fontSize = UIScreen.main.bounds.size.height/812 * self.font.pointSize
        if fontSize < 15 { fontSize = 15 }
        self.font = self.font.withSize(fontSize)
    }
}
