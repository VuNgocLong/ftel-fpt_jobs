//
//  ISCAlamofireConstant.swift
//  ISCCamera
//
//  Created by Long Vu on 4/17/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

struct AC {
    struct errorMessage {
        static let noInternetError = "There is no internet connection!"
        static let inCorrectTypeResponse = "Data type is not correct!"
        static let failureConnectionAPI = "Failure Connection!"
        static let noMessageApiErrorReturn = "Null Error Message!"
    }
}
