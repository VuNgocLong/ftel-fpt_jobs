//
//  ISCResponseStructure.swift
//  ISCCamera
//
//  Created by Long Vu on 4/17/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

struct APIResponse<T: Decodable>: Decodable {
    var ReturnMesage:String?
    var ErrCode:Int?
    var ErrorCode:String?
    var Data:T?
    var DataList:[T?]?
    var ListData:[T?]?
    var Succeeded:Bool?
    var Result:Bool?
    var Errors:[APIFailureResponse?]?
    var access_token:String?
    var refresh_token:String?
    var token_type:String?
    var error:String?
    var otherType:T?
    var otherTypeList:[T?]?
    var MsgAll:String?
    var TotalCount:Int?
    var TotalRecords:Int?
}
struct APIResponseData<T: Decodable>: Decodable {
    
}
struct APIFailureResponse: Decodable {
    var Code:String?
    var Description:String?
}

extension Encodable {
    func encode(with encoder: JSONEncoder = JSONEncoder()) throws -> Data {
        return try encoder.encode(self)
    }
}

extension Decodable {
    static func decode(with decoder: JSONDecoder = JSONDecoder(), from data: Data) throws -> Self {
        return try decoder.decode(Self.self, from: data)
    }
}
