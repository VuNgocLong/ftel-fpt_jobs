//
//  ISCAlamofireConfig.swift
//  ISCCamera
//
//  Created by Long Vu on 4/17/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import Alamofire

struct K {
    struct serverAPI {
        //Staging
//        static let baseURL = "https://sapi.fpt.vn"
//        static let api = baseURL + "/fptjobsapistag/api/"
//        static let token = baseURL + "/fptjobsapistag/token"
//        static let tokenKong = baseURL + "/token/"
//        static let baseImageURL = "https://fptjobs.com/staging"
//        static let domain = "https://fptjobs.com/staging"
        
        //product
        static let baseURL = "https://sapi.fpt.vn"
        static let api = baseURL + "/fptjobsapi/api/"
        static let token = baseURL + "/fptjobsapi/token"
        static let tokenKong = baseURL + "/token/"
        static let baseImageURL = "https://fptjobs.com"
        static let domain = "https://fptjobs.com"
        
    }
}

enum httpHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum contentType: String {
    case json = "application/json; charset=utf-8"
    case application = "application/x-www-form-urlencoded"
}

enum APIMethod: String {
    case connect = "CONNECT"
    case delete  = "DELETE"
    case get     = "GET"
    case head    = "HEAD"
    case options = "OPTIONS"
    case patch   = "PATCH"
    case post    = "POST"
    case put     = "PUT"
    case trace   = "TRACE"
}

public let successRange = 200..<299
