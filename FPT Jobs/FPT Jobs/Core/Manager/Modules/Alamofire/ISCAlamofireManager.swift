//
//  ISCAlamofireManager.swift
//  ISCCamera
//
//  Created by Long Vu on 4/17/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import Alamofire

class Connectivity {
   
}
struct NetworkState {
    var isConnected: Bool {
//        return NetworkReachabilityManager()!.isReachable
        return NetworkReachabilityManager(host: "www.google.com")!.isReachable
    }
}

class ISCAlamofireManager {
    static let shared = ISCAlamofireManager()
    private lazy var manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30.0
        let alamofireManager = SessionManager(configuration: configuration)
        return alamofireManager
    }()
    
    func isConnectedToInternet() throws -> Bool{
           return NetworkReachabilityManager()!.isReachable
       }
    
    func headers(contentType:contentType) -> [String:String] {
        let headers:[String:String] = {
            var h:[String:String] = [:]
            h["Content-Type"] = contentType.rawValue
            h["Accept-Language"] = Locale.preferredLanguages[0].contains("vi") ? "vi-VN":"en-US"
            guard UserDefaults.standard.getValue(key: Common.keyAccessToken) != "" else { return h }
            h["Authorization"] = String(format: "Bearer %@", UserDefaults.standard.getValue(key: Common.keyAccessToken))
            return h
        }()
        return headers
    }
    
    func urlsForeCast(url:String) -> String {
        let arr = url.components(separatedBy: "?")
        if arr.count > 1 {
            return (url + "&jwt=\(Common.shared.getKongToken())").replacingOccurrences(of: "\"", with: "")
        }
        return (url + "?jwt=\(Common.shared.getKongToken())").replacingOccurrences(of: "\"", with: "")
    }

    func apiConnection<T:Encodable,X:Codable>(_ url:String, isCheckHeader : Bool = true  ,_ param:T? ,_ method:APIMethod , _ contentType: contentType,
                        _ onSuccess: @escaping (_ response: APIResponse<X>)->(),
                       _ onFailure: @escaping (_ message: String)->()){
       DispatchQueue.main.async {
            guard NetworkState().isConnected else {
               onFailure(AppLanguage.errorNetworkNoInternet)
               return
           }
        }
        do {
            let status = try isConnectedToInternet()
            if !status {
                onFailure(AppLanguage.errorNetworkNoInternet)
                return
            }
        }catch {
            onFailure(AppLanguage.errorNetworkNoInternet)
            return
        }
        let encoder: URLEncoding = URLEncoding.default
        
        refreshToken(K.serverAPI.token,isCheckHeader: isCheckHeader, { (value) in
            if value == "SUCCESS" {
                self.manager.request(self.urlsForeCast(url: url), method: HTTPMethod(rawValue: method.rawValue)!, parameters: param?.dictionary, encoding: encoder, headers: self.headers(contentType: contentType))
                    .validate(statusCode: successRange)
                    .responseJSON { (data) in
                    switch data.result {
                    case .success(_):
                        do {
                            if let resData = data.data {
                                if let arrDict = try JSONSerialization.jsonObject(with: resData, options: []) as? [[String : Any]] {
                                    var returnData = APIResponse<X>()
                                    var arrT: [X?]? = []
                                    if arrDict.count > 0 {
                                        for i in 0...arrDict.count-1 {
                                            let dat:Data = try JSONSerialization.data(withJSONObject: arrDict[i], options: JSONSerialization.WritingOptions.prettyPrinted)
                                            arrT?.append(try X.decode(from: dat))
                                        }
                                    }
                                    returnData.otherTypeList = arrT
                                    onSuccess(returnData)
                                }
                                else if let dict = try JSONSerialization.jsonObject(with: resData, options: []) as? [String : Any] {
                                    if !dict.keys.contains("ReturnMesage") && !dict.keys.contains("Data") && !dict.keys.contains("access_token") && !dict.keys.contains("error")
                                    && !dict.keys.contains("ErrCode")
                                    {
                                        var returnData = APIResponse<X>()
                                        returnData.otherType = try X.decode(from: resData)
                                        onSuccess(returnData)
                                    }else {
                                        let returnData = try APIResponse<X>.decode(from:resData)
                                        onSuccess(returnData)
                                    }
                                }
                                else {
                                    let returnData = try APIResponse<X>.decode(from:resData)
                                    if returnData.access_token != nil {
                                       onSuccess(returnData)
                                    }
                                    else {
                                        // Other cases of this project will success or self check after
                                        onSuccess(returnData)
                                    }
                                }
                            }
                            else {
                                onFailure(AppLanguage.errorNetworkInCorrectTypeResponse)
                            }
                        }catch let jsonError {
                            print(jsonError)
                            onFailure(AppLanguage.errorNetworkInCorrectTypeResponse)
                        }
                        break
                    case .failure( _):
                        if let httpStatusCode = data.response?.statusCode {
                            if httpStatusCode == 401 {
                                UserDefaults.standard.removeObject(forKey: Common.keyUserProfile)
                                UserDefaults.standard.removeObject(forKey: Common.keyAccessToken)
                                UserDefaults.standard.removeObject(forKey: Common.bearTokenKey)
                                onFailure(AppLanguage.unauthorizedMessage)
//                                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
//                                _ = appDelegate.grabLogin()
                            }
                            else {
                                do {
                                    if let resData = data.data {
                                        let returnData = try JSONDecoder().decode(APIResponse<X>.self, from: resData)
                                        if returnData.error != nil {
                                            onFailure(returnData.error!)
                                        }
                                        else {
                                            onFailure(AppLanguage.errorNetworkFailureConnectionApi)
                                        }
                                    }
                                    else {
                                        onFailure(AppLanguage.errorNetworkInCorrectTypeResponse)
                                    }
                                }catch let jsonError {
                                    print(jsonError)
                                    onFailure(AppLanguage.errorNetworkInCorrectTypeResponse)
                                }
                            }
                        }
                        else {
                            onFailure(AppLanguage.errorNetworkFailureConnectionApi)
                        }
                        break
                    }
                }
            }else {
                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                _ = appDelegate.grabLogin()
            }
        }) { (error) in
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            _ = appDelegate.grabLogin()
            return
        }
    }
    
    
    
    func refreshToken(_ url:String, isCheckHeader : Bool = true,_ onSuccess: @escaping (_ response: String)->(), _ onFailure: @escaping (_ message: String)->()) {
        if Common.shared.checkValidKongToken() {
            onSuccess("SUCCESS")
            return
        }
        if isCheckHeader == false {
            onSuccess("SUCCESS")
            return
        }
        let kongToken = Common.shared.getBearToken()
        let parameters : JSON = [
            "grant_type"  : "refresh_token",
            "refresh_token" : kongToken.refresh_token
        ]
        Alamofire.request(urlsForeCast(url:url) , method: .post, parameters: parameters, encoding : URLEncoding.httpBody, headers: headers(contentType: .application))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success :
                    if let dict = response.result.value as? Dictionary<String, AnyObject> {
                        if let access_token = dict["access_token"] as? String {
                            if let refresh_token = dict["refresh_token"] as? String {
                                let kongToken : Dictionary<String,Any> = [
                                    "access_token" : access_token,
                                    "refresh_token" : refresh_token,
                                    "DateReponse" : Common.shared.getTodayString()
                                ]
                                UserDefaults.standard.removeObject(forKey: Common.bearTokenKey)
                                Common.shared.setBearToken(dict: kongToken)
                                onSuccess("SUCCESS")
                                return
                            }
                        }
                    }
                    onFailure("FAIL")
                    return
                case . failure(_):
                    onFailure("FAIL")
                }
        }
        
    }
    
   
    func getKongToken(_ url:String  ,_ method:APIMethod ,
                        _ onSuccess: @escaping (_ response: String)->(),
                       _ onFailure: @escaping (_ message: String)->()){
        DispatchQueue.main.async {
            guard NetworkState().isConnected else {
               onFailure(AppLanguage.errorNetworkNoInternet)
               return
           }
        }
        let encoder: URLEncoding = URLEncoding.default
        self.manager.request(url, method: HTTPMethod(rawValue: method.rawValue)!, parameters: nil, encoding: encoder, headers: [
            "Authorization":Common.authorizationKong
         ])
            .validate(statusCode: successRange)
            .responseString { (data) in
            switch data.result {
            case .success(_):
                if let resData = data.result.value {
                    onSuccess(resData)
                }
                else {
                    onFailure(AppLanguage.errorNetworkInCorrectTypeResponse)
                }
                break
            case .failure(let error):
                onFailure(AppLanguage.errorNetworkFailureConnectionApi)
                if let httpStatusCode = data.response?.statusCode {
                    if httpStatusCode == 401 {
                        print("Expired Token!")
                    }
                    else {
                        print(error)
                    }
                }
                else {
                    print("Unknown Error!")
                }
                break
            }
        }
    }
    
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}
