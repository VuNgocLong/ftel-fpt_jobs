//
//  ISCAuthSignInThirdParty.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/28/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class GoogleAuth {
    
    static func getInstance() -> GIDSignIn {
        return GIDSignIn.sharedInstance()
    }

    static func isLogin() -> Bool {
        guard let signIn = GIDSignIn.sharedInstance() else { return false }
        if (signIn.hasPreviousSignIn()) {
          signIn.restorePreviousSignIn()
            if (signIn.currentUser.authentication.clientID != Common.googleClientID) {
            signIn.signOut()
            return false
          }
        }
        return true
    }

    static func signOut() {
        getInstance().signOut()
    }

    static func isValidatedWithUrl(url: NSURL) -> Bool {
        return url.scheme!.hasPrefix(Bundle.main.bundleIdentifier!) || url.scheme!.hasPrefix("com.googleusercontent.apps.")
    }
}

class FacebookAuth {
    static func isLogin() -> Bool {
        return AccessToken.current != nil
    }

    static func signOut() {
        LoginManager().logOut()
    }

    static func isValidatedWithUrl(url: NSURL) -> Bool {
        return url.scheme!.hasPrefix("fb\(Settings.appID ?? "")") && url.host == "authorize"
    }
}
