//
//  BaseService.swift
//  MobisalesVN
//
//  Created by PHAM CHI HIEU on 9/27/18.
//  Copyright © 2018 FPT. All rights reserved.
//

import Foundation
import Alamofire

class BaseService {
    let DOMAIN = ""
    enum ServiceFailureReason : Int, Error {
        case unAuthorized = 401
        case notFound = 404
        case errorService = 400
        case errorNetwork = -1
        case success = 0
    }
    var request_headers: HTTPHeaders = [
        "Content-Type" : "application/json",
        "Accept-Language" : Locale.preferredLanguages[0].contains("vi") ? "vi-VN":"en-US",
        "Authorization" : String(format: "Bearer %@", UserDefaults.standard.getValue(key: Common.keyAccessToken))
    ]
    
    func postAPI(url : String, isCheckHeader : Bool = true, parameters :Dictionary<String, Any>, completion : @escaping (_ result : ResultService<Dictionary<String, AnyObject>, ServiceFailureReason>) -> Void) {

        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 300
        Alamofire.request(url, method: .post, parameters: parameters, encoding : JSONEncoding.default, headers: request_headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success :
                     if let dict = response.result.value as? Dictionary<String, AnyObject> {
                        completion(.success(payload: dict))
                    }
                    break
                case . failure(_):
                    break
                    completion(.failure(.errorNetwork))
                }
        }
    }
    
    func postRefreshToken(url : String, isCheckHeader : Bool = true, parameters :Dictionary<String, Any>, completion : @escaping (_ result : ResultService<Dictionary<String, AnyObject>, ServiceFailureReason>) -> Void) {
        
        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 300
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]

        Alamofire.request(url, method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers)
            .responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
            case.failure(let error):
                print("Not Success",error)
                
            }

        }
        
//        Alamofire.request(url, method: .post, parameters: parameters, encoding : JSONEncoding.default, headers: [
//            "Content-Type" : "application/x-www-form-urlencoded"
//            ])
//            .validate()
//            .responseJSON { response in
//                switch response.result {
//                case .success :
//                     if let dict = response.result.value as? Dictionary<String, AnyObject> {
//                        completion(.success(payload: dict))
//                    }
//                    break
//                case . failure(_):
//                    break
//                    completion(.failure(.errorNetwork))
//                }
//        }
    }

}
 extension BaseService.ServiceFailureReason {
    func getErrorMessage() -> String? {
        var strMessage : String = "Không kết nối được. Kiểm tra lại mạng và kết nối lại."
        switch self {
        case .unAuthorized:
            strMessage = "Hêt phiên làm việc, làm ơn login lại"
            break
        case .notFound:
            strMessage = "Không gọi được api."
            break
        case .errorService:
            strMessage = "Lỗi trong quá trình gọi api."
            break
        default :
            strMessage = "Không kết nối được. Kiểm tra lại mạng và kết nối lại."
            break
        }
        return strMessage
    }
}
enum ResultService<T, U> where U: Error {
    case success(payload : T)
    case failure(U?)
}

enum ResultData<T, U> {
    case success(payload : T)
    case error(U?)
}

enum EmptyResult<U> where U: Error {
    case success
    case failure(U?)
}

enum ResultModel {
    case success
    case error
}
enum Result<T, U> where U: Error {
    case success(payload : T)
    case failureSection(U?)
    case failure(U?)
}
