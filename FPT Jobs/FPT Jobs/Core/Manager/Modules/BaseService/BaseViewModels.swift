//
//  BaseViewModel.swift
//  MobisalesVN
//
//  Created by PHAM CHI HIEU on 11/10/18.
//  Copyright © 2018 FPT. All rights reserved.
//

import Foundation
protocol ConfigViewModel {
    var navigatePush: (() -> ())?  { get set }
    var onShowError: ((_ alert: SingleButtonAlert) -> Void)?  { get set }
    var showLoading: Bindable<Bool> { get }
    var onShowSuccess: ((_ alert: SingleButtonAlert) -> Void)?  { get set }
}

class BaseViewModels : ConfigViewModel{

    var navigatePush: (() -> ())?
    var onShowError: ((SingleButtonAlert) -> Void)?
    let showLoading: Bindable = Bindable(false)
    var onShowSuccess: ((SingleButtonAlert) -> Void)?

    func showError(message : String) {
        let okAlert = SingleButtonAlert(
            title: "Thông Báo",
            message: message,
            action: AlertAction(buttonTitle: "Ok", handler: {})
        )
        self.onShowError?(okAlert)
    }
    
  
}
