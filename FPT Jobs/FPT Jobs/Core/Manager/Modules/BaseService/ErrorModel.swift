//
//  ErrorModel.swift
//  MobisalesVN
//
//  Created by PHAM CHI HIEU on 9/27/18.
//  Copyright © 2018 FPT. All rights reserved.
//

import Foundation
struct ErrorModel {
    var Error : String = ""
    var ErrorCode : Int = 0
    init?(jsonError : Dictionary<String, Any>) {
        if let ErrorCode = jsonError["ErrorCode"] as? Int {
            self.ErrorCode = ErrorCode
        }
        if let Error = jsonError["Error"] as? String {
            self.Error = Error
        }
        
    }

    init?(ErrorCode : Int, Error : String) {
        self.ErrorCode = ErrorCode
        self.Error = Error
    }
}
