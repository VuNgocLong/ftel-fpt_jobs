//
//  JSON.swift
//  MobisalesVN
//
//  Created by PHAM CHI HIEU on 9/28/18.
//  Copyright © 2018 FPT. All rights reserved.
//

import Foundation
typealias JSON = Dictionary<String, Any>
enum ServiceFailureReason : Int, Error {
    case unAuthorized = 401
    case notFound = 404
    case errorService = 400
    case errorNetwork = -1
    case success = 0
}
