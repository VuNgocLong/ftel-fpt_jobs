//
//  ISCAppExtension.swift
//  ISCCamera
//
//  Created by Long Vu on 4/17/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

// TODO: UserDefault set and get value
extension UserDefaults {
    func setValue(value: String, key: String) {
        set(value, forKey: key)
        synchronize()
    }
    func getValue(key: String) -> String {
        return string(forKey: key) ?? ""
    }
}


// TODO: Extension for Textfield
extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

extension UIViewController {
    func pushViewControlerCustom(_ viewController:UIViewController,_ backButtonTitle:String) {
//        viewController.backButtonTitle = backButtonTitle
//        viewController.isRootViewController = false
//        viewController.isSearchViewController = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func pushSearchViewControler(_ viewController:UIBaseViewController,_ backButtonTitle:String) {
        viewController.backButtonTitle = backButtonTitle
        viewController.isRootViewController = false
        viewController.isSearchViewController = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func pushViewControler(_ viewController:UIBaseViewController,_ backButtonTitle:String) {
        viewController.backButtonTitle = backButtonTitle
        viewController.isRootViewController = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func pushViewControlerTitle(_ viewController:UIBaseViewController,_ backButtonTitle:String) {
        viewController.backButtonTitle = backButtonTitle
        viewController.titleOnly = true
        viewController.isRootViewController = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func pushViewControler(_ viewController:UIBaseViewController,_ backButtonTitle:String, colorButtonTitle : UIColor, colorTitle : UIColor) {
        viewController.backButtonTitle = backButtonTitle
        viewController.tintButtonColor = colorButtonTitle
        viewController.tintColor = colorTitle
        viewController.isRootViewController = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func pushViewControllerBlock(_ viewController:UIBaseViewController,_ backButtonTitle:String,
    completion: (() -> Void)?) {
        viewController.isRootViewController = false
        viewController.backButtonTitle = backButtonTitle
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.navigationController?.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
}

extension UIColor {
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
    
    class var darkGrey: UIColor {
        return UIColor(hex: 0x222225)
    }
    
    class var charcoalGreyTwo: UIColor {
        return UIColor(hex: 0x47474c)
    }
    
    static var blackOpa50: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    }
    
    static var blackOpa30: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    class var niceBlue: UIColor {
        return UIColor(hex: 0x0960bd)
    }
    
    class var scanBlue: UIColor {
        return UIColor(hex: 0x7EB9CC)
    }
}

extension UINavigationController {
    
    func popBack(_ nb: Int) {
        let viewControllers: [UIViewController] = self.viewControllers
        guard viewControllers.count < nb else {
            self.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
            return
        }
    }
    
    func popBack<T: UIViewController>(toControllerType: T.Type) {
        var viewControllers: [UIViewController] = self.viewControllers
        viewControllers = viewControllers.reversed()
        for currentViewController in viewControllers {
            if currentViewController .isKind(of: toControllerType) {
                self.popToViewController(currentViewController, animated: true)
                break
            }
        }
    }
    
}

// MARK: - Extension for UITableview
extension UITableView {
    
    @objc func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 18)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    @objc func setEmptyMessage(_ message: String, image: UIImage) {
        let width = self.bounds.size.width
        let height = width
        let heightScaleFactor = UIDevice.current.heightScaleFactor
        let imgTopAchor = 72 * heightScaleFactor
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: imgTopAchor, width: width, height: height))
        imageView.image = image
        
        let messageLabel = UIAutoSizeLabel()
        messageLabel.frame = CGRect(x: 0, y: imageView.bounds.height + imgTopAchor, width: width, height: 50 * heightScaleFactor)
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 18 * heightScaleFactor)
        messageLabel.alpha = 0.8
        
        let view = UIView()
        view.addSubview(imageView)
        view.addSubview(messageLabel)
        view.backgroundColor = UIColor.clear
        self.backgroundView = view
        self.separatorStyle = .none
    }
    
    @objc func restore() {
        self.backgroundView = nil
        self.separatorStyle = .none
    }
}

extension Notification.Name {
    static let ISC_CreatePlace = Notification.Name(
        rawValue: "ISC_CreatePlace")
}

extension URL {
    static var documentsDirectory: URL {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        return try! documentsDirectory.asURL()
    }
    
    static func urlInDocumentsDirectory(with filename: String) -> URL {
        return documentsDirectory.appendingPathComponent(filename)
    }
}

extension StringProtocol {
    subscript(offset: Int) -> Element {
        return self[index(startIndex, offsetBy: offset)]
    }
    subscript(_ range: Range<Int>) -> SubSequence {
        return prefix(range.lowerBound + range.count)
            .suffix(range.count)
    }
    subscript(range: ClosedRange<Int>) -> SubSequence {
        return prefix(range.lowerBound + range.count)
            .suffix(range.count)
    }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence {
        return prefix(range.upperBound.advanced(by: 1))
    }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence {
        return prefix(range.upperBound)
    }
    subscript(range: PartialRangeFrom<Int>) -> SubSequence {
        return suffix(Swift.max(0, count - range.lowerBound))
    }
}


extension UserDefaults {
    open func set<Key, Value>(dictionary: [Key: Value]?, forKey key: String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: dictionary as Any)
        set(data, forKey: key)
    }
    open func dict<Key, Value>(forKey key: String) -> [Key: Value]? {
        guard let data = object(forKey: key) as? Data else { return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? [Key: Value]
    }
}

extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}
