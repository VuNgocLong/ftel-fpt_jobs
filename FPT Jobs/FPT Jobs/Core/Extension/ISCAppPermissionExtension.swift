//
//  ISCAppPermissionExtension.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/19/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
import AVFoundation
import Photos

class AppPermission {
    static let shared = AppPermission()
    
    var permissionPhoto : Bool {
        var result : Bool = false

        switch PHPhotoLibrary.authorizationStatus() {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    result = true
                } else {
                    self.showPermissionPhoto()
                    result = false
                }
            })
            break
        case .restricted, .denied:
            self.showPermissionPhoto()
            result = false
            break
        default:
            result = true
            break
        }
        return result
    }
    
    var permissionCamera : Bool {
        var result : Bool = false
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        result = true
                    }else {
                        result = false
                        self.showPermissionCamera()
                    }
                }
                break
            case .restricted, .denied:
               result = false
               showPermissionCamera()
               break
           default:
               result = true
               break
        }
        return result
    }
    
    var permissionMicrophone : Bool {
        var result : Bool = false
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .audio) { granted in
                    if granted {
                        result = true
                    }else {
                        result = false
                        self.showPermissionMicrophone()
                    }
                }
                break
            case .restricted, .denied:
               result = false
               showPermissionMicrophone()
               break
           default:
               result = true
               break
        }
        return result
    }
    
    func showPermissionCamera(){
        let alert = UIAlertController(
            title: AppLanguage.commonAppName,
            message: Locale.preferredLanguages[0].contains("vi") ? "FPT Jobs cần bạn cho phép truy cập camera":"FPT Jobs needs your permission to access the camera",
            preferredStyle: UIAlertController.Style.alert
         )
        alert.addAction(UIAlertAction(title:  AppLanguage.commonAppSetting, style: .default, handler: { (alert) in
            self.goToAppSetting()
        }))
        if let topVC = UIApplication.topViewController() {
            topVC.present(alert, animated: true, completion: nil)
        }
    }
    
    func showPermissionPhoto(){
        let alert = UIAlertController(
            title: AppLanguage.commonAppName,
            message: Locale.preferredLanguages[0].contains("vi") ? "FPT Jobs cần bạn cho phép truy cập thư viện ảnh":"FPT Jobs needs your permission to access the photo library",
            preferredStyle: UIAlertController.Style.alert
         )
        alert.addAction(UIAlertAction(title:  AppLanguage.commonAppSetting, style: .default, handler: { (alert) in
            self.goToAppSetting()
        }))
        if let topVC = UIApplication.topViewController() {
            topVC.present(alert, animated: true, completion: nil)
        }
    }
    
    func showPermissionMicrophone(){
        let alert = UIAlertController(
            title: AppLanguage.commonAppName,
            message: Locale.preferredLanguages[0].contains("vi") ? "FPT Jobs cần bạn cho phép truy cập microphone":"FPT Jobs needs your permission to access the microphone",
            preferredStyle: UIAlertController.Style.alert
         )
        alert.addAction(UIAlertAction(title:  AppLanguage.commonAppSetting, style: .default, handler: { (alert) in
            self.goToAppSetting()
        }))
        if let topVC = UIApplication.topViewController() {
            topVC.present(alert, animated: true, completion: nil)
        }
    }
    
    func goToAppSetting(){
        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
        }
    }
}
