//
//  ISCTabbarViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/24/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCTabbarViewController: UITabBarController , UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.setValue(true, forKey: "hidesShadow")
//        let sizeBtn = UIScreen.main.bounds.size.height/812 * 60
//        let chatBtn = UIAutoSizeButton(frame: CGRect(x: UIScreen.main.bounds.size.width * 0.9 - sizeBtn, y: UIScreen.main.bounds.size.height * 0.9 - self.tabBar.frame.size.height, width: sizeBtn, height: sizeBtn))
//        chatBtn.setImage(UIImage(named: "ico-chat-buble"), for: .normal)
//        chatBtn.addTarget(self, action:#selector(self.buttonMoved(sender:event:)), for:.touchDragInside)
//        chatBtn.addTarget(self, action:#selector(self.buttonMoved(sender:event:)), for: .touchDragOutside)
//        chatBtn.addTarget(self, action:#selector(self.buttonTapped), for: .touchUpInside)
//        self.view.addSubview(chatBtn)
        
        let sizeBtn = UIScreen.main.bounds.size.height/812 * 60
        let chatBtn = UIAutoSizeChatButton(frame: CGRect(x: UIScreen.main.bounds.size.width * 0.9 - sizeBtn, y: UIScreen.main.bounds.size.height * 0.8 - (self.tabBarController?.tabBar.frame.size.height ?? 0) , width: sizeBtn, height: sizeBtn))
        chatBtn.setImage(UIImage(named: "ico-chat-buble"), for: .normal)
        chatBtn.addTarget(self, action:#selector(self.buttonMoved(sender:event:)), for:.touchDragInside)
        chatBtn.addTarget(self, action:#selector(self.buttonMoved(sender:event:)), for: .touchDragOutside)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.buttonTapped))
        tapGesture.numberOfTapsRequired = 1
        chatBtn.addGestureRecognizer(tapGesture)
        self.view.addSubview(chatBtn)
    }
    

//    @objc func buttonMoved(sender: AnyObject, event: UIEvent) {
//        guard let control = sender as? UIControl else { return }
//        guard let touches = event.allTouches else { return }
//        guard let touch = touches.first else { return }
//
//        let prev = touch.previousLocation(in: control)
//        let p = touch.location(in: control)
//        var center = control.center
//        center.x += p.x - prev.x
//        center.y += p.y - prev.y
//        control.center = center
//        print(control.frame)
//    }
    
    @objc func buttonMoved(sender: AnyObject, event: UIEvent) {
        let ylimit = UIScreen.main.bounds.size.height * 0.8 - (self.tabBarController?.tabBar.frame.size.height ?? 0)
        guard let control = sender as? UIControl else { return }
        guard let touches = event.allTouches else { return }
        guard let touch = touches.first else { return }

        let prev = touch.previousLocation(in: control)
        let p = touch.location(in: control)
        var center = control.center
        center.x += p.x - prev.x
        center.y += p.y - prev.y
        if center.y >= ylimit + 20 {
            return
        }
        print(center.y)
        control.center = center
    }
    
     @objc func buttonTapped(sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TAP_LIVE"), object: nil, userInfo: nil)
//        let vc = ISCLiveChatViewController()
//        vc.hidesBottomBarWhenPushed = true
//        self.pushViewControlerCustom(vc, AppLanguage.jobsTabTitle)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x: 0, y: size.height - 10, width: lineSize.width, height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }

}
