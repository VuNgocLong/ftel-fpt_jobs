//
//  ISCBiomectionConfig.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/13/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCBiomectionConfig {
    static let getPrivatePolicy = K.serverAPI.api + "Catalog/GetConfig/PrivatePolicy"
}
