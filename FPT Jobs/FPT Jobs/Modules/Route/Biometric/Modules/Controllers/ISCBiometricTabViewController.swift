//
//  ISCBiometricTabViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/25/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Alamofire

class ISCBiometricTabViewController: UIBaseViewController, AVCapturePhotoCaptureDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var imgGallary: UIImageView!
    @IBOutlet var capture: UIButton!
    @IBOutlet var preView: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var viewResult: UIView!
    @IBOutlet weak var viewScan: UIView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var imaFigerScan: UIImageView!
    @IBOutlet weak var imgLogofiger: UIImageView!
    @IBOutlet weak var spaceBottomLine: NSLayoutConstraint!
    @IBOutlet weak var lblResult: UIAutoSizeLabel!
    var captureSession: AVCaptureSession?
    var capturePhotoOutput: AVCapturePhotoOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    @objc var captureDevice: AVCaptureDevice?
    var simpleImagePicker = UIImagePickerController()
    var access : String = ""
    var refresh : String = ""
    var idImage1 : Int = 0
    var idImage2 : Int = 0
    var timer = Timer()
    var timerScan = Timer()
    var typRunScan : String = "UP"
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppLanguage.biometricTabTitle
        self.isSearchViewController = true
        self.isShowChat = true
        self.initNavController()
        
        imaFigerScan.image = UIImage(named: "fingerprint")?.withColor(.white)
        imgLogofiger.image = UIImage(named: "fingerprint")?.withColor(.white)
        imgGallary.image = UIImage(named: "gallary")?.withColor(.darkGray)
        // Set up the video preview view.
       viewImage.isHidden = true
       preView.isHidden = true
       viewInfo.isHidden = false
       viewResult.isHidden = true
       viewScan.isHidden = true
        
        self.view.applyGradient(colours: [UIColor(hexString: "#FBAB4A"), UIColor(hexString: "#F3792A")])
        self.viewResult.applyGradient(colours: [UIColor(hexString: "#FBAB4A"), UIColor(hexString: "#F3792A")])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        getToken()
    }
    
    func showViewScan(){
        viewScan.isHidden = false
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(runningLine), userInfo: nil, repeats: true)
    }
    
    func stopViewScan() {
        self.timer.invalidate()
        viewScan.isHidden = true
    }
    
    @objc func runningLine(){
        
        UIView.animate(withDuration: 0.01, delay: 0.0, options: .curveEaseInOut, animations: {
            if self.spaceBottomLine.constant == 159 {
                self.typRunScan = "DOWN"
            }else if self.spaceBottomLine.constant == 1 {
                self.typRunScan = "UP"
            }
            if self.typRunScan == "UP" {
                self.spaceBottomLine.constant += 1
            }else {
                self.spaceBottomLine.constant -= 1
            }
            
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func btnCapture_touch(_ sender: Any) {
       
        if AppPermission.shared.permissionCamera {
            if !(self.captureSession?.isRunning ?? false) {
               configureSession()
            }
            viewImage.isHidden = true
            preView.isHidden = false
            viewInfo.isHidden = true
        }
    }
    @IBAction func btnGalary_touch(_ sender: Any) {
        if AppPermission.shared.permissionPhoto {
            self.simpleImagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                self.simpleImagePicker.delegate = self
                self.simpleImagePicker.sourceType = .photoLibrary;
                self.simpleImagePicker.allowsEditing = false
                self.present(self.simpleImagePicker, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnConfirm_touch(_ sender: Any) {
        if img1.image != nil && img2.image != nil {
            let dict : Dictionary<String, Any> = [
                "idImage1" : idImage1,
                "idImage2" : idImage2
            ]
            UserDefaults.standard.set(dictionary: dict, forKey: Common.keyUserCMND)
            showViewScan()
            self.timerScan = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(scanResult), userInfo: nil, repeats: true)
            
        }else {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.messageBiometricsConfirm)
        }
        
    }
    
    @objc func scanResult(){
        self.timerScan.invalidate()
        getResult(id: self.idImage1)
    }
    
    
    @IBAction private func capturePhoto(_ photoButton: UIButton) {
        
        if AppPermission.shared.permissionCamera {
            let photoSettings : AVCapturePhotoSettings!
            photoSettings = AVCapturePhotoSettings.init(format: [AVVideoCodecKey: AVVideoCodecJPEG])
            photoSettings.isAutoStillImageStabilizationEnabled = true
            photoSettings.flashMode = .off
            photoSettings.isHighResolutionPhotoEnabled = false
            self.capturePhotoOutput?.capturePhoto(with: photoSettings, delegate: self)
            viewImage.isHidden = false
            preView.isHidden = true
            viewInfo.isHidden = true
        }
    }


    private func configureSession() {
        self.captureSession = AVCaptureSession()
        self.captureSession?.sessionPreset = .photo
        self.capturePhotoOutput = AVCapturePhotoOutput()
        self.captureDevice = AVCaptureDevice.default(for: .video)
        let input = try! AVCaptureDeviceInput(device: self.captureDevice!)
        self.captureSession?.addInput(input)
        self.captureSession?.addOutput(self.capturePhotoOutput!)

        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
        self.previewLayer?.frame = self.preView.bounds
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        self.preView.layer.insertSublayer(previewLayer!, at: 0)
        self.captureSession?.startRunning()
        
    }

    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
       // Make sure we get some photo sample buffer
        guard error == nil, let photoSampleBuffer = photoSampleBuffer else {
            self.alertMessage(title: AppLanguage.commonAppName, message: "Error capturing photo: \(String(describing: error))")
            return
        }

        guard let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else {
            return
        }
        // Initialise a UIImage with our image data
        let capturedImage = UIImage.init(data: imageData , scale: 1.0)
        if capturedImage != nil {
            if img1.image == nil {
                 self.uploadImage(selectedImage: capturedImage!, imageIndex: 1, token: access)
             }else {
                 self.uploadImage(selectedImage: capturedImage!, imageIndex: 2, token: access)
             }
            
        }
    }
    

    func photoOutput(_ captureOutput: AVCapturePhotoOutput,
            didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings,
            error: Error?) {

        guard error == nil else {
            self.alertMessage(title: AppLanguage.commonAppName, message: "Error in capture process: \(String(describing: error))")

            return
        }
    }

    override func observeValue(forKeyPath keyPath: String?,
        of object: Any?,
        change: [NSKeyValueChangeKey: Any]?,
        context: UnsafeMutableRawPointer?) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        var img : UIImage? = nil
        if let editedImage = info[.editedImage] as? UIImage {
            img = editedImage
            
            
        } else if let originalImage = info[.originalImage] as? UIImage {
            img = originalImage
        }
        if img != nil
        {
            if img1.image == nil {
                self.uploadImage(selectedImage: img!, imageIndex: 1, token: access)
            }else {
                self.uploadImage(selectedImage: img!, imageIndex: 2, token: access)
            }
            viewInfo.isHidden = true
            viewImage.isHidden = false
        }
        simpleImagePicker.dismiss(animated: true) {() -> Void in }
    }
}


extension ISCBiometricTabViewController {
    func getToken() {
        self.showHUD(message: AppLanguage.commonAppName)
        let request_headers: HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        let parameters :Dictionary<String, Any> = [
            "username" : "fingerprint_client",
            "password" : "62NkVy9UzQ4Y5K7T"
        ]
        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 300
        Alamofire.request("https://fingerprint.csoc.fpt.net/api/token", method: .post, parameters: parameters, encoding : JSONEncoding.default, headers: request_headers)
               .validate()
               .responseJSON { response in
                   switch response.result {
                   case .success :
                    self.hideHUD()
                        if let dict = response.result.value as? Dictionary<String, AnyObject> {
                            if let access = dict["access"] as? String {
                                self.access = access
                               
                            }
                            if let refresh = dict["refresh"] as? String {
                                self.refresh = refresh
                            }
                            let cmndInfo: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserCMND)
                            if cmndInfo != nil {
                                if let id = cmndInfo?["idImage1"] as? Int {
                                    self.idImage1 = id
                                    self.showViewScan()
                                    self.timerScan = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.scanResult), userInfo: nil, repeats: true)
                                    return
                                }
                            }
                        }
                       break
                   case . failure(_):
                        self.hideHUD()
                       break
                      
                }
        }
    }
    
    func uploadImage(selectedImage : UIImage, imageIndex : Int, token : String){
        self.showHUD(message: AppLanguage.commonAppName)
        if let data = selectedImage.jpegData(compressionQuality: 0.5) {
            let parameters: Parameters = [:]
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key,value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                multipartFormData.append(data, withName: "image", fileName: "img_cmnd.jpg", mimeType: "image/jpeg")},
                 usingThreshold:UInt64.init(),
                 to: "https://fingerprint.csoc.fpt.net/api/upload",
                 method:.post,
                 headers: ["Content-Type" : "application/json","Authorization" : "Bearer \(token)"],
                 encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.hideHUD()
                            if let dict = response.result.value as? [Dictionary<String, AnyObject>] {
                                if dict.count > 0 {
                                    let obj = dict[0]
                                    if let pk = obj["pk"] as? Int {
                                        if imageIndex == 1 {
                                            self.idImage1 = pk
                                            self.img1.image = selectedImage
                                        }else {
                                            self.idImage2 = pk
                                            self.img2.image = selectedImage
                                        }
                                        return
                                    }
                                }
                            }
                            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.uploadImageFail)
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        self.hideHUD()
                        self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.uploadImageFail)
                        break
                    }
            })
        }
    }
    
    func getResult(id: Int) {
        let request_headers: HTTPHeaders = [
            "Content-Type" : "application/json",
            "Authorization" : "Bearer \(self.access)"
        ]
        let url = "https://fingerprint.csoc.fpt.net/api/upload?id=\(id)"

        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 300
        Alamofire.request(url, method: .get, parameters: nil, encoding : JSONEncoding.default, headers: request_headers)
               .validate()
               .responseJSON { response in
                   switch response.result {
                   case .success :
                        self.stopViewScan()
                        self.viewImage.isHidden = true
                        self.preView.isHidden = true
                        self.viewInfo.isHidden = true
                        self.viewResult.isHidden = false
                        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
                        if saved == nil {
                            let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                                    message: Locale.preferredLanguages[0].contains("vi") ? "XIN VUI LÒNG ĐĂNG NHẬP ĐỂ XEM TOÀN BỘ KẾT QUẢ" : "PLEASE LOG IN TO SEE THE FULL RESULTS",
                                                                    preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .default){ _ in
                                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                _ = appDelegate.grabLogin()
                            })
                            alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .cancel){ _ in
                            })
                            self.present(alertController, animated: true)
                        }
                        if let dict = response.result.value as? Dictionary<String, AnyObject> {
                            if let personality = dict["personality"] as? String {
                                if personality != "" {
                                    self.lblResult.text = personality
                                    return
                                }
                            }
                            if let type = dict["type"] as? String {
                                if type != "" {
                                    self.lblResult.text = Locale.preferredLanguages[0].contains("vi") ? "Chủng vân tay của bạn là : \(type) kết quả chi tiết hơn sẽ được cập nhật sớm nhất" : "Your fingerprint strain is: \(type) more detailed results will be updated soon"
                                    return
                                }
                            }
                            self.lblResult.text = Locale.preferredLanguages[0].contains("vi") ? "Vẫn đang chờ kết quả từ server..." : "Still waiting for server results..."
                       }
                        break
                   case . failure(_):
                       break
                      
                }
        }
    }
}

class PreviewViewCapture: UIView {
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }

    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
}
