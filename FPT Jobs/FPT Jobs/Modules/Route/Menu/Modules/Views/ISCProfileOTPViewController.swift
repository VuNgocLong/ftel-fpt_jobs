//
//  ISCProfileOTPViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/17/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCProfileOTPViewControllerDelegate: class {
    func closeISCProfileOTPViewController ()
}
class ISCProfileOTPViewController: UIBaseViewController {

    @IBOutlet weak var txtOTP: UIAutoSizeTextField!
    weak var delegate:ISCProfileOTPViewControllerDelegate?
    var CellPhoneNumber:String = ""
    var Email:String = ""
    var FullName:String = ""
    var paramUpdate : JSON = JSON()
    let loginVM : LoginVM = LoginVM()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bindViewModel()
//        sendOTP()
    }
    
    func bindViewModel(){
        loginVM.sendOTPSuccess = { [weak self] in
            self?.hideHUD()
        }
        loginVM.updateProfileSuccess = { [weak self] in
            self?.hideHUD()
            let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                    message: AppLanguage.updateAccountSuccess,
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                self?.dismiss(animated: true, completion: nil)
                self?.delegate?.closeISCProfileOTPViewController()
            })
            self?.present(alertController, animated: true)
        }
        loginVM.onShowError = { [weak self] alert in
            self?.hideHUD()
            self?.presentSingleButtonDialog(alert: alert)
        }
    }
    
    func sendOTP() {
        self.showHUD(message: AppLanguage.connecting)
        loginVM.sendOTP(param: ["CellPhoneNumber":CellPhoneNumber,"Email":Email,"FullName":FullName])
    }

    @IBAction func btnBack_touch(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnConfirm_touch(_ sender: Any) {
        paramUpdate["OTPCode"] = txtOTP.text
       loginVM.updateProfileOTPSuccess = { [weak self] in
           self?.hideHUD()
       }
       
        loginVM.updateProfile(param: paramUpdate)
    }

    @IBAction func btnSendOTP_touch(_ sender: Any) {
        sendOTP()
    }
}
