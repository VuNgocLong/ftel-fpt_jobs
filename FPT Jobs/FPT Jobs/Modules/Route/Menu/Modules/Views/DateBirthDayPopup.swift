//
//  DateBirthDayPopup.swift
//  MobiSale
//
//  Created by PHAM CHI HIEU on 4/9/19.
//  Copyright © 2019 FPT.RAD.FTool. All rights reserved.
//

import UIKit
protocol DateBirthDayPopupDelegate: class {
    func closeDateBirthDayPopup (selectedDate : String)
}
class DateBirthDayPopup: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    weak var delegate:DateBirthDayPopupDelegate?
    var strDate : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if strDate != "" {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy/MM/dd"
           let date = dateFormatter.date(from: strDate)
           datePicker.date = date!
        }
        
    }
    
    @IBAction func btnBack_touch(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func datePickerChanged(sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd 00:00:00"
        strDate = dateFormatter.string(from: datePicker.date)
    }
    @IBAction func btnConfirm_touch(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd 00:00:00"
        strDate = dateFormatter.string(from: datePicker.date)
        self.delegate?.closeDateBirthDayPopup(selectedDate: strDate)
        self.dismiss(animated: true, completion: nil)
    }
}
