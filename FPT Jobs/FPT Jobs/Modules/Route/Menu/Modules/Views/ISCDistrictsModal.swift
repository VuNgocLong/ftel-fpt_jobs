//
//  ISCDistrictsModal.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/13/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCDistrictsModalDelegate: class {
    func closeDistrictsModal (model : ISCDistrictsOutput)
}
class ISCDistrictsModal: UIViewController, UISearchBarDelegate  {

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var widthConstraintsSearchKey: NSLayoutConstraint!
    weak var delegate:ISCDistrictsModalDelegate?
    var arrayDistricts : [ISCDistrictsOutput] = []
    var arrDataSearch : [ISCDistrictsOutput] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        arrDataSearch = arrayDistricts
        
        let cellNib = UINib(nibName: ISCProvinceTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCProvinceTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 44
        
        self.searchBar.backgroundImage = UIImage()
        self.searchBar.backgroundColor = .clear
        self.searchBar.isTranslucent = true
        self.searchBar.tintColor = UIColor(hexString: "#3E3E3E")
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = UIColor.white
        }
        self.searchBar.delegate = self
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.arrDataSearch = []
        for item in arrayDistricts {
            let valueItem = item.DistrictName?.folding(options: .diacriticInsensitive, locale: .current).lowercased()
            let valueSearch = searchText.folding(options: .diacriticInsensitive, locale: .current).lowercased()
            if valueItem?.contains(valueSearch) ?? false {
                self.arrDataSearch.append(item)
            }
        }
        self.tblList.reloadData()
    }
    
    @IBAction func btnSearch_touch(_ sender: Any) {
        if self.widthConstraintsSearchKey.constant > 0 {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.widthConstraintsSearchKey.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            searchBar.resignFirstResponder()
        }else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.widthConstraintsSearchKey.constant = UIScreen.main.bounds.width - 98
                self.view.layoutIfNeeded()
            }, completion: nil)
            searchBar.becomeFirstResponder()
        }
    }
    
    @IBAction func btnBack_click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
// MARK: - UITableView Delegate & Datasource
extension ISCDistrictsModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDataSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCProvinceTableViewCell.name, for: indexPath) as? ISCProvinceTableViewCell
            else {
                return UITableViewCell()
        }
        if arrDataSearch.count > 0 {
            let obj = arrDataSearch[indexPath.row]
            cell.lblProvinceName.text = obj.DistrictName
            cell.selectionStyle = .none
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        let obj = arrDataSearch[indexPath.row]
        self.delegate?.closeDistrictsModal(model: obj)
        self.dismiss(animated: true, completion: nil)
    }
}
