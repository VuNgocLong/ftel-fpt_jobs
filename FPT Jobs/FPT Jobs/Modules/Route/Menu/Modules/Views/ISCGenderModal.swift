//
//  ISCGenderModal.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/13/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCGenderModalDelegate: class {
    func closeISCGenderModal (model : SelectOption)
}
class ISCGenderModal: UIViewController {

    @IBOutlet weak var tblList: UITableView!
    weak var delegate:ISCGenderModalDelegate?
    var arrayData : [SelectOption] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let cellNib = UINib(nibName: ISCProvinceTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCProvinceTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 44
    }

    @IBAction func btnBack_click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
// MARK: - UITableView Delegate & Datasource
extension ISCGenderModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCProvinceTableViewCell.name, for: indexPath) as? ISCProvinceTableViewCell
            else {
                return UITableViewCell()
        }
        if arrayData.count > 0 {
            let obj = arrayData[indexPath.row]
            cell.lblProvinceName.text = obj.Value
            cell.selectionStyle = .none
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrayData[indexPath.row]
        self.delegate?.closeISCGenderModal(model: obj)
        self.dismiss(animated: true, completion: nil)
    }
}
