//
//  ISCJobsFollowConfigTableViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/18/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCJobsFollowConfigTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblJobCate: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lblProvince: UIAutoSizeLabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    static var name: String {
           return String(describing: ISCJobsFollowConfigTableViewCell.self)
       }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
