//
//  ISCMenuObj.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/8/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCMenuInput:Encodable {

}
struct ISCMenuOutput: Codable {
    var Name:String?
    var Value:String?
}
struct ISCUpdateUserProfileInput:Encodable {
    var BirthAddress:String?
    var BirthPlaceDistrictCode:String?
    var BirthPlaceProvinceCode:String?
    var CellPhoneNumber:String?
    var DistrictCode:String?
    var DoB:String?
    var DoB_Day:Int?
    var DoB_Month:Int?
    var DoB_Year:Int?
    var Email:String?
    var EmailConfirmed:Int?
    var FullName:String?
    var Height:String?
    var ID:Int?
    var InterviewerCode:String?
    var KenhTuyenDung:String?
    var Majors:[ISCMajorInput]?
    var MaritalStatus:String?
    var PersonalInformation:String?
    var PhoneNumberConfirmed:String?
    var PhoneTemp:String?
    var PortraitImage:String?
    var ProvinceCode:String?
    var QualificationType:String?
    var RegisteredTimeOnline:String?
    var RequirementSalaryFrom:String?
    var RequirementSalaryTo:String?
    var ResidentAddrDisplay:String?
    var ResidentAddress:String?
    var ResidentDistrictCode:String?
    var ResidentProvinceCode:String?
    var SSN:String?
    var SYLL:String?
    var SchoolName:String?
    var Sex:Int?
    var SkillToArray:[String]?
    var Skills:String?
    var Weight:String?
    var WorkDistrict:String?
    var WorkProvide:String?
    var YearsOfExperience:Int?
    var CVFile:String?
    var EmailTemp:String?
}
struct ISCMajorInput: Encodable {
    var MajorId:Int?
    var MajorName:String?
}
struct ISCMajorGet: Encodable {

}
struct ISCMajorOutput: Codable {
    var Id:Int?
    var MajorName:String?
}
struct ISCSkillInput: Encodable {

}
struct ISCSkillOutput: Codable {
    var SkillId:Int?
    var SkillName:String?
}
struct ISCUpdateAvatarInput: Encodable {
    var ImagePath:String?
}
struct ISCUpdateAvatarOutput: Codable {

}
struct ISCAllJobCategoriesInput: Encodable {
    
}
struct ISCAllJobCategoriesOutput: Codable {
    var JobCategoryId:Int?
    var CategoryName:String?
}
struct InsertJobsFollowInfoInput: Encodable {
    var JobCategoryId:Int?
    var CategoryName:String?
    var ProvinceName:String?
    var ProvinceId:Int?
}
struct InsertJobsFollowInfoOutput: Codable {
    
}
struct ISCJobsFollowInfoInput: Encodable {
    
}
struct ISCJobsFollowInfoOutput: Codable {
    var JobCategoryId:Int?
    var CategoryName:String?
    var ProvinceName:String?
    var ProvinceId:Int?
}

struct ISCJobsFollowDeleteInput: Encodable {
    var JobCategoryId:Int?
    var ProvinceId:Int?
}
struct ISCJobsFollowDeleteOutput: Codable {
    
}
struct ISCSendOTPInput: Encodable {
    var CellPhoneNumber:String?
    var Email:String?
    var FullName:String?
}
struct ISCInterviewCalendarOutput: Codable {
    var CalendarId:Int?
    var ApplicantApplyReId:Int?
    var InterviewerCode:String?
    var InterviewDate:String?
    var InterviewTime:String?
    var TimeGap:Int?
    var TimeLeft:Double?
    var CountDownToInterview:Double?
}
