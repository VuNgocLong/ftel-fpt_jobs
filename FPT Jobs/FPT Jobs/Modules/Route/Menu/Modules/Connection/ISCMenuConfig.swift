//
//  ISCMenuConfig.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/8/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCMenuConfig {
    static let getPrivatePolicy = K.serverAPI.api + "Catalog/GetConfig/PrivatePolicy"
    static let getTermsOfService = K.serverAPI.api + "Catalog/GetConfig/TermsOfService"
    static let getQuestion = K.serverAPI.api + "Catalog/GetConfig/cau-hoi-thuong-gap"
    static let getAllMajors = K.serverAPI.api + "Catalog/GetAllMajors"
    static let getAllSkills = K.serverAPI.api + "Catalog/GetAllSkills"
    static let updateProfile = K.serverAPI.api + "Applicant/UpdateProfile"
    static let upFileAvatar = K.serverAPI.api + "Catalog/UpFileAvatar"
    static let updateAvatar = K.serverAPI.api + "Applicant/UpdateAvatar"
    static let getAllJobCategories = K.serverAPI.api + "Catalog/GetAllJobCategories"
    static let getJobFollowInfo = K.serverAPI.api + "Applicant/GetJobFollowInfo"
    static let deleteJobFollowInfo = K.serverAPI.api + "Applicant/DeleteJobFollowInfo"
    static let sendOTP = K.serverAPI.api + "Applicant/SendOTP"
    static let getInterviewCalendar = K.serverAPI.api + "InterviewOnline/GetInterviewCalendar"
    static let upFileCV = K.serverAPI.api + "Catalog/UpFileCV"
//    static let upFileCV = K.serverAPI.api + "AWSSimpleNotification/DisableEnpoint"
}

