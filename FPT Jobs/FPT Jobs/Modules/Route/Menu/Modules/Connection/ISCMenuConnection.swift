//
//  ISCMenuConnection.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/8/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation

extension ISCMenuLoginViewController {
    func updateAvatar(_ param : ISCUpdateAvatarInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCUpdateAvatarOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.updateAvatar, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func getProfile(
                   _ onSuccess: @escaping (_ response: APIResponse<ISCGetProfileOutput>)->(),
                   _ onFailure: @escaping (_ message: String)->()){
           ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.getProfile, ISCGetProfileInput(), .get, .application, { (value) in
               onSuccess(value)
           }) { (error) in
               onFailure(error)
           }
       }
}

extension ISCMenuConfigViewController {
    func getPrivatePolicy(
                _ onSuccess: @escaping (_ response: APIResponse<ISCMenuOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.getPrivatePolicy,isCheckHeader: false, ISCMenuInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getTermsOfService(
                _ onSuccess: @escaping (_ response: APIResponse<ISCMenuOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.getTermsOfService,isCheckHeader: false, ISCMenuInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getQuestion(
                _ onSuccess: @escaping (_ response: APIResponse<ISCMenuOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.getQuestion,isCheckHeader: false, ISCMenuInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCMenuUserProfileViewController {
    func getProfile(
                _ onSuccess: @escaping (_ response: APIResponse<ISCGetProfileOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.getProfile, ISCGetProfileInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func getProvinces(
                _ onSuccess: @escaping (_ response: APIResponse<ISCProvinceOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCContactConfig.getProvinces,isCheckHeader: false, ISCProvinceInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getDistricts(_ provinceCode : String,
                _ onSuccess: @escaping (_ response: APIResponse<ISCDistrictsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection("\(ISCContactConfig.getDistricts)/\(provinceCode)",isCheckHeader: false, ISCDistrictsInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }

}

extension ISCMenuUpdateProfileViewController {

    func getProvinces(
                _ onSuccess: @escaping (_ response: APIResponse<ISCProvinceOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCContactConfig.getProvinces,isCheckHeader: false, ISCProvinceInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getDistricts(_ provinceCode : String,
                _ onSuccess: @escaping (_ response: APIResponse<ISCDistrictsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){

        ISCAlamofireManager.shared.apiConnection("\(ISCContactConfig.getDistricts)/\(provinceCode)",isCheckHeader: false, ISCDistrictsInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getSchool(
                _ onSuccess: @escaping (_ response: APIResponse<ISCSchoolOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCContactConfig.getSchools,isCheckHeader: false, ISCSchoolInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getAllMajors(
                _ onSuccess: @escaping (_ response: APIResponse<ISCMajorOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.getAllMajors,isCheckHeader: false, ISCMajorGet(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getAllSkills(
                _ onSuccess: @escaping (_ response: APIResponse<ISCSkillOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.getAllSkills,isCheckHeader: false, ISCSkillInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }

    func updateProfile(_ param:ISCUpdateUserProfileInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCLoginOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.updateProfile, param, .post, .json, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }

}
extension ISCMenuJobsApplyViewController {
    func getJobsApply(
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplyOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.getJobsApply, ISCJobsDetailInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func acceptInterView(_ param:ISCAcceptJobsInterviewInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplyOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.acceptInterview, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
   
    func denyInterview(_ param:ISCAcceptJobsInterviewInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplyOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.denyInterview, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func acceptJob(_ param:ISCAcceptJobsInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplyOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.acceptJob, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func delayJob(_ param:ISCAcceptJobsInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplyOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.delayJob, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func denyJob(_ param:ISCAcceptJobsInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplyOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.denyJob, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func checkExamExists(_ param: CheckExamExistsInput,
                _ onSuccess: @escaping (_ response: APIResponse<CheckExamExistsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCTestConfig.checkExamExists, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func getInterviewCalendar(_ applyReId: String,
                _ onSuccess: @escaping (_ response: APIResponse<ISCInterviewCalendarOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection("\(ISCMenuConfig.getInterviewCalendar)?applyReId=\(applyReId)", ISCMenuInput(), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCMenuJobsFollowViewController {
    func getJobFollow(
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsFollowOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.getJobFollow, ISCJobsDetailInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCMenuConfigJobFollowViewController {
    func getProvinces(
                _ onSuccess: @escaping (_ response: APIResponse<ISCProvinceOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCContactConfig.getProvinces, ISCProvinceInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func getAllJobCategories(
                _ onSuccess: @escaping (_ response: APIResponse<ISCAllJobCategoriesOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.getAllJobCategories, ISCAllJobCategoriesInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getJobFollowInfo(
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsFollowInfoOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.getJobFollowInfo, ISCJobsFollowInfoInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func deleteJobFollowInfo(_ param : ISCJobsFollowDeleteInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsFollowDeleteOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.deleteJobFollowInfo, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}

extension ISCProfileOTPViewController {
    func sendOTP(
                _ onSuccess: @escaping (_ response: APIResponse<ISCLoginOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCMenuConfig.sendOTP, ISCSendOTPInput(CellPhoneNumber: CellPhoneNumber, Email: Email, FullName: FullName), .post, .json, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}

extension ISCMenuJobDeplayViewController {
    
       func delayInterview(_ param:ISCDelayJobsInterviewInput,
                   _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplyOutput>)->(),
                   _ onFailure: @escaping (_ message: String)->()){
           
           ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.delayInterview, param, .post, .application, { (value) in
               onSuccess(value)
           }) { (error) in
               onFailure(error)
           }
       }
}
