//
//  ISCMenuUpdateProfileViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/9/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import Alamofire
import Photos
import MobileCoreServices
class ISCMenuUpdateProfileViewController: UIBaseViewController {

    @IBOutlet weak var lblKenhTuyenDung: UIAutoSizeLabel!
    @IBOutlet weak var lblWorkProvide: UIAutoSizeLabel!
    @IBOutlet weak var lblWorkDistrict: UIAutoSizeLabel!
    @IBOutlet weak var lblQualificationType: UIAutoSizeLabel!
    @IBOutlet weak var txtPersonInfomation: UITextView!
    @IBOutlet weak var lblYearsOfExperience: UIAutoSizeLabel!
    @IBOutlet weak var lblDOB: UIAutoSizeLabel!
    @IBOutlet weak var lblGender: UIAutoSizeLabel!
    @IBOutlet weak var lblMarital: UIAutoSizeLabel!
    @IBOutlet weak var lblBirthPlaceProvince: UIAutoSizeLabel!
    @IBOutlet weak var lblBirthPlaceDistrict: UIAutoSizeLabel!
    @IBOutlet weak var lblResidentProvince: UIAutoSizeLabel!
    @IBOutlet weak var lblResidentDistrict: UIAutoSizeLabel!
    @IBOutlet weak var lblMajor: UIAutoSizeLabel!
    @IBOutlet weak var lblCV: UIAutoSizeLabel!
    @IBOutlet weak var txtSalaryFrom: UIAutoSizeTextField!
    @IBOutlet weak var txtSalaryTo: UIAutoSizeTextField!
    @IBOutlet weak var txtFullName: UIAutoSizeTextField!
    @IBOutlet weak var txtPhone: UIAutoSizeTextField!
    @IBOutlet weak var txtEmail: UIAutoSizeTextField!
    @IBOutlet weak var txtHeight: UIAutoSizeTextField!
    @IBOutlet weak var txtWight: UIAutoSizeTextField!
    @IBOutlet weak var txtCMND: UIAutoSizeTextField!
    @IBOutlet weak var txtResidentAddress: UIAutoSizeTextField!
    @IBOutlet weak var viewMajor: UIView!
    @IBOutlet weak var viewSkill: UIView!
    @IBOutlet weak var btnUpdate: UIAutoSizeButton!
    @IBOutlet weak var tblSchool: UITableView!
    @IBOutlet weak var tblSkill: UITableView!
    @IBOutlet weak var heightViewMajorConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightViewSkillConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTableSchoolConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTableSkillConstraint: NSLayoutConstraint!
    @IBOutlet weak var spaceTopTextSkill: NSLayoutConstraint!
    @IBOutlet weak var spaceLeftTextSkill: NSLayoutConstraint!
    @IBOutlet weak var txtSchool: UIAutoSizeTextField!{
       didSet {
           txtSchool.delegate = self
           txtSchool.addTarget(self, action: #selector(txtSchoolDidChanged), for: .editingChanged)
       }
    }
    @IBOutlet weak var txtSkill: UIAutoSizeTextField!{
       didSet {
            txtSkill.returnKeyType = UIReturnKeyType.done
           txtSkill.delegate = self
           txtSkill.addTarget(self, action: #selector(txtSkillDidChanged), for: .editingChanged)
       }
    }
    var arrayMajorSelect : [String] = []
    var arraySkillSelect : [String] = []
    var arrayProvince : [ISCProvinceOutput] = []
    var arraySchool : [ISCSchoolOutput] = []
    var arraySchoolSearch : [ISCSchoolOutput] = []
    var arrayMajor : [ISCMajorOutput] = []
    var arraySkill : [ISCSkillOutput] = []
    var arraySkillSearch : [ISCSkillOutput] = []
    var userModel : ISCUpdateUserProfileInput?
    var WorkProvideCode : String = ""
    var ResidentProvinceName : String = ""
    var ResidentDistrictName : String = ""
    var BirthPlaceProvinceName : String = ""
    var BirthPlaceDistrictName : String = ""
    var paramUpdate : JSON = JSON()
    let loginVM : LoginVM = LoginVM()
    var simpleImagePicker = UIImagePickerController()
    var isChangeSkill : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppLanguage.userProfile
        // Do any additional setup after loading the view.
        self.btnUpdate.applyGradient(colours: [UIColor(hexString: "#14D164"), UIColor(hexString: "#24B24B")])
        
        let cellNib = UINib(nibName: ISCProvinceTableViewCell.name, bundle: Bundle.main)
        tblSchool.register(cellNib, forCellReuseIdentifier: ISCProvinceTableViewCell.name)
        tblSchool.delegate = self
        tblSchool.dataSource = self
        tblSchool.separatorStyle = .none
        tblSchool.showsVerticalScrollIndicator = false
        tblSchool.rowHeight = UITableView.automaticDimension
        tblSchool.estimatedRowHeight = 44
        
        tblSkill.register(cellNib, forCellReuseIdentifier: ISCProvinceTableViewCell.name)
        tblSkill.delegate = self
        tblSkill.dataSource = self
        tblSkill.separatorStyle = .none
        tblSkill.showsVerticalScrollIndicator = false
        tblSkill.rowHeight = UITableView.automaticDimension
        tblSkill.estimatedRowHeight = 44
        
        
        
        loadData()
        loadAllSkills()
        loadSchool()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtSkill {
            if txtSkill.text != "" {
               arraySkillSelect.append(txtSkill.text ?? "")
               userModel?.SkillToArray = arraySkillSelect
               createTagCloudSkill(OnView: self.viewSkill, withArray: arraySkillSelect as [AnyObject])
               var strSkills : String = ""
               for item in userModel?.SkillToArray ?? [] {
                   if strSkills == "" {
                       strSkills = item
                   }else {
                       strSkills += ",\(item)"
                   }
               }
               userModel?.Skills = strSkills
               txtSkill.text = ""
           }
           UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
               self.heightTableSkillConstraint.constant = 0
               self.heightTableSchoolConstraint.constant = 0
               self.view.layoutIfNeeded()
           }, completion: nil)
           isChangeSkill = false
        }
       
    }
    
    override func dismissKeyboard() {
        if !isChangeSkill {
            view.endEditing(true)
        }
    }

    @objc override func backAvatarAction() {
        if let view = self.navigationController?.viewControllers {
            var arrViewControllerMain: [UIViewController] = []
            for n in 0...view.count - 3 {
                arrViewControllerMain.append(view[n])
            }
            
            self.navigationController?.viewControllers = arrViewControllerMain
       }
    }
    
    @IBAction func btnUpdateProfile_click(_ sender: Any) {
        if userModel?.KenhTuyenDung == "" || userModel?.KenhTuyenDung == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullKenhTuyenDung)
            return
        }
        if userModel?.WorkProvide == "" || userModel?.WorkProvide == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullWorkProvice)
            return
        }
        if userModel?.WorkDistrict == "" || userModel?.WorkDistrict == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullWorkProvice)
            return
        }
        if userModel?.QualificationType == "" || userModel?.QualificationType == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullQualificationType)
            return
        }
        if userModel?.SchoolName == "" || userModel?.SchoolName == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullSchoolName)
            return
        }
//        if userModel?.Majors?.count == 0 {
//            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullMajors)
//            return
//        }
        if self.txtPersonInfomation.text == "" {
//            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPersonInfomation)
//            return
        }else {
            userModel?.PersonalInformation = self.txtPersonInfomation.text
        }
        if userModel?.YearsOfExperience == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullYearsOfExperience)
            return
        }
        if userModel?.Skills?.count == 0 {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullSkills)
            return
        }
        if self.txtFullName.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullFullName)
            return
        }else {
            userModel?.FullName = self.txtFullName.text
        }
        if self.txtPhone.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPhone)
            return
        }else {
            userModel?.CellPhoneNumber = self.txtPhone.text
        }
        if self.txtEmail.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullEmail)
            return
        }else {
            if !Common.shared.checkEmail(str: self.txtEmail.text ?? "") {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.emailInvalid)
                return
            }
            userModel?.Email = self.txtEmail.text
        }
        if self.txtCMND.text == "" {
//            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullIDCard)
//            return
        }else {
            userModel?.SSN = self.txtCMND.text
        }
        if self.txtHeight.text == "" {
//            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullHeight)
//            return
        }else {
            userModel?.Height = self.txtHeight.text
        }
        if self.txtWight.text == "" {
//            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullWeight)
//            return
        }else {
            userModel?.Weight = self.txtWight.text
        }
        if userModel?.Sex == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullSex)
            return
        }
        if userModel?.DoB == nil || userModel?.DoB == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullBirthday)
            return
        }
        if self.txtSalaryFrom.text != "" {
//            if let SalaryFrom : Int = Int(self.txtSalaryFrom.text ?? "0") {
//                userModel?.RequirementSalaryFrom = "\(SalaryFrom)"
//            }else {
//                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.salaryInvalid)
//                return
//            }
            userModel?.RequirementSalaryFrom = self.txtSalaryFrom.text
        }
        if self.txtSalaryTo.text != "" {
//            if let SalaryTo : Int = Int(self.txtSalaryTo.text ?? "0") {
//                userModel?.RequirementSalaryTo = "\(SalaryTo)"
//            }else {
//                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.salaryInvalid)
//                return
//            }
            userModel?.RequirementSalaryTo = self.txtSalaryTo.text
        }
        if userModel?.BirthPlaceProvinceCode == "" || userModel?.BirthPlaceProvinceCode == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullBirthDayProvince)
            return
        }
        if userModel?.BirthPlaceDistrictCode == "" || userModel?.BirthPlaceDistrictCode == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullBirthDayDistrict)
            return
        }
        if userModel?.ResidentProvinceCode == "" || userModel?.ResidentProvinceCode == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullResidentProvince)
            return
        }
        if userModel?.ResidentDistrictCode == "" || userModel?.ResidentDistrictCode == nil {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullResidentDistrict)
            return
        }
        if self.txtResidentAddress.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullResidentAddress)
            return
        }
       
        self.showHUD(message: AppLanguage.connecting)
        var jsonMajor : [JSON] = []
        for item in userModel!.Majors! {
            jsonMajor.append(["MajorId":item.MajorId ?? 0,"MajorName":item.MajorName ?? ""])
        }
        var strSkills : String = ""
        for item in userModel?.SkillToArray ?? [] {
            if strSkills == "" {
               strSkills = item
           }else {
               strSkills += ",\(item)"
           }
        }
        userModel?.Skills = strSkills
        paramUpdate["BirthAddress"] = "\(lblResidentDistrict.text ?? "") - \(lblResidentProvince.text ?? "")"
        paramUpdate["BirthPlaceDistrictCode"] = userModel?.BirthPlaceDistrictCode  as Any
        paramUpdate["BirthPlaceProvinceCode"] = userModel?.BirthPlaceProvinceCode as Any
        paramUpdate["CellPhoneNumber"] = userModel?.CellPhoneNumber as Any
        paramUpdate["DistrictCode"] = userModel?.DistrictCode as Any
        paramUpdate["DoB"] = userModel?.DoB as Any
        paramUpdate["DoB_Year"] = userModel?.DoB_Year as Any
        paramUpdate["DoB_Day"] = userModel?.DoB_Day as Any
        paramUpdate["DoB_Month"] = userModel?.DoB_Month as Any
        paramUpdate["Email"] = userModel?.Email as Any
        paramUpdate["EmailConfirmed"] = userModel?.EmailConfirmed as Any
        paramUpdate["EmailTemp"] = userModel?.EmailTemp as Any
        paramUpdate["FullName"] = userModel?.FullName as Any
        paramUpdate["Height"] = userModel?.Height as Any
        paramUpdate["ID"] = userModel?.ID as Any
        paramUpdate["InterviewerCode"] = userModel?.InterviewerCode as Any
        paramUpdate["KenhTuyenDung"] = userModel?.KenhTuyenDung as Any
        paramUpdate["Majors"] = jsonMajor
        paramUpdate["MaritalStatus"] = userModel?.MaritalStatus as Any
        paramUpdate["PersonalInformation"] = userModel?.PersonalInformation as Any
        paramUpdate["PhoneNumberConfirmed"] = userModel?.PhoneNumberConfirmed as Any
        paramUpdate["PhoneTemp"] = userModel?.PhoneTemp as Any
        paramUpdate["PortraitImage"] = userModel?.PortraitImage as Any
        paramUpdate["ProvinceCode"] = userModel?.ProvinceCode as Any
        paramUpdate["QualificationType"] = userModel?.QualificationType as Any
        paramUpdate["RegisteredTimeOnline"] = userModel?.RegisteredTimeOnline as Any
        paramUpdate["RequirementSalaryFrom"] = userModel?.RequirementSalaryFrom as Any
        paramUpdate["RequirementSalaryTo"] = userModel?.RequirementSalaryTo as Any
        paramUpdate["ResidentAddrDisplay"] = "\(txtResidentAddress.text ?? "") - \(lblResidentDistrict.text ?? "") - \(lblResidentProvince.text ?? "")"
        paramUpdate["ResidentAddress"] = txtResidentAddress.text ?? ""
        paramUpdate["ResidentDistrictCode"] = userModel?.ResidentDistrictCode as Any
        paramUpdate["ResidentProvinceCode"] = userModel?.ResidentProvinceCode as Any
        paramUpdate["SSN"] = userModel?.SSN as Any
        paramUpdate["SYLL"] = userModel?.SYLL as Any
        paramUpdate["SchoolName"] = userModel?.SchoolName as Any
        paramUpdate["Sex"] = userModel?.Sex as Any
        paramUpdate["Skills"] = userModel?.Skills as Any
        paramUpdate["SkillToArray"] = userModel?.SkillToArray as Any
        paramUpdate["Weight"] = userModel?.Weight as Any
        paramUpdate["WorkDistrict"] = userModel?.WorkDistrict as Any
        paramUpdate["WorkProvide"] = userModel?.WorkProvide as Any
        paramUpdate["YearsOfExperience"] = userModel?.YearsOfExperience as Any

        loginVM.updateProfileSuccess = { [weak self] in
            self?.hideHUD()
            let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                    message: AppLanguage.updateAccountSuccess,
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                self?.navigationController?.popViewController(animated: true)
            })
            self?.present(alertController, animated: true)
        }
        loginVM.updateProfileEmailSuccess = { [weak self] in
            self?.hideHUD()
            let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                    message: Locale.preferredLanguages[0].contains("vi") ? "Cập nhật thông tin thành công! Một email xác thực đã được gửi đến email mới của bạn, hãy kiểm tra email và xác thực đó là email của bạn":"Updated successfully! A confirmation e mail has been sent to your new email,please check your email for verification link",
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                self?.navigationController?.popViewController(animated: true)
            })
            self?.present(alertController, animated: true)
        }
        loginVM.updateProfileOTPSuccess = { [weak self] in
            self?.hideHUD()
            self?.sendOTP()
        }
        
        loginVM.onShowError = { [weak self] alert in
            self?.hideHUD()
            self?.presentSingleButtonDialog(alert: alert)
        }
        
        loginVM.updateProfile(param: paramUpdate)
    }
    
    @IBAction func btnKenhTuyenDung_click(_ sender: Any) {
        let modal = ISCSelectOptionModal()
        modal.arrayData = Common.shared.arayRecruitment
        modal.titleModal = "Nguồn tuyển dụng"
        modal.delegate = self
        modal.modalPresentationStyle = .fullScreen
        modal.modalTransitionStyle = .coverVertical
        self.present(modal, animated: true, completion: nil)
    }
    @IBAction func btnWorkProvide(_ sender: Any) {
        if arrayProvince.count > 0 {
            let modal = ISCProviceModal()
            modal.arrayProvince = self.arrayProvince
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self.present(modal, animated: true, completion: nil)
        }else {
            loadProvince()
        }
    }
    
    @IBAction func btnWorkDistrict_click(_ sender: Any) {
       loadDistricts()
    }
    @IBAction func btnQualificationType_click(_ sender: Any) {
        let modal = ISCQualificationTypeModal()
        modal.arrayData = Common.shared.arayQualification
        modal.delegate = self
        modal.modalPresentationStyle = .fullScreen
        modal.modalTransitionStyle = .coverVertical
        self.present(modal, animated: true, completion: nil)
    }
    @IBAction func btnSchoolName_click(_ sender: Any) {
        if arraySchool.count > 0 {
            let modal = ISCSchoolModal()
            modal.arrayData = self.arraySchool
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self.present(modal, animated: true, completion: nil)
        }else {
            loadSchool()
        }
    }
    @IBAction func btnMajor_click(_ sender: Any) {
        if arrayMajor.count > 0 {
            let modal = ISCMajorModal()
            modal.arrayData = self.arrayMajor
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self.present(modal, animated: true, completion: nil)
        }else {
            loadAllMajors()
        }
    }
    @IBAction func btnYearsOfExperience_click(_ sender: Any) {
        let modal = ISCYearOfExperienceModal()
        modal.arrayData = Common.shared.arayExperience
        modal.delegate = self
        modal.modalPresentationStyle = .fullScreen
        modal.modalTransitionStyle = .coverVertical
        self.present(modal, animated: true, completion: nil)
    }
    @IBAction func btnSkill_click(_ sender: Any) {
        if arraySkill.count > 0 {
            let modal = ISCSkillModal()
            modal.arrayData = arraySkill
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self.present(modal, animated: true, completion: nil)
        }else {
            loadAllSkills()
        }
    }
    @IBAction func btnDOB_click(_ sender: Any) {
        var strBirth : String = ""
        if userModel?.DoB != nil {
            if userModel?.DoB != "" {
                let dob = userModel?.DoB?.replacingOccurrences(of: "T", with: " ")
                let arrDOB = dob?.components(separatedBy: " ")
                if arrDOB!.count > 0 {
                    strBirth = arrDOB![0]
                }
            }
        }
        if strBirth == "" {
            let date = Date()
           let formatter = DateFormatter()
           formatter.dateFormat = "yyyy-MM-dd"
           strBirth = formatter.string(from: date)
        }
        let modalView = DateBirthDayPopup()
        modalView.strDate = strBirth
        modalView.delegate = self
        modalView.modalPresentationStyle = .fullScreen
        modalView.modalTransitionStyle = .coverVertical
        self.present(modalView, animated: true, completion: nil)
    }
    @IBAction func btnGender_click(_ sender: Any) {
        let modal = ISCGenderModal()
        modal.arrayData = Common.shared.arayGender
        modal.delegate = self
        modal.modalPresentationStyle = .fullScreen
        modal.modalTransitionStyle = .coverVertical
        self.present(modal, animated: true, completion: nil)
    }
    @IBAction func btnMarital_click(_ sender: Any) {
        let modal = ISCMaritalModal()
        modal.arrayData = Common.shared.arayMarital
        modal.delegate = self
        modal.modalPresentationStyle = .fullScreen
        modal.modalTransitionStyle = .coverVertical
        self.present(modal, animated: true, completion: nil)
    }
    @IBAction func btnBirthPlaceProvince_click(_ sender: Any) {
        if arrayProvince.count > 0 {
            let modal = ISCBirthPlaceProviceModal()
            modal.arrayProvince = self.arrayProvince
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self.present(modal, animated: true, completion: nil)
        }else {
            loadBirthPlaceProvince()
        }
    }
    @IBAction func btnBirthPlaceDistrict_click(_ sender: Any) {
        loadBirthPlaceDistricts()
    }
    @IBAction func btnResidentProvince_click(_ sender: Any) {
        if arrayProvince.count > 0 {
            let modal = ISCResidentProvinceModal()
            modal.arrayProvince = self.arrayProvince
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self.present(modal, animated: true, completion: nil)
        }else {
            loadResidentProvince()
        }
    }
    @IBAction func btnResidentDistrict_click(_ sender: Any) {
        loadResidentDistricts()
    }
    
    @IBAction func btnUploadCV_click(_ sender: Any) {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),String(kUTTypePNG),String(kUTTypeJPEG),String(kUTTypeData)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    
    func loadDistricts(){
        var arrayDistricts : [ISCDistrictsOutput] = []
        self.getDistricts(userModel?.ProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",{ (data) in
            self.hideHUD()
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        arrayDistricts.append(value)
                    }
                }
                let modal = ISCDistrictsModal()
                modal.arrayDistricts = arrayDistricts
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadBirthPlaceDistricts(){
        var arrayDistricts : [ISCDistrictsOutput] = []
        self.getDistricts(userModel?.BirthPlaceProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",{ (data) in
            self.hideHUD()
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        arrayDistricts.append(value)
                    }
                }
                let modal = ISCBirthPlaceDistrictsModal()
                modal.arrayDistricts = arrayDistricts
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadResidentDistricts(){
        var arrayDistricts : [ISCDistrictsOutput] = []
        self.getDistricts(userModel?.ResidentProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",{ (data) in
            self.hideHUD()
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        arrayDistricts.append(value)
                    }
                }
                let modal = ISCResidentDistrictsModal()
                modal.arrayDistricts = arrayDistricts
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadProvince(){
        self.showHUD(message: AppLanguage.connecting)
        self.getProvinces({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayProvince.append(value)
                    }
                }
                let modal = ISCProviceModal()
                modal.arrayProvince = self.arrayProvince
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadResidentProvince(){
        self.showHUD(message: AppLanguage.connecting)
        self.getProvinces({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayProvince.append(value)
                    }
                }
                let modal = ISCResidentProvinceModal()
                modal.arrayProvince = self.arrayProvince
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadBirthPlaceProvince(){
        self.showHUD(message: AppLanguage.connecting)
        self.getProvinces({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayProvince.append(value)
                    }
                }
                let modal = ISCBirthPlaceProviceModal()
                modal.arrayProvince = self.arrayProvince
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    
    
    func loadSchool(){
        self.showHUD(message: AppLanguage.connecting)
        self.getSchool({ (data) in
            self.hideHUD()
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        self.arraySchool.append(value)
                    }
                }
                self.arraySchoolSearch = self.arraySchool
//                let modal = ISCSchoolModal()
//                modal.arrayData = self.arraySchool
//                modal.delegate = self
//                modal.modalPresentationStyle = .fullScreen
//                modal.modalTransitionStyle = .coverVertical
//                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadAllMajors(){
        self.showHUD(message: AppLanguage.connecting)
        self.getAllMajors({ (data) in
            self.hideHUD()
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayMajor.append(value)
                    }
                }
                let modal = ISCMajorModal()
                modal.arrayData = self.arrayMajor
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadAllSkills(){
        self.getAllSkills({ (data) in
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        self.arraySkill.append(value)
                    }
                }
                self.arraySkillSearch = self.arraySkill
//                let modal = ISCSkillModal()
//                modal.arrayData = self.arraySkill
//                modal.delegate = self
//                modal.modalPresentationStyle = .fullScreen
//                modal.modalTransitionStyle = .coverVertical
//                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadData(){
        for item in Common.shared.arayRecruitment {
            if Int(userModel?.KenhTuyenDung ?? "0") == item.Key {
                lblKenhTuyenDung.text = item.Value
                break
            }
        }
        if let WorkProvide = userModel?.WorkProvide {
            lblWorkProvide.text = WorkProvide
        }
        if let WorkDistrict = userModel?.WorkDistrict {
            lblWorkDistrict.text = WorkDistrict
        }
        
        for item in Common.shared.arayQualification {
            if Int(userModel?.QualificationType ?? "0") == item.Key {
                lblQualificationType.text = item.Value
                break
            }
        }
        if let SchoolName = userModel?.SchoolName {
            if SchoolName != "" {
                txtSchool.text = SchoolName
            }
        }
        
        for item in userModel?.Majors ?? [] {
            arrayMajorSelect.append(item.MajorName ?? "")
        }
        if arrayMajorSelect.count > 0 {
            lblMajor.text = ""
        }
        createTagCloud(OnView: self.viewMajor, withArray: arrayMajorSelect as [AnyObject])
        txtPersonInfomation.text = userModel?.PersonalInformation
        for item in Common.shared.arayExperience {
            if userModel?.YearsOfExperience == item.Key {
                lblYearsOfExperience.text = item.Value
                break
            }
        }
        for item in userModel?.SkillToArray ?? [] {
            arraySkillSelect.append(item)
        }

        createTagCloudSkill(OnView: viewSkill, withArray: arraySkillSelect as [AnyObject] )
        if let RequirementSalaryFrom = userModel?.RequirementSalaryFrom {
            txtSalaryFrom.text = RequirementSalaryFrom
        }else {
            txtSalaryFrom.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Lương":"Salary"
        }
        if let RequirementSalaryTo = userModel?.RequirementSalaryTo {
            txtSalaryTo.text = RequirementSalaryTo
        }else {
            txtSalaryTo.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Lương":"Salary"
        }
        if let FullName = userModel?.FullName {
            txtFullName.text = FullName
        }else {
            txtFullName.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Họ và tên":"FullName"
        }
        if let Email = userModel?.Email {
            txtEmail.text = Email
        }else {
            txtEmail.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Email":"Email"
        }
        if let CellPhoneNumber = userModel?.CellPhoneNumber {
            txtPhone.text = CellPhoneNumber
        }else {
            txtPhone.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Số điện thoại":"Phone"
        }
        for item in Common.shared.arayGender {
            if userModel?.Sex == item.Key {
                lblGender.text = item.Value
                break
            }
        }
        if let Height = userModel?.Height {
            txtHeight.text = Height
        }else {
            txtHeight.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Chiều cao":"Height"
        }
        if let Weight = userModel?.Weight {
            txtWight.text = Weight
        }else {
            txtWight.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Cân nặng":"Weight"
        }
        if let SSN = userModel?.SSN {
            txtCMND.text = SSN
        }else {
            txtCMND.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Chứng minh nhân dân":"Please enter id card"
        }
        for item in Common.shared.arayMarital {
            if userModel?.MaritalStatus == item.Key {
                lblMarital.text = item.Value
                break
            }
        }
        if let ResidentAddress = userModel?.ResidentAddress {
            txtResidentAddress.text = ResidentAddress
        }else {
            txtResidentAddress.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Địa chỉ":"Address"
        }
        if ResidentProvinceName !=  "" {
            lblResidentProvince.text = ResidentProvinceName
        }
        if ResidentDistrictName !=  "" {
            lblResidentDistrict.text = ResidentDistrictName
        }
        if BirthPlaceProvinceName !=  "" {
            lblBirthPlaceProvince.text = BirthPlaceProvinceName
        }
        if BirthPlaceDistrictName !=  "" {
            lblBirthPlaceDistrict.text = BirthPlaceDistrictName
        }
        lblCV.text = userModel?.CVFile
        
        let dob = userModel?.DoB?.replacingOccurrences(of: "T", with: " ")
        let dateBirth = Common.shared.convertDateTimeFormaterToString(dob ?? "", type: "dd/MM/yyyy")
        if dateBirth != "" {
            lblDOB.text = dateBirth
        }
        lblCV.text = userModel?.SYLL
    }
}
extension ISCMenuUpdateProfileViewController : ISCSelectOptionModalDelegate, ISCProviceModalDelegate, ISCDistrictsModalDelegate, ISCQualificationTypeModalDelegate, ISCSchoolModalDelegate, ISCMajorModalDelegate, ISCYearOfExperienceModalDelegate, ISCSkillModalDelegate, ISCGenderModalDelegate, ISCMaritalModalDelegate, ISCBirthPlaceProviceModalDelegate, ISCBirthPlaceDistrictsModalDelegate, ISCResidentProvinceModalDelegate, ISCResidentDistrictsModalDelegate, DateBirthDayPopupDelegate {
    
    func closeProvinceModal(model: ISCProvinceOutput) {
        lblWorkProvide.text = model.ProvinceName
        userModel?.WorkProvide = model.ProvinceName
        userModel?.ProvinceCode = model.ProvinceCode ?? ""
        lblWorkDistrict.text = Locale.preferredLanguages[0].contains("vi") ? "Chọn quận huyện":"Select district"
        userModel?.WorkDistrict = ""
        userModel?.DistrictCode = ""
    }
    func closeDistrictsModal(model: ISCDistrictsOutput) {
        lblWorkDistrict.text = model.DistrictName
        userModel?.WorkDistrict = model.DistrictName
        userModel?.DistrictCode = model.DistrictCode
    }
    func closeQualificationTypeModal(model: SelectOption) {
        lblQualificationType.text = model.Value
        userModel?.QualificationType = "\(model.Key ?? 0)"
    }
    func closeSelectOptionModal(id: String, name: String) {
        lblKenhTuyenDung.text = name
        userModel?.KenhTuyenDung = id
    }
    func closeSchoolModal(model: ISCSchoolOutput) {
        userModel?.SchoolName = model.SchoolName
    }
    func closeISCMajorModal(model: ISCMajorOutput) {
        if arrayMajorSelect.count >= 3 {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.lengthMajor)
            return
        }
        if arrayMajorSelect.contains(where: { $0 == model.MajorName}){
            return
        }
        lblMajor.text = ""
        arrayMajorSelect.append(model.MajorName ?? "")
        userModel?.Majors?.append(ISCMajorInput(MajorId: model.Id, MajorName: model.MajorName))
        createTagCloud(OnView: self.viewMajor, withArray: arrayMajorSelect as [AnyObject])
    }
    func closeISCYearOfExperienceModal(model: SelectOption) {
        lblYearsOfExperience.text = model.Value
        userModel?.YearsOfExperience = model.Key ?? 0
    }
    func closeISCSkillModal(model: ISCSkillOutput) {
        if arraySkillSelect.count >= 3 {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.lengthSkill)
            return
        }
        if arraySkillSelect.contains(where: { $0 == model.SkillName}){
            return
        }
        arraySkillSelect.append(model.SkillName ?? "")
        userModel?.SkillToArray = arraySkillSelect
        createTagCloudSkill(OnView: self.viewSkill, withArray: arraySkillSelect as [AnyObject])
        var strSkills : String = ""
        for item in userModel?.SkillToArray ?? [] {
            if strSkills == "" {
               strSkills = item
           }else {
               strSkills += ",\(item)"
           }
        }
        userModel?.Skills = strSkills
    }
    func closeISCGenderModal(model: SelectOption) {
        lblGender.text = model.Value
        userModel?.Sex = model.Key
    }
    func closeISCMaritalModal(model: SelectOptionString) {
        lblMarital.text = model.Value
        userModel?.MaritalStatus = model.Key
    }
    
    func closeISCBirthPlaceProviceModal(model: ISCProvinceOutput) {
        lblBirthPlaceProvince.text = model.ProvinceName
        userModel?.BirthPlaceProvinceCode = model.ProvinceCode
        lblBirthPlaceDistrict.text = Locale.preferredLanguages[0].contains("vi") ? "Chọn quận huyện":"Select district"
        userModel?.BirthPlaceDistrictCode = ""
    }
    func closeISCBirthPlaceDistrictsModal(model: ISCDistrictsOutput) {
        lblBirthPlaceDistrict.text = model.DistrictName
        userModel?.BirthPlaceDistrictCode = model.DistrictCode
    }
    func closeISCResidentProvinceModal(model: ISCProvinceOutput) {
        lblResidentProvince.text = model.ProvinceName
        userModel?.ResidentProvinceCode = model.ProvinceCode
        lblResidentDistrict.text = Locale.preferredLanguages[0].contains("vi") ? "Chọn quận huyện":"Select district"
        userModel?.ResidentDistrictCode = ""
        userModel?.BirthAddress = model.ProvinceName
    }
    func closeISCResidentDistrictsModal(model: ISCDistrictsOutput) {
        lblResidentDistrict.text = model.DistrictName
        userModel?.ResidentDistrictCode = model.DistrictCode
        
    }
    func closeDateBirthDayPopup(selectedDate: String) {
        if selectedDate != "" {
            lblDOB.text = Common.shared.convertDateTimeFormaterToString(selectedDate, type: "dd/MM/yyyy")
            userModel?.DoB = selectedDate
        }else {
            print(selectedDate)
        }
        
    }
}
extension ISCMenuUpdateProfileViewController: ISCProfileOTPViewControllerDelegate, UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
          guard let myURL = urls.first else {
               return
          }
        do {
            switch myURL.pathExtension.lowercased() {
            case "pdf", "doc", "docx", "xls", "xlsx" :
                 let fileData = try Data(contentsOf:  myURL, options: .mappedIfSafe)
                 uploadCV(data: fileData, fileName : myURL.lastPathComponent, type: myURL.pathExtension.lowercased())
                break
            default:
                self.alertMessage(title: AppLanguage.commonAppName, message: Locale.preferredLanguages[0].contains("vi") ? "File upload không đúng định dạng(word, excel, pdf)":"File upload is not valid(word, excel, pdf)")
                break
            }

        }catch {
            
        }
    }

    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
                print("view was cancelled")
                dismiss(animated: true, completion: nil)
    }
    
    @objc func removeTagCC(_ sender: AnyObject) {
        arrayMajorSelect.remove(at: (sender.tag - 1))
        userModel?.Majors?.remove(at: (sender.tag - 1))
        createTagCloud(OnView: self.viewMajor, withArray: arrayMajorSelect as [AnyObject])
        if arrayMajorSelect.count > 0 {
            lblMajor.text = ""
        }else {
            lblMajor.text = Locale.preferredLanguages[0].contains("vi") ? "Chọn chuyên ngành":"Select Major"
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
               self.heightViewMajorConstraint.constant = 38
               self.view.layoutIfNeeded()
           }, completion: nil)
        }
    }
    
    @objc func removeTagSkill(_ sender: AnyObject) {
        arraySkillSelect.remove(at: (sender.tag - 1))
        userModel?.SkillToArray?.remove(at: (sender.tag - 1))
        
        createTagCloudSkill(OnView: self.viewSkill, withArray: arraySkillSelect as [AnyObject])
    }
    
    func createTagCloud(OnView viewtag: UIView, withArray data:[AnyObject]) {
        for tempView in viewtag.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 5.0
        //        var ypos: CGFloat = 130.0
        var ypos: CGFloat = 5.0
//        var widthLineTag : CGFloat = 0
        var tag: Int = 1
    
        for str in data  {
            let startstring = str as! String
            var width = startstring.widthOfString(usingFont: UIFont(name:"UTM-Avo", size: 15.0)!)
            
            let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            
            if checkWholeWidth > UIScreen.main.bounds.size.width - 70.0 {
                //we are exceeding size need to change xpos
                if xPos != 5.0 {
                    xPos = 5.0
                    ypos = ypos + 29.0 + 8.0
                }
                
            }
            
            if width > UIScreen.main.bounds.size.width - 120.0 {
                 width = UIScreen.main.bounds.size.width - 120.0
            }
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + 10.0 + 28.5 , height: 29.0))
            bgView.layer.cornerRadius = 14.5
            //            bgView.backgroundColor = UIColor(red: 33.0/255.0, green: 135.0/255.0, blue:199.0/255.0, alpha: 1.0)
            bgView.backgroundColor = UIColor(hexString: "#C8C8C8")
            bgView.tag = tag
            
            let textlable = UILabel(frame: CGRect(x: 10.0, y: 0.0, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "UTM-Avo", size: 15.0)
            textlable.text = startstring
            textlable.textColor = UIColor(hexString: "#333333")
            bgView.addSubview(textlable)
            
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: bgView.frame.size.width - 2.5 - 23.0, y: 3.0, width: 23.0, height: 23.0)
            button.backgroundColor = UIColor(hexString: "#555")
            button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
            button.setImage(UIImage(named: "CrossWithoutCircle")?.withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = UIColor.white
            button.tag = tag
            button.addTarget(self, action: #selector(removeTagCC(_:)), for: .touchUpInside)
            bgView.addSubview(button)
            xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(10.0) + CGFloat(43.0)
            
            viewtag.addSubview(bgView)
            tag = tag  + 1
//            widthLineTag = UIScreen.main.bounds.size.width - xPos - 10
             print(ypos)
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.heightViewMajorConstraint.constant = ypos + 38
                self.view.layoutIfNeeded()
            }, completion: nil)
        }

    }
    
    
    func createTagCloudSkill(OnView viewtag: UIView, withArray data:[AnyObject]) {
        for tempView in viewtag.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 5.0
        //        var ypos: CGFloat = 130.0
        var ypos: CGFloat = 5.0
        var xPosText:CGFloat = 5.0
        var yPosText: CGFloat = 5.0
        var widthLineTag : CGFloat = UIScreen.main.bounds.size.width - 30
        var tag: Int = 1
        for str in data  {
            let startstring = str as! String
            var width = startstring.widthOfString(usingFont: UIFont(name:"UTM-Avo", size: 15.0)!)
            
            let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
//            widthLineTag = checkWholeWidth
            
           if checkWholeWidth > UIScreen.main.bounds.size.width - 70.0 {
               //we are exceeding size need to change xpos
               xPos = 5.0
               ypos = ypos + 29.0 + 8.0
           }
            if width > UIScreen.main.bounds.size.width - 120.0 {
                 width = UIScreen.main.bounds.size.width - 120.0
            }
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + 10.0 + 28.5 , height: 29.0))
            bgView.layer.cornerRadius = 14.5
            //            bgView.backgroundColor = UIColor(red: 33.0/255.0, green: 135.0/255.0, blue:199.0/255.0, alpha: 1.0)
            bgView.backgroundColor = UIColor(hexString: "#C8C8C8")
            bgView.tag = tag
            
            let textlable = UILabel(frame: CGRect(x: 10.0, y: 0.0, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "UTM-Avo", size: 15.0)
            textlable.text = startstring
            textlable.textColor = UIColor(hexString: "#333333")
            bgView.addSubview(textlable)
            
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: bgView.frame.size.width - 2.5 - 23.0, y: 3.0, width: 23.0, height: 23.0)
            button.backgroundColor = UIColor(hexString: "#555")
            button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
            button.setImage(UIImage(named: "CrossWithoutCircle")?.withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = UIColor.white
            button.tag = tag
            button.addTarget(self, action: #selector(removeTagSkill), for: .touchUpInside)
            bgView.addSubview(button)
            xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(10.0) + CGFloat(43.0)
            xPosText = xPos
            viewtag.addSubview(bgView)
            tag = tag  + 1
            widthLineTag = UIScreen.main.bounds.size.width - xPos - 10
             print(ypos)
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
//                self.heightViewSkillConstraint.constant = ypos + 38
//                self.view.layoutIfNeeded()
//            }, completion: nil)
        }
        yPosText = ypos
        if widthLineTag < 60 {
            xPosText = 5.0
            yPosText = ypos + 29.0 + 8.0
            widthLineTag = UIScreen.main.bounds.size.width - 30
        }
        if data.count >= 3 {
            txtSkill.isHidden = true
        }else {
            txtSkill.isHidden = false
            
        }
        spaceLeftTextSkill.constant = xPosText
        spaceTopTextSkill.constant = yPosText

    }
    func sendOTP() {
        loginVM.sendOTPSuccess = { [weak self] in
            self?.hideHUD()
            let modal = ISCProfileOTPViewController()
            modal.CellPhoneNumber = self?.userModel?.CellPhoneNumber ?? ""
            modal.Email = self?.userModel?.Email ?? ""
            modal.FullName = self?.userModel?.FullName ?? ""
            modal.paramUpdate = self?.paramUpdate ?? [:]
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self?.present(modal, animated: true, completion: nil)
        }
        self.showHUD(message: AppLanguage.connecting)
        loginVM.sendOTP(param: ["CellPhoneNumber": userModel?.CellPhoneNumber ?? "","Email": userModel?.Email ?? "","FullName": userModel?.FullName ?? ""])
    }
    
    func closeISCProfileOTPViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func uploadCV(data : Data, fileName : String, type : String){
        self.showHUD(message: AppLanguage.commonAppName)
        let url = (ISCMenuConfig.upFileCV + "?jwt=\(Common.shared.getKongToken())").replacingOccurrences(of: "\"", with: "")
        let parameters: Parameters = [:]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key,value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            multipartFormData.append(data, withName: "file_1", fileName: fileName, mimeType: "application/\(type)")},
             usingThreshold:UInt64.init(),
             to: url,
             method:.post,
             headers: [
                "Authorization" : "Bearer \(UserDefaults.standard.getValue(key: Common.keyAccessToken))",
                "Accept-Language" : Locale.preferredLanguages[0].contains("vi") ? "vi-VN":"en-US"
            ],
             encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        if let dict = response.result.value as? Dictionary<String, AnyObject> {
                            if let access : Bool = dict["Succeeded"] as? Bool {
                                if access {
                                    if let ReturnMessage : String = dict["ReturnMesage"] as? String{
                                        self.lblCV.text = ReturnMessage
                                        self.userModel?.SYLL = ReturnMessage
                                        self.hideHUD()
                                        return
                                    }
                                }
                            }
                        }
                        self.hideHUD()
                        self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.updateAvatarFail)
                    }
                case .failure(_):
                    self.hideHUD()
                    self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.updateAvatarFail)
                    break
                }
        })
    }
    
    
}
class LoginVM : BaseViewModels {

    var updateProfileSuccess: (() -> ())?
    var updateProfileEmailSuccess: (() -> ())?
    var updateProfileOTPSuccess: (() -> ())?
    var sendOTPSuccess: (() -> ())?
    let fptJobsService : FPTJobsService
    init(fptJobsService : FPTJobsService = FPTJobsService()) {
        self.fptJobsService = fptJobsService
    }
    var modelUpdate : JSON? = nil
    func updateProfile(param : JSON) {
        fptJobsService.updateProfile(parameters: param) { [weak self] result in
            switch result {
            case .success(let model):
                if let Succeeded = model["Succeeded"] as? Int {
                    if Succeeded == 0 {
                        if let Errors = model["Errors"] as? [JSON] {
                            if Errors.count > 0 {
                                if let Code = Errors[0]["Code"] as? String {
                                    if Code == "OTP_EMPTY" {
                                        self?.updateProfileOTPSuccess?()
                                        return
                                    }
                                }
                                if let Description = Errors[0]["Description"] as? String {
                                    let okAlert = SingleButtonAlert(
                                           title: AppLanguage.commonAppName,
                                           message: Description,
                                           action: AlertAction(buttonTitle: "Ok", handler: {
                                               
                                           })
                                       )
                                       self?.onShowError?(okAlert)
                                       return
                                }
                            }
                        }
                    }else {
                        if let data = model["Data"] as? JSON {
                            if let EmailConfirmed = data["EmailConfirmed"] as? Int {
                                if EmailConfirmed == 0 {
                                    self?.updateProfileEmailSuccess?()
                                    return
                                }
                            }
                        }
                        self?.updateProfileSuccess?()
                        return
                    }
                }
               
            case .error(let error):
                let okAlert = SingleButtonAlert(
                    title: AppLanguage.commonAppName,
                    message: error?.Error,
                    action: AlertAction(buttonTitle: "Ok", handler: {
                        
                    })
                )
                self?.onShowError?(okAlert)
                break
            }
        }
    }
    
    func sendOTP(param : JSON) {
        fptJobsService.sendOTP(parameters: param) { [weak self] result in
            switch result {
            case .success(let model):
                if let Succeeded = model["Succeeded"] as? Int {
                    if Succeeded == 0 {
                        if let Errors = model["Errors"] as? [JSON] {
                            if Errors.count > 0 {
                                if let Description = Errors[0]["Description"] as? String {
                                    let okAlert = SingleButtonAlert(
                                           title: AppLanguage.commonAppName,
                                           message: Description,
                                           action: AlertAction(buttonTitle: "Ok", handler: {
                                               
                                           })
                                       )
                                       self?.onShowError?(okAlert)
                                       return
                                }
                            }
                        }
                    }else {
                        self?.sendOTPSuccess?()
                        return
                    }
                }
               
            case .error(let error):
                let okAlert = SingleButtonAlert(
                    title: AppLanguage.commonAppName,
                    message: error?.Error,
                    action: AlertAction(buttonTitle: "Ok", handler: {
                        
                    })
                )
                self?.onShowError?(okAlert)
                break
            }
        }
    }
}
// MARK: - UITableView Delegate & Datasource
extension ISCMenuUpdateProfileViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @objc
    func txtSkillDidChanged(textField: UITextField){
        isChangeSkill = true
       self.arraySkillSearch = []
       for item in arraySkill {
           let strText : String = textField.text ?? ""
            let valueItem = item.SkillName?.folding(options: .diacriticInsensitive, locale: .current).lowercased()
           let valueSearch = strText.folding(options: .diacriticInsensitive, locale: .current).lowercased()
           if valueItem?.contains(valueSearch) ?? false {
               self.arraySkillSearch.append(item)
           }
       }
       if arraySkillSearch.count > 0 {
           if self.tblSkill.frame.height == 0 {
               UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                   self.heightTableSkillConstraint.constant = 150
                   self.view.layoutIfNeeded()
               }, completion: nil)
           }
       }else {
           UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
              self.heightTableSkillConstraint.constant = 0
              self.view.layoutIfNeeded()
          }, completion: nil)
       }
       self.tblSkill.reloadData()
    }
        
    @objc
    func txtSchoolDidChanged(textField: UITextField){
        self.arraySchoolSearch = []
        for item in arraySchool {
            let strText : String = textField.text ?? ""
            let valueItem = item.SchoolName?.folding(options: .diacriticInsensitive, locale: .current).lowercased()
            let valueSearch = strText.folding(options: .diacriticInsensitive, locale: .current).lowercased()
            if valueItem?.contains(valueSearch) ?? false {
                self.arraySchoolSearch.append(item)
            }
        }
        if arraySchoolSearch.count > 0 {
            if self.tblSchool.frame.height == 0 {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.heightTableSchoolConstraint.constant = 150
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }else {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
               self.heightTableSchoolConstraint.constant = 0
               self.view.layoutIfNeeded()
           }, completion: nil)
        }
        userModel?.SchoolName = textField.text
        self.tblSchool.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case tblSchool:
            return arraySchoolSearch.count
        default:
            return arraySkillSearch.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case tblSchool:
            guard let cell = tblSchool.dequeueReusableCell(withIdentifier: ISCProvinceTableViewCell.name, for: indexPath) as? ISCProvinceTableViewCell
               else {
                   return UITableViewCell()
           }
           if arraySchoolSearch.count > 0 {
               let obj = arraySchoolSearch[indexPath.row]
               cell.lblProvinceName.text = obj.SchoolName
               cell.selectionStyle = .none
               
           }
           return cell
        default:
            guard let cell = tblSkill.dequeueReusableCell(withIdentifier: ISCProvinceTableViewCell.name, for: indexPath) as? ISCProvinceTableViewCell
                else {
                    return UITableViewCell()
            }
            if arraySkillSearch.count > 0 {
                let obj = arraySkillSearch[indexPath.row]
                cell.lblProvinceName.text = obj.SkillName
                cell.selectionStyle = .none
                
            }
            return cell
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case tblSchool:
            let obj = arraySchoolSearch[indexPath.row]
            userModel?.SchoolName = obj.SchoolName
            txtSchool.text = obj.SchoolName
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.heightTableSchoolConstraint.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            break
        default :
            txtSkill.text = ""
            let obj = arraySkillSearch[indexPath.row]
            arraySkillSelect.append(obj.SkillName ?? "")
            userModel?.SkillToArray = arraySkillSelect
            createTagCloudSkill(OnView: self.viewSkill, withArray: arraySkillSelect as [AnyObject])
            var strSkills : String = ""
            for item in userModel?.SkillToArray ?? [] {
                if strSkills == "" {
                    strSkills = item
                }else {
                    strSkills += ",\(item)"
                }
            }
            userModel?.Skills = strSkills
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.heightTableSkillConstraint.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            break
        }
        view.endEditing(true)
    }
}
