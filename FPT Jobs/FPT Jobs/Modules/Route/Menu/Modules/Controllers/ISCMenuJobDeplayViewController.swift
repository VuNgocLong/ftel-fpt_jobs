//
//  ISCMenuJobDeplayViewController.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/20/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCMenuJobDeplayViewController: UIBaseViewController, UITextViewDelegate {

    @IBOutlet weak var tvDescription: UITextView!
    @IBOutlet weak var btnConfirm: UIAutoSizeButton!
    @IBOutlet weak var lbl15: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lbl45: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lblday: UIAutoSizeLabelNomarl!
    @IBOutlet weak var imgRadio15: UIImageView!
    @IBOutlet weak var imgRadio45: UIImageView!
    @IBOutlet weak var imgRadioDay: UIImageView!
    @IBOutlet weak var lblTitle: UIAutoSizeLabel!
    
    var obj : ISCJobsApplyOutput? = nil
    var Status : Int = 0
    var Reason : String = ""
    var FkSurveyDataId : Int = 0
    override func viewDidLoad() {
        
        self.title = AppLanguage.jobsApplyList
        super.viewDidLoad()

        tvDescription.text = Locale.preferredLanguages[0].contains("vi") ? "Ghi chú ngày lùi phỏng vấn":"Note the interview back date"
        tvDescription.textColor = UIColor.lightGray
        tvDescription.delegate = self
        
        // Do any additional setup after loading the view.
        self.btnConfirm.applyGradient(colours: [UIColor(hexString: "#14D164"), UIColor(hexString: "#24B24B")])
        self.btnConfirm.setTitle(Locale.preferredLanguages[0].contains("vi") ? "XÁC NHẬN":"CONFIRM", for: .normal)
        self.lblTitle.text = Locale.preferredLanguages[0].contains("vi") ? "XIN ĐỔI LỊCH PHỎNG VẤN":"CHANGE INTERVIEW SCHEDULE"
        self.lbl15.text = Locale.preferredLanguages[0].contains("vi") ? "Lùi lịch phỏng vấn 15 phút":"Delay 15 minutes"
        self.lbl45.text = Locale.preferredLanguages[0].contains("vi") ? "Lùi lịch phỏng vấn 45 phút":"Delay 45 minutes"
        self.lblday.text = Locale.preferredLanguages[0].contains("vi") ? "Lùi lịch phỏng vấn sang ngày khác":"Move to other day"
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if tvDescription.textColor == UIColor.lightGray {
            tvDescription.text = nil
            tvDescription.textColor = UIColor(hexString: "#3E3E3E")
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if tvDescription.text.isEmpty {
            tvDescription.text = Locale.preferredLanguages[0].contains("vi") ? "Ghi chú ngày lùi phỏng vấn":"Note the interview back date"
            tvDescription.textColor = UIColor.lightGray
        }
    }
    @IBAction func btnDelay15_click(_ sender: Any) {
        clearCheck()
        Status = 1
        imgRadio15.image = UIImage(named: "ic_radio_on")
    }
    @IBAction func btnDelay45_click(_ sender: Any) {
        clearCheck()
        Status = 2
        imgRadio45.image = UIImage(named: "ic_radio_on")
    }
    @IBAction func btnDelayDay_click(_ sender: Any) {
        clearCheck()
        Status = 3
        imgRadioDay.image = UIImage(named: "ic_radio_on")
    }
    
    @IBAction func btnConfirm_click(_ sender: Any) {
        delayJobsInterview()
    }
    
    func clearCheck(){
        imgRadio15.image = UIImage(named: "ic_un_radio")
        imgRadio45.image = UIImage(named: "ic_un_radio")
        imgRadioDay.image = UIImage(named: "ic_un_radio")
    }
    
    func delayJobsInterview() {
        self.showHUD(message: AppLanguage.connecting)
        switch Status {
        case 1:
            Reason = "\(lbl15.text ?? ""), \(tvDescription?.text ?? "")"
            break
        case 2:
            Reason = "\(lbl45.text ?? ""), \(tvDescription?.text ?? "")"
            break
        case 3:
            Reason = "\(lblday.text ?? ""), \(tvDescription?.text ?? "")"
            break
        default:
            break
        }
        self.delayInterview(ISCDelayJobsInterviewInput(CalendarId: obj?.CalendarId, Status: 0, Reason: Reason, Type: obj?.Type, FkSurveyDataId: obj?.FkSurveyDataId), { (data) in
            self.hideHUD()
            if data.ErrCode == 0 {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.delayJobsInterviewSucces)
                
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.delayJobsInterviewFail)
            }
        }) { (error) in
            self.hideHUD()
             if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
}
