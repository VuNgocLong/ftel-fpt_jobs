//
//  ISCMenuUserProfileViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/8/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCMenuUserProfileViewController: UIBaseViewController {

    @IBOutlet weak var lblRecruimentSource: UIAutoSizeLabel!
    @IBOutlet weak var lblWorkProvide: UIAutoSizeLabel!
    @IBOutlet weak var lblWorkDistrict: UIAutoSizeLabel!
    @IBOutlet weak var lblSchoolName: UIAutoSizeLabel!
    @IBOutlet weak var lblMajor: UIAutoSizeLabel!
    @IBOutlet weak var lblCV: UIAutoSizeLabel!
    @IBOutlet weak var lblYearExperince: UIAutoSizeLabel!
    @IBOutlet weak var lblPersonInfomation: UIAutoSizeLabel!
    @IBOutlet weak var lblSkills: UIAutoSizeLabel!
    @IBOutlet weak var lblSalary: UIAutoSizeLabel!
    @IBOutlet weak var lblFullName: UIAutoSizeLabel!
    @IBOutlet weak var lblPhone: UIAutoSizeLabel!
    @IBOutlet weak var lblEmail: UIAutoSizeLabel!
    @IBOutlet weak var lblDOB: UIAutoSizeLabel!
    @IBOutlet weak var lblGender: UIAutoSizeLabel!
    @IBOutlet weak var lblHeighWeight: UIAutoSizeLabel!
    @IBOutlet weak var lblCMND: UIAutoSizeLabel!
    @IBOutlet weak var lblMaritalStatus: UIAutoSizeLabel!
    @IBOutlet weak var lblAddress: UIAutoSizeLabel!
    @IBOutlet weak var lblResident: UIAutoSizeLabel!
    @IBOutlet weak var lblQualityType: UIAutoSizeLabel!
    var ResidentProvinceName : String = ""
    var ResidentDistrictName : String = ""
    var BirthPlaceProvinceName : String = ""
    var BirthPlaceDistrictName : String = ""
    var arrayProvince : [ISCProvinceOutput] = []
    var arrayDistricts : [ISCDistrictsOutput] = []
    var userModel : ISCUpdateUserProfileInput?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = AppLanguage.userProfile
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        loadProfile()
    }
    
    @objc override func backAvatarAction() {
        self.navigationController?.popViewController(animated: true)
    }

    func loadProfile(){
        self.showHUD(message: AppLanguage.connecting)
        self.getProfile({ (value) in
            self.hideHUD()
                if let data = value.Data {
                    let dict = data.asDictionary
                    UserDefaults.standard.set(dictionary: dict, forKey: Common.keyUserProfile)
                    self.loadData()
                }else {
                    let alert = UIAlertController(title: AppLanguage.commonAppName, message: AppLanguage.errorNetworkInCorrectTypeResponse, preferredStyle: UIAlertController.Style.alert)
                    self.present(alert, animated: true) {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
            }
            }) { (error) in
                self.hideHUD()
                if error == AppLanguage.unauthorizedMessage {
                    let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                            message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                            preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        _ = appDelegate.grabLogin()
                    })
                    self.present(alertController, animated: true)
                    return
                }else {
                    let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
                    self.present(alert, animated: true) {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
        }
    }
    
    func loadData(){
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            userModel = ISCUpdateUserProfileInput()
            if let ID = saved!["ID"] as? Int {
                userModel?.ID = ID
            }
            if let InterviewerCode = saved!["InterviewerCode"] as? String {
                userModel?.InterviewerCode = InterviewerCode
            }
            if let EmailConfirmed = saved!["EmailConfirmed"] as? Int {
                userModel?.EmailConfirmed = EmailConfirmed
            }
            if let EmailTemp = saved!["EmailTemp"] as? String {
                userModel?.EmailTemp = EmailTemp
            }
            if let DoB_Year = saved!["DoB_Year"] as? Int {
                userModel?.DoB_Year = DoB_Year
            }
            if let DoB_Month = saved!["DoB_Month"] as? Int {
                userModel?.DoB_Month = DoB_Month
            }
            if let DoB_Day = saved!["DoB_Day"] as? Int {
                userModel?.DoB_Day = DoB_Day
            }
            if let ProvinceCode = saved!["ProvinceCode"] as? String {
                userModel?.ProvinceCode = ProvinceCode
            }
            if let DistrictCode = saved!["DistrictCode"] as? String {
                userModel?.DistrictCode = DistrictCode
            }
            if let ResidentProvinceCode = saved!["ResidentProvinceCode"] as? String {
                userModel?.ResidentProvinceCode = ResidentProvinceCode
            }
            if let ResidentDistrictCode = saved!["ResidentDistrictCode"] as? String {
                userModel?.ResidentDistrictCode = ResidentDistrictCode
            }
            if let ResidentAddrDisplay = saved!["ResidentAddrDisplay"] as? String {
                userModel?.ResidentAddrDisplay = ResidentAddrDisplay
            }
            if let RegisteredTimeOnline = saved!["RegisteredTimeOnline"] as? String {
                userModel?.RegisteredTimeOnline = RegisteredTimeOnline
            }
            if let BirthPlaceProvinceCode = saved!["BirthPlaceProvinceCode"] as? String {
                userModel?.BirthPlaceProvinceCode = BirthPlaceProvinceCode
            }
            if let BirthPlaceDistrictCode = saved!["BirthPlaceDistrictCode"] as? String {
                userModel?.BirthPlaceDistrictCode = BirthPlaceDistrictCode
            }
            if let PortraitImage = saved!["PortraitImage"] as? String {
                userModel?.PortraitImage = PortraitImage
            }
            if let PhoneTemp = saved!["PhoneTemp"] as? String {
                userModel?.PhoneTemp = PhoneTemp
            }
            if let PhoneNumberConfirmed = saved!["PhoneNumberConfirmed"] as? String {
                userModel?.PhoneNumberConfirmed = PhoneNumberConfirmed
            }
            if let KenhTuyenDung = saved!["KenhTuyenDung"] as? String {
                for item in Common.shared.arayRecruitment {
                    if Int(KenhTuyenDung) == item.Key {
                        lblRecruimentSource.text = item.Value
                        userModel?.KenhTuyenDung = KenhTuyenDung
                        break
                    }
                }
            }
            if let WorkProvide = saved!["WorkProvide"] as? String {
                lblWorkProvide.text = WorkProvide
                userModel?.WorkProvide = WorkProvide
            }
            if let WorkDistrict = saved!["WorkDistrict"] as? String {
                lblWorkDistrict.text = WorkDistrict
                userModel?.WorkDistrict = WorkDistrict
            }
            if let SchoolName = saved!["SchoolName"] as? String {
                lblSchoolName.text = SchoolName
                userModel?.SchoolName = SchoolName
            }
            if let Majors = saved!["Majors"] as? [[String: Any]] {
                var strMajors = ""
                userModel?.Majors = []
                for item in Majors {
                    if strMajors == "" {
                         strMajors += "- \(item["MajorName"] ?? "")"
                    }else {
                        strMajors += "<br>- \(item["MajorName"] ?? "")"
                    }
//                    strMajors += "<span style=\"font-family: Helvetica Neue; font-size: 15; color: #1569B2\">- \(item["MajorName"] ?? "")</span><br>"
                    userModel?.Majors?.append(ISCMajorInput(MajorId: item["MajorId"] as? Int, MajorName: item["MajorName"] as? String))
                }
//                lblMajor.attributedText = strMajors.htmlToAttributedString
                lblMajor.text = strMajors.htmlToString
            }
            if let SYLL = saved!["SYLL"] as? String {
                if SYLL != "" {
                    lblCV.text = SYLL
                    userModel?.SYLL = SYLL
                }else {
                    lblCV.text = "Bạn chưa có file CV"
                    userModel?.SYLL = ""
                }
               
            }
            if let YearsOfExperience = saved!["YearsOfExperience"] as? String {
                for item in Common.shared.arayExperience {
                    if Int(YearsOfExperience) == item.Key {
                        lblYearExperince.text = item.Value
                        userModel?.YearsOfExperience = Int(YearsOfExperience)
                        break
                    }
                }
            }
            if let PersonalInformation = saved!["PersonalInformation"] as? String {
                lblPersonInfomation.text = PersonalInformation
                userModel?.PersonalInformation = PersonalInformation
            }
            if let SkillToArray = saved!["SkillToArray"] as? [String] {
                var strSkill = ""
                var strSkills = ""
                userModel?.SkillToArray = []
                for item in SkillToArray {
//                    strSkill += "<span style=\"font-family: Roboto-Regular; font-size: 14; color: #1b51a0\">\(item)</span><br>"
                   
                    userModel?.SkillToArray?.append(item)
                    if strSkills == "" {
                        strSkills = item
                        strSkill += "- \(item)"
                    }else {
                        strSkills += ",\(item)"
                        strSkill += "<br>- \(item)"
                    }
                }
//                lblSkills.attributedText = strSkill.htmlToAttributedString
                lblSkills.text = strSkill.htmlToString
                userModel?.Skills = strSkills
            }
            
            if let RequirementSalaryFrom = saved!["RequirementSalaryFrom"] as? String, let RequirementSalaryTo = saved!["RequirementSalaryTo"] as? String {
                if RequirementSalaryFrom != "" && RequirementSalaryTo != "" {
//                    let min = Int(RequirementSalaryFrom)
//                    let max = Int(RequirementSalaryTo)
//                    let nf = NumberFormatter()
//                    nf.numberStyle = .decimal
//                    lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0")  VND"
                    lblSalary.text = "\(RequirementSalaryFrom) - \(RequirementSalaryTo)  VND"
                }else {
                    lblSalary.text = "Thoả thuận"
                }
                userModel?.RequirementSalaryFrom = RequirementSalaryFrom
                userModel?.RequirementSalaryTo = RequirementSalaryTo
               
            }
            if let FullName = saved!["FullName"] as? String {
                lblFullName.text = FullName
                userModel?.FullName = FullName
            }
            if let CellPhoneNumber = saved!["CellPhoneNumber"] as? String {
                lblPhone.text = CellPhoneNumber
                userModel?.CellPhoneNumber = CellPhoneNumber
            }
            if let Email = saved!["Email"] as? String {
                if let EmailTemp = userModel?.EmailTemp {
                    if EmailTemp != "" {
                        if userModel?.EmailConfirmed == 0 {
                            lblEmail.text = "\(EmailTemp) \(Locale.preferredLanguages[0].contains("vi") ? "Chưa xác thực":"Not authenticated"))"
                        }
                    }else {
                        lblEmail.text = Email
                    }
                }else {
                    lblEmail.text = Email
                }
                userModel?.Email = Email
            }
            if let DoB = saved!["DoB"] as? String {
                let dob = DoB.replacingOccurrences(of: "T", with: " ")
                lblDOB.text = Common.shared.convertDateTimeFormaterToString(dob, type: "dd/MM/yyyy")
                userModel?.DoB = DoB
            }
            if let Sex = saved!["Sex"] as? Int {
                for item in Common.shared.arayGender {
                    if Sex == item.Key {
                        lblGender.text = item.Value
                        userModel?.Sex = Sex
                        break
                    }
                }
            }
            if let Height = saved!["Height"] as? String, let Weight = saved!["Weight"] as? String {
                lblHeighWeight.text = "\(Height)cm/\(Weight)kg"
                userModel?.Height = Height
                userModel?.Weight = Weight
            }
            if let SSN = saved!["SSN"] as? String {
                lblCMND.text = SSN
                userModel?.SSN = SSN
            }
            lblMaritalStatus.text = "..."
            if let MaritalStatus = saved!["MaritalStatus"] as? String {
                for item in Common.shared.arayMarital {
                   if MaritalStatus == item.Key {
                        lblMaritalStatus.text = item.Value
                        userModel?.MaritalStatus = MaritalStatus
                        break
                   }
               }
            }
            if let ResidentAddress = saved!["ResidentAddress"] as? String {
                lblAddress.text = ResidentAddress
                userModel?.ResidentAddress = ResidentAddress
            }
            if let BirthAddress = saved!["BirthAddress"] as? String {
                userModel?.BirthAddress = BirthAddress
            }
            if let QualificationType = saved!["QualificationType"] as? String {
                for item in Common.shared.arayQualification {
                    if Int(QualificationType ) == item.Key {
                        lblQualityType.text = item.Value
                        userModel?.QualificationType = QualificationType
                        break
                    }
                }
                
            }
        }
        loadProvince()
        loadDistricts()
        loadDistrictsBirthPlace()
    }
    
    func loadProvince(){
        self.lblResident.text = ""
        arrayProvince = []
        self.getProvinces({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayProvince.append(value)
                        if self.userModel?.ResidentProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) == value.ProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) {
                            if self.lblResident.text != "" {
                                self.lblResident.text! += "\(value.ProvinceName ?? "") - \(self.lblResident.text ?? "")"
                            }else {
                                self.lblResident.text = value.ProvinceName
                            }
                            self.ResidentProvinceName = value.ProvinceName ?? ""
                        }
                        if self.userModel?.BirthPlaceProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) == value.ProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) {
                            self.BirthPlaceProvinceName = value.ProvinceName ?? ""
                        }
                    }
                }
            }
        }) { (error) in
            self.hideHUD()
            let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func loadDistricts(){
        arrayDistricts = []
        var code : String = "0"
        if userModel?.ResidentProvinceCode != nil {
            code = userModel?.ResidentProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        }
        self.getDistricts(code,{ (data) in
            self.hideHUD()
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayDistricts.append(value)
                        if self.userModel?.ResidentDistrictCode?.trimmingCharacters(in: .whitespacesAndNewlines) == value.DistrictCode?.trimmingCharacters(in: .whitespacesAndNewlines) {
                            if self.lblResident.text != "" {
                                self.lblResident.text! += " - \(value.DistrictName ?? "")"
                            }else {
                                self.lblResident.text = value.DistrictName
                            }
                            self.ResidentDistrictName = value.DistrictName ?? ""
                            return
                        }
                    }
                }
            }
        }) { (error) in
            self.hideHUD()
            let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func loadDistrictsBirthPlace(){
        var code : String = "0"
        if userModel?.BirthPlaceProvinceCode != nil {
            code = userModel?.BirthPlaceProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        }
        self.getDistricts(code,{ (data) in
            self.hideHUD()
            if let array = data.otherTypeList {
                array.forEach { (val) in
                    if let value = val {
                        if self.userModel?.BirthPlaceDistrictCode?.trimmingCharacters(in: .whitespacesAndNewlines) == value.DistrictCode?.trimmingCharacters(in: .whitespacesAndNewlines) {
                            self.BirthPlaceDistrictName = value.DistrictName ?? ""
                            return
                        }
                    }
                }
            }
        }) { (error) in
            self.hideHUD()
            let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func btnUpdateProfile_click(_ sender: Any) {
        if userModel != nil
        {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuUpdateProfileViewController") as! ISCMenuUpdateProfileViewController
            vc.userModel = userModel
            vc.ResidentProvinceName = ResidentProvinceName
            vc.ResidentDistrictName = ResidentDistrictName
            vc.BirthPlaceProvinceName = BirthPlaceProvinceName
            vc.BirthPlaceDistrictName = BirthPlaceDistrictName
            self.pushViewControler(vc, AppLanguage.questionOfService)
        }else {
           
            let alert = UIAlertController(title: AppLanguage.commonAppName, message: AppLanguage.errorNetworkInCorrectTypeResponse, preferredStyle: UIAlertController.Style.alert)
            present(alert, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
    }
}
