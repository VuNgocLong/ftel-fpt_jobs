//
//  ISCMenuConfigJobFollowViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/18/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import Alamofire

class ISCMenuConfigJobFollowViewController: UIBaseViewController, ISCProviceModalDelegate, ISCJobsCategoryModalDelegate {

    @IBOutlet weak var btnFollow: UIAutoSizeButton!
    @IBOutlet weak var lblJons: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lblProvince: UIAutoSizeLabelNomarl!
    @IBOutlet weak var imgDownJob: UIImageView!
    @IBOutlet weak var imgDownProvince: UIImageView!
    @IBOutlet weak var tblList: UITableView!
    var arrayProvince : [ISCProvinceOutput] = []
    var objProvince : ISCProvinceOutput? = nil
    var arrayJobCategories : [ISCAllJobCategoriesOutput] = []
    var objJobCategories : ISCAllJobCategoriesOutput? = nil
    var arrayJobInfo : [ISCJobsFollowInfoOutput] = []
    var arrayJobInfoSelect : [ISCJobsFollowInfoOutput] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppLanguage.jobsFollowList
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        // Do any additional setup after loading the view.
        
        imgDownJob.image = UIImage(named: "ic_arrow_down")?.withColor(UIColor(hexString: "#000000"))
        imgDownProvince.image = UIImage(named: "ic_arrow_down")?.withColor(UIColor(hexString: "#000000"))
        
        let cellNib = UINib(nibName: ISCJobsFollowConfigTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCJobsFollowConfigTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 44
        loadJobFollowInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        btnFollow.setTitle(AppLanguage.btnJobFollow, for: .normal)
    }
    
    @objc override func backAvatarAction() {
        if let view = self.navigationController?.viewControllers {
               var arrViewControllerMain: [UIViewController] = []
               for n in 0...view.count - 3 {
                   arrViewControllerMain.append(view[n])
               }
               
               self.navigationController?.viewControllers = arrViewControllerMain
          }
    }
    
    @IBAction func btnFollow_click(_ sender: Any) {
        if objProvince != nil && objJobCategories != nil {
            if arrayJobInfo.contains(where: { ($0.JobCategoryId == objJobCategories?.JobCategoryId && $0.ProvinceId == objProvince?.Id)}) {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.jobFollowConfigExist)
                return
            }
            arrayJobInfo.append(ISCJobsFollowInfoOutput(JobCategoryId: objJobCategories?.JobCategoryId, CategoryName: objJobCategories?.CategoryName, ProvinceName: objProvince?.ProvinceName, ProvinceId: objProvince?.Id))
            arrayJobInfoSelect.append(ISCJobsFollowInfoOutput(JobCategoryId: objJobCategories?.JobCategoryId, CategoryName: objJobCategories?.CategoryName, ProvinceName: objProvince?.ProvinceName, ProvinceId: objProvince?.Id))
            
        }
        if arrayJobInfoSelect.count > 0 {
            confirmFollowJob()
        }
        
//        if arrayJobInfoSelect.count > 0 {
//            self.btnConfirm.applyGradient(colours: [UIColor(hexString: "#14D164"), UIColor(hexString: "#24B24B")])
//            self.btnConfirm.isEnabled = true
//        }else {
//            self.btnConfirm.backgroundColor = UIColor.lightGray
//            self.btnConfirm.isEnabled = false
//        }
    }
    @IBAction func btnConfirm_click(_ sender: Any) {
        confirmFollowJob()
    }
    func loadJobFollowInfo(){
        arrayJobInfo = []
        arrayJobInfoSelect = []
        self.showHUD(message: AppLanguage.connecting)
        self.getJobFollowInfo({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayJobInfo.append(value)
                    }
                }
                self.tblList.reloadData()
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    @IBAction func btnJobs_click(_ sender: Any) {
        if arrayProvince.count > 0 {
            let modal = ISCJobsCategoryModal()
            modal.arrayCategories = self.arrayJobCategories
            modal.delegate = self
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            self.present(modal, animated: true, completion: nil)
        }else {
            loadAllJobCategories()
        }
    }
    
    @IBAction func btnProvince_click(_ sender: Any) {
        if arrayProvince.count > 0 {
           let modal = ISCProviceModal()
           modal.arrayProvince = self.arrayProvince
           modal.delegate = self
           modal.modalPresentationStyle = .fullScreen
           modal.modalTransitionStyle = .coverVertical
           self.present(modal, animated: true, completion: nil)
       }else {
           loadProvince()
       }
    }
    
    func loadAllJobCategories(){
        self.showHUD(message: AppLanguage.connecting)
        self.getAllJobCategories({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayJobCategories.append(value)
                    }
                }
                let modal = ISCJobsCategoryModal()
                modal.arrayCategories = self.arrayJobCategories
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func closeISCJobsCategoryModal(model: ISCAllJobCategoriesOutput) {
        lblJons.text = model.CategoryName
        objJobCategories = model
    }
    
    
    func loadProvince(){
        self.showHUD(message: AppLanguage.connecting)
        self.getProvinces({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayProvince.append(value)
                    }
                }
                let modal = ISCProviceModal()
                modal.arrayProvince = self.arrayProvince
                modal.delegate = self
                modal.modalPresentationStyle = .fullScreen
                modal.modalTransitionStyle = .coverVertical
                self.present(modal, animated: true, completion: nil)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func closeProvinceModal(model: ISCProvinceOutput) {
        lblProvince.text = model.ProvinceName
        objProvince = model
    }

}
// MARK: - UITableView Delegate & Datasource
extension ISCMenuConfigJobFollowViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayJobInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCJobsFollowConfigTableViewCell.name, for: indexPath) as? ISCJobsFollowConfigTableViewCell
            else {
                return UITableViewCell()
        }
        let obj = arrayJobInfo[indexPath.row]
        cell.lblProvince.text = obj.ProvinceName
        cell.lblJobCate.text = obj.CategoryName
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(self.deleteJobsFollow(_:)), for: .touchUpInside)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func deleteJobsFollow(_ sender:UIButton) {
        let obj = arrayJobInfo[sender.tag]
        if let index = arrayJobInfoSelect.firstIndex(where: { $0.JobCategoryId == obj.JobCategoryId && $0.ProvinceId == obj.ProvinceId }) {
            arrayJobInfoSelect.remove(at: index)
        }else {
            deleteFollowJobs(obj: ISCJobsFollowDeleteInput(JobCategoryId: obj.JobCategoryId, ProvinceId: obj.ProvinceId))
        }
        arrayJobInfo.remove(at: sender.tag)
       
        tblList.reloadData()
    }
    
    func deleteFollowJobs(obj : ISCJobsFollowDeleteInput){
        self.showHUD(message: AppLanguage.connecting)
        self.deleteJobFollowInfo(obj, { (data) in
            self.hideHUD()
            if data.ErrCode == 0 {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.deleteJobFollowSucces)
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    func confirmFollowJob(){
        self.showHUD(message: AppLanguage.connecting)
        let strUrl = K.serverAPI.api + "Applicant/InsertJobsFollowInfo?jwt=\(Common.shared.getKongToken())"
        var req = URLRequest(url: URL(string: strUrl.replacingOccurrences(of: "\"", with: ""))!)
        req.httpMethod = "POST"
        req.setValue("application/json", forHTTPHeaderField: "Content-Type")
        req.setValue(String(format: "Bearer %@", UserDefaults.standard.getValue(key: Common.keyAccessToken)),forHTTPHeaderField: "Authorization")
        var paramBody : [JSON] = []
        for item in arrayJobInfoSelect {
            paramBody.append([
                "JobCategoryId":item.JobCategoryId as Any,
                "ProvinceId":item.ProvinceId as Any,
            ])
        }
        req.httpBody = try! JSONSerialization.data(withJSONObject: paramBody)
        Alamofire.request(req)
            .responseJSON { res in
                switch res.result {
                case .failure(_):
                    self.hideHUD()
                    if let data = res.data, let resString = String(data: data, encoding: .utf8) {
                        self.alertMessage(title: AppLanguage.commonAppName, message: resString)
                    }
                    
                case .success(_):
                    self.hideHUD()
                    self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.confirmJobFollowSucces)
                    self.loadJobFollowInfo()
                }
        }
    }
}
