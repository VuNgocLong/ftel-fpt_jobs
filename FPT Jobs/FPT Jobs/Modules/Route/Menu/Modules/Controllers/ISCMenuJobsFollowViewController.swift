//
//  ISCMenuJobsFollowViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/18/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCMenuJobsFollowViewController: UIBaseViewController {

    @IBOutlet weak var btnFollowJob: UIAutoSizeButton!
    @IBOutlet weak var tblList: UITableView!
    var arrData : [ISCJobsFollowOutput] = []
    var isLoadMore : Bool = false
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = AppLanguage.jobsFollowList
        // Do any additional setup after loading the view.
        let cellNib = UINib(nibName: ISCJobsFollowTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCJobsFollowTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 90
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        btnFollowJob.setTitle(AppLanguage.btnEditobFollow, for: .normal)
        loadJobFollow()
    }
    
    @objc override func backAvatarAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadJobFollow(){
        self.showHUD(message: AppLanguage.connecting)
        self.arrData = []
        self.getJobFollow({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrData.append(value)
                    }
                }
            }
            if data.DataList?.count ?? 0 > 0 {
                if data.DataList?.count ?? 0 < 10 {
                    self.isLoadMore = false
                }else {
                    self.isLoadMore = true
                }
            }else {
                self.isLoadMore = false
            }
            self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
               let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                       message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                       preferredStyle: .alert)
               alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                   let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                   _ = appDelegate.grabLogin()
               })
               self.present(alertController, animated: true)
               return
           }else {
               self.alertMessage(title: AppLanguage.commonAppName, message: error)
           }
        }
    }
    @IBAction func btnEditJobsFollow_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuConfigJobFollowViewController") as! ISCMenuConfigJobFollowViewController
        self.pushViewControler(vc, "")
    }
    
}
// MARK: - UITableView Delegate & Datasource
extension ISCMenuJobsFollowViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCJobsFollowTableViewCell.name, for: indexPath) as? ISCJobsFollowTableViewCell
            else {
                return UITableViewCell()
        }
        if arrData.count > 0 {
            let obj = arrData[indexPath.row]
            cell.lblTitle.text = obj.Title
            if obj.MinSalary == nil && obj.MaxSalary == nil {
                cell.lblSalary.text = AppLanguage.jobsSalary
            }else {
                let min = Int(obj.MinSalary ?? "0")
                let max = Int(obj.MaxSalary ?? "0")
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0")"
            }

            cell.lblNumberPosition.text = "\(obj.NumberPositions ?? 0)"
            cell.lblAreaName.text = obj.ProvinceName
            if obj.EndDate != nil {
                let createdDate = obj.EndDate?.replacingOccurrences(of: "T", with: " ")
                let arraycreatedDate = createdDate?.split(separator: ".")
                if arraycreatedDate!.count > 0 {
                    cell.lblEndDate.text = Common.shared.convertDateTimeFormaterToString(String(arraycreatedDate![0]), type: "dd.MM.yyyy")
                }else {
                    cell.lblEndDate.text = ""
                }
            }
            cell.selectionStyle = .none
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrData[indexPath.row]
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
        vc.RecruitmentId = obj.RecruitmentId ?? 0
        vc.hidesBottomBarWhenPushed = false
        self.pushViewControler(vc, "Tin tức việc làm")
    }
    
    @objc func testingJobs(_ sender:UIButton) {
        
       
    }

}


