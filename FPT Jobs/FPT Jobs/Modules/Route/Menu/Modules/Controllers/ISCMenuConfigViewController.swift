//
//  ISCMenuConfigViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/8/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import WebKit
class ISCMenuConfigViewController: UIBaseViewController, WKNavigationDelegate {

    @IBOutlet weak var webContent: WKWebView!
    var titleService : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        webContent.navigationDelegate = self
        if titleService == AppLanguage.privatePolicy {
            loadPrivatePolicy()
        }else if titleService == AppLanguage.termsOfService {
            loadTermsOfService()
        }else {
            loadQuestion()
        }
        self.title = titleService
    }

    
    func loadTermsOfService(){
        self.showHUD(message: AppLanguage.connecting)
        self.getTermsOfService({ (data) in
            if let obj = data.Data {
                self.webContent.loadHTMLString(obj.Value ?? "", baseURL: nil)
            }else {
                self.hideHUD()
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    func loadPrivatePolicy(){
        self.showHUD(message: AppLanguage.connecting)
        self.getPrivatePolicy({ (data) in
            if let obj = data.Data {
                self.webContent.loadHTMLString(obj.Value ?? "", baseURL: nil)
            }else {
                self.hideHUD()
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    func loadQuestion(){
        self.showHUD(message: AppLanguage.connecting)
        self.getQuestion({ (data) in
            if let obj = data.Data {
                self.webContent.loadHTMLString(obj.Value ?? "", baseURL: nil)
            }else {
                self.hideHUD()
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    @IBAction func btnBack_click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideHUD()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideHUD()
    }

}
