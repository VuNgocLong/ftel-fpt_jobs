//
//  ISCMenuViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/7/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import StoreKit

class ISCMenuViewController: UIBaseViewController {

    @IBOutlet weak var viewAvatar: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.isShowChat = false
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.viewAvatar.layer.borderWidth = 3
        self.viewAvatar.layer.borderColor = UIColor.white.cgColor
        self.viewAvatar.layer.cornerRadius = viewAvatar.frame.size.width / 2
        self.viewAvatar.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
   
    @IBAction func btnBack_click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnLogin_click(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        _ = appDelegate.grabLogin()
        
    }

    @IBAction func btnPrivacy_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuConfigViewController") as! ISCMenuConfigViewController
        vc.titleService = AppLanguage.termsOfService
        self.pushViewControler(vc, AppLanguage.termsOfService)
    }
    @IBAction func btnSecurity_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuConfigViewController") as! ISCMenuConfigViewController
        vc.titleService = AppLanguage.privatePolicy
        self.pushViewControler(vc, AppLanguage.privatePolicy)
    }
    @IBAction func btnQuestion_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuConfigViewController") as! ISCMenuConfigViewController
        vc.titleService = AppLanguage.questionOfService
        self.pushViewControler(vc, AppLanguage.questionOfService)
    }
    @IBAction func btnRating_click(_ sender: Any) {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()

        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "1530902231") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func btnCallHotline_clicj(_ sender: Any) {
        if let url = NSURL(string: "tel://0944552888"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    @IBAction func btnLikedin_click(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "linkedin:")!) {
            UIApplication.shared.open(URL(string: "linkedin://#profile/3A1463101440720")!, options: [:], completionHandler: nil)
        }else {
            UIApplication.shared.open(URL(string: "http://www.linkedin.com/company/fpt-telecom")!, options: [:], completionHandler: nil)
        }
    }
    @IBAction func btnFacebook_click(_ sender: Any) {
        let appURL = URL(string: "fb://profile/fptjobs")!
        if UIApplication.shared.canOpenURL(appURL) {
            UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
        }else {
            UIApplication.shared.open(URL(string: "https://www.facebook.com/fptjobs")!, options: [:], completionHandler: nil)
        }
    }
    @IBAction func btnSkype_click(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "skype:")!) {
            UIApplication.shared.open(URL(string: "skype:fptjobs.ga")!, options: [:], completionHandler: nil)
        }else {
            UIApplication.shared.open(URL(string: "https://itunes.apple.com/in/app/skype/id304878510?mt=8")!, options: [:], completionHandler: nil)
        }
    }
    @IBAction func btnYoutube_click(_ sender: Any) {
        let appURL = URL(string: "youtube://channel/UC5w0hfouiUqtAs5rGHsQ6Pw")!
        if UIApplication.shared.canOpenURL(appURL) {
            UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
        }else {
            UIApplication.shared.open(URL(string: "https://www.youtube.com/channel/UC5w0hfouiUqtAs5rGHsQ6Pw")!, options: [:], completionHandler: nil)
        }
    }
}
