//
//  ISCMenuLoginViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/7/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import StoreKit
import SDWebImage
import Photos
import Alamofire
class ISCMenuLoginViewController: UIBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var viewAvatar: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblFullName: UIAutoSizeLabel!
    @IBOutlet weak var heightViewMenu: NSLayoutConstraint!
    var simpleImagePicker = UIImagePickerController()
    var selectedImage: UIImage?
    let loginNoti : LoginNotiVM = LoginNotiVM()
    var isUpdateProfile : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.isShowChat = false
        // Do any additional setup after loading the view.
   
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.viewAvatar.layer.borderWidth = 3
        self.viewAvatar.layer.borderColor = UIColor.white.cgColor
        self.viewAvatar.layer.cornerRadius = viewAvatar.frame.size.width / 2
        self.viewAvatar.layer.masksToBounds = true
        
        var height : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            let bottomPadding = window?.safeAreaInsets.bottom
            height = (topPadding ?? 0) + (bottomPadding ?? 0)
        } else {
           
        }
        
        heightViewMenu.constant = UIScreen.main.bounds.size.height - 160 - height
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            if let FullName = saved!["FullName"] as? String {
                self.lblFullName.text = FullName
            }
            if let PortraitImage = saved!["PortraitImage"] as? String {
                SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(PortraitImage)"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                    if let img = image {
                        self.imgAvatar.image = img
                    }
                }
            }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        loadProfile()
    }
    
    func loadProfile(){
        self.showHUD(message: AppLanguage.commonAppName)
        self.getProfile({ (value) in
            self.hideHUD()
                if let data = value.Data {
                    let dict = data.asDictionary
                    self.lblFullName.text = data.FullName
                    if Common.shared.isKeyPresentInUserDefaults(key: Common.key_avatar){
                        let dataImg = Common.shared.getImgAvatar()
                        self.imgAvatar.image = UIImage(data: dataImg as Data)
                    }else {
                        if data.PortraitImage != "" {
                            SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(data.PortraitImage ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                                if let img = image {
                                    self.imgAvatar.image = img
                                }
                            }
                        }
                    }
                    
                    UserDefaults.standard.set(dictionary: dict, forKey: Common.keyUserProfile)
                }
            }) { (error) in
                self.hideHUD()
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    @IBAction func btnBack_click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnLogOut_click(_ sender: Any) {
        
        let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                      message: AppLanguage.confirmLogout,
                                                      preferredStyle: .alert)
          alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .default))
          alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
            let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
                if saved != nil {
                    if let ID = saved!["ID"] as? Int {
                        self.showHUD(message: AppLanguage.commonAppName)
                        self.loginNoti.disableEnpoint(deviceToken: Common.DEVICE_TOKEN, applicationId: "\(ID)") { (data) in
                            self.hideHUD()
                           UserDefaults.standard.removeObject(forKey: Common.keyUserProfile)
                           UserDefaults.standard.removeObject(forKey: Common.keyAccessToken)
                           let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                           _ = appDelegate.grabLogin()
                        }
                    }
                }else {
                    UserDefaults.standard.removeObject(forKey: Common.keyUserProfile)
                    UserDefaults.standard.removeObject(forKey: Common.keyAccessToken)
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
            }
          })
                  
        self.present(alertController, animated: true)
       
    }
    @IBAction func btnPrivacy_click(_ sender: Any) {
       
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuConfigViewController") as! ISCMenuConfigViewController
        vc.titleService = AppLanguage.termsOfService
        self.pushViewControler(vc, AppLanguage.termsOfService)
    }
    @IBAction func btnSecurity_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuConfigViewController") as! ISCMenuConfigViewController
        vc.titleService = AppLanguage.privatePolicy
        self.pushViewControler(vc, AppLanguage.privatePolicy)
    }
    @IBAction func btnQuestion_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuConfigViewController") as! ISCMenuConfigViewController
        vc.titleService = AppLanguage.questionOfService
        self.pushViewControler(vc, AppLanguage.questionOfService)
    }
    @IBAction func btnRating_click(_ sender: Any) {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()

        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "1473563535") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func btnProfile_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuUserProfileViewController") as! ISCMenuUserProfileViewController
        self.pushViewControler(vc, AppLanguage.userProfile)
    }
    @IBAction func btnJobsApply_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsApplyViewController") as! ISCMenuJobsApplyViewController
        self.pushViewControler(vc, AppLanguage.userProfile)
    }
    @IBAction func btnJobsFollow_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsFollowViewController") as! ISCMenuJobsFollowViewController
        self.pushViewControler(vc, "")
    }
    
    @IBAction func btnChangePass_click(_ sender: Any) {
        let vc:ISCChangePasswordViewController = ISCChangePasswordViewController()
        let nav:UINavigationController = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    @IBAction func btnLinked_click(_ sender: Any) {
         if UIApplication.shared.canOpenURL(URL(string: "linkedin:")!) {
            UIApplication.shared.open(URL(string: "linkedin://#profile/3A1463101440720")!, options: [ : ], completionHandler: nil)
               }else {
            UIApplication.shared.open(URL(string: "http://www.linkedin.com/company/fpt-telecom")!, options: [ : ], completionHandler: nil)
               }
    }
    @IBAction func btnFacebook_click(_ sender: Any) {
        let appURL = URL(string: "fb://profile/fptjobs")!
        if UIApplication.shared.canOpenURL(appURL) {
            UIApplication.shared.open(appURL, options: [ : ], completionHandler: nil)
        }else {
            UIApplication.shared.open(URL(string: "https://www.facebook.com/fptjobs")!, options: [ : ], completionHandler: nil)
        }
        
    }
    @IBAction func btnSkype_click(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "skype:")!) {
            UIApplication.shared.open(URL(string: "skype:fptjobs.ga")!, options: [ : ], completionHandler: nil)
        }else {
            UIApplication.shared.open(URL(string: "https://itunes.apple.com/in/app/skype/id304878510?mt=8")!, options: [ : ], completionHandler: nil)
        }
    }
    @IBAction func btnYoutube_click(_ sender: Any) {
        let appURL = URL(string: "youtube://channel/UC5w0hfouiUqtAs5rGHsQ6Pw")!
        if UIApplication.shared.canOpenURL(appURL) {
            UIApplication.shared.open(appURL, options: [ : ], completionHandler: nil)
        }else {
            UIApplication.shared.open(URL(string: "https://www.youtube.com/channel/UC5w0hfouiUqtAs5rGHsQ6Pw")!, options: [ : ], completionHandler: nil)
        }
    }
    
    @IBAction func btnCallHotline_click(_ sender: Any) {
        if let url = URL(string: "tel://0944552888"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
        }
    }
    
    @IBAction func btnUpdateAvatar_click(_ sender: Any) {
        if AppPermission.shared.permissionPhoto {
            self.simpleImagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                self.simpleImagePicker.delegate = self
                self.simpleImagePicker.sourceType = .photoLibrary;
                self.simpleImagePicker.allowsEditing = false
                self.present(self.simpleImagePicker, animated: true, completion: nil)
            }
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
       
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
        }
        
        simpleImagePicker.dismiss(animated: true) {() -> Void in }
        
        uploadImageAvatar()
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func uploadImageAvatar(){
        let url = (ISCMenuConfig.upFileAvatar + "?jwt=\(Common.shared.getKongToken())").replacingOccurrences(of: "\"", with: "")
        if let data = resizeImage(image: selectedImage!, targetSize: CGSize(width: 768, height: 1024)).jpegData(compressionQuality: 1) {
            let parameters: Parameters = [:]
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key,value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                multipartFormData.append(data, withName: "file_1", fileName: "img_avartar.jpg", mimeType: "image/jpeg")},
                 usingThreshold:UInt64.init(),
                 to: url,
                 method:.post,
                 headers: [
                    "Authorization" : "Bearer \(UserDefaults.standard.getValue(key: Common.keyAccessToken))",
                    "Accept-Language" : Locale.preferredLanguages[0].contains("vi") ? "vi-VN":"en-US"
                    ],
                 encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                                if let access : Bool = dict["Succeeded"] as? Bool {
                                    if access {
                                        if let ReturnMessage : String = dict["ReturnMesage"] as? String{
                                            self.updateAvatar(imagePath: ReturnMessage)
                                            Common.shared.saveImgAvatar(imgURL: data as NSData)
                                            
                                            return
                                        }
                                    }
                                }
                            }
                            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.updateAvatarFail)
                        }
                    case .failure(_):
                        self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.updateAvatarFail)
                        break
                    }
            })
        }
    }
    
    func updateAvatar(imagePath : String){
        self.showHUD(message: AppLanguage.connecting)
        self.updateAvatar(ISCUpdateAvatarInput(ImagePath: imagePath), { (value) in
            if value.Succeeded ?? false {
                self.loadProfile()
                
            }else {
                self.hideHUD()
                if (value.Errors ?? []).count > 0 {
                   self.alertMessage(title: AppLanguage.commonAppName, message: value.Errors?[0]?.Description ?? AppLanguage.errorNetworkFailureConnectionApi)
               }
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
}
