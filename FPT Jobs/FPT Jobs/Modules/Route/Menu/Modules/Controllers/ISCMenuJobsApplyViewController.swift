//
//  ISCMenuJobsApplyViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCMenuJobsApplyViewController: UIBaseViewController {

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnApplyJob: UIButton!
    var arrData : [ISCJobsApplyOutput] = []
    var isLoadMore : Bool = false
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = AppLanguage.jobsApplyList
        // Do any additional setup after loading the view.
        let cellNib = UINib(nibName: ISCJobsApplyTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCJobsApplyTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 90
        
        btnApplyJob.setTitle(Locale.preferredLanguages[0].contains("vi") ? "ỨNG TUYỂN VỊ TRÍ MỚI" : "APPLY FOR NEW POSITION", for: .normal)
        
        SignalRManager.sharedInstance.onLeavingPage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadJobsApply()
    }
    @IBAction func btnJobNews_touch(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        _ = appDelegate.grabStoryboard()
    }
    
    func loadJobsApply(){
        self.showHUD(message: AppLanguage.connecting)
        self.arrData = []
        self.getJobsApply({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrData.append(value)
                    }
                }
            }
            if data.DataList?.count ?? 0 > 0 {
                if data.DataList?.count ?? 0 < 10 {
                    self.isLoadMore = false
                }else {
                    self.isLoadMore = true
                }
            }else {
                self.isLoadMore = false
            }
            self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
               let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                       message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                       preferredStyle: .alert)
               alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                   let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                   _ = appDelegate.grabLogin()
               })
               self.present(alertController, animated: true)
               return
           }else {
               self.alertMessage(title: AppLanguage.commonAppName, message: error)
           }
        }
    }
    
    func acceptJobsInterview(obj : ISCJobsApplyOutput) {
        self.showHUD(message: AppLanguage.connecting)
        self.acceptInterView(ISCAcceptJobsInterviewInput(CalendarId: obj.CalendarId, Status: 0, Reason: "0", Type: obj.Type), { (data) in
            self.hideHUD()
            if data.Succeeded! {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.acceptJobsInterviewSucces,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    self.loadJobsApply()
                })
                self.present(alertController, animated: true)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.acceptJobsInterviewFail)
            }
        }) { (error) in
            self.hideHUD()
             if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    
    func denyJobsInterview(obj : ISCJobsApplyOutput) {
        self.showHUD(message: AppLanguage.connecting)
        self.denyInterview(ISCAcceptJobsInterviewInput(CalendarId: obj.CalendarId, Status: 0, Reason: "0", Type: obj.Type), { (data) in
            self.hideHUD()
            if data.Succeeded! {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.messageUnConfirmInterviewSuccess,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    self.loadJobsApply()
                })
                self.present(alertController, animated: true)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.acceptJobsInterviewFail)
            }
        }) { (error) in
            self.hideHUD()
             if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    func acceptJobs(obj : ISCJobsApplyOutput) {
        self.showHUD(message: AppLanguage.connecting)
        self.acceptJob(ISCAcceptJobsInput(ApplicantApplyReId: obj.ApplicantApplyReId, Reason: 0, Status: 0), { (data) in
            self.hideHUD()
            if data.Succeeded! {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.acceptJobsInterviewSucces,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    self.loadJobsApply()
                })
                self.present(alertController, animated: true)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.acceptJobsInterviewFail)
            }
        }) { (error) in
            self.hideHUD()
             if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    func delayJobs(obj : ISCJobsApplyOutput) {
        self.showHUD(message: AppLanguage.connecting)
        self.delayJob(ISCAcceptJobsInput(ApplicantApplyReId: obj.ApplicantApplyReId, Reason: 0, Status: 0), { (data) in
            self.hideHUD()
            if data.Succeeded! {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.acceptJobsInterviewSucces,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    self.loadJobsApply()
                })
                self.present(alertController, animated: true)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.acceptJobsInterviewFail)
            }
        }) { (error) in
            self.hideHUD()
             if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    func denyJobs(obj : ISCJobsApplyOutput) {
        self.showHUD(message: AppLanguage.connecting)
        self.denyJob(ISCAcceptJobsInput(ApplicantApplyReId: obj.ApplicantApplyReId, Reason: 0, Status: 0), { (data) in
            self.hideHUD()
            if data.Succeeded! {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.acceptJobsInterviewSucces,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    self.loadJobsApply()
                })
                self.present(alertController, animated: true)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.acceptJobsInterviewFail)
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    func checkExamExists(obj : ISCJobsApplyOutput){
        self.showHUD(message: AppLanguage.connecting)
        self.checkExamExists(CheckExamExistsInput(ApplicantApplyReId: obj.ApplicantApplyReId), { (data) in
            self.hideHUD()
            if data.ErrorCode == "0" {
                let storyboard = UIStoryboard(name: "Test", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCTestConfirmPrivacyViewController") as! ISCTestConfirmPrivacyViewController
                vc.ExamResultId = data.Data?.ExamResultId ?? 0
                vc.objJobs = obj
                vc.hidesBottomBarWhenPushed = true
                vc.hidesBottomBarWhenPushed = true
                self.pushViewControler(vc, "")
            }else {
                switch data.ErrorCode {
                case "-1":
                    self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.CheckExamExpire)
                    break
                case "-2":
                    self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.CheckExamFinish)
                    break
                default:
                    self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.CheckExamNotExist)
                    break
                }
               
            }
        }, { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        })
    }
    
    func loadInterviewCalendar(obj : ISCJobsApplyOutput){
        self.showHUD(message: AppLanguage.commonAppName)
        self.getInterviewCalendar("\(obj.ApplicantApplyReId ?? 0)", { (data) in
            self.hideHUD()
            if let obj = data.Data {
                if (obj.InterviewDate == "" || obj.InterviewDate == nil) ||  (obj.InterviewTime == "" || obj.InterviewTime == nil) ||  (obj.TimeGap == 0 || obj.TimeGap == nil) {
                    self.alertMessage(title: AppLanguage.commonAppName, message: Locale.preferredLanguages[0].contains("vi") ? "Hôm nay bạn không có lịch phỏng vấn nào" : "You have no interview schedule today")
                    return
                }
                if Int(obj.CountDownToInterview ?? 0) > 0 {
                    self.alertMessage(title: AppLanguage.commonAppName, message: Locale.preferredLanguages[0].contains("vi") ? "Chưa đến thời gian phỏng vấn" : "It's not time to interview")
                                       return
                }
                if Int(obj.TimeLeft ?? 0) <= 0 || Int(obj.TimeLeft ?? 0) < -1 {
                    self.alertMessage(title: AppLanguage.commonAppName, message: Locale.preferredLanguages[0].contains("vi") ? "Hết thời gian phỏng vấn" : "End of interview time")
                    return
                }
                let vc = ISCInterViewVideoCallViewController()
                vc.timeMinus = Int(obj.TimeLeft ?? 0)
                vc.hidesBottomBarWhenPushed = true
                self.pushViewControler(vc, AppLanguage.jobsInterViewOnline)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: Locale.preferredLanguages[0].contains("vi") ? "Hôm nay bạn không có lịch phỏng vấn nào" : "You have no interview schedule today")
                return
            }
            
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
}
// MARK: - UITableView Delegate & Datasource
extension ISCMenuJobsApplyViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCJobsApplyTableViewCell.name, for: indexPath) as? ISCJobsApplyTableViewCell
            else {
                return UITableViewCell()
        }
        if arrData.count > 0 {
            let obj = arrData[indexPath.row]
            cell.lblTitle.text = obj.Title
            cell.btnTestJob.tag = indexPath.row
            if let createdDate = obj.CreatedDate?.replacingOccurrences(of: "T", with: " ") {
                let arraycreatedDate = createdDate.split(separator: ".")
                if arraycreatedDate.count > 0 {
                    cell.lblDate.text = Locale.preferredLanguages[0].contains("vi") ? "Ngày ứng tuyển : \(Common.shared.convertDateTimeFormaterToString(String(arraycreatedDate[0]), type: "dd/MM/yyyy"))" : "Date of submission : \(Common.shared.convertDateTimeFormaterToString(String(arraycreatedDate[0]), type: "dd/MM/yyyy"))"
                }else {
                    cell.lblDate.text = ""
                }
            }else {
                cell.lblDate.text = ""
            }
            cell.lblStatus.text = Common.shared.strStatusJobsApply(ProcessStatus: obj.ProcessStatus ?? 0)
            cell.spaceRightViewMenu.constant = -150
//            cell.spaceRightViewMenu.constant = 10
            switch obj.ProcessStatus {
            case 10, 17:
                 cell.lblJobsStatus.text = AppLanguage.jobsConfirmInterview
                 cell.viewJobsStatus.isHidden = false
                 cell.spaceRightViewMenu.constant = 0
                break
            case 26:
                cell.lblJobsStatus.text = AppLanguage.jobsConfirm
                cell.viewJobsStatus.isHidden = false
                cell.spaceRightViewMenu.constant = 0
                break
            case 1,2:
                cell.lblJobsStatus.text = AppLanguage.jobsStatusTesting
                cell.viewJobsStatus.isHidden = false
                cell.spaceRightViewMenu.constant = -37
                break
            case 3,6:
                cell.lblJobsStatus.text = AppLanguage.jobsStatusInterview
                cell.viewJobsStatus.isHidden = false
                cell.spaceRightViewMenu.constant = -37
                break
            case 12:
                cell.lblJobsStatus.text = AppLanguage.jobsStatusInterviewOnline
                cell.viewJobsStatus.isHidden = false
                cell.spaceRightViewMenu.constant = -37
                break
            case 30:
                cell.lblJobsStatus.text = AppLanguage.jobsContinuteInterviewOnline
                cell.viewJobsStatus.isHidden = false
                cell.spaceRightViewMenu.constant = -37
                break
            default:
                cell.viewJobsStatus.isHidden = true
                break
            }
            cell.btnTestJob.addTarget(self, action: #selector(self.testingJobs(_:)), for: .touchUpInside)
//            cell.delegate = self
            cell.selectionStyle = .none
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrData[indexPath.row]
        if (obj.SurveyStructureId ?? 0 > 0) {
            let storyboard = UIStoryboard(name: "News", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCNewsDetailViewController") as! ISCNewsDetailViewController
            vc.Title = obj.Title ?? ""
            vc.Date = ""
            vc.Description = ""
            vc.Content = obj.Url ?? ""
            vc.tabTitle = AppLanguage.eventTabTitle
            vc.hidesBottomBarWhenPushed = false
            self.pushViewControler(vc, AppLanguage.eventTabTitle)
        }else {
            let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
            vc.RecruitmentId = obj.RecruitmentId ?? 0
            vc.hidesBottomBarWhenPushed = false
            self.pushViewControler(vc, "Tin tức việc làm")
        }
        
    }
    
    @objc func testingJobs(_ sender:UIButton) {
        let obj = arrData[sender.tag]
        switch obj.ProcessStatus {
        case 0:
            break
        case 10, 17:
            FTPopOverMenuConfiguration.default()?.menuWidth = 240
            FTPopOverMenuConfiguration.default()?.menuRowHeight = 34
            FTPopOverMenuConfiguration.default()?.menuIconMargin = 10
            FTPopOverMenuConfiguration.default()?.menuTextMargin = 10
            FTPopOverMenuConfiguration.default()?.textFont = UIFont(name: "Helvetica Neue", size: 13)
            FTPopOverMenuConfiguration.default()?.textColor = UIColor(hexString: "#F27024")
            FTPopOverMenuConfiguration.default()?.tintColor = UIColor.white
            FTPopOverMenuConfiguration.default()?.borderColor = UIColor(hexString: "#D1D4DF")
            FTPopOverMenuConfiguration.default()?.shadowColor = UIColor.gray
            FTPopOverMenuConfiguration.default()?.shadowOffsetX = 2.0
            FTPopOverMenuConfiguration.default()?.shadowOffsetY = 2.0
            FTPopOverMenuConfiguration.default()?.showUpDown = ""
            let arrFunc : [String] = [AppLanguage.jobsConfirmInterview,AppLanguage.jobsDeplayInterview,AppLanguage.jobsUnConfirmInterview]
            FTPopOverMenu.show(forSender: sender as UIView, withMenuArray: arrFunc, doneBlock: { (selectedIndex) in
                switch selectedIndex {
                case 0 :
                    self.acceptJobsInterview(obj: obj)
                    break
                case 1 :
                    let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobDeplayViewController") as! ISCMenuJobDeplayViewController
                    vc.obj = obj
                    self.pushViewControler(vc, "")
                    break
                case 2 :
                    self.denyJobsInterview(obj: obj)
                    break
                default:
                    break
                }
                     
            }) {
                      
            }
            break
        case 26:
            FTPopOverMenuConfiguration.default()?.menuWidth = 160
            FTPopOverMenuConfiguration.default()?.menuRowHeight = 34
            FTPopOverMenuConfiguration.default()?.menuIconMargin = 10
            FTPopOverMenuConfiguration.default()?.menuTextMargin = 10
            FTPopOverMenuConfiguration.default()?.textFont = UIFont(name: "Helvetica Neue", size: 13)
            FTPopOverMenuConfiguration.default()?.textColor = UIColor(hexString: "#F27024")
            FTPopOverMenuConfiguration.default()?.tintColor = UIColor.white
            FTPopOverMenuConfiguration.default()?.borderColor = UIColor(hexString: "#D1D4DF")
            FTPopOverMenuConfiguration.default()?.shadowColor = UIColor.gray
            FTPopOverMenuConfiguration.default()?.shadowOffsetX = 2.0
            FTPopOverMenuConfiguration.default()?.shadowOffsetY = 2.0
            FTPopOverMenuConfiguration.default()?.showUpDown = "UP"
            let arrFunc : [String] = [AppLanguage.jobsConfirm,AppLanguage.jobsDeplayConfirm,AppLanguage.jobsUnConfirm]
            FTPopOverMenu.show(forSender: sender as UIView, withMenuArray: arrFunc, doneBlock: { (selectedIndex) in
               switch selectedIndex {
               case 0 :
                   self.acceptJobs(obj: obj)
                   break
               case 1 :
                    self.delayJobs(obj: obj)
                   break
               case 2 :
                   self.denyJobs(obj: obj)
                   break
               default:
                   break
               }
                     
            }) {
                      
            }
            break
        case 1,2:
            self.checkExamExists(obj: obj)
            break
        case 3,6:
            let storyboard = UIStoryboard(name: "InterviewOnline", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCRobotInterviewGuidViewController") as! ISCRobotInterviewGuidViewController
            vc.jobcode = obj.JobCode ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, AppLanguage.jobsInterViewOnline)
            break
        case 12:
            if AppPermission.shared.permissionCamera {
                if AppPermission.shared.permissionMicrophone {
                    self.loadInterviewCalendar(obj: obj)
                }
            }
            break
        case 30:
            if AppPermission.shared.permissionCamera {
                if AppPermission.shared.permissionMicrophone {
                    self.loadInterviewCalendar(obj: obj)
                }
            }
            break
        default:
            break
        }
       
    }

}


