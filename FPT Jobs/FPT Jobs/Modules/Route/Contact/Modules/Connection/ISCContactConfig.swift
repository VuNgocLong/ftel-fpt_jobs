//
//  ISCContactConfig.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/1/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCContactConfig {
    static let getContact = K.serverAPI.api + "Contact/GetContact"
    static let searchContact = K.serverAPI.api + "Contact/SearchContact"
    static let getProvinces = K.serverAPI.api + "Catalog/GetAllProvinces"
    static let getDistricts = K.serverAPI.api + "Catalog/GetDistrictsByProvinceCode"
    static let getSchools = K.serverAPI.api + "Catalog/GetAllSchools"
}
