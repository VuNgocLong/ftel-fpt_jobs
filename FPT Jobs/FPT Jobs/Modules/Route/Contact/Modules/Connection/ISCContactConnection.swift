//
//  ISCContactConnection.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/1/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
extension ISCContactTabViewController {
    func getContact(
                _ onSuccess: @escaping (_ response: APIResponse<ISCContactOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCContactConfig.getContact,isCheckHeader: false, ISCContactInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func searchContact(
                _ onSuccess: @escaping (_ response: APIResponse<ISCContactOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection("\(ISCContactConfig.searchContact)/\(ProvinceCode)",isCheckHeader: false, ISCContactInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func getProvinces(
                _ onSuccess: @escaping (_ response: APIResponse<ISCProvinceOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCContactConfig.getProvinces,isCheckHeader: false, ISCProvinceInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getDistricts(
                _ onSuccess: @escaping (_ response: APIResponse<ISCDistrictsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection("\(ISCContactConfig.getDistricts)/\(ProvinceCode)",isCheckHeader: false, ISCDistrictsInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getSchool(
                _ onSuccess: @escaping (_ response: APIResponse<ISCSchoolOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCContactConfig.getSchools,isCheckHeader: false, ISCSchoolInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
