//
//  ISCContactObj.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/1/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCContactInput:Encodable {
}
struct ISCContactOutput: Codable {
    var BranchContactId:Int?
    var BranchContactName:String?
    var Address:String?
    var ProvinceCode:String?
    var Latitude:String?
    var Longtitude:String?
    var ContactName:String?
    var Email:String?
    var Hotline:String?
    var IPPhone:String?
    var IsMain:Int?
}
struct ISCProvinceInput:Encodable {
}
struct ISCProvinceOutput: Codable {
    var Id:Int?
    var ProvinceCode:String?
    var ProvinceName:String?
}

struct ISCDistrictsInput:Encodable {
}
struct ISCDistrictsOutput: Codable {
    var Id:Int?
    var DistrictCode:String?
    var DistrictName:String?
    var ProvinceCode:String?
}
struct ISCSchoolInput:Encodable {
}
struct ISCSchoolOutput: Codable {
    var Id:Int?
    var SchoolName:String?
    var TypeSchool:Int?
}
