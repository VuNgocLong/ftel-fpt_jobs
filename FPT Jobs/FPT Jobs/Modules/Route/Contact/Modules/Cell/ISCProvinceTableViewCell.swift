//
//  ISCProvinceTableViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/1/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCProvinceTableViewCell: UITableViewCell {

    @IBOutlet weak var lblProvinceName: UILabel!
    
    static var name: String {
        return String(describing: ISCProvinceTableViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
