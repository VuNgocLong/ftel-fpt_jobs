//
//  ISCContactTabViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/25/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

class ISCContactTabViewController: UIBaseViewController, ISCProviceModalDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var viewInfo: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewProvince: UIView!
    @IBOutlet weak var lblProvinceName: UIAutoSizeLabel!
    @IBOutlet weak var lblBranchContactName: UIAutoSizeLabel!
    @IBOutlet weak var lblContactName: UIAutoSizeLabel!
    @IBOutlet weak var lblEmail: UIAutoSizeLabel!
    @IBOutlet weak var lblIPPhone: UIAutoSizeLabel!
    @IBOutlet weak var lblBranchList: UIAutoSizeLabel!
    @IBOutlet weak var heightViewInfoConstrain: NSLayoutConstraint!
    @IBOutlet weak var spaceTopViewInfoConstrain: NSLayoutConstraint!
    var swipeup : UISwipeGestureRecognizer? = nil
    var swipedown : UISwipeGestureRecognizer? = nil
    var viewMainMaps : GMSMapView?
    let locationManager = CLLocationManager()
    var arrayContact : [ISCContactOutput] = []
    var arrayProvince : [ISCProvinceOutput] = []
    var arrayBranch : [ISCContactOutput] = []
    var isLoadTarget : Bool = false
    var isShow : Bool = false
    var ProvinceCode : String = "0"
    var viewCenter :CGPoint = CGPoint.zero
    var ipphone : String = "02473002222"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        viewMainMaps = GMSMapView()
        self.isShowChat = true
        self.isSearchViewController = true
        self.initNavController()
        
        viewMainMaps = GMSMapView(frame: CGRect(x: 0, y: 0, width: viewMain.frame.size.width, height: viewMain.frame.size.height))
        viewMainMaps!.delegate = self
        locationManager.delegate = self
        viewMainMaps!.isMyLocationEnabled = false
        viewMainMaps!.settings.myLocationButton = false
        viewMainMaps!.isIndoorEnabled = false
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        viewMain.addSubview(viewMainMaps!)
        
        
        self.viewProvince.layer.borderWidth = 1
        self.viewProvince.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.viewProvince.layer.cornerRadius = 6
        self.viewProvince.layer.masksToBounds = true
        
        heightViewInfoConstrain.constant = self.view.frame.size.height - 55
        spaceTopViewInfoConstrain.constant = -self.view.frame.size.height
        self.showHUD(message: AppLanguage.connecting)
        getProvinces()
        loadContact()
        scrollView.delaysContentTouches = false
        scrollView.delegate = self
//        scrollView.isScrollEnabled = false

        let swipe = UIPanGestureRecognizer(target: self, action: #selector(draggingView))
        viewInfo.addGestureRecognizer(swipe)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = AppLanguage.contactTabTitle
    }
     
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    @objc func draggingView(_ sender : UIPanGestureRecognizer) {
        let fileView = sender.view
        let translation = sender.translation(in: view)
        let velocity = sender.velocity(in: view)
        switch sender.state {
        case .began:
            viewCenter = fileView!.center
            break
        case .changed:
            print(self.viewInfo.frame.origin.y)

            if velocity.y > 0 {
                if self.viewInfo.frame.origin.y <= (self.view.frame.size.height - 220) {
                    fileView?.center = CGPoint(x: fileView?.center.x ?? 0, y: (fileView?.center.y ?? 0) + translation.y)
                    sender.setTranslation(CGPoint.zero, in: view)
                }
            }else {
                if self.viewInfo.frame.origin.y >= 55 {
                  fileView?.center = CGPoint(x: fileView?.center.x ?? 0, y: (fileView?.center.y ?? 0) + translation.y)
                  sender.setTranslation(CGPoint.zero, in: view)
                }
            }
            break
        case .ended:
            if velocity.y > 0 {
                if self.viewInfo.frame.origin.y != (self.view.frame.size.height - 190) {
                    UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                        let he = self.tabBarController?.tabBar.frame.size.height
                        if Int(he ?? 0) > 49 {
                            self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 325
                        }else {
                            self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 270
                        }
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }else {
                if self.viewInfo.frame.origin.y != 55 {
                    UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                        self.spaceTopViewInfoConstrain.constant = 0
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
            break
        default:
            break
        }

    }
    
    @IBAction func btnSendMail_touch(_ sender: Any) {
        if let url = URL(string: "mailto://\(lblEmail.text ?? "")"), UIApplication.shared.canOpenURL(url as URL) {
                   UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
               }
    }

    @IBAction func btnPhone_touch(_ sender: Any) {
        if let url = URL(string: "tel://\(ipphone ?? "")"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
        }
    }
    func loadContact(){
        arrayContact = []
        self.getContact({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayContact.append(value)
                        self.addMarker(latitude: Double(value.Latitude ?? "0")!, longitude: Double(value.Longtitude ?? "0")!, model: value)
                    }
                }
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func getProvinces(){
        arrayProvince = []
        self.getProvinces({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayProvince.append(value)
                    }
                }
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    func searchContact(){
        arrayBranch = []
        self.searchContact({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayBranch.append(value)
                        self.loadListBranch()
                        self.loadMapBranch(model: value)
                    }
                }
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadListBranch(){
        var strBranch : String = ""
        for item in arrayBranch {
            strBranch += "<p style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">- \(item.Address ?? "") </p>"
        }
        lblBranchList.attributedText = strBranch.htmlToAttributedString
    }
    
    func loadMapBranch(model : ISCContactOutput){
        self.addMarker(latitude: Double(model.Latitude ?? "0")!, longitude: Double(model.Longtitude ?? "0")!, model: model)
    }
    
    @objc func loadProvince(sender : UITapGestureRecognizer) {
        let modal = ISCProviceModal()
        modal.arrayProvince = arrayProvince
        modal.delegate = self
        modal.modalPresentationStyle = .fullScreen
        modal.modalTransitionStyle = .coverVertical
        self.present(modal, animated: true, completion: nil)
    }
    
    @IBAction func btnProvince_click(_ sender: Any) {
        let modal = ISCProviceModal()
        modal.arrayProvince = arrayProvince
        modal.delegate = self
        modal.modalPresentationStyle = .fullScreen
        modal.modalTransitionStyle = .coverVertical
        self.present(modal, animated: true, completion: nil)
    }

    func closeProvinceModal(model: ISCProvinceOutput) {
        self.showHUD(message: AppLanguage.connecting)
        ProvinceCode = (model.ProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines))!
        searchContact()
        lblProvinceName.text = model.ProvinceName
        for item in arrayContact {
            if item.ProvinceCode == model.ProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) {
                let locationKH = CLLocationCoordinate2D(latitude: Double(item.Latitude ?? "0")! , longitude: Double(item.Longtitude ?? "0")!)
                viewMainMaps!.camera = GMSCameraPosition(target: locationKH, zoom: 12, bearing: 0, viewingAngle: 0)
                lblBranchContactName.text = item.BranchContactName?.uppercased()
                lblContactName.text = item.ContactName
                lblEmail.text = item.Email
                lblIPPhone.text = item.IPPhone
                let arrStrPhone = item.IPPhone?.components(separatedBy: "máy lẻ")
               if arrStrPhone?.count ?? 0 > 1 {
                   let textContent = arrStrPhone?[0]
                
                   let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "UTM-Avo", size: 15.0)!, NSAttributedString.Key.foregroundColor : UIColor(hexString: "#3E3E3E")]
                   let myString = NSMutableAttributedString(string: lblIPPhone.text!, attributes: myAttribute)
                   let myRange = NSRange(location: 0, length: textContent?.count ?? 0)
                   myString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "UTM-Avo", size: 15.0) as Any, range: myRange)
                   myString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: "#1569B2") , range: myRange)
                   lblIPPhone.attributedText = myString
               }
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    let he = self.tabBarController?.tabBar.frame.size.height
                    if Int(he ?? 0) > 49 {
                        self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 325
                    }else {
                        self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 270
                    }
                    
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
        
    }
    @IBAction func btnShowMoreInfo_click(_ sender: Any) {
        if isShow {
            isShow = false
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                let he = self.tabBarController?.tabBar.frame.size.height
                if Int(he ?? 0) > 49 {
                    self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 325
                }else {
                    self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 270
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
        }else {
            isShow = true
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.spaceTopViewInfoConstrain.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        
    }
    
    
    
}

// MARK: - GMSMap Delegate
extension ISCContactTabViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    
    func startStandardUpdates(){
        locationManager
    }
    func addMarker(latitude : Double, longitude : Double, model : ISCContactOutput ){
        let position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let marker = GMSMarker(position: position)
        marker.userData = model
        marker.icon = UIImage(named: "ic_marker_fpt")
        marker.snippet = "\(model.ProvinceCode ?? "")-\(model.BranchContactName ?? "")"
        marker.map = viewMainMaps
    }
    func setupMap(latitude : Double, longitude : Double){
        isLoadTarget = true
        let locationKH = CLLocationCoordinate2D(latitude: latitude , longitude: longitude)
        viewMainMaps!.camera = GMSCameraPosition(target: locationKH, zoom: 8, bearing: 0, viewingAngle: 0)
        viewMainMaps!.isMyLocationEnabled = true
        viewMainMaps!.settings.myLocationButton = true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
           guard status == .authorizedWhenInUse else {
               return
           }
           locationManager.startUpdatingLocation()
       }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
           
           for location in locations {
            if !isLoadTarget {
                setupMap(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let obj : ISCContactOutput = (marker.userData as? ISCContactOutput)!
        lblBranchContactName.text = obj.BranchContactName?.uppercased()
        lblContactName.text = obj.ContactName
        lblEmail.text = obj.Email
        lblIPPhone.text = obj.IPPhone
        let arrStrPhone = obj.IPPhone?.components(separatedBy: "máy lẻ")
        if arrStrPhone?.count ?? 0 > 1 {
            let textContent = arrStrPhone?[0]
            let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "UTM-Avo", size: 15.0)!, NSAttributedString.Key.foregroundColor : UIColor(hexString: "#3E3E3E")]
            let myString = NSMutableAttributedString(string: lblIPPhone.text!, attributes: myAttribute)
            let myRange = NSRange(location: 0, length: textContent?.count ?? 0)
            myString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "UTM-Avo", size: 15.0) as Any, range: myRange)
            myString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: "#1569B2") , range: myRange)
            lblIPPhone.attributedText = myString
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            let he = self.tabBarController?.tabBar.frame.size.height
            
            if Int(he ?? 0) > 49 {
                self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 325
            }else {
                self.spaceTopViewInfoConstrain.constant = self.view.frame.size.height - 270
            }
            self.view.layoutIfNeeded()
        }, completion: nil)

        let locationKH = CLLocationCoordinate2D(latitude: marker.position.latitude , longitude: marker.position.longitude)
        viewMainMaps!.camera = GMSCameraPosition(target: locationKH, zoom: 12, bearing: 0, viewingAngle: 0)
        for item in arrayProvince {
            if obj.ProvinceCode == item.ProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines) {
                lblProvinceName.text = item.ProvinceName
            }
        }
        if obj.IsMain == 1 {
            self.showHUD(message: AppLanguage.connecting)
            ProvinceCode = (obj.ProvinceCode?.trimmingCharacters(in: .whitespacesAndNewlines))!
            searchContact()
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        
    }
}
