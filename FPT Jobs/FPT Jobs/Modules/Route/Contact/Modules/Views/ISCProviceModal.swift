//
//  ISCProviceModal.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/1/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCProviceModalDelegate: class {
    func closeProvinceModal (model : ISCProvinceOutput)
}
class ISCProviceModal: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var tblList: UITableView!
    weak var delegate:ISCProviceModalDelegate?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var widthConstraintsSearchKey: NSLayoutConstraint!
    var arrayProvince : [ISCProvinceOutput] = []
    var arrDataSearch : [ISCProvinceOutput] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        arrDataSearch = arrayProvince
        let cellNib = UINib(nibName: ISCProvinceTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCProvinceTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 44
        
        self.searchBar.backgroundImage = UIImage()
        self.searchBar.backgroundColor = .clear
        self.searchBar.isTranslucent = true
        self.searchBar.tintColor = UIColor(hexString: "#3E3E3E")
        if #available(iOS 13.0, *) {
           searchBar.searchTextField.backgroundColor = UIColor.white
        }
        self.searchBar.delegate = self
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
           
            if searchText.count > 1 {
                self.arrDataSearch = []
                for item in arrayProvince {
                    if item.ProvinceName?.uppercased().contains(searchText.uppercased()) ?? false {
                        self.arrDataSearch.append(item)
                    }
                }
                self.tblList.reloadData()
            }else {
                self.arrDataSearch = arrayProvince
                self.tblList.reloadData()
            }
    }

    @IBAction func btnBack_click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnSearch_touch(_ sender: Any) {
        if self.widthConstraintsSearchKey.constant > 0 {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.widthConstraintsSearchKey.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            searchBar.resignFirstResponder()
        }else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.widthConstraintsSearchKey.constant = UIScreen.main.bounds.width - 98
                self.view.layoutIfNeeded()
            }, completion: nil)
            searchBar.becomeFirstResponder()
        }
    }
}
// MARK: - UITableView Delegate & Datasource
extension ISCProviceModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDataSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCProvinceTableViewCell.name, for: indexPath) as? ISCProvinceTableViewCell
            else {
                return UITableViewCell()
        }
        if arrDataSearch.count > 0 {
            let obj = arrDataSearch[indexPath.row]
            cell.lblProvinceName.text = obj.ProvinceName
            cell.selectionStyle = .none
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        let obj = arrDataSearch[indexPath.row]
        self.delegate?.closeProvinceModal(model: obj)
        self.dismiss(animated: true, completion: nil)
    }
}
