//
//  ISCNewsObj.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/30/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
struct ISCJobsInput:Encodable {
    var PageIndex: Int = 0
    var PageSize: Int = 0
    var currentPageIndex: Int = 1
    var Keyword: String = ""
    var JobCategory: String = ""
    var Area: String = ""
}
struct ISCJobsOutput: Codable {
    var DisplayFlag:Int?
    var RecruitmentId:Int?
    var BranchId:Int?
    var JobCode:Int?
    var BranchAddressId:Int?
    var AreaId:Int?
    var AreaName:String?
    var JobTypeId:Int?
    var Title:String?
    var ContactPerson:String?
    var NumberPositions:Int?
    var JobPositionId:Int?
    var JobLevelId:Int?
    var MinSalary:String?
    var MaxSalary:String?
    var CurrentUnit:String?
    var ShowSalary:Int?
    var JobDescription:String?
    var JobRequirement:String?
    var JobRequest:String?
    var JobBenefit:String?
    var JobInfomation:String?
    var NewsType:Int?
    var JobType:Int?
    var Gender:Int?
    var AgeFrom:Int?
    var AgeTo:Int?
    var Highlight:Int?
    var JobCategoryId:Int?
    var DescriptionImage:String?
    var DescriptionVideo:String?
    var CountView:Int?
    var CountApply:Int?
    var CreatedUser:String?
    var CreatedDate:String?
    var ModifiedUser:String?
    var ModifiedDate:String?
    var StartDate:String?
    var EndDate:String?
    var Hot:Int?
    var ExamType:Int?
    var ProcessStatus:Int?
    var CouncilId:Int?
    var company_name:String?
    var company_info:String?
    var JobFullDescription:String?
    var Address:String?
    var HashTags:String?
    var BranchName:String?
    var Description:String?
    var FullName:String?
    var Email:String?
    var Telephone:String?
    var MobilePhone:String?
    var RelativeList:String?
    var BranchAddresViewModels:String?
    var EndDateStr:String?
}
struct ISCJobsDetailInput:Encodable {
}
struct ISCJobsDetailOutput: Codable {
    var RecruitmentId:Int?
    var Title:String?
    var NumberPositions:Int?
    var MinSalary:String?
    var MaxSalary:String?
    var CurrentUnit:String?
    var ShowSalary:Int?
    var JobDescription:String?
    var JobRequirement:String?
    var JobRequest:String?
    var JobBenefit:String?
    var JobInfomation:String?
    var company_name:String?
    var company_info:String?
    var JobFullDescription:String?
    var Address:String?
    var HashTags:String?
    var BranchName:String?
    var Description:String?
    var FullName:String?
    var Email:String?
    var Telephone:String?
    var MobilePhone:String?
    var EndDateStr:String?
    var JobCode:String?
    var ExamType:Int?
    var JobDetailsForIOS:String?
    var JobBenefitForIOS:String?
    var JobRequestForIOS:String?
    var JobInfomationForIOS:String?
}

struct ISCJobsApplyOutput: Codable {
    var ApplicantApplyReId:Int?
    var InterviewerCode:String?
    var Title:String?
    var JobCode:String?
    var CreatedDate:String?
    var ProcessName:String?
    var ProcessStatus:Int?
    var InterviewDate:String?
    var InterviewTime:String?
    var InviteDate:String?
    var AcceptanceDate:String?
    var RecruitmentId:Int?
    var CalendarId:Int?
    var `Type`:Int?
    var SurveyStructureId:Int?
    var FkSurveyDataId:Int?
    var Url:String?
}


struct ISCJobsApplySubmitInput: Encodable {
    var ApplicantApplyReId:Int?
    var CalendarId:Int?
    var ExamType:Int?
    var FkSurveyDataId:Int?
    var JobCode:String?
    var NumberDay:Int?
    var ProcessStatus:Int?
    var SurveyStructureId:Int?
    var `Type`:Int?
    var Title:String?
}
struct ISCJobsApplySubmitOutput:Codable {
}
struct ISCAcceptJobsInterviewInput:Codable {
    var CalendarId:Int?
    var Status:Int?
    var Reason:String?
    var `Type`:Int?
}
struct ISCDelayJobsInterviewInput:Codable {
    var CalendarId:Int?
    var Status:Int?
    var Reason:String?
    var `Type`:Int?
    var FkSurveyDataId:Int?
}
struct ISCAcceptJobsInput:Codable {
    var ApplicantApplyReId:Int?
    var Reason:Int?
    var Status:Int?
}
struct ISCJobsFollowOutput:Codable {
    var CategoryName:String?
    var EndDate:String?
    var MinSalary:String?
    var MaxSalary:String?
    var NumberPositions:Int?
    var ProvinceName:String?
    var RecruitmentId:Int?
    var StartDate:String?
    var Title:String?
}
struct ISCSuitableJobsInput:Encodable {
    var PageIndex: Int = 0
    var PageSize: Int = 0
}
