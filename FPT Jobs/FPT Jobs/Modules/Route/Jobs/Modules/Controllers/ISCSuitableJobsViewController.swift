//
//  ISCSuitableJobsViewController.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/20/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCSuitableJobsViewController: UIBaseViewController {

    @IBOutlet weak var tblList: UITableView!
    
    var PageIndex: Int = 0
    var PageSize: Int = 10
    var isLoadMore : Bool = false
    var timer = Timer()
    var arrSuitableJobs : [ISCJobsOutput] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = Locale.preferredLanguages[0].contains("vi") ? "Việc làm phù hợp" : "Suitable Jobs"
        let nibLoading = UINib(nibName: LoadingTableViewCell.name, bundle: nil)
        tblList.register(nibLoading, forCellReuseIdentifier: LoadingTableViewCell.name)
        let cellNib = UINib(nibName: ISCJobsTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCJobsTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 100
        self.showHUD(message: AppLanguage.connecting)
        loadSuitableJobs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    @objc override func backAvatarAction() {
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuLoginViewController") as! ISCMenuLoginViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }else {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuViewController") as! ISCMenuViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }
    }


    func loadSuitableJobs() {
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            self.hideHUD()
            if let ID = saved!["ID"] as? Int {
                self.getSuitableJobs(ID, ISCSuitableJobsInput(PageIndex: PageIndex, PageSize: PageSize), { (data) in
                    if let array = data.DataList {
                        array.forEach { (val) in
                            if let value = val {
                                self.arrSuitableJobs.append(value)
                            }
                        }
                        
                    }
                    if data.DataList?.count ?? 0 > 0 {
                        if data.DataList?.count ?? 0 < 10 {
                            self.isLoadMore = false
                        }else {
                            self.isLoadMore = true
                        }
                    }else {
                        self.isLoadMore = false
                    }
                    self.tblList.reloadData()
                }) { (error) in
                    self.hideHUD()
                    self.alertMessage(title: AppLanguage.commonAppName, message: error)
                }
            }
        }
        
    }
}
// MARK: - UITableView Delegate & Datasource
extension ISCSuitableJobsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSuitableJobs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCJobsTableViewCell.name, for: indexPath) as? ISCJobsTableViewCell
            else {
                return UITableViewCell()
        }
        if arrSuitableJobs.count > 0 {
            let obj = arrSuitableJobs[indexPath.row]
            cell.lblTitle.text = obj.Title?.uppercased()
            if obj.MinSalary == nil && obj.MaxSalary == nil {
                cell.lblSalary.text = AppLanguage.jobsSalary
            }else {
                let min = Int(obj.MinSalary ?? "0")
                let max = Int(obj.MaxSalary ?? "0")
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0") \(obj.CurrentUnit ?? "")"
            }
            cell.lblStatus.text = ""
            cell.viewStatus.isHidden = true
            cell.spaceRightTitleConstraint.constant = 10
            cell.lblNumberPosition.text = "\(obj.NumberPositions ?? 0)"
            cell.lblAreaName.text = obj.AreaName
            cell.lblEndDate.text = obj.EndDateStr
            cell.selectionStyle = .none
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrSuitableJobs[indexPath.row]
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
        vc.RecruitmentId = obj.RecruitmentId ?? 0
        vc.TypePush = "MAIN"
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, AppLanguage.jobsTabTitle)
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tblList.dequeueReusableCell(withIdentifier: LoadingTableViewCell.name) as! LoadingTableViewCell
        cell.contentView.backgroundColor = .groupTableViewBackground
        return cell

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if isLoadMore {
            return 50
        }
        return 5
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrSuitableJobs.count - 1 {
            if isLoadMore {
                isLoadMore = false
                timer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(loadmore), userInfo: nil, repeats: true)
            }
        }
    }
    @objc func loadmore(){
        timer.invalidate()
        PageIndex += 10
        loadSuitableJobs()
    }
}
