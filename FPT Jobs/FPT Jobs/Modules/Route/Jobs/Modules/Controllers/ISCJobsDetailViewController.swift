//
//  ISCJobsDetailViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/25/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import FBSDKShareKit
//import LinkedinSwift

class ISCJobsDetailViewController: UIBaseViewController, SharingDelegate {

    @IBOutlet weak var lblJobTitle: UIAutoSizeLabel!
    @IBOutlet weak var lblJobAddress: UIAutoSizeLabel!
    @IBOutlet weak var lblNumberPosition: UIAutoSizeLabel!
    @IBOutlet weak var lblJobEndDate: UIAutoSizeLabel!
    @IBOutlet weak var lblAreaName: UIAutoSizeLabel!
    @IBOutlet weak var lblCompanyName: UIAutoSizeLabel!
    @IBOutlet weak var lblSalary: UIAutoSizeLabel!
    @IBOutlet weak var lblJobsDescription: UILabel!
    @IBOutlet weak var lblJobsRequest: UIAutoSizeLabel!
    @IBOutlet weak var lblJobsBenefit: UIAutoSizeLabel!
    @IBOutlet weak var lblJobsInfomation: UIAutoSizeLabel!
    @IBOutlet weak var lblPhone: UIAutoSizeLabel!
    @IBOutlet weak var lblMobile: UIAutoSizeLabel!
    @IBOutlet weak var lblEmail: UIAutoSizeLabel!
    @IBOutlet weak var lblContactname: UIAutoSizeLabel!
    var RecruitmentId : Int = 0
    var jobsObject : ISCJobsDetailOutput? = nil
    var TypePush : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = AppLanguage.jobsDetailTitle
        loadJobsDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc override func backAvatarAction() {
        if TypePush != "" {
            let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
            if saved != nil {
                let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuLoginViewController") as! ISCMenuLoginViewController
                vc.hidesBottomBarWhenPushed = true
                self.pushViewControler(vc, "")
            }else {
                let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuViewController") as! ISCMenuViewController
                vc.hidesBottomBarWhenPushed = true
                self.pushViewControler(vc, "")
            }
        }else {
            if let view = self.navigationController?.viewControllers {
                 var arrViewControllerMain: [UIViewController] = []
                 for n in 0...view.count - 3 {
                     arrViewControllerMain.append(view[n])
                 }
                 self.navigationController?.viewControllers = arrViewControllerMain
            }
        }
        
    }
    func loadJobsDetail(){
        self.showHUD(message: AppLanguage.connecting)
        self.getJobsDetail({ (data) in
            self.hideHUD()
            self.jobsObject = data.Data!
            if self.jobsObject != nil {
                self.loadData()
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }

    func loadData(){
        lblJobTitle.text = jobsObject?.Title?.uppercased()
        lblJobAddress.text = jobsObject?.Address
        lblNumberPosition.text = "\(jobsObject?.NumberPositions ?? 0)"
        lblJobEndDate.text = jobsObject?.EndDateStr
        lblAreaName.text = jobsObject?.Address
        lblCompanyName.text = jobsObject?.company_name
        if jobsObject?.MinSalary == nil && jobsObject?.MaxSalary == nil {
            lblSalary.text = AppLanguage.jobsSalary
        }else {
            let min = Int(jobsObject?.MinSalary ?? "0")
            let max = Int(jobsObject?.MaxSalary ?? "0")
            let nf = NumberFormatter()
            nf.numberStyle = .decimal
            lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0") \(jobsObject?.CurrentUnit ?? "")"
        }
        
        lblContactname.text = jobsObject?.FullName
        lblEmail.text = jobsObject?.Email
        lblMobile.text = jobsObject?.MobilePhone
        lblPhone.text = jobsObject?.Telephone
        
        let modifiedJobDescription = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobRequirement ?? "")</span>"
        lblJobsDescription.attributedText = modifiedJobDescription.htmlToAttributedString
        let modifiedJobRequest = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobRequestForIOS ?? "")</span>"
        lblJobsRequest.attributedText = modifiedJobRequest.htmlToAttributedString
        let modifiedJobBenefit = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobBenefitForIOS ?? "")</span>"
        lblJobsBenefit.attributedText = modifiedJobBenefit.htmlToAttributedString
       let modifiedJobInfomation = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobInfomationForIOS ?? "")</span>"
        lblJobsInfomation.attributedText = modifiedJobInfomation.htmlToAttributedString
        
//        let modifiedJobDescription = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobDescription ?? "")</span>"
//       lblJobsDescription.attributedText = modifiedJobDescription.htmlToAttributedString
//       let modifiedJobRequest = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobRequest ?? "")</span>"
//       lblJobsRequest.attributedText = modifiedJobRequest.htmlToAttributedString
//       let modifiedJobBenefit = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobBenefit ?? "")</span>"
//       lblJobsBenefit.attributedText = modifiedJobBenefit.htmlToAttributedString
//        let modifiedJobInfomation = "<span style=\"font-family: UTM-Avo; font-size: 15; color: #3E3E3E\">\(jobsObject?.JobInfomation ?? "")</span>"
//       lblJobsInfomation.attributedText = modifiedJobInfomation.htmlToAttributedString
  
    }
    @IBAction func btnShareFacebook_click(_ sender: Any) {
        shareTextOnFaceBook()
    }
    @IBAction func btnShareLikedin_click(_ sender: Any) {
        UIApplication.shared.open(URL.init(string: "https://www.linkedin.com/shareArticle?mini=true&amp;url=\(K.serverAPI.domain)/\(Common.shared.replaceStringCharacter(str: jobsObject?.Title ?? ""))-\(RecruitmentId)")!, options: [:], completionHandler: nil)
    }
    
    func shareTextOnFaceBook() {
        let shareContent = ShareLinkContent()
        shareContent.contentURL = URL.init(string: "\(K.serverAPI.domain)/\(Common.shared.replaceStringCharacter(str: jobsObject?.Title ?? ""))-\(RecruitmentId)")! //your link
//        shareContent.quote = "Text to be shared"
        ShareDialog(fromViewController: self, content: shareContent, delegate: self).show()
    }

    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        if sharer.shareContent.pageID != nil {
            print("Share: Success")
        }
    }
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("Share: Fail")
    }
    func sharerDidCancel(_ sharer: Sharing) {
        print("Share: Cancel")
    }
    @IBAction func btnSendEmai_touch(_ sender: Any) {
        if let url = URL(string: "mailto://\(lblEmail.text ?? "")"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
        }
    }
    @IBAction func btnPhone_touch(_ sender: Any) {
        if let url = URL(string: "tel://\(lblPhone.text ?? "")"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
        }
    }
    
    @IBAction func btnMobile_touch(_ sender: Any) {
        if let url = URL(string: "tel://\(lblMobile.text ?? "")"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
        }
    }
    @IBAction func btnApplyJobs_click(_ sender: Any) {
        self.showHUD(message: AppLanguage.connecting)
        self.applyJobs(ISCJobsApplySubmitInput(ApplicantApplyReId: 0, CalendarId: 0, ExamType: 0, FkSurveyDataId: 0, JobCode: jobsObject?.JobCode, NumberDay: 0, ProcessStatus: 0, SurveyStructureId: 0, Type: 0, Title : jobsObject?.Title), { (data) in
            self.hideHUD()
            if data.Succeeded! {
                if self.jobsObject?.ExamType == 1 {
                    self.presentSingleButtonDialogDissmiss(alert: SingleAlert(title: AppLanguage.commonAppName, message: AppLanguage.jobsApplySuccessExamTypeMessage)) {
                         let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                               let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsApplyViewController") as! ISCMenuJobsApplyViewController
                               self.pushViewControler(vc, AppLanguage.userProfile)
                    }
                    return
                }else {
                    self.presentSingleButtonDialogDissmiss(alert: SingleAlert(title: AppLanguage.commonAppName, message: AppLanguage.jobsApplySuccessNotExamTypeMessage)) {
                         let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                               let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsApplyViewController") as! ISCMenuJobsApplyViewController
                               self.pushViewControler(vc, AppLanguage.userProfile)
                    }
                    return
                }
                
            }else {
                if data.Errors?.count ?? 0 > 0 {
                    let objError = data.Errors?[0]
                    if objError?.Description == "NOT_ENOUGH_INFO" || objError?.Code == "3" {
                        let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                                message: AppLanguage.jobsApplyFailNotEnoughInfoMessage,
                                                                preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .default))
                        alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
//                            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
//                            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuLoginViewController") as! ISCMenuLoginViewController
//                            vc.hidesBottomBarWhenPushed = true
//                            self.pushViewControler(vc, "")
                            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuUserProfileViewController") as! ISCMenuUserProfileViewController
                            self.pushViewControler(vc, AppLanguage.userProfile)
                        })
                        self.present(alertController, animated: true)
                        return
                    }
                    self.alertMessage(title: AppLanguage.commonAppName, message: objError?.Description ?? "")
                    return
                }
                
            }
            
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .default))
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
}

