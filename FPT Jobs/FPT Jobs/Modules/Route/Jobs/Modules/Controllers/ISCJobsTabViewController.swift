//
//  ISCJobsTabViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/25/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SignalRSwift

class ISCJobsTabViewController: UIBaseViewController, UIScrollViewDelegate {

    @IBOutlet weak var btnNewJob: UIButton!
    @IBOutlet weak var btnHotJob: UIButton!
    @IBOutlet weak var btnJobManager: UIButton!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var clvJob: UICollectionView!
    @IBOutlet weak var lblTiteJobSuit: UILabel!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var viewJobSuit: UIView!
    @IBOutlet weak var spaceTopJob: NSLayoutConstraint!
    
    var PageIndex: Int = 0
    var PageSize: Int = 10
    var currentPageIndex: Int = 1
    var Keyword: String = ""
    var JobCategory: String = ""
    var Area: String = ""
    var isLoadMore : Bool = false
    var timer = Timer()
    var type : String = "NEWS"
    var arrData : [ISCJobsOutput] = []
    var arrSuitableJobs : [ISCJobsOutput] = []
    override func viewDidLoad() {
        super.viewDidLoad()

       
        self.isSearchViewController = true
        self.isShowChat = true
        self.initNavController()
        
        let nibLoading = UINib(nibName: LoadingTableViewCell.name, bundle: nil)
        tblList.register(nibLoading, forCellReuseIdentifier: LoadingTableViewCell.name)
        let cellNib = UINib(nibName: ISCJobsTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCJobsTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 100
        
        
        let cellCLVNib = UINib(nibName: ISCSuitableJobsCollectionViewCell.name, bundle: Bundle.main)
        clvJob.register(cellCLVNib, forCellWithReuseIdentifier: ISCSuitableJobsCollectionViewCell.name)
        clvJob.delegate = self
        clvJob.dataSource = self
        
        self.showHUD(message: AppLanguage.connecting)
        setUIButton()

        self.btnNewJob.applyGradient(colours: [UIColor(hexString: "#2292C6"), UIColor(hexString: "#1569B2")])
        self.btnHotJob.applyGradient(colours: [UIColor(hexString: "#FBAB4A"), UIColor(hexString: "#F3792A")])
        self.btnJobManager.applyGradient(colours: [UIColor(hexString: "#14D164"), UIColor(hexString: "#24B24B")])
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = AppLanguage.jobsTabTitle
//        self.viewJobSuit.isHidden = true
        self.spaceTopJob.constant = -225
        arrData = []
        arrSuitableJobs = []
        loadNewsJobs()
        loadSuitableJobs()
    }
    
    func loadSuitableJobs() {
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            if let ID = saved!["ID"] as? Int {
                self.getSuitableJobs(ID, ISCSuitableJobsInput(PageIndex: 0, PageSize: 5), { (data) in
                    if let array = data.DataList {
                        if array.count > 0 {
                           UIView.animate(withDuration: 0.3) {
//                                self.viewJobSuit.isHidden = false
                                self.spaceTopJob.constant = 0
                                self.view.layoutIfNeeded()
                            }
                           
                        }
                        array.forEach { (val) in
                            if let value = val {
                                self.arrSuitableJobs.append(value)
                            }
                        }
                        self.clvJob.reloadData()
                    }
                }) { (error) in
                    self.alertMessage(title: AppLanguage.commonAppName, message: error)
                }
            }
        }
        
    }

    func loadNewsJobs(){
        self.getNewsJobs({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrData.append(value)
                    }
                }
            }
            if data.DataList?.count ?? 0 > 0 {
                if data.DataList?.count ?? 0 < 10 {
                    self.isLoadMore = false
                }else {
                    self.isLoadMore = true
                }
            }else {
                self.isLoadMore = false
            }
            if self.PageSize == 0 {
                self.tblList.setContentOffset(.zero, animated: true)
                self.spaceTopJob.constant = 0
            }
            self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }

    func loadHotsJobs(){
        self.getHotsJobs({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrData.append(value)
                    }
                }
            }
            if data.DataList?.count ?? 0 > 0 {
                if data.DataList?.count ?? 0 < 10 {
                    self.isLoadMore = false
                }else {
                    self.isLoadMore = true
                }
            }else {
                self.isLoadMore = false
            }
            if self.PageSize == 0 {
                self.tblList.setContentOffset(.zero, animated: true)
                self.spaceTopJob.constant = 0
            }
            self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }

    func loadJobsManager(){
        self.getJobsManager({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrData.append(value)
                    }
                }
            }	
            if data.DataList?.count ?? 0 > 0 {
                if data.DataList?.count ?? 0 < 10 {
                    self.isLoadMore = false
                }else {
                    self.isLoadMore = true
                }
            }else {
                self.isLoadMore = false
            }
            self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func setUIButton(){
        self.btnNewJob.layer.borderWidth = 1
        self.btnNewJob.layer.borderColor = UIColor.clear.cgColor
        self.btnNewJob.layer.cornerRadius = 6
        self.btnNewJob.layer.masksToBounds = true
        
        self.btnHotJob.layer.borderWidth = 1
        self.btnHotJob.layer.borderColor = UIColor.clear.cgColor
        self.btnHotJob.layer.cornerRadius = 6
        self.btnHotJob.layer.masksToBounds = true
        
        self.btnJobManager.layer.borderWidth = 1
        self.btnJobManager.layer.borderColor = UIColor.clear.cgColor
        self.btnJobManager.layer.cornerRadius = 6
        self.btnJobManager.layer.masksToBounds = true

    }
    @IBAction func btnAll_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCSuitableJobsViewController") as! ISCSuitableJobsViewController
        vc.hidesBottomBarWhenPushed = false
        self.pushViewControler(vc, AppLanguage.jobsTabTitle)
    }
    @IBAction func btnNewsJobs_click(_ sender: Any) {

        type = "NEWS"
        self.showHUD(message: AppLanguage.connecting)
        PageIndex = 0
        arrData = []
        loadNewsJobs()
        
    }
    @IBAction func btnHotsJobs_click(_ sender: Any) {

        type = "HOTS"
        self.showHUD(message: AppLanguage.connecting)
        PageIndex = 0
        arrData = []
        loadHotsJobs()
    }
    @IBAction func btnJobsManager_click(_ sender: Any) {

        type = "MANAGER"
        self.showHUD(message: AppLanguage.connecting)
        PageIndex = 0
        arrData = []
        loadJobsManager()
    }

    @IBAction func backAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuViewController") as! ISCMenuViewController
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnUserInfo_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuUserProfileViewController") as! ISCMenuUserProfileViewController
        self.pushViewControler(vc, AppLanguage.userProfile)
    }
    
    private var lastContentOffset: CGFloat = 0
    private var beginContentOffset: CGFloat = 0
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.beginContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if arrSuitableJobs.count > 0 {
            if (self.beginContentOffset > scrollView.contentOffset.y) {
                if scrollView.contentOffset.y <= 280 && self.spaceTopJob.constant != 0{
                   UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions(), animations: {
                       self.spaceTopJob.constant = -scrollView.contentOffset.y
                       self.view.layoutIfNeeded()
                   }, completion: nil)
                    print("y : \(scrollView.contentOffset.y); top : \(self.spaceTopJob.constant)")
                    
                }
            }
            else if (self.beginContentOffset < scrollView.contentOffset.y) {
                if scrollView.contentOffset.y <= 225 {
                    print("y : \(scrollView.contentOffset.y); top : \(self.spaceTopJob.constant)")
                     UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions(), animations: {
                          self.spaceTopJob.constant = -scrollView.contentOffset.y
                          self.view.layoutIfNeeded()
                      }, completion: nil)
                }
            }
            
            
            // update the new position acquired
//            self.beginContentOffset = scrollView.contentOffset.y
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.y)
    }

//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        if arrSuitableJobs.count > 0 {
//            if self.beginContentOffset < scrollView.contentOffset.y {
//                if self.spaceTopJob.constant >= 10 {
//                       UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions(), animations: {
//                           self.spaceTopJob.constant = 10
//                           self.view.layoutIfNeeded()
//                       }, completion: nil)
//                   }
//               }else if self.beginContentOffset > scrollView.contentOffset.y {
//                    if self.spaceTopJob.constant <= 225 {
//                         UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions(), animations: {
//                             self.spaceTopJob.constant = 225
//                             self.view.layoutIfNeeded()
//                         }, completion: nil)
//                }
//            }
//            self.beginContentOffset = scrollView.contentOffset.y
//        }
//
//    }
}

// MARK: - UITableView Delegate & Datasource
extension ISCJobsTabViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCJobsTableViewCell.name, for: indexPath) as? ISCJobsTableViewCell
            else {
                return UITableViewCell()
        }
        if arrData.count > 0 {
            let obj = arrData[indexPath.row]
            cell.lblTitle.text = obj.Title?.uppercased()
            if obj.MinSalary == nil && obj.MaxSalary == nil {
                cell.lblSalary.text = AppLanguage.jobsSalary
            }else {
                let min = Int(obj.MinSalary ?? "0")
                let max = Int(obj.MaxSalary ?? "0")
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0") \(obj.CurrentUnit ?? "")"
            }
            if type == "NEWS" {
                
                if obj.DisplayFlag == 1 {
                    cell.lblStatus.text = "HOT"
                    cell.viewStatus.isHidden = false
                }else {
                    cell.lblStatus.text = ""
                    cell.viewStatus.isHidden = true
                }
                 cell.viewStatus.backgroundColor = UIColor(hexString: "#F27228")
                cell.spaceRightTitleConstraint.constant = 60
            }else if type == "HOTS" {
                if obj.DisplayFlag == 1 {
                    cell.lblStatus.text = "NEW"
                    cell.viewStatus.isHidden = false
                }else {
                    cell.lblStatus.text = ""
                    cell.viewStatus.isHidden = true
                }
                cell.viewStatus.backgroundColor = UIColor(hexString: "#4EB848")
                cell.spaceRightTitleConstraint.constant = 60
            }else {
                cell.lblStatus.text = ""
                cell.viewStatus.isHidden = true
                cell.spaceRightTitleConstraint.constant = 10
            }
            cell.lblNumberPosition.text = "\(obj.NumberPositions ?? 0)"
            cell.lblAreaName.text = obj.AreaName
            cell.lblEndDate.text = obj.EndDateStr
            cell.selectionStyle = .none
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrData[indexPath.row]
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
        vc.RecruitmentId = obj.RecruitmentId ?? 0
        vc.TypePush = "MAIN"
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, AppLanguage.jobsTabTitle)
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tblList.dequeueReusableCell(withIdentifier: LoadingTableViewCell.name) as! LoadingTableViewCell
        cell.contentView.backgroundColor = .groupTableViewBackground
        return cell

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if isLoadMore {
            return 50
        }
        return 5
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrData.count - 1 {
            if isLoadMore {
                isLoadMore = false
                timer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(loadmore), userInfo: nil, repeats: true)
            }
        }
    }
    @objc func loadmore(){
        timer.invalidate()
        if type == "NEWS" {
            PageIndex += 10
            loadNewsJobs()
        }else if type == "HOTS"{
            PageIndex += 10
            loadHotsJobs()
        }else {
            PageIndex += 10
            loadJobsManager()
        }
    }
}


// MARK: - UICollectionView Delegate & Datasource
extension ISCJobsTabViewController:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrSuitableJobs.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: 300, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ISCSuitableJobsCollectionViewCell.name, for: indexPath) as! ISCSuitableJobsCollectionViewCell
        let obj = arrSuitableJobs[indexPath.row]
        cell.lblJobsName.text = obj.Title
        cell.lblPositionNumber.text = "\(obj.NumberPositions ?? 0)"
        cell.lblWorkArea.text = obj.AreaName
        cell.lblStartDate.text = obj.EndDateStr
        if obj.MinSalary == nil && obj.MaxSalary == nil {
            cell.lblSalary.text = AppLanguage.jobsSalary
        }else {
           let min = Int(obj.MinSalary ?? "0")
           let max = Int(obj.MaxSalary ?? "0")
           let nf = NumberFormatter()
           nf.numberStyle = .decimal
           cell.lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0") \(obj.CurrentUnit ?? "")"
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrSuitableJobs[indexPath.row]
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
        vc.RecruitmentId = obj.RecruitmentId ?? 0
        vc.TypePush = "MAIN"
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, AppLanguage.jobsTabTitle)
    }
}
