//
//  ISCNewsConnection.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/30/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
extension ISCJobsTabViewController {
    func getNewsJobs(
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.getJobsNews,isCheckHeader: false, ISCJobsInput(PageIndex: PageIndex, PageSize: PageSize, currentPageIndex: currentPageIndex, Keyword: Keyword, JobCategory: JobCategory, Area: Area), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getHotsJobs(
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.getJobsHots,isCheckHeader: false, ISCJobsInput(PageIndex: PageIndex, PageSize: PageSize, currentPageIndex: currentPageIndex, Keyword: Keyword, JobCategory: JobCategory, Area: Area), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getJobsManager(
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.getJobsManager,isCheckHeader: false, ISCJobsInput(PageIndex: PageIndex, PageSize: PageSize, currentPageIndex: currentPageIndex, Keyword: Keyword, JobCategory: JobCategory, Area: Area), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func getSuitableJobs(_ userId : Int,_ param : ISCSuitableJobsInput ,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection("\(ISCJobsConfig.getSuitableJobs)/\(userId)",isCheckHeader: false, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCSuitableJobsViewController {
    func getSuitableJobs(_ userId : Int,_ param : ISCSuitableJobsInput ,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection("\(ISCJobsConfig.getSuitableJobs)/\(userId)", param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCJobsDetailViewController {
    func getJobsDetail(
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsDetailOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection("\(ISCJobsConfig.getJobsDetail)/\(RecruitmentId)",isCheckHeader: false, ISCJobsDetailInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func applyJobs(_ param:ISCJobsApplySubmitInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCJobsApplySubmitOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCJobsConfig.applyJobs,isCheckHeader: false, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
