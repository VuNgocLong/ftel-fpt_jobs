//
//  ISCNewsConfig.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/30/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCJobsConfig {
    static let getSuitableJobs = K.serverAPI.api + "RecruitmentNews/GetSuitableJobs"
    static let getJobsNews = K.serverAPI.api + "RecruitmentNews/SearchJobNewsForMobile"
    static let getJobsHots = K.serverAPI.api + "RecruitmentNews/SearchJobHots"
    static let getJobsManager = K.serverAPI.api + "RecruitmentNews/SearchManagers"
    static let getJobsDetail = K.serverAPI.api + "RecruitmentNews/GetDetailRecruitmentNews"
    static let getJobsApply = K.serverAPI.api + "Applicant/AppliedListJob"
    static let getJobFollow = K.serverAPI.api + "Applicant/GetJobFollow"
    static let applyJobs = K.serverAPI.api + "Applicant/ApplyForMobile"
    static let acceptInterview = K.serverAPI.api + "Applicant/AcceptInterview"
    static let delayInterview = K.serverAPI.api + "Applicant/DelayInterview"
    static let denyInterview = K.serverAPI.api + "Applicant/DenyInterview"
    static let acceptJob = K.serverAPI.api + "Applicant/AcceptJob"
    static let delayJob = K.serverAPI.api + "Applicant/DelayJob"
    static let denyJob = K.serverAPI.api + "Applicant/DenyJob"
}
	
