//
//  ISCJobButtonTableViewCell.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 6/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCJobButtonTableViewCellDelegate: class {
    func FunctionJobNew ()
    func FunctionJobHot ()
    func FunctionJobManager ()
}
class ISCJobButtonTableViewCell: UITableViewCell {

    weak var delegate:ISCJobButtonTableViewCellDelegate?
    @IBOutlet weak var btnNewJob: UIButton!
    @IBOutlet weak var btnHotJob: UIButton!
    @IBOutlet weak var btnJobManager: UIButton!
    static var name: String {
        return String(describing: ISCJobButtonTableViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setUIButton()
               
       self.btnNewJob.applyGradient(colours: [UIColor(hexString: "#2292C6"), UIColor(hexString: "#1569B2")])
       self.btnHotJob.applyGradient(colours: [UIColor(hexString: "#FBAB4A"), UIColor(hexString: "#F3792A")])
       self.btnJobManager.applyGradient(colours: [UIColor(hexString: "#14D164"), UIColor(hexString: "#24B24B")])
    }
    
    func setUIButton(){
        self.btnNewJob.layer.borderWidth = 1
        self.btnNewJob.layer.borderColor = UIColor.clear.cgColor
        self.btnNewJob.layer.cornerRadius = 6
        self.btnNewJob.layer.masksToBounds = true
        
        self.btnHotJob.layer.borderWidth = 1
        self.btnHotJob.layer.borderColor = UIColor.clear.cgColor
        self.btnHotJob.layer.cornerRadius = 6
        self.btnHotJob.layer.masksToBounds = true
        
        self.btnJobManager.layer.borderWidth = 1
        self.btnJobManager.layer.borderColor = UIColor.clear.cgColor
        self.btnJobManager.layer.cornerRadius = 6
        self.btnJobManager.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnNew_touch(_ sender: Any) {
        self.delegate?.FunctionJobNew()
    }
    @IBAction func btnHot_touch(_ sender: Any) {
         self.delegate?.FunctionJobHot()
    }
    @IBAction func btnManager_touch(_ sender: Any) {
         self.delegate?.FunctionJobManager()
    }

}
