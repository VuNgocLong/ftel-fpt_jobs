//
//  LoadingTableViewCell.swift
//  Fox Steps
//
//  Created by PHAM CHI HIEU on 7/13/19.
//  Copyright © 2019 FPT. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    @IBOutlet weak var activityLoading: UIActivityIndicatorView!
    static var name: String {
        return String(describing: LoadingTableViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
