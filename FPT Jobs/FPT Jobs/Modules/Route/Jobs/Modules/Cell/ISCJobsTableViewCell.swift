//
//  ISCJobsTableViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/25/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCJobsTableViewCell: UITableViewCell {

    @IBOutlet weak var viewJob: UIView!
    @IBOutlet weak var lblNumberPosition: UIAutoSizeLabel!
    @IBOutlet weak var lblTitle: UIAutoSizeLabelTitle!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblAreaName: UIAutoSizeLabel!
    @IBOutlet weak var lblSalary: UIAutoSizeLabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lblStatus: UIAutoSizeLabel!
    @IBOutlet weak var spaceRightTitleConstraint: NSLayoutConstraint!
    static var name: String {
           return String(describing: ISCJobsTableViewCell.self)
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.viewJob.layer.borderWidth = 1
        self.viewJob.layer.borderColor = UIColor.clear.cgColor
        self.viewJob.layer.cornerRadius = 8
        self.viewJob.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
