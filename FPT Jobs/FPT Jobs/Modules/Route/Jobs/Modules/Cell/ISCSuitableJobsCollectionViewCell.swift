//
//  ISCSuitableJobsCollectionViewCell.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/20/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCSuitableJobsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblJobsName: UIAutoSizeLabel!
    @IBOutlet weak var lblPositionNumber: UIAutoSizeLabel!
    @IBOutlet weak var lblStartDate: UIAutoSizeLabel!
    @IBOutlet weak var lblWorkArea: UIAutoSizeLabel!
    @IBOutlet weak var lblSalary: UIAutoSizeLabel!
    static var name: String {
        return String(describing: ISCSuitableJobsCollectionViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
