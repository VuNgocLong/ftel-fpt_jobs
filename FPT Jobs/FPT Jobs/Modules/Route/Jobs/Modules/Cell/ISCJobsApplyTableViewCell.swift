//
//  ISCJobsApplyTableViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
//protocol ISCJobsApplyTableViewCellDelegate: class {
//    func testingJobs (index : Int)
//}
class ISCJobsApplyTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UIAutoSizeLabel!
    @IBOutlet weak var lblDate: UIAutoSizeLabel!
    @IBOutlet weak var lblStatus: UIAutoSizeLabel!
    @IBOutlet weak var lblJobsStatus: UIAutoSizeLabel!
    @IBOutlet weak var btnTestJob: UIAutoSizeButton!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var viewJobsStatus: UIView!
    @IBOutlet weak var spaceRightViewMenu: NSLayoutConstraint!
    @IBOutlet weak var spaceRightButtonStatus: NSLayoutConstraint!
    //    weak var delegate:ISCJobsApplyTableViewCellDelegate?
    static var name: String {
        return String(describing: ISCJobsApplyTableViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgDown.image = UIImage(named: "ic_arrow_down")?.withColor(UIColor(hexString: "#FFFFFF"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnTestJob_click(_ sender: Any) {
        
       
    }
}
