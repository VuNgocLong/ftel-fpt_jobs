//
//  ISCSuitableJobTableViewCell.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 6/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCSuitableJobTableViewCellDelegate: class {
    func FunctionJobNew ()
    func FunctionJobHot ()
    func FunctionJobManager ()
    func FunctionAll ()
}
class ISCSuitableJobTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    weak var delegate:ISCSuitableJobTableViewCellDelegate?
    @IBOutlet weak var clvJob: UICollectionView!
    @IBOutlet weak var lblTiteJobSuit: UILabel!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnNewJob: UIButton!
    @IBOutlet weak var btnHotJob: UIButton!
    @IBOutlet weak var btnJobManager: UIButton!
    static var name: String {
        return String(describing: ISCSuitableJobTableViewCell.self)
    }
    var arrSuitableJobs : [ISCJobsOutput] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let cellCLVNib = UINib(nibName: ISCSuitableJobsCollectionViewCell.name, bundle: Bundle.main)
        clvJob.register(cellCLVNib, forCellWithReuseIdentifier: ISCSuitableJobsCollectionViewCell.name)
        clvJob.delegate = self
        clvJob.dataSource = self
        
        setUIButton()
        
        self.btnNewJob.applyGradient(colours: [UIColor(hexString: "#2292C6"), UIColor(hexString: "#1569B2")])
        self.btnHotJob.applyGradient(colours: [UIColor(hexString: "#FBAB4A"), UIColor(hexString: "#F3792A")])
        self.btnJobManager.applyGradient(colours: [UIColor(hexString: "#14D164"), UIColor(hexString: "#24B24B")])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUIButton(){
        self.btnNewJob.layer.borderWidth = 1
        self.btnNewJob.layer.borderColor = UIColor.clear.cgColor
        self.btnNewJob.layer.cornerRadius = 6
        self.btnNewJob.layer.masksToBounds = true
        
        self.btnHotJob.layer.borderWidth = 1
        self.btnHotJob.layer.borderColor = UIColor.clear.cgColor
        self.btnHotJob.layer.cornerRadius = 6
        self.btnHotJob.layer.masksToBounds = true
        
        self.btnJobManager.layer.borderWidth = 1
        self.btnJobManager.layer.borderColor = UIColor.clear.cgColor
        self.btnJobManager.layer.cornerRadius = 6
        self.btnJobManager.layer.masksToBounds = true

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSuitableJobs.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: 300, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ISCSuitableJobsCollectionViewCell.name, for: indexPath) as! ISCSuitableJobsCollectionViewCell
        let obj = arrSuitableJobs[indexPath.row]
        cell.lblJobsName.text = obj.Title?.uppercased()
        cell.lblPositionNumber.text = "\(obj.NumberPositions ?? 0)"
        cell.lblWorkArea.text = obj.AreaName
        cell.lblStartDate.text = obj.EndDateStr
        if obj.MinSalary == nil && obj.MaxSalary == nil {
            cell.lblSalary.text = AppLanguage.jobsSalary
        }else {
           let min = Int(obj.MinSalary ?? "0")
           let max = Int(obj.MaxSalary ?? "0")
           let nf = NumberFormatter()
           nf.numberStyle = .decimal
           cell.lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0") \(obj.CurrentUnit ?? "")"
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrSuitableJobs[indexPath.row]
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
        vc.RecruitmentId = obj.RecruitmentId ?? 0
        vc.TypePush = "MAIN"
        vc.hidesBottomBarWhenPushed = true
//        self.pushViewControler(vc, AppLanguage.jobsTabTitle)
    }
    
    @IBAction func btnNew_touch(_ sender: Any) {
        self.delegate?.FunctionJobNew()
    }
    @IBAction func btnHot_touch(_ sender: Any) {
        self.delegate?.FunctionJobHot()
    }
    @IBAction func btnManager_touch(_ sender: Any) {
        self.delegate?.FunctionJobManager()
    }
    @IBAction func btnAll_touch(_ sender: Any) {
        self.delegate?.FunctionAll()
    }
}
