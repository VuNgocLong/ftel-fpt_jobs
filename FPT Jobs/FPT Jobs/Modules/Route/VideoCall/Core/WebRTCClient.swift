//
//  WebRTCClient
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//


import Foundation
import WebRTC

public enum WebRTCClientState {
    case disconnected
    case connecting
    case connected
}
public protocol WebRTCClientDelegate: class {
    func webRTCClient(client : WebRTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack)
    func webRTCClient(client : WebRTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack)
    func webRTCClient(client : WebRTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer)
    func webRTCClient(client : WebRTCClient, didChangeState state: WebRTCClientState)
    func webRTCClient(client : WebRTCClient, didChangeConnectionState connectionState: RTCIceConnectionState)
    func webRTCClient(client : WebRTCClient, didGenerateIceCandidate candidate: RTCIceCandidate)
    func webRTCClient(client : WebRTCClient, startCallWithSdp rtcSessionDescription: RTCSessionDescription)
    func webRTCClient(client : WebRTCClient, didReceiveError error: Error)
}

public struct ErrorDomain {
    public static let videoPermissionDenied = "Video permission denied"
    public static let audioPermissionDenied = "Audio permission denied"
}

public class WebRTCClient: NSObject{
    
    fileprivate var iceServers: [RTCIceServer] = [
        RTCIceServer(urlStrings: ["stun:118.69.164.52:2222"]),
        RTCIceServer(urlStrings: ["turn:118.69.164.52:2222?transport=tcp"], username:"stun",credential: "9j<LM{.+%D>>ps=x[,_^Y.S%u#535d:%"),
        RTCIceServer(urlStrings: ["turn:118.69.164.52:2222?transport=udp"], username:"stun",credential: "9j<LM{.+%D>>ps=x[,_^Y.S%u#535d:%")
    ]
//    fileprivate var peerConnection: RTCPeerConnection?
    fileprivate var peerConnections = [String: RTCPeerConnection]()
    fileprivate var connectionFactory: RTCPeerConnectionFactory = RTCPeerConnectionFactory()
    fileprivate var audioTrack: RTCAudioTrack? // Save instance to be able to mute the call
    fileprivate var remoteIceCandidates: [RTCIceCandidate] = []
    fileprivate var isVideoCall = true

    public weak var delegate: WebRTCClientDelegate?

    fileprivate let audioCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio" : "true"],
                                                         optionalConstraints: nil)
    fileprivate let videoCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio" : "true", "OfferToReceiveVideo": "true"],
                                                                     optionalConstraints: nil)
    var callConstraint : RTCMediaConstraints {
        return self.isVideoCall ? self.videoCallConstraint : self.audioCallConstraint
    }

    fileprivate let defaultConnectionConstraint = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: ["DtlsSrtpKeyAgreement": "true"])

    fileprivate var mediaConstraint: RTCMediaConstraints {
        let constraints = ["minWidth": "0", "minHeight": "0", "maxWidth" : "480", "maxHeight": "640"]
        return RTCMediaConstraints(mandatoryConstraints: constraints, optionalConstraints: nil)
    }

    private var state: WebRTCClientState = .connecting {
        didSet {
            self.delegate?.webRTCClient(client: self, didChangeState: state)
        }
    }
    var arrConnectId : [String] = []

    public override init() {
        super.init()
    }

    public convenience init(videoCall: Bool = true) {
        self.init()
        self.isVideoCall = videoCall
//        createRTCPeerConnection()
    }

    deinit {
//        guard let peerConnection = self.peerConnection else {
//            return
//        }
//        if let stream = peerConnection.localStreams.first {
//            audioTrack = nil
//            peerConnection.remove(stream)
//        }
//
        for peerConnection in peerConnections {
            if let stream = peerConnection.value.localStreams.first {
                audioTrack = nil
                peerConnection.value.remove(stream)
            }
        }
    }
    func createRTCPeerConnection(conectionId : String) {
        RTCPeerConnectionFactory.initialize()
        self.connectionFactory = RTCPeerConnectionFactory()

        let configuration = RTCConfiguration()
        configuration.iceServers = self.iceServers
        self.peerConnections[conectionId] = self.connectionFactory.peerConnection(with: configuration,
                                                                    constraints: self.defaultConnectionConstraint,
                                                                    delegate: self)
        self.state = .connecting
        let localStream = self.localStream()
        self.peerConnections[conectionId]?.add(localStream)
        if let localVideoTrack = localStream.videoTracks.first {
            self.delegate?.webRTCClient(client: self, didReceiveLocalVideoTrack: localVideoTrack)
        }
    }

    func startConnection(conectionId : String) {
        guard let peerConnection = self.peerConnections[conectionId] else {
            return
        }
        self.state = .connecting
        let localStream = self.localStream()
        peerConnection.add(localStream)
        if let localVideoTrack = localStream.videoTracks.first {
            self.delegate?.webRTCClient(client: self, didReceiveLocalVideoTrack: localVideoTrack)
        }
    }

    public func createOfffer(conectionId : String) {
        guard let peerConnection = self.peerConnections[conectionId] else {
            return
        }
        peerConnection.offer(for: self.callConstraint) { (sdpDescription, error) in
            if let error = error {
                self.delegate?.webRTCClient(client: self, didReceiveError: error)
            }else {
                guard let sdpDesc = sdpDescription  else {
                    return
                }
                self.peerConnections[conectionId]?.setLocalDescription(sdpDesc, completionHandler: { (error) in
                    if let error = error {
                        self.delegate?.webRTCClient(client: self, didReceiveError: error)
                    }else {
                        self.delegate?.webRTCClient(client: self, startCallWithSdp: sdpDesc)
                    }
                })
            }
        }
    }

    public func muteCall(_ mute: Bool) {
        self.audioTrack?.isEnabled = !mute
    }
    
    func onAnswer(_ sdp: String, conectionId : String) {
        guard let peerConnection = self.peerConnections[conectionId] else {
            return
        }
         let desc = RTCSessionDescription(type: .answer, sdp: sdp)
         peerConnection.setRemoteDescription(desc) { (error) in
             if error == nil {
                 self.handleRemoteDescriptionSet(conectionId: conectionId)
             } else {
                self.delegate?.webRTCClient(client: self, didReceiveError: error!)
             }
         }
     }

    func onOffer(_ sdp: String, conectionId : String) {
        guard let peerConnection = self.peerConnections[conectionId] else {
            return
        }
        let desc = RTCSessionDescription(type: .offer, sdp: sdp)
        peerConnection.setRemoteDescription(desc) { (error) in
            if error == nil {
                self.handleRemoteDescriptionSet(conectionId: conectionId)
                peerConnection.answer(for: self.callConstraint, completionHandler:
                    { (sdpDescription, error) in
                        if error == nil {
                            self.delegate?.webRTCClient(client: self, startCallWithSdp: sdpDescription!)
                            self.state = .connected
                        } else {
                            self.delegate?.webRTCClient(client: self, didReceiveError: error!)
                        }
                })
            } else {
               self.delegate?.webRTCClient(client: self, didReceiveError: error!)
            }
        }
    }

    func handleRemoteDescriptionSet(conectionId : String) {
        for iceCandidate in self.remoteIceCandidates {
            self.peerConnections[conectionId]?.add(iceCandidate)
        }
        self.remoteIceCandidates = []
    }

    // Generate local stream and keep it live and add to new peer connection
    func localStream() -> RTCMediaStream {
        let factory = self.connectionFactory
        let localStream = factory.mediaStream(withStreamId: "RTCmS")

        if self.isVideoCall {
            if !AVCaptureState.isVideoDisabled {
                let videoSource: RTCVideoSource = factory.videoSource()
                let capturer = RTCCameraVideoCapturer(delegate: videoSource)
                self.delegate?.webRTCClient(client: self, didCreateLocalCapturer: capturer)
                let videoTrack = factory.videoTrack(with: videoSource, trackId: "RTCvS0")
                videoTrack.isEnabled = true
                localStream.addVideoTrack(videoTrack)
            } else {
                // show alert for video permission disabled
                let error = NSError.init(domain: ErrorDomain.videoPermissionDenied, code: 0, userInfo: nil)
                self.delegate?.webRTCClient(client: self, didReceiveError: error)
            }
        }

        if !AVCaptureState.isAudioDisabled {
            let audioTrack = factory.audioTrack(withTrackId: "RTCaS0")
            self.audioTrack = audioTrack
           
            localStream.addAudioTrack(audioTrack)
        } else {
            // show alert for audio permission disabled
            let error = NSError.init(domain: ErrorDomain.audioPermissionDenied, code: 0, userInfo: nil)
            self.delegate?.webRTCClient(client: self, didReceiveError: error)
        }
        
        return localStream
    }

    func initialisePeerConnectionFactory () {
        RTCPeerConnectionFactory.initialize()
        self.connectionFactory = RTCPeerConnectionFactory()
    }

//    func initialisePeerConnection () {
//        let configuration = RTCConfiguration()
//        configuration.iceServers = self.iceServers
//        self.peerConnection = self.connectionFactory.peerConnection(with: configuration,
//                                                                    constraints: self.defaultConnectionConstraint,
//                                                                    delegate: self)
//    }


    func addIceCandidate(iceCandidate: RTCIceCandidate, conectionId : String) {
           // Set ice candidate after setting remote description
           if self.peerConnections[conectionId]?.remoteDescription != nil {
               self.peerConnections[conectionId]?.add(iceCandidate)
           } else {
               self.remoteIceCandidates.append(iceCandidate)
           }
    }
}

extension WebRTCClient : RTCPeerConnectionDelegate {
    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        if stream.videoTracks.count > 0 {
            self.delegate?.webRTCClient(client: self, didReceiveRemoteVideoTrack: stream.videoTracks[0])
        }
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        self.delegate?.webRTCClient(client: self, didChangeConnectionState: newState)
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {

    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        self.delegate?.webRTCClient(client: self, didGenerateIceCandidate: candidate)
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        
    }
    
}
