//
//  AVCaptureState
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//


import Foundation
import AVFoundation

class AVCaptureState {
    static var isVideoDisabled: Bool {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        return status == .restricted || status == .denied
    }

    static var isAudioDisabled: Bool {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        return status == .restricted || status == .denied
    }
}
