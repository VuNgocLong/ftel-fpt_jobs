//
//  ISCInterViewVideoCallViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import WebRTC

let TAG = "ISCInterViewVideoCallViewController"
let AUDIO_TRACK_ID = TAG + "AUDIO"
let LOCAL_MEDIA_STREAM_ID = TAG + "STREAM"

class ISCInterViewVideoCallViewController: UIBaseViewController, ISCVideoCallConfirmModalDelegate {

    @IBOutlet weak var btnMuti: UIButton!
    @IBOutlet weak var btnCalling: UIAutoSizeButton!
    @IBOutlet weak var btnEnCall: UIAutoSizeButton!
    @IBOutlet weak var lblTime: UIAutoSizeLabel!
    @IBOutlet private weak var localView: RTCEAGLVideoView!
    @IBOutlet private weak var localViewThum: RTCEAGLVideoView!
    @IBOutlet private weak var localViewThum1: RTCEAGLVideoView!
    @IBOutlet private weak var localViewThum2: RTCEAGLVideoView!
    @IBOutlet private weak var remoteView: RTCEAGLVideoView!
    @IBOutlet weak var lblWelcome: UIAutoSizeLabel!
    
    var videoClient: WebRTCClient?
    var captureController: RTCCapturer!
    var localVideoView: RTCMTLVideoView?
    var remoteVideoView: RTCMTLVideoView?
    var localVideoTrack: RTCVideoTrack?
    var remoteVideoTracks: [RTCVideoTrack] = []
    var remoteVideoTrack: RTCVideoTrack?
    var remoteVideoTrack1: RTCVideoTrack?
    var remoteVideoTrack2: RTCVideoTrack?
    var localViewThums: [RTCEAGLVideoView] = []
    private var videoCapturer: RTCVideoCapturer?
    
    private var localDataChannel: RTCDataChannel?
    private var remoteDataChannel: RTCDataChannel?
    var arrConnectId : [String] = []
    var ipConnect : String = ""
    var timeMinus : Int = 0
    var timeSecond : Int = 0
    var timer = Timer()
    var isMutiAudio : Bool = false
    var indexSelectThum : Int = 0
    var startDateExam : Date = Date()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = AppLanguage.jobsInterViewOnline
        setupUI()
        self.localView.delegate = self
        self.remoteView.delegate = self
        configureVideoClient()
        
       
        let gestureRecognizer:UIGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLocalViewThum))
        localViewThum.addGestureRecognizer(gestureRecognizer)
        
        let gestureRecognizer1:UIGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLocalViewThum1))
        localViewThum1.addGestureRecognizer(gestureRecognizer1)
        
        let gestureRecognizer2:UIGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLocalViewThum2))
        localViewThum2.addGestureRecognizer(gestureRecognizer2)
               
    }
    
    func setupUI(){

        SignalRManager.sharedInstance.delegate = self
        SignalRManager.sharedInstance.signalRConnect()
        btnMuti.isHidden = true
        btnCalling.isHidden = true
        btnEnCall.isHidden = true
        lblWelcome.isHidden = true
        
        self.localView.layer.borderWidth = 2
        self.localView.layer.borderColor = UIColor.clear.cgColor
        self.localView.layer.cornerRadius = localView.frame.size.width / 2
        self.localView.layer.masksToBounds = true
        
        self.localViewThum.layer.borderWidth = 2
        self.localViewThum.borderColor = UIColor(hexString: "#00DC24")
        self.localViewThum.layer.cornerRadius = (localViewThum.frame.size.width ) / 2
        self.localViewThum.layer.masksToBounds = true
        self.localViewThum.isHidden = true
        
        self.localViewThum1.layer.borderWidth = 2
        self.localViewThum1.layer.borderColor = UIColor.clear.cgColor
        self.localViewThum1.layer.cornerRadius = (localViewThum1.frame.size.width ) / 2
        self.localViewThum1.layer.masksToBounds = true
        self.localViewThum1.isHidden = true
        
        self.localViewThum2.layer.borderWidth = 2
        self.localViewThum2.layer.borderColor = UIColor.clear.cgColor
        self.localViewThum2.layer.cornerRadius = (localViewThum2.frame.size.width ) / 2
        self.localViewThum2.layer.masksToBounds = true
        self.localViewThum2.isHidden = true

        
        
    }

    func configureVideoClient() {
        let client = WebRTCClient(videoCall: true)
        client.delegate = self
        self.videoClient = client
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
           
    }
     @objc func appBecomeActive() {
        calculatorHour()
    }
        
        @objc func appDidEnterBackground() {
    //        startDateExam = Date()
        }
    func calculatorHour(){
        
        let secondDate = Date()
        let distanceBetweenDates: TimeInterval? = startDateExam.timeIntervalSince(secondDate)
        
        let secondsInAnMinuts: Double = 60
        let minutes = Int((distanceBetweenDates! / secondsInAnMinuts))

        let seconds = Int(distanceBetweenDates ?? 0) - (minutes * 60)
        timeMinus = Int(minutes)
        timeSecond = Int(seconds)
        calcular()

    }
    
    @objc func calcular(){

        if timeSecond == 0 {
            if timeMinus > 0 {
                timeSecond = 59
                timeMinus -= 1
            }else {
                timer.invalidate()
                SignalRManager.sharedInstance.endCall()
                self.navigationController?.popViewController(animated: true)
            }
        }else {
            timeSecond -= 1
        }
        let strTimeMinus : String = timeMinus < 10 ? "0\(timeMinus)" : "\(timeMinus)"
        let strTimeSecond : String = timeSecond < 10 ? "0\(timeSecond)" : "\(timeSecond)"
        lblTime.text = "\(strTimeMinus) : \(strTimeSecond)"
    }
    
    @objc func tapLocalViewThum() {
        self.remoteVideoTracks[indexSelectThum].remove(self.remoteView)
        indexSelectThum = 0
        clearFocus()
        self.localViewThum.layer.borderColor = UIColor(hexString: "#00DC24").cgColor
        self.remoteVideoTracks[indexSelectThum].add(self.remoteView)
        self.view.layoutIfNeeded()
    }
    
    @objc func tapLocalViewThum1() {
        self.remoteVideoTracks[indexSelectThum].remove(self.remoteView)
        indexSelectThum = 1
        clearFocus()
        self.localViewThum1.borderColor = UIColor(hexString: "#00DC24")
        self.remoteVideoTracks[indexSelectThum].add(self.remoteView)
        self.view.layoutIfNeeded()
    }
    
    @objc func tapLocalViewThum2() {
        self.remoteVideoTracks[indexSelectThum].remove(self.remoteView)
        indexSelectThum = 2
        clearFocus()
        self.localViewThum2.borderColor = UIColor(hexString: "#00DC24")
        self.remoteVideoTracks[indexSelectThum].add(self.remoteView)
        self.view.layoutIfNeeded()
    }
    
    @IBAction func btnMuti_touch(_ sender: Any) {
        if isMutiAudio {
            isMutiAudio = false
            btnMuti.setImage(UIImage(named: "icon_microphone"), for: .normal)
        }else {
            isMutiAudio = true
            btnMuti.setImage(UIImage(named: "icon_muti"), for: .normal)
        }
        videoClient?.muteCall(isMutiAudio)
    }

    @IBAction func btnIncomingCall_touch(_ sender: Any) {
        SignalRManager.sharedInstance.accept()
        
    }
    @IBAction func btnEndCall_touch(_ sender: Any) {
        timer.invalidate()
        SignalRManager.sharedInstance.endCall()
    }
    
    override func backAction() {
        SignalRManager.sharedInstance.onLeavingPage()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func backAvatarAction() {
        SignalRManager.sharedInstance.onLeavingPage()
        if let view = self.navigationController?.viewControllers {
             var arrViewControllerMain: [UIViewController] = []
             for n in 0...view.count - 3 {
                 arrViewControllerMain.append(view[n])
             }
             
             self.navigationController?.viewControllers = arrViewControllerMain
        }
    }
    
    func clearFocus() {
        self.localViewThum.layer.borderColor = UIColor.clear.cgColor
        self.localViewThum1.layer.borderColor = UIColor.clear.cgColor
        self.localViewThum2.layer.borderColor = UIColor.clear.cgColor
    }
    
    func hideLocalThum() {
        self.localViewThum.isHidden = true
        self.localViewThum1.isHidden = true
        self.localViewThum2.isHidden = true
    }
}

//Delegate RignalR
extension ISCInterViewVideoCallViewController : SignalRVideoCallDelegate {
    
    func acceptCalling() {
        SignalRManager.sharedInstance.accept()
    }
    
    func endCalling() {
        SignalRManager.sharedInstance.endCall()
    }

     func didChangeState(_ status: SignalRManagerState) {
        switch status {
        case .disconnected:
            SignalRManager.sharedInstance.signalRConnect()
            break
        case .connecting:
            break
        case .connected:
            let userProfile: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
            if userProfile != nil {
                SignalRManager.sharedInstance.regisUser(UserDefaults.standard.getValue(key: Common.keyAccessToken))
            }
            break
        case .reconnecting:
            break
        }
    }


    func didRegisterUser(_ status: SignalRVideoAuthenState, _ connectionID: String) {
        switch status {
        case .success:
             self.videoClient?.createRTCPeerConnection(conectionId: connectionID)
            break
        case .authfailed:
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.unauthorizedMessage)
            break
        }
    }

    func didRecieveCalling(_ status: CallingState, _ ips: [String], _ time : Double) {
        switch status {
        case .callaccepted:
            
            self.arrConnectId = ips
            if ips.count > 0 {
                self.ipConnect = ips[0]
            }
            for ip in self.arrConnectId  {
                self.videoClient?.createRTCPeerConnection(conectionId: ip)
                self.videoClient?.createOfffer(conectionId: ip)
            }
            
            if time > 0{
                timeMinus = Int(time)
                timeSecond = Int((time - Double(timeMinus)) * 60)
                let date = Date()
                startDateExam = Calendar.current.date(byAdding: .minute, value: timeMinus, to: date)!
                startDateExam = Calendar.current.date(byAdding: .second, value: timeSecond, to: startDateExam)!
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(calcular), userInfo: nil, repeats: true)
                lblWelcome.text = lblWelcome.text?.replacingOccurrences(of: "{time}", with: "\(timeMinus)")
            }
            lblWelcome.isHidden = false
            self.btnMuti.isHidden = false
            self.btnEnCall.isHidden = false

            break
        case .callcanceled:
            break
        case .incomingcall:

            let modal = ISCVideoCallConfirmModal()
            modal.modalPresentationStyle = .fullScreen
            modal.modalTransitionStyle = .coverVertical
            modal.delegate = self
            self.present(modal, animated: true, completion: nil)
            break
        case .cantacceptcall:
            break
        case .callended:
            self.navigationController?.popViewController(animated: true)
            break
        }
    }
    func didDisconnectedUser(_ connectionID: String) {
        var index : Int = 0
        for item in arrConnectId {
            if item == connectionID {
                arrConnectId.remove(at: index)
                remoteVideoTracks.remove(at: index)
                remoteVideoTrack?.remove(localViewThums[index])
                localViewThums.remove(at: index)
                if indexSelectThum == index {
                    remoteVideoTrack?.remove(remoteView)
                    indexSelectThum = 0
                }
            }
            index += 1
        }
        timer.invalidate()
        clearRemoteTrack()
        addRemoteTrack()
    }
    func didReceiveOffer(_ sdp: String, _ connectionID: String) {
        if arrConnectId.contains(connectionID) {
            videoClient?.onOffer(sdp, conectionId: connectionID)
        }else {
            self.ipConnect = connectionID
            arrConnectId.append(connectionID)
            switch arrConnectId.count {
            case 1:
                self.localViewThum.isHidden = false
                self.localViewThums[indexSelectThum].borderColor = UIColor(hexString: "#00DC24")
                self.view.layoutIfNeeded()
                break
            case 2:
                self.localViewThum1.isHidden = false
                break
            case 3:
                self.localViewThum2.isHidden = false
                break
            default:
                break
            }
            self.videoClient?.createRTCPeerConnection(conectionId: connectionID)
            self.videoClient?.createOfffer(conectionId: connectionID)
        }
    }

    func didRecieveAnswer(_ sdp: String, _ connectionID: String) {
        videoClient?.onAnswer(sdp, conectionId: connectionID)
         switch arrConnectId.count {
           case 1:
               self.localViewThum.isHidden = false
               self.localViewThums[indexSelectThum].borderColor = UIColor(hexString: "#00DC24")
               self.view.layoutIfNeeded()
               break
           case 2:
               self.localViewThum1.isHidden = false
               break
           case 3:
               self.localViewThum2.isHidden = false
               break
           default:
               break
           }
    }

    func didRecieveICECandidate(candidate: String, sdpMLineIndex: Int32, sdpMid: String?, isCandidate: Bool, _ conectionID: String) {
        self.videoClient?.addIceCandidate(iceCandidate: RTCIceCandidate(sdp: candidate, sdpMLineIndex: sdpMLineIndex, sdpMid: sdpMid), conectionId: conectionID)
       
    }
    
    func clearRemoteTrack(){
        var index : Int = 0
        for remoteVideoTrack in remoteVideoTracks {
            remoteVideoTrack.remove(localViewThums[index])
            index += 1
        }
    }
    func addRemoteTrack(){
        hideLocalThum()
        clearFocus()
        localViewThums = []
        var index : Int = 0
        for remoteVideoTrack in remoteVideoTracks {
            if index == 0 {
                localViewThums.append(self.localViewThum)
                self.localViewThum.isHidden = false
            }else if index == 1 {
                localViewThums.append(self.localViewThum1)
                self.localViewThum1.isHidden = false
            }else if index == 2 {
                localViewThums.append(self.localViewThum2)
                self.localViewThum2.isHidden = false
            }
            remoteVideoTrack.add(localViewThums[index])
            index += 1
        }
        if remoteVideoTracks.count > 0 {
            remoteVideoTracks[indexSelectThum].add(remoteView)
            self.localViewThums[indexSelectThum].borderColor = UIColor(hexString: "#00DC24")
            self.view.layoutIfNeeded()
        }
        
    }
}

//Delegate WebRTC
extension ISCInterViewVideoCallViewController : WebRTCClientDelegate, RTCVideoViewDelegate {
    
    func webRTCClient(client: WebRTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack) {
        
        
        if (self.localVideoTrack != nil) {
            self.localVideoTrack?.remove(self.localView)
            self.localVideoTrack = nil
            self.localView.renderFrame(nil)
        }
        self.localVideoTrack = localVideoTrack
        self.localVideoTrack?.add(self.localView)
    }
    
    func webRTCClient(client: WebRTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack) {
        remoteVideoTracks.append(remoteVideoTrack)
        if arrConnectId.count == 1 {
            self.remoteVideoTrack = remoteVideoTrack
            self.remoteVideoTracks[0].add(self.remoteView)
            self.remoteVideoTracks[0].add(self.localViewThum)
            self.localViewThums.append(self.localViewThum)
        }else if arrConnectId.count == 2 {
//            self.remoteVideoTrack[1] = remoteVideoTrack
         
            self.remoteVideoTracks[1].add(self.localViewThum1)
            self.localViewThums.append(self.localViewThum1)
        }else if arrConnectId.count == 3 {
//            self.remoteVideoTrack[2] = remoteVideoTrack
            self.remoteVideoTracks[2].add(self.localViewThum2)
            self.localViewThums.append(self.localViewThum2)
        }
      
    }
    
    func webRTCClient(client: WebRTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer) {
    // To handle when camera is not available
        let settingsModel = RTCCapturerSettingsModel()
        self.captureController = RTCCapturer.init(withCapturer: capturer, settingsModel: settingsModel)
        captureController.startCapture()
    }
    
    func webRTCClient(client: WebRTCClient, startCallWithSdp rtcSessionDescription: RTCSessionDescription) {
        
        let json:[String: Any] = [
            "isCandidate" : true,
            "sdp"  : [
                "type" : "offer",
                "sdp" : rtcSessionDescription.sdp.description
            ]
        ]
        SignalRManager.sharedInstance.sendOffer(json.dict2json(), self.ipConnect)
        
    }
    
    func webRTCClient(client: WebRTCClient, didGenerateIceCandidate candidate: RTCIceCandidate) {
        let json:[String: Any] = [
            "isCandidate" : true,
            "candidate" : [
                "candidate" : candidate.sdp as AnyObject,
                "serverUrl" : candidate.serverUrl as AnyObject,
                "sdpMLineIndex" : candidate.sdpMLineIndex as AnyObject,
                "sdpMid" : candidate.sdpMid as AnyObject
            ]
        ]
        SignalRManager.sharedInstance.sendICECandidate(json.dict2json(), self.ipConnect)
    }
    
    func webRTCClient(client: WebRTCClient, didChangeConnectionState connectionState: RTCIceConnectionState) {
        
    }
    
    
    func webRTCClient(client: WebRTCClient, didChangeState state: WebRTCClientState) {
        
    }
    
    func webRTCClient(client: WebRTCClient, didReceiveError error: Error) {
        self.alertMessage(title: AppLanguage.commonAppName, message: error.localizedDescription)
    }
    
    func videoView(_ videoView: RTCVideoRenderer, didChangeVideoSize size: CGSize) {
        
    }

}

extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func dict2json() -> String {
        return json
    }
}
