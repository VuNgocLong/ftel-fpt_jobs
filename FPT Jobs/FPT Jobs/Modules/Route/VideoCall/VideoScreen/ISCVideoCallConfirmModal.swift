//
//  ISCVideoCallConfirmModal.swift
//  FPT Jobs
//
//  Created by Pham Chi Hieu on 8/27/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCVideoCallConfirmModalDelegate: class {
    func acceptCalling ()
    func endCalling ()
}
class ISCVideoCallConfirmModal: UIViewController {

    weak var delegate:ISCVideoCallConfirmModalDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnAccept_touch(_ sender: Any) {
        self.delegate?.acceptCalling()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnEndCall_touch(_ sender: Any) {
        self.delegate?.endCalling()
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
