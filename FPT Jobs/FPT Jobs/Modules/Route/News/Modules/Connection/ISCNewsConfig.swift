//
//  ISCNewsConfig.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/2/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCNewsConfig {
    static let getAllNews = K.serverAPI.api + "News/GetAllNewsAndShares"
    static let getAllServey = K.serverAPI.api + "SurveyStructure/GetAllServey"
    static let getFPTPersonForMobile = K.serverAPI.api + "FPTPerson/GetFPTPersonForMobile"
}

