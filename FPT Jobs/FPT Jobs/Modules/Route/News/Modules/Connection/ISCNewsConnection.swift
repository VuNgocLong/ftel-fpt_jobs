//
//  ISCNewsConnection.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/2/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
extension ISCNewsTabViewController {
    func getAllNews(
                _ onSuccess: @escaping (_ response: APIResponse<ISCNewsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCNewsConfig.getAllNews, isCheckHeader: false, ISCNewsInput(PageIndex: PageIndex, PageSize: PageSize, StartRow: StartRow), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getAllServey(
                _ onSuccess: @escaping (_ response: APIResponse<ISCNewsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCNewsConfig.getAllServey,isCheckHeader: false, ISCNewsInput(PageIndex: PageIndex, PageSize: PageSize, StartRow: StartRow), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func getFPTPersonForMobile(
                _ onSuccess: @escaping (_ response: APIResponse<ISCFPTPersonOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCNewsConfig.getFPTPersonForMobile,isCheckHeader: false,ISCFPTPersonInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCListNewsViewController {
    func getAllNews(
                _ onSuccess: @escaping (_ response: APIResponse<ISCNewsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCNewsConfig.getAllNews,isCheckHeader: false, ISCNewsInput(PageIndex: PageIndex, PageSize: PageSize, StartRow: StartRow), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCListEventsViewController {
    func getAllServey(
                _ onSuccess: @escaping (_ response: APIResponse<ISCNewsOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCNewsConfig.getAllServey,isCheckHeader: false, ISCNewsInput(PageIndex: PageIndex, PageSize: PageSize, StartRow: StartRow), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
