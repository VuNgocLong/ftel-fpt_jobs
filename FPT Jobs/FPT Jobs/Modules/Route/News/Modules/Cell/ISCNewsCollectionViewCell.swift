//
//  ISCNewsCollectionViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/27/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCNewsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var viewNews: UIView!
    @IBOutlet weak var lblTitle: UIAutoSizeLabel!
    @IBOutlet weak var lblDate: UIAutoSizeLabel!
    static var name: String {
        return String(describing: ISCNewsCollectionViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
