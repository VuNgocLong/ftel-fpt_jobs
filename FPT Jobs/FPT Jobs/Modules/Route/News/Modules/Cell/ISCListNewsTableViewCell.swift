//
//  ISCListNewsTableViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/27/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCListNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var viewNews: UIView!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblTitle: UIAutoSizeLabel!
    @IBOutlet weak var lblDate: UIAutoSizeLabel!
    @IBOutlet weak var lblDescription: UIAutoSizeLabel!
    static var name: String {
        return String(describing: ISCListNewsTableViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
