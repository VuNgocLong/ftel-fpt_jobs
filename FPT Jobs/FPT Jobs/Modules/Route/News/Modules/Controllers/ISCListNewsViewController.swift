//
//  ISCListNewsViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 12/27/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SDWebImage
class ISCListNewsViewController: UIBaseViewController {

    @IBOutlet weak var tblList: UITableView!
    
    var PageIndex : Int = 1
    let PageSize : Int = 10
    let StartRow : Int = 0
    var timer = Timer()
    var isLoadMore : Bool = false
    var arrayNews : [ISCNewsOutput] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = AppLanguage.newsShareTitle
        
        let nibLoading = UINib(nibName: LoadingTableViewCell.name, bundle: nil)
        tblList.register(nibLoading, forCellReuseIdentifier: LoadingTableViewCell.name)
        let cellNib = UINib(nibName: ISCListNewsTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCListNewsTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 70
        
        self.showHUD(message: AppLanguage.connecting)
        loadNews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func backAvatarAction() {
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuLoginViewController") as! ISCMenuLoginViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }else {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuViewController") as! ISCMenuViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }
    }
    
    func loadNews(){
        self.getAllNews({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayNews.append(value)
                    }
                }
            }
            if data.DataList?.count ?? 0 > 0 {
                if data.DataList?.count ?? 0 < 10 {
                    self.isLoadMore = false
                }else {
                    self.isLoadMore = true
                }
            }else {
                self.isLoadMore = false
            }
            self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
}

// MARK: - UITableView Delegate & Datasource
extension ISCListNewsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNews.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCListNewsTableViewCell.name, for: indexPath) as? ISCListNewsTableViewCell
            else {
                return UITableViewCell()
        }
        if arrayNews.count > 0 {
            let obj = arrayNews[indexPath.row]
            SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(obj.ImagePath ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                if let img = image {
                    cell.imgNews.image = img
                }else {
                    cell.imgNews.image = nil
                }
            }
            cell.lblTitle.text = obj.Title?.uppercased()
            cell.lblDescription.text = obj.Description
            cell.lblDate.text = obj.CreatedDateStr
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrayNews[indexPath.row]
        let storyboard = UIStoryboard(name: "News", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCNewsDetailViewController") as! ISCNewsDetailViewController
        vc.Title = obj.Title ?? ""
        vc.Date = obj.CreatedDateStr ?? ""
        vc.Description = obj.Description ?? ""
        vc.Content = obj.Content ?? ""
        vc.UrlForMobile = obj.UrlForMobile ?? ""
        vc.tabTitle = AppLanguage.newsShareTitle
        vc.hidesBottomBarWhenPushed = false
        self.pushViewControler(vc, "Tin tức và chia sẽ")
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tblList.dequeueReusableCell(withIdentifier: LoadingTableViewCell.name) as! LoadingTableViewCell
        cell.contentView.backgroundColor = .groupTableViewBackground
        return cell

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if isLoadMore {
            return 50
        }
        return 5
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if indexPath.row == arrayNews.count - 1 {
            if isLoadMore {
                isLoadMore = false
                timer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(loadmore), userInfo: nil, repeats: true)
            }
        }
    }
    @objc func loadmore(){
        timer.invalidate()
        PageIndex += 1
        loadNews()
    }
}
