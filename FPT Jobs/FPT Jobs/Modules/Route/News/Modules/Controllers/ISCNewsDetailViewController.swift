//
//  ISCNewsDetailViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/3/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import WebKit
class ISCNewsDetailViewController: UIBaseViewController, WKNavigationDelegate {

    @IBOutlet weak var viewContent: WKWebView!
    var Title : String = ""
    var Date : String = ""
    var Description : String = ""
    var Content : String = ""
    var tabTitle : String = ""
    var UrlForMobile : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = tabTitle
        // Do any additional setup after loading the view.
        
        self.title = tabTitle
        viewContent.navigationDelegate = self
        self.showHUD(message: AppLanguage.connecting)
        if tabTitle == AppLanguage.eventTabTitle {
            let url = NSURL (string: Content);
            let request = NSURLRequest(url: url! as URL)
            viewContent.load(request as URLRequest)
        }else {
            let url = NSURL (string: UrlForMobile);
            let request = NSURLRequest(url: url! as URL)
            viewContent.load(request as URLRequest)
        }
        
    }
     
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func backAvatarAction() {
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuLoginViewController") as! ISCMenuLoginViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }else {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuViewController") as! ISCMenuViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }
    }
    
    func loadHtml(){
        let path = Bundle.main.path(forResource: "news_detail", ofType: "html")
        do {
            let fileHtml = try NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue)
            var strHtml = fileHtml as String
            strHtml = strHtml.replacingOccurrences(of: "{Title}", with: Title)
            strHtml = strHtml.replacingOccurrences(of: "{Date}", with: Date)
            strHtml = strHtml.replacingOccurrences(of: "{Description}", with: Description)
            strHtml = strHtml.replacingOccurrences(of: "{Content}", with: Content)
            viewContent.loadHTMLString(strHtml, baseURL: nil)
        }
        catch {
            self.hideHUD()
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideHUD()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideHUD()
    }
}
