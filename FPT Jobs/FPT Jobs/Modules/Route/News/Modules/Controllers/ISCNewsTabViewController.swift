//
//  ISCNewsTabViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/25/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SDWebImage
class ISCNewsTabViewController: UIBaseViewController , UITextFieldDelegate {
    @IBOutlet weak var clvNews: UICollectionView!
    @IBOutlet weak var clvEvent: UICollectionView!
    
    @IBOutlet weak var viewAvatarManager1: UIView!
    @IBOutlet weak var viewAvatarManager2: UIView!
    @IBOutlet weak var imgAvatar1: UIImageView!
    @IBOutlet weak var imgAvatar2: UIImageView!
    @IBOutlet weak var lblFullName1: UIAutoSizeLabel!
    @IBOutlet weak var lblPosition1: UIAutoSizeLabel!
    @IBOutlet weak var lblFullName2: UIAutoSizeLabel!
    @IBOutlet weak var lblPosition2: UIAutoSizeLabel!
    
    let PageIndex : Int = 1
    let PageSize : Int = 4
    let StartRow : Int = 0
    
    var arrayNews : [ISCNewsOutput] = []
    var arrayServey : [ISCNewsOutput] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.isShowChat = true
//        self.overrideRightButtonAction(#selector(self.showPrintTestRightIcon))
        self.isSearchViewController = true
        self.initNavController()
        self.searchTft?.delegate = self
        
        let cellNib = UINib(nibName: ISCNewsCollectionViewCell.name, bundle: Bundle.main)
        clvNews.register(cellNib, forCellWithReuseIdentifier: ISCNewsCollectionViewCell.name)
        clvNews.delegate = self
        clvNews.dataSource = self
        
        clvEvent.register(cellNib, forCellWithReuseIdentifier: ISCNewsCollectionViewCell.name)
        clvEvent.delegate = self
        clvEvent.dataSource = self
        
        self.viewAvatarManager1.layer.borderWidth = 3
        self.viewAvatarManager1.layer.borderColor = UIColor.white.cgColor
        self.viewAvatarManager1.layer.cornerRadius = 60
        self.viewAvatarManager1.layer.masksToBounds = true
        
        self.viewAvatarManager2.layer.borderWidth = 3
        self.viewAvatarManager2.layer.borderColor = UIColor.white.cgColor
        self.viewAvatarManager2.layer.cornerRadius = 60
        self.viewAvatarManager2.layer.masksToBounds = true
        
        loadServey()
        loadNews()
        loadFPTPerson()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = AppLanguage.newsTabTitle
    }
    
    func loadNews(){
        arrayNews = []
        self.showHUD(message: AppLanguage.connecting)
        self.getAllNews({ (data) in
            self.hideHUD()
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayNews.append(value)
                    }
                }
            }
            self.clvNews.reloadData()
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadServey(){
        arrayServey = []
        self.getAllServey({ (data) in
            if let array = data.DataList {
                array.forEach { (val) in
                    if let value = val {
                        self.arrayServey.append(value)
                    }
                }
            }
            self.clvEvent.reloadData()
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func loadFPTPerson(){
        self.getFPTPersonForMobile({ (data) in
            if let array = data.DataList {
                if array.count > 1 {
                    var x : Int = 0
                    array.forEach { (val) in
                        if let value = val {
                            if x == 0 {
                                SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(value.ImagePath ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                                    if let img = image {
                                        self.imgAvatar1.image = img
                                    }
                                }
                                self.lblFullName1.text = value.FullName
                                self.lblPosition1.text = value.Position
                            }else {
                                SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(value.ImagePath ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                                    if let img = image {
                                        self.imgAvatar2.image = img
                                    }
                                }
                                self.lblFullName2.text = value.FullName
                                self.lblPosition2.text = value.Position
                            }
                        }
                        x += 1
                    }
                }
            }
        }) { (error) in
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
  
    @IBAction func btnNewsList_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "News", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCListNewsViewController") as! ISCListNewsViewController
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, AppLanguage.newsShareTitle)
    }
    @IBAction func btnEventsList_click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "News", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCListEventsViewController") as! ISCListEventsViewController
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, AppLanguage.newsShareTitle)
    }
}

extension ISCNewsTabViewController:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == clvNews {
            return arrayNews.count
        }else {
            return arrayServey.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: UIScreen.main.bounds.width - 70, height: clvNews.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == clvNews {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ISCNewsCollectionViewCell.name, for: indexPath) as! ISCNewsCollectionViewCell
            if arrayNews.count > 0 {
                let obj = arrayNews[indexPath.row]
                SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(obj.ImagePath ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                    if let img = image {
                        cell.imgNews.image = img
                    }
                }
                cell.lblTitle.text = obj.Title?.uppercased()
                cell.lblDate.text = obj.CreatedDateStr
            }
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ISCNewsCollectionViewCell.name, for: indexPath) as! ISCNewsCollectionViewCell
            if arrayServey.count > 0 {
                let obj = arrayServey[indexPath.row]
                SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(obj.ImageAva ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                    if let img = image {
                        cell.imgNews.image = img
                    }
                }
                cell.lblTitle.text = obj.Title?.uppercased()
                cell.lblDate.text = obj.CreatedDateStr
            }
            return cell
              
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "News", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCNewsDetailViewController") as! ISCNewsDetailViewController
        var obj = arrayNews[indexPath.row]
        vc.tabTitle = AppLanguage.newsShareTitle
        vc.Content = obj.Content ?? ""
        vc.Description = obj.Description ?? ""
        if collectionView == clvEvent {
            obj = arrayServey[indexPath.row]
            vc.tabTitle = AppLanguage.eventTabTitle
            vc.Content = obj.Url ?? ""
            vc.Description = obj.Description ?? ""
        }
        vc.Title = obj.Title ?? ""
        vc.Date = obj.CreatedDateStr ?? ""
        vc.UrlForMobile = obj.UrlForMobile ?? ""
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, AppLanguage.newsShareTitle)
    }
}
