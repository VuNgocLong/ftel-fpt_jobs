//
//  ISCNewsObj.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/2/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCNewsInput:Encodable {
    var PageIndex : Int?
    var PageSize : Int?
    var StartRow : Int?
}
struct ISCNewsOutput: Codable {
    var ArticleId:Int?
    var ArticleType:String?
    var Title:String?
    var Description:String?
    var Url:String?
    var Content:String?
    var ImagePath:String?
    var Banner:String?
    var CreatedDate:String?
    var CreatedDateStr:String?
    var JobDetailsForIOS:String?
    var UrlForMobile:String?
    var ImageAva:String?
}
struct ISCFPTPersonInput:Encodable {
}
struct ISCFPTPersonOutput: Codable {
    var Id:Int?
    var FullName:String?
    var Status:Int?
    var Position:String?
    var ImagePath:String?
    var LinkUrl:String?
    var CreatedBy:String?
    var ModifiedBy:String?
    var CreatedDate:String?
    var ModifiedDate:String?
    var DisplayOrder:Int?
}
