//
//  ISCInterviewObj.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/10/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCGenExamVideoQuestionListInput:Encodable {
    var jobCode:String?
}
struct ISCGenExamVideoQuestionListOutput: Codable {

}
struct ISCGetExamVideoQuestionOutput: Codable {
    var StartTime:String?
    var EndTime:String?
    var InterviewerCode:String?
    var JobCode:String?
    var Status:Int?
    var ExamLength:Int?
    var TotalQuestionaireNumber:Int?
    var ExamQuestionId:Int?
    var ExamVideoResultId:Int?
    var OrderNo:Int?
    var QuestionnaireCode:String?
    var Questionnaire:String?
    var QuestionnaireEn:String?
    var TimeLength:Int?
    var StartTimeQuestion:String?
    var EndTimeQuestion:String?
    var VideoUrl:String?
    var FileName:String?
    var ReturnInt:Int?
    var FileIndex:Int?
    
    var asDictionary : [String:Any] {
      let mirror = Mirror(reflecting: self)
      let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?,value:Any) -> (String,Any)? in
        guard label != nil else { return nil }
        if let val : [ISCGetExamVideoQuestionOutput] = value as? [ISCGetExamVideoQuestionOutput] {
            var temp :[[String:Any]] = []
            val.forEach { (vali) in
                temp.append(vali.asDictionary)
            }
            return(label!,temp)
        }
        return (label!,value)
      }).compactMap{ $0 })
      return dict
    }
}
struct ISCUpdateStatusQuestionVideoInput:Encodable {
    var examQuestionId:Int?
}
struct ISCUpdateStatusQuestionVideoOutput: Codable {

}

struct ISCExamVideoFinishInput:Encodable {
    var examVideoResultId:Int?
    var jobCode:String?
}
struct ISCExamVideoFinishOutput: Codable {

}
