//
//  ISCtesinterviewViewController.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 3/25/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Photos
import Alamofire
class ISCRobotInterviewViewController: UIBaseViewController {
    
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var viewCamera: UIView!
    @IBOutlet weak var lblTitleQuestion: UIAutoSizeLabel!
    @IBOutlet weak var lblDescription: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lblQuestion: UIAutoSizeLabel!
    @IBOutlet weak var lblTime: UIAutoSizeLabel!
    //    private var _session: AVCaptureSession = AVCaptureSession()
    var previewLayer: AVCaptureVideoPreviewLayer?
    private var _deviceInput: AVCaptureDeviceInput?
    private var _audioConnection: AVCaptureConnection?
    private var _videoConnection: AVCaptureConnection?
    private var _captureSession: AVCaptureSession?
    private var _videoOutput: AVCaptureVideoDataOutput = AVCaptureVideoDataOutput()
    private var _audioOutput: AVCaptureAudioDataOutput = AVCaptureAudioDataOutput()
    private var _assetWriter: AVAssetWriter?
    private var _assetWriterVideoInput: AVAssetWriterInput?
    private var _assetWriterAudioInput: AVAssetWriterInput?
    
    private var _assetSplitWriter: AVAssetWriter?
    private var _assetSplitWriterVideoInput: AVAssetWriterInput?
    private var _assetSplitWriterAudioInput: AVAssetWriterInput?
    private var _assetCurrentWriter: AVAssetWriter?
    private var _assetCurrentWriterVideoInput: AVAssetWriterInput?
    private var _assetCurrentWriterAudioInput: AVAssetWriterInput?
    private var _adpater: AVAssetWriterInputPixelBufferAdaptor?
    private var writer: AVAssetWriter?
    private var _filename = ""
    private var _filenameSplit = ""
    private var _time: Double = 0
    private var _timeSecond: Int = 0
    private var fileIndex: Int = 1
    var timer = Timer()
    var arrURL : [ISCGetExamVideoQuestionOutput] = []
    var jobcode : String = ""
    var timeMinus : Int = 0
    var timeSecond : Int = 0
    var objExamVideo : ISCGetExamVideoQuestionOutput? = nil
    var numUpload : Int = 0
    var isComplete : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = AppLanguage.jobsInterViewOnline
        GetExamVideoQuestion()
         _setupCaptureSession()
    }
    
   
    @available(iOS 10.0, *)
    private func _setupCaptureSession() {
        let session: AVCaptureSession = AVCaptureSession()
        session.sessionPreset = .high
        guard
            let device = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: AVCaptureDevice.Position.front),
            let input = try? AVCaptureDeviceInput(device: device),
            session.canAddInput(input) else { return }

        session.beginConfiguration()
        session.addInput(input)
        session.commitConfiguration()

        let output = AVCaptureVideoDataOutput()
        guard session.canAddOutput(output) else { return }
        output.setSampleBufferDelegate(self, queue: DispatchQueue(label: "com.yusuke024.video"))
        session.beginConfiguration()
        session.addOutput(output)
        let connectionVideo = output.connection(with: AVMediaType.video)
        session.commitConfiguration()

        let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio)
        let audioIn = try? AVCaptureDeviceInput(device: audioDevice!)

        if session.canAddInput(audioIn!) {
            session.addInput(audioIn!)
        }

        let outputAudio = AVCaptureAudioDataOutput()
        outputAudio.setSampleBufferDelegate(self, queue: DispatchQueue(label: "com.yusuke024.video"))
        if session.canAddOutput(outputAudio) {
            session.addOutput(outputAudio)
        }

        let connectionAudio = outputAudio.connection(with: AVMediaType.audio)

        DispatchQueue.main.async {
            self.previewLayer = AVCaptureVideoPreviewLayer(session: session)
            self.previewLayer?.frame = self.viewCamera.bounds
            self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
            self.viewCamera.layer.insertSublayer(self.previewLayer!, at: 0)
//           self.viewCamera.videoPreviewLayer.session = session
//            self.viewCamera.videoPreviewLayer.videoGravity = AVLayerVideoGravity.resize
        }

        session.startRunning()
        _videoOutput = output
        _audioOutput = outputAudio
        _captureSession = session
        _videoConnection = connectionVideo
        _audioConnection = connectionAudio
       
    }

    private enum _CaptureState {
        case idle, start, capturing, end, done
    }
    private var _captureState = _CaptureState.idle
    @IBAction func btnFinishExam_touch(_ sender: Any) {
        self.showHUD(message: AppLanguage.commonAppName)
        self.timer.invalidate()
        _captureState = .end
    }
    
   override func backAction() {
       if let view = self.navigationController?.viewControllers {
          var arrViewControllerMain: [UIViewController] = []
           if view.count > 5 {
              for n in 0...view.count - 6 {
                  arrViewControllerMain.append(view[n])
              }
              self.navigationController?.viewControllers = arrViewControllerMain
          }else{
             self.navigationController?.popViewController(animated: true)
           }
        }
       
   }
   
   @objc override func backAvatarAction() {
       if let view = self.navigationController?.viewControllers {
          var arrViewControllerMain: [UIViewController] = []
           if view.count > 5 {
              for n in 0...view.count - 6 {
                  arrViewControllerMain.append(view[n])
              }
              self.navigationController?.viewControllers = arrViewControllerMain
          }else {
              self.navigationController?.popViewController(animated: true)
          }
       }
   }
}

extension ISCRobotInterviewViewController: AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
    
        let timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer).seconds
        switch _captureState {
        case .start:
            // Set up recorder
            _filename = Common.shared.getTodayString().replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ":", with: "").replacingOccurrences(of: " ", with: "")
            let videoPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("\(_filename).mp4")
            let writer = try! AVAssetWriter(outputURL: videoPath, fileType: .mp4)
            writer.movieFragmentInterval = CMTime.invalid
            writer.shouldOptimizeForNetworkUse = true
            let inputVideo = AVAssetWriterInput(mediaType: .video, outputSettings: [
                AVVideoCodecKey: AVVideoCodecH264,
                AVVideoWidthKey: 720,
                AVVideoHeightKey: 1280,
                AVVideoCompressionPropertiesKey: [
                    AVVideoAverageBitRateKey: 2300000,
                ],
            ])
            inputVideo.expectsMediaDataInRealTime = true
//            inputVideo.mediaTimeScale = CMTimeScale(bitPattern: 600)
//            inputVideo.expectsMediaDataInRealTime = true
            inputVideo.transform = CGAffineTransform(rotationAngle: .pi/2)

            let audioSettings = [
               AVFormatIDKey : kAudioFormatMPEG4AAC,
               AVNumberOfChannelsKey : 2,
               AVSampleRateKey : 44100.0,
               AVEncoderBitRateKey: 192000
            ] as [String : Any]
            let inputAudio = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: audioSettings)
            inputAudio.expectsMediaDataInRealTime = true

            if writer.canAdd(inputVideo) {
                writer.add(inputVideo)
            }

            if writer.canAdd(inputAudio) {
                writer.add(inputAudio)
            }
            
            writer.startWriting()
            let presentationTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            writer.startSession(atSourceTime: presentationTime)
            _assetWriter = writer
            _assetWriterVideoInput = inputVideo
            _assetWriterAudioInput = inputAudio
            _captureState = .capturing
            _time = timestamp
        case .capturing:
            let description = CMSampleBufferGetFormatDescription(sampleBuffer)!
            if CMFormatDescriptionGetMediaType(description) == kCMMediaType_Audio {
                if _assetWriterAudioInput?.isReadyForMoreMediaData == true {
                    self._assetWriterAudioInput?.append(sampleBuffer)
                }
             } else {
                if _assetWriterVideoInput?.isReadyForMoreMediaData == true {
                    self._assetWriterVideoInput?.append(sampleBuffer)
                }
            }
                
            if _timeSecond == 15 * fileIndex {
                fileIndex += 1
                DispatchQueue.main.async {
                    self._filenameSplit = self._filename
                    self._assetSplitWriter = self._assetWriter
                    self._assetSplitWriterVideoInput = self._assetWriterVideoInput
                    self._assetSplitWriterAudioInput = self._assetWriterAudioInput
//                    guard self._assetSplitWriterVideoInput?.isReadyForMoreMediaData == true, self._assetSplitWriter!.status != .failed else { return }
                    let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("\(self._filenameSplit).mp4")
                    self._assetSplitWriterVideoInput?.markAsFinished()
                    self._assetSplitWriter?.finishWriting { [weak self] in
                        self?._assetSplitWriter = nil
                        self?._assetSplitWriterVideoInput = nil
                        var obj = self?.objExamVideo
                        obj?.VideoUrl = url.path
                        obj?.FileName = self?._filenameSplit
                        obj?.FileIndex = self?.fileIndex
                        self?.arrURL.append(obj!)
                        print("Upload : đang upload............")
                        self?.uploadVideo(obj: obj!, {(errorCode) in
                            self?.numUpload += 1
                            self?.checkComplete()
                        })
                        
                    }
                }
                _captureState = .start
            }
            break
        case .end:
            _captureState = .idle
            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("\(_filename).mp4")
            _assetWriterVideoInput?.markAsFinished()
            _assetWriter?.finishWriting { [weak self] in
                self?._assetWriter = nil
                self?._assetWriterVideoInput = nil
                var obj = self?.objExamVideo
                obj?.VideoUrl = url.path
                obj?.FileName = self?._filename
                obj?.FileIndex = self?.fileIndex
                self?.arrURL.append(obj!)
                self?.uploadVideo(obj: obj!, {(errorCode) in
                    self?.numUpload += 1
                    self?.checkComplete()
                })
                self?.updateStatusQuestionVideo()
            }
            break
        default:
            break
        }
    }
    
    
}

// MARK: - Function
extension ISCRobotInterviewViewController {
    
    func loadQuestion(obj : ISCGetExamVideoQuestionOutput){
        if obj.OrderNo ==  obj.TotalQuestionaireNumber{
            btnRecord.setTitle(Locale.preferredLanguages[0].contains("vi") ? "KẾT THÚC":"FINISH", for: .normal)
        }
            lblTitleQuestion.text = Locale.preferredLanguages[0].contains("vi") ? "Câu hỏi: \(obj.OrderNo ?? 0) / \(obj.TotalQuestionaireNumber ?? 0)":"Question: \(obj.OrderNo ?? 0) / \(obj.TotalQuestionaireNumber ?? 0)"
            lblDescription.text = Locale.preferredLanguages[0].contains("vi") ? "(Bạn có \(obj.TimeLength ?? 0) phút để trả lời câu hỏi này)":"(You have \(obj.TimeLength ?? 0) minutes to answer this question)"
            lblQuestion.text = Locale.preferredLanguages[0].contains("vi") ? obj.Questionnaire:obj.QuestionnaireEn
            timeMinus = obj.TimeLength ?? 0
            timeSecond = 0
            let strTimeMinus : String = timeMinus < 10 ? "0\(timeMinus)" : "\(timeMinus)"
            let strTimeSecond : String = timeSecond < 10 ? "0\(timeSecond)" : "\(timeSecond)"
            lblTime.text = "\(strTimeMinus) : \(strTimeSecond)"
             _captureState = .start
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(calcular), userInfo: nil, repeats: true)
        }
        
        @objc func calcular(){
           _timeSecond += 1
           if timeSecond == 0 {
               if timeMinus > 0 {
                   timeSecond = 59
                   timeMinus -= 1
               }else {
                   timer.invalidate()
                    _captureState = .end
               }
           }else {
               timeSecond -= 1
           }
            let strTimeMinus : String = timeMinus < 10 ? "0\(timeMinus)" : "\(timeMinus)"
           let strTimeSecond : String = timeSecond < 10 ? "0\(timeSecond)" : "\(timeSecond)"
           lblTime.text = "\(strTimeMinus) : \(strTimeSecond)"
        }
    
    func GetExamVideoQuestion(){
        _timeSecond = 0
        fileIndex = 1
        self.showHUD(message: AppLanguage.commonAppName)
        self.getExamVideoQuestion(ISCGenExamVideoQuestionListInput(jobCode: jobcode), { (data) in
            self.hideHUD()
            if data.ErrorCode == "0" {
                self.loadQuestion(obj: data.Data!)
                self.objExamVideo = data.Data
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: data.MsgAll ?? "")
            }
            
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    func updateStatusQuestionVideo(){
        
        self.updateStatusQuestionVideo(ISCUpdateStatusQuestionVideoInput(examQuestionId: objExamVideo?.ExamQuestionId), { (data) in
            self.hideHUD()
            if data.ErrorCode == "0" {
                self.showHUD(message: AppLanguage.commonAppName)
                if self.objExamVideo?.OrderNo ?? 0 < self.objExamVideo?.TotalQuestionaireNumber ?? 0 {
                    self.GetExamVideoQuestion()
                }else {
                    self.examVideoFinish()
                }
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: data.MsgAll ?? "")
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    func examVideoFinish() {
        self.showHUD(message: AppLanguage.commonAppName)
        self.examVideoFinish(ISCExamVideoFinishInput(examVideoResultId: objExamVideo?.ExamVideoResultId, jobCode: objExamVideo?.JobCode), { (data) in
            
            
            if data.ErrorCode == "0" {
                self.isComplete = true
                self.checkComplete()
            }else {
                self.hideHUD()
                self.alertMessage(title: AppLanguage.commonAppName, message: data.MsgAll ?? "")
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }

    func uploadVideo(obj : ISCGetExamVideoQuestionOutput, _ onCommplete: @escaping (_ errorCode: Int)->()){
        
        var startTimeQuestion : String = ""
        var startTimeQuestionURL : String = ""
        if obj.StartTimeQuestion != nil {
            let years = obj.StartTimeQuestion?.components(separatedBy: "-")
            if years?.count ?? 0 > 1 {
                let year : Int = Int(years![0]) ?? 0
                if year < 2000 {
                    let dateStr = Common.shared.getTodayString()
                    startTimeQuestionURL = dateStr.replacingOccurrences(of: " ", with: "T")
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                    let date = dateFormatter.date(from: dateStr)
                    dateFormatter.dateFormat = "M/d/yyyy h:mm:ss a"
                    let Date12 = dateFormatter.string(from: date!)
                    startTimeQuestion = Date12
                }else {
                    startTimeQuestion = obj.StartTimeQuestion?.replacingOccurrences(of: "T", with: " ") ?? ""
                    if startTimeQuestion != "" {
                        let dateFormatter = DateFormatter()
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                        let date = dateFormatter.date(from: startTimeQuestion)
                        dateFormatter.dateFormat = "M/d/yyyy h:mm:ss a"
                        let Date12 = dateFormatter.string(from: date!)
                        startTimeQuestion = Date12
                    }
                }
            }
            
        }

        let tokenExamVideo : String = startTimeQuestion + (obj.InterviewerCode ?? "") + "\(obj.ExamQuestionId ?? 0)" + "\(obj.TimeLength ?? 0)" + "!@$v@22cdf|"
        startTimeQuestionURL = encodeURIComponent(s: startTimeQuestionURL)
        let urlString = (ISCInterviewConfig.PostRecordedAudioVideoAsync + "?jwt=\(Common.shared.getKongToken())").replacingOccurrences(of: "\"", with: "")
        let param : String = "&InterviewCode=\(self.objExamVideo?.InterviewerCode ?? "")&questionCode=\(self.objExamVideo?.QuestionnaireCode ?? "")&jobCode=\(self.jobcode)&examQuestionId=\(self.objExamVideo?.ExamQuestionId ?? 0)&startTime=\(encodeURIComponent(s: obj.StartTime ?? ""))&tokenExamVideo=\(tokenExamVideo.MD5)&examLength=\(self.objExamVideo?.ExamLength ?? 0)&startTimeQuestion=\(startTimeQuestionURL)&timeLength=\(self.objExamVideo?.TimeLength ?? 0)&isIOS=true&iosFileName=\(obj.FileName ?? "")"
        let parameters: Parameters = [:]
        do {
            let urlFile = URL(fileURLWithPath: obj.VideoUrl ?? "")
            let videoData = try Data(contentsOf:  urlFile, options: .mappedIfSafe)
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key,value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                multipartFormData.append(videoData, withName: "file_1", fileName: "\(obj.FileName ?? "").mp4", mimeType: "video/mp4")},
                 usingThreshold:UInt64.init(),
                 to: urlString + param,
                 method:.post,
                 headers: ["Authorization" : "Bearer \(UserDefaults.standard.getValue(key: Common.keyAccessToken))"],
                 encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response)
                            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                                if let ReturnInt = dict["ReturnInt"] as? Int {
                                    if ReturnInt == 1 {
                                        print("Upload : thành công............")
                                        self.updateStatusFileUpload(obj : obj)
                                    }
                                }
                            }
                        }
                        onCommplete(1)
                        break
                    case .failure(let encodingError):
                        onCommplete(0)
                        print(encodingError)
                        break
                    }
            })
        } catch  {
            onCommplete(0)
        }
    }
    
    func updateStatusFileUpload(obj : ISCGetExamVideoQuestionOutput){
        var index : Int = 0
         for item in arrURL {
            if obj.FileIndex == item.FileIndex && obj.ExamQuestionId == item.ExamQuestionId {
                var dataResult = item
                dataResult.ReturnInt = 1
                arrURL[index] = dataResult
                let fileManager: FileManager = FileManager()
                if fileManager.isDeletableFile(atPath: item.VideoUrl ?? "") {
                    _ = try? fileManager.removeItem(atPath: item.VideoUrl ?? "")
                    print("Upload : xoá file local............")
                }
                return
            }
            index += 1
         }
    }
    
    func checkComplete(){
        self.hideHUD()
        var arrUploadFile : [[String:Any]] = []
        if self.numUpload == self.arrURL.count && self.isComplete {
            for item in self.arrURL {
                 if item.ReturnInt != 1 {
                     arrUploadFile.append(item.asDictionary)
                 }
             }
             if arrUploadFile.count > 0 {
                 let dict : [String:Any] = [
                     "array" : arrUploadFile
                 ]
                 UserDefaults.standard.set(dictionary: dict, forKey: Common.keyUserInterView)
             }
             
            if let view = self.navigationController?.viewControllers {
                var arrViewControllerMain: [UIViewController] = []
                 if view.count > 5 {
                    for n in 0...view.count - 6 {
                        arrViewControllerMain.append(view[n])
                    }
                    self.navigationController?.viewControllers = arrViewControllerMain
                }else {
                    for n in 0...view.count - 3 {
                        arrViewControllerMain.append(view[n])
                    }
                    self.navigationController?.viewControllers = arrViewControllerMain
                }
             }
        }
    }
}


