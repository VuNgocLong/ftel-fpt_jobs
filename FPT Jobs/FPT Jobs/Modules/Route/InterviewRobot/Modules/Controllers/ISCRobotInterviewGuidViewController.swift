//
//  ISCRobotInterviewGuidViewController.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/7/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Photos
class ISCRobotInterviewGuidViewController: UIBaseViewController {

    var jobcode : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppLanguage.jobsInterViewOnline
        // Do any additional setup after loading the view.
        GenExamVideoQuestionList()
    }
    
    
    override func backAction() {

        if let view = self.navigationController?.viewControllers {
             var arrViewControllerMain: [UIViewController] = []
//            print(view.count)
            if view.count > 4 {
                for n in 0...view.count - 5 {
                    arrViewControllerMain.append(view[n])
                }

                self.navigationController?.viewControllers = arrViewControllerMain
            }else {
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    func GenExamVideoQuestionList(){
        self.showHUD(message: AppLanguage.commonAppName)
        self.genExamVideoQuestionList(ISCGenExamVideoQuestionListInput(jobCode: jobcode), { (data) in
            self.hideHUD()
            if Int(data.ErrorCode ?? "0") ?? 0 < 0 {
                let alert = UIAlertController(title: AppLanguage.commonAppName, message: data.MsgAll ?? "", preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction func btnInterView_touch(_ sender: Any) {
        if AppPermission.shared.permissionCamera {
            if AppPermission.shared.permissionMicrophone {
                let storyboard = UIStoryboard(name: "InterviewOnline", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCRobotInterviewViewController") as! ISCRobotInterviewViewController
                vc.jobcode = self.jobcode
                vc.hidesBottomBarWhenPushed = true
                self.pushViewControler(vc, AppLanguage.jobsTabTitle)
            }
        }
    }

}
