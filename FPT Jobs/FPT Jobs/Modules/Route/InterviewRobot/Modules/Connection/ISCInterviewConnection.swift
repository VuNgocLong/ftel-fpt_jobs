//
//  ISCInterviewConnection.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/10/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
extension ISCRobotInterviewGuidViewController {
    func genExamVideoQuestionList(_ param : ISCGenExamVideoQuestionListInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCGenExamVideoQuestionListOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCInterviewConfig.GenExamVideoQuestionList, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCRobotInterviewViewController{
    func getExamVideoQuestion(_ param : ISCGenExamVideoQuestionListInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCGetExamVideoQuestionOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCInterviewConfig.GetExamVideoQuestion, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func updateStatusQuestionVideo(_ param : ISCUpdateStatusQuestionVideoInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCUpdateStatusQuestionVideoOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCInterviewConfig.UpdateStatusQuestionVideo, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func examVideoFinish(_ param : ISCExamVideoFinishInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCExamVideoFinishOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCInterviewConfig.ExamVideoFinish, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
