//
//  ISCInterviewConfig.swift
//  FPT Jobs
//
//  Created by Chi Hieu on 4/10/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCInterviewConfig {
    static let GenExamVideoQuestionList = K.serverAPI.api + "Interview/GenExamVideoQuestionList"
    static let GetExamVideoQuestion = K.serverAPI.api + "Interview/GetExamVideoQuestion"
    static let PostRecordedAudioVideoAsync = K.serverAPI.api + "Interview/PostRecordedAudioVideoAsync"
    static let UpdateStatusQuestionVideo = K.serverAPI.api + "Interview/UpdateStatusQuestionVideo"
    static let ExamVideoFinish = K.serverAPI.api + "Interview/ExamVideoFinish"
}
