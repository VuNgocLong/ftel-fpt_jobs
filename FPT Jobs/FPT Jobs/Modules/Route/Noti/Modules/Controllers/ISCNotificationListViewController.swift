//
//  ISCNotificationListViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/26/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCNotificationListViewController: UIBaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var viewSpaceTop: NSLayoutConstraint!
    
    var PageIndex : Int = 0
    let PageSize : Int = 10
    let StartRow : Int = 0
    var ApplicantId : Int = 0;
    var timer = Timer()
    var isLoadMore : Bool = false
    var arrayNoti: [ISCNotiListOutput] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.isShowChat = false
        // Do any additional setup after loading the view.
        self.title = AppLanguage.jobsNotiTitle
        
        let nibLoading = UINib(nibName: LoadingTableViewCell.name, bundle: nil)
        tblList.register(nibLoading, forCellReuseIdentifier: LoadingTableViewCell.name)
        let cellNib = UINib(nibName: ISCNotiListTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCNotiListTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 70
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        arrayNoti = []
        PageIndex = 0
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            self.showHUD(message: AppLanguage.connecting)
            if let ID = saved!["ID"] as? Int {
                ApplicantId = ID
            }

            loadNoti()
        }else {
            viewSpaceTop.constant = 0
        }
    }
    
    @objc override func backAvatarAction() {
        let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if saved != nil {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuLoginViewController") as! ISCMenuLoginViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }else {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuViewController") as! ISCMenuViewController
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, "")
        }
        
    }
    
    @IBAction func btnLogin_touch(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        _ = appDelegate.grabLogin()
    }
    
    func loadNoti(){
        self.getPagingForMobile({ (data) in
            self.hideHUD()
            if let array = data.DataList {
               array.forEach { (val) in
                   if let value = val {
                       self.arrayNoti.append(value)
                   }
               }
           }
           if data.DataList?.count ?? 0 > 0 {
               if data.DataList?.count ?? 0 < 10 {
                   self.isLoadMore = false
               }else {
                   self.isLoadMore = true
               }
           }else {
               self.isLoadMore = false
           }
           self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                let alert = UIAlertController(title: AppLanguage.commonAppName, message: error, preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    func maskReadNoti(obj : ISCNotiListOutput){
        self.showHUD(message: AppLanguage.connecting)
        self.maskAsRead(obj.Id ?? 0, { (data) in
            self.hideHUD()
            switch obj.NotifyType {
            case 0,3,7:
                let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsApplyViewController") as! ISCMenuJobsApplyViewController
                self.pushViewControler(vc, AppLanguage.userProfile)
                break
            case 1,2:
                let storyboard = UIStoryboard(name: "News", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCListNewsViewController") as! ISCListNewsViewController
                vc.hidesBottomBarWhenPushed = true
                self.pushViewControler(vc, AppLanguage.newsShareTitle)
                break
            case 4,5:
                let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsFollowViewController") as! ISCMenuJobsFollowViewController
                self.pushViewControler(vc, AppLanguage.userProfile)
                break
            default:
                break
            }
        }) { (error) in
             self.hideHUD()
             self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return arrayNoti.count
       }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCNotiListTableViewCell.name, for: indexPath) as? ISCNotiListTableViewCell
               else {
                   return UITableViewCell()
           }
           if arrayNoti.count > 0 {
               let obj = arrayNoti[indexPath.row]
            cell.lblTItle.text = obj.NotifyTitle
            let Content = "<span style=\"font-family: Helvetica-Neue; font-size: 15; color: #3E3E3E\">\(obj.NotifyContent ?? "")</span>"
            cell.lblContent.attributedText = Content.htmlToAttributedString
            cell.lblDate.text = ""
            if obj.IsNewNotify ?? false {
                cell.viewBackground.backgroundColor = UIColor(hexString: "#C6C6C8")
            }else {
                cell.viewBackground.backgroundColor = UIColor(hexString: "#FFFFFF")
            }
           }
           cell.selectionStyle = .none
           return cell
       }
       
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let obj = arrayNoti[indexPath.row]
            if obj.IsNewNotify ?? false {
                maskReadNoti(obj: obj)
            }else {
                switch obj.NotifyType {
                case 0,3,7:
                    let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsApplyViewController") as! ISCMenuJobsApplyViewController
                    self.pushViewControler(vc, AppLanguage.userProfile)
                    break
                case 1,2:
                    let storyboard = UIStoryboard(name: "News", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ISCListNewsViewController") as! ISCListNewsViewController
                    vc.hidesBottomBarWhenPushed = true
                    self.pushViewControler(vc, AppLanguage.newsShareTitle)
                    break
                case 4,5:
                    let storyboard = UIStoryboard(name: "Menu", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ISCMenuJobsFollowViewController") as! ISCMenuJobsFollowViewController
                    self.pushViewControler(vc, AppLanguage.userProfile)
                    break
                default:
                    break
                }
            }
            
       }
       
       func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
           let cell = tblList.dequeueReusableCell(withIdentifier: LoadingTableViewCell.name) as! LoadingTableViewCell
           cell.contentView.backgroundColor = .groupTableViewBackground
           return cell

       }
       func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
           if isLoadMore {
               return 50
           }
           return 5
       }
       func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           if indexPath.row == arrayNoti.count - 1 {
               if isLoadMore {
                   isLoadMore = false
                   timer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(loadmore), userInfo: nil, repeats: true)
               }
           }
       }
       @objc func loadmore(){
            timer.invalidate()
            PageIndex += 1
            loadNoti()
       }

}
