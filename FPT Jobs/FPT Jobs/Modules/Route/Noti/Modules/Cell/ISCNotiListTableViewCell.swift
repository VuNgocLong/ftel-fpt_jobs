//
//  ISCNotiListTableViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/26/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCNotiListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTItle: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lblContent: UIAutoSizeLabel!
    @IBOutlet weak var lblDate: UIAutoSizeLabelNomarl!
    @IBOutlet weak var viewBackground: UIView!
    static var name: String {
        return String(describing: ISCNotiListTableViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
