//
//  ISCNotiObj.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/26/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCNotiListInput:Encodable {
    var ApplicantId : Int?
    var PageIndex : Int?
    var PageSize : Int?
}
struct ISCNotiListOutput: Codable {
    var ApplicantId:Int?
    var CreatedDate:String?
    var Deleted:Bool?
    var Id:Int?
    var IsNewNotify:Bool?
    var NotifyContent:String?
    var NotifyTitle:String?
    var NotifyType:Int?
    var NotifyUrl:String?
}
