//
//  ISCNotiConfig.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/26/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCNotiConfig {
    static let getPagingForMobile = K.serverAPI.api + "ApplicantNotifications/GetPagingForMobile"
    static let maskAsRead = K.serverAPI.api + "ApplicantNotifications/MaskAsRead"
}
