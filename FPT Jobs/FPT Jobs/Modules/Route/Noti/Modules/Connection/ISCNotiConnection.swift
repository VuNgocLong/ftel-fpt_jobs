//
//  ISCNotiConnection.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/26/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
extension ISCNotificationListViewController {
    func getPagingForMobile(
                _ onSuccess: @escaping (_ response: APIResponse<ISCNotiListOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCNotiConfig.getPagingForMobile, ISCNotiListInput(ApplicantId : ApplicantId, PageIndex: PageIndex, PageSize: PageSize), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func maskAsRead(_ id : Int,
                _ onSuccess: @escaping (_ response: APIResponse<ISCNotiListOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection("\(ISCNotiConfig.maskAsRead)?id=\(id)", ISCNotiListInput(PageIndex: PageIndex, PageSize: PageSize), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension UIBaseViewController {
    func getCountNoti(_ param : ISCNotiListInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCNotiListOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCNotiConfig.getPagingForMobile, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
