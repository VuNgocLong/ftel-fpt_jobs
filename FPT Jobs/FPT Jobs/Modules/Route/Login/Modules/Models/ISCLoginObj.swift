//
//  ISCLoginObj.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/26/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

struct ISCLoginInput:Encodable {
    var username: String
    var password: String
    var grant_type: String = "password"
    var ip: String = ""
}
struct ISCLoginOutput: Codable {
}

struct ISCGetProfileInput:Encodable {
}
struct ISCGetProfileOutput: Codable {
    var ID:Int?
    var InterviewerCode: String?
    var PortraitImage: String?
    var KenhTuyenDung: String?
    var WorkProvide: String?
    var WorkDistrict: String?
    var ProvinceCode: String?
    var DistrictCode: String?
    var SchoolName: String?
    var QualificationType: String?
    var MajorContent:String?
    var SYLL: String?
    var FullName: String?
    var CellPhoneNumber: String?
    var Email: String?
    var DoB: String?
    var DoB_Day:Int?
    var DoB_Month:Int?
    var DoB_Year:Int?
    var Sex:Int?
    var Weight: String?
    var Height: String?
    var SSN: String?
    var MaritalStatus: String?
    var BirthPlaceProvinceCode: String?
    var BirthPlaceDistrictCode: String?
    var BirthAddress: String?
    var ResidentAddress: String?
    var ResidentDistrictCode: String?
    var ResidentProvinceCode: String?
    var RegisteredTimeOnline: String?
    var ResidentAddrDisplay: String?
    var JobCode: String?
    var OTPCode: String?
    var EmailTokenCode: String?
    var EmailConfirmed: Int?
    var PhoneNumberConfirmed: Int?
    var Majors: [ISCGetProfileMajorOutput?]?
    var AplicantMajors:[Int?]?
    var SkillsApplicant:[String?]?
    var Skills: String?
    var SkillToArray:[String?]?
    var EmailTemp: String?
    var PhoneTemp: String?
    var PersonalInformation: String?
    var CVFile:[UInt8]?
    var RequirementSalaryFrom: String?
    var RequirementSalaryTo: String?
    var YearsOfExperience: String?
    
    var asDictionary : [String:Any] {
      let mirror = Mirror(reflecting: self)
      let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?,value:Any) -> (String,Any)? in
        guard label != nil else { return nil }
        if let val : [ISCGetProfileMajorOutput] = value as? [ISCGetProfileMajorOutput] {
            var temp :[[String:Any]] = []
            val.forEach { (vali) in
                temp.append(vali.asDictionary)
            }
            return(label!,temp)
        }
        return (label!,value)
      }).compactMap{ $0 })
      return dict
    }
}

struct ISCGetProfileMajorOutput: Codable {
    var ApplciantId:Int?
    var MajorId:Int?
    var MajorName:String?
    
    var asDictionary : [String:Any] {
      let mirror = Mirror(reflecting: self)
      let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?,value:Any) -> (String,Any)? in
        guard label != nil else { return nil }
        return (label!,value)
      }).compactMap{ $0 })
      return dict
    }
}

struct ISCExternalLoginInput:Encodable {
    var Email: String
    var ExternalAccessToken: String
    var FullName: String
    var Provider: String
    var UserName: String
}

struct ISCForgetPasswordInput:Encodable {
    var Email: String
}

struct ISCForgetPasswordOutput:Codable {
    var Email: String
    var FullName: String
}

struct ISCRegisterInput:Encodable {
    var Email: String
    var ConfirmPassword: String
    var FullName: String
    var Password: String
}

struct ISCRegisterOutput:Codable {
}
struct ISCChangePassInput:Encodable {
    var OldPasword: String
    var NewPassword: String
    var ConfirmNewPassword: String
}

struct ISCChangePassOutput:Codable {
}

struct ISCUpdatePassInput:Encodable {
    var ConfirmNewPassword: String
    var NewPassword: String
    var TokenCode: String
}

struct ISCUpdatePassOutput:Codable {
}
struct ISCRegisterSNSOutput: Codable {
}

struct ISCRegisterSNSInput:Encodable {
    var AWSToken: String
    var ApplicantId: String
}
