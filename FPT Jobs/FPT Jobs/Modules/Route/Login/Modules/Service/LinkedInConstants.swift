//
//  LinkedInConstants.swift
//  LinkedInSignInExample
//
//  Created by John Codeos on 11/8/19.
//  Copyright © 2019 John Codeos. All rights reserved.
//

import Foundation


struct LinkedInConstants {
    
    static let CLIENT_ID = "8101nlzdc6vezx"
    static let CLIENT_SECRET = "8101nlzdc6vezx"
    static let REDIRECT_URI = "https://fptjobs.com"
    static let SCOPE = "r_liteprofile%20r_emailaddress" //Get lite profile info and e-mail address
    
    static let AUTHURL = "https://www.linkedin.com/oauth/v2/authorization"
    static let TOKENURL = "https://www.linkedin.com/oauth/v2/accessToken"
}
