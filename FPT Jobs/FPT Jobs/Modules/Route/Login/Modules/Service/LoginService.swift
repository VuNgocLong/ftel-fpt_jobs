//
//  LoginService.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/18/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
class FPTJobsService: BaseService {
    enum FPTJobsAPIUrl: String {
        case UPDATE_PROFILE                          = "/Applicant/UpdateProfile"
        case INSERT_JOBS_FOLLOWINFO                  = "/Applicant/InsertJobsFollowInfo"
        case SEND_OTP                                = "/Applicant/SendOTP"
        case REGISTER_SNS                            = "/AWSSimpleNotification/RegisterSNS"
        case DISABLE_ENPOINT                         = "/AWSSimpleNotification/DisableEnpoint"
       
    }

    func updateProfile(parameters : Dictionary<String, Any>, completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let urlRequest = K.serverAPI.api + FPTJobsAPIUrl.UPDATE_PROFILE.rawValue + "?jwt=\(Common.shared.getKongToken())"
        postAPI(url: urlRequest.replacingOccurrences(of: "\"", with: ""), parameters: parameters) { result in
            switch result {
            case .success(let json):
                completion(.success(payload: json))
                break
            case .failure(let error) :
                let errorCode : ErrorModel = ErrorModel(ErrorCode: -1, Error: error?.getErrorMessage() ?? "Connect to server fail")!
                completion(.error(errorCode))
                break
            }
        }
    }
    
    func insertJobsFollowInfo(parameters : Dictionary<String, Any>, completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let urlRequest = K.serverAPI.api + FPTJobsAPIUrl.INSERT_JOBS_FOLLOWINFO.rawValue + "?jwt=\(Common.shared.getKongToken())"
        postAPI(url: urlRequest.replacingOccurrences(of: "\"", with: ""), parameters: parameters) { result in
            switch result {
            case .success(let json):
                completion(.success(payload: json))
                break
            case .failure(let error) :
                let errorCode : ErrorModel = ErrorModel(ErrorCode: -1, Error: error?.getErrorMessage() ?? "Connect to server fail")!
                completion(.error(errorCode))
                break
            }
            
        }
    }
    
    func sendOTP(parameters : Dictionary<String, Any>, completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let urlRequest = K.serverAPI.api + FPTJobsAPIUrl.SEND_OTP.rawValue + "?jwt=\(Common.shared.getKongToken())"
        postAPI(url: urlRequest.replacingOccurrences(of: "\"", with: ""), parameters: parameters) { result in
            switch result {
            case .success(let json):
                completion(.success(payload: json))
                break
            case .failure(let error) :
                let errorCode : ErrorModel = ErrorModel(ErrorCode: -1, Error: error?.getErrorMessage() ?? "Connect to server fail")!
                completion(.error(errorCode))
                break
            }
            
        }
    }
    
    func refreshToken(parameters : Dictionary<String, Any>, completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let urlRequest = K.serverAPI.token + "?jwt=\(Common.shared.getKongToken())"
        postRefreshToken(url: urlRequest.replacingOccurrences(of: "\"", with: ""), parameters: parameters) { result in
            switch result {
            case .success(let json):
                completion(.success(payload: json))
                break
            case .failure(let error) :
                let errorCode : ErrorModel = ErrorModel(ErrorCode: -1, Error: error?.getErrorMessage() ?? "Connect to server fail")!
                completion(.error(errorCode))
                break
            }
        }
    }
    
    func RegisterSNS(parameters : Dictionary<String, Any>, completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let urlRequest = K.serverAPI.api + FPTJobsAPIUrl.REGISTER_SNS.rawValue + "?jwt=\(Common.shared.getKongToken())"
        postAPI(url: urlRequest.replacingOccurrences(of: "\"", with: ""), parameters: parameters) { result in
            switch result {
            case .success(let json):
                completion(.success(payload: json))
                break
            case .failure(let error) :
                let errorCode : ErrorModel = ErrorModel(ErrorCode: -1, Error: error?.getErrorMessage() ?? "Connect to server fail")!
                completion(.error(errorCode))
                break
            }
        }
    }
    
    func DisableEnpoint(parameters : Dictionary<String, Any>, completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let urlRequest = K.serverAPI.api + FPTJobsAPIUrl.DISABLE_ENPOINT.rawValue + "?jwt=\(Common.shared.getKongToken())"
        postAPI(url: urlRequest.replacingOccurrences(of: "\"", with: ""), parameters: parameters) { result in
            switch result {
            case .success(let json):
                completion(.success(payload: json))
                break
            case .failure(let error) :
                let errorCode : ErrorModel = ErrorModel(ErrorCode: -1, Error: error?.getErrorMessage() ?? "Connect to server fail")!
                completion(.error(errorCode))
                break
            }
        }
    }
    
}
