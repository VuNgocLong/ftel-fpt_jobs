//
//  ISCRegisterViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 1/13/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCRegisterViewController: UIBaseViewController {

    @IBOutlet weak var tftName: UIAutoSizeTextField!
    @IBOutlet weak var tftEmail: UIAutoSizeTextField!
    @IBOutlet weak var tftPassword: UIAutoSizeTextField!
    @IBOutlet weak var tftRePassword: UIAutoSizeTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isShowChat = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        guard validate() else {
            return
        }
        self.showHUD(message: AppLanguage.connecting)
        let name = self.tftName.text ?? ""
        let email = self.tftEmail.text ?? ""
        let password = self.tftPassword.text ?? ""
        let repassword = self.tftRePassword.text ?? ""
        self.registerAccount(ISCRegisterInput(Email: email, ConfirmPassword: repassword, FullName: name, Password: password), { (value) in
            self.hideHUD()
            if value.Succeeded ?? false {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.registerAccountSuccess)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else {
                if (value.Errors ?? []).count > 0 {
                    self.alertMessage(title: AppLanguage.commonAppName, message: value.Errors?[0]?.Description ?? AppLanguage.errorNetworkFailureConnectionApi)
                }
                else {
                    self.alertMessage(title: AppLanguage.commonAppName, message: value.error ?? AppLanguage.errorNetworkFailureConnectionApi)
                }
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    func validate() -> Bool {
        let name = self.tftName.text ?? ""
        let email = self.tftEmail.text ?? ""
        let password = self.tftPassword.text ?? ""
        let repassword = self.tftRePassword.text ?? ""
        if name == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullEmail)
            return false
        }
        if email == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullEmail)
            return false
        }
        if password == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPassword)
            return false
        }
        if repassword == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullRePassword)
            return false
        }
        if !email.isValidEmail() {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.emailInvalid)
            return false
        }
        if !(password.count <= 20 && password.count >= 8) {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.passwordInvalid)
            return false
        }
        if password != repassword {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.repassInvalid)
            return false
        }
        return true
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.dismiss(animated: false) {
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            _ = appDelegate.grabLogin()
        }
    }
    
}
