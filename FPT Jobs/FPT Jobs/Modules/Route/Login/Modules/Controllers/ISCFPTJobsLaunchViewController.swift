//
//  ISCFPTJobsLaunchViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/16/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCFPTJobsLaunchViewController: UIViewController {

    let fptJobsService : FPTJobsService = FPTJobsService()
    let v = UIView(frame: UIScreen.main.bounds)
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getToken()
    }

    @objc func getToken() {
        self.showHUD(message: AppLanguage.connecting)
        getKongToken({ (token) in
            self.hideHUD()
            
            Common.shared.saveKongToken(token: token)
            let saved: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
               if saved != nil {
                   appDelegate.grabStoryboard()
               }else {
                _ = appDelegate.grabLogin()
            }
        }) { (error) in
            self.hideHUD()
            self.showAlert(with: AppLanguage.commonAppName, message: error, style: UIAlertController.Style.alert, actions: [
                UIAlertAction(title: AppLanguage.commonOkTitle, style: UIAlertAction.Style.cancel, handler: { _ in
                    self.getToken()
                }),
            ])
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
}
extension ISCFPTJobsLaunchViewController {
    func showHUD(message:String) {
        ISCProgressHUD().loadingIndicator(self.tabBarController ?? self, self.v, message) { (complete) in }
    }
    func hideHUD() {
        ISCProgressHUD().stopIndicator(self.tabBarController ?? self, self.v) { (complete) in }
    }
    func showAlert(with title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)

        if actions.isEmpty {
            let action = UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel, handler: nil)
            alert.addAction(action)
        } else {
            actions.forEach(alert.addAction)
        }

        self.present(alert, animated: true, completion: nil)
    }
    func alertMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
