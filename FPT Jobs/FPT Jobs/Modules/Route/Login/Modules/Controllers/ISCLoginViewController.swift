//
//  ISCLoginViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/26/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
//import LinkedinSwift
import WebKit
import AuthenticationServices
class ISCLoginViewController: UIBaseViewController {

    @IBOutlet weak var tftEmail: UIAutoSizeTextField!
    @IBOutlet weak var tftPassword: UIAutoSizeTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    let loginNoti : LoginNotiVM = LoginNotiVM()
    @IBOutlet weak var viewLoginApple: UIView!
    
    var linkedInId = ""
    var linkedInFirstName = ""
    var linkedInLastName = ""
    var linkedInEmail = ""
    var linkedInProfilePicURL = ""
    var linkedInAccessToken = ""
    var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.isShowChat = false
        UserDefaults.standard.removeObject(forKey: Common.keyAccessToken)
        UserDefaults.standard.removeObject(forKey: Common.keyUserProfile)
        UserDefaults.standard.removeObject(forKey: Common.keyUserLiveChat)
        UserDefaults.standard.removeObject(forKey: Common.keyUserCMND)
        UserDefaults.standard.removeObject(forKey: Common.bearTokenKey)
        UserDefaults.standard.removeObject(forKey: Common.key_avatar)
        if #available(iOS 13.0, *) {
             setupLoginProviderView()
         } else {
             // Fallback on earlier versions
         }
    }

    @IBAction func btnLoginAct(_ sender: Any) {
        if tftEmail.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullEmail)
            return
        }else {
            if !Common.shared.checkEmail(str: tftEmail.text ?? "") {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.emailInvalid)
                return
            }
        }
        if tftPassword.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPassword)
            return
        }
        self.showHUD(message: AppLanguage.connecting)
        self.loginViaPassword({ (value) in
            guard let accessToken = value.access_token else {
                self.hideHUD()
                return
            }
            Common.shared.saveAccessToken(token:accessToken)
            let kongToken : Dictionary<String,Any> = [
                "access_token" : value.access_token ?? "",
                "refresh_token" : value.refresh_token ?? "",
                "DateReponse" : Common.shared.getTodayString(),
                "username" : self.tftEmail.text ?? "",
                "password" : self.tftPassword.text ?? "",
            ]
            Common.shared.setBearToken(dict: kongToken)
            self.getProfile({ (value) in
                self.hideHUD()
                    if let data = value.Data {
                        let dict = data.asDictionary
                         UserDefaults.standard.set(dictionary: dict, forKey: Common.keyUserProfile)
                        self.registerNoti(applicationId: "\(data.ID ?? 0)")
                    }
                }) { (error) in
                    self.hideHUD()
                    self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }) { (error) in
            self.hideHUD()
            if error == "EMAIL_OR_PASSWORD_INCORRECT" {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.EMAIL_OR_PASSWORD_INCORRECT)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
            
        }
    }

    @IBAction func btnSkip_click(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        appDelegate.grabStoryboard()
    }
    
    @IBAction func btnFBAct(_ sender: Any) {
        self.FBSignIn()
    }

    @IBAction func btnGGAct(_ sender: Any) {
        self.GGSignIn()
    }
    
    @IBAction func btnLIAct(_ sender: Any) {
        self.LikedInSignIn()
       
    }
    
    
    
    @IBAction func btnRegisterAct(_ sender: Any) {
        let vc:ISCRegisterViewController = ISCRegisterViewController()
        let nav:UINavigationController = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func btnForgotPasswordAct(_ sender: Any) {
        let vc:ISCForgetPasswordViewController = ISCForgetPasswordViewController()
        let nav:UINavigationController = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    
    
}

extension ISCLoginViewController : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            self.loginExternal(ISCExternalLoginInput(Email: user.profile.email, ExternalAccessToken: "google", FullName: user.profile.name, Provider: "google", UserName: user.profile.email))
        } else {
            print("\(error.localizedDescription)")
        }
    }
    func FBSignIn() {
        //        if UIApplication.shared.canOpenURL(requestManager.getAppOAuthUrlstravaScheme()!) {
        //            UIApplication.shared.open(requestManager.getAppOAuthUrlstravaScheme()!, options: [:])
        //        } else {

        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (error == nil) {
                print("Process error")
            }else if result?.isCancelled ?? false {
                print("Cancelled")
            }else {
                 print("Logged in")
            }
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email")) {
                    if((AccessToken.current) != nil){
                        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                            if (error == nil){
                                let dict = result as! [String : AnyObject]
                                self.loginExternal(ISCExternalLoginInput(Email: dict["email"] as? String ?? "", ExternalAccessToken: "facebook", FullName: dict["name"] as? String ?? "", Provider: "facebook", UserName: dict["email"] as? String ?? ""))
                            }
                        })
                    }
                }
            }
        }
    }
    func GGSignIn() {
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    func validate() -> Bool {
        let email = self.tftEmail.text ?? ""
        let password = self.tftPassword.text ?? ""
        if email == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullEmail)
            return false
        }
        if password == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPassword)
            return false
        }
        if !email.isValidEmail() {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.emailInvalid)
            return false
        }
        if !(password.count <= 20 && password.count >= 8) {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.passwordInvalid)
            return false
        }
        return true
    }
    func loginExternal(_ param:ISCExternalLoginInput) {
        self.showHUD(message: AppLanguage.connecting)
        self.externalLogin(param, { (value) in
            guard let accessToken = value.access_token else {
                self.hideHUD()
                return
            }
            Common.shared.saveAccessToken(token:accessToken)
            let kongToken : Dictionary<String,Any> = [
                "access_token" : value.access_token ?? "",
                "refresh_token" : value.refresh_token ?? "",
                "DateReponse" : Common.shared.getTodayString(),
                "username" : "",
                "password" : "",
            ]
            Common.shared.setBearToken(dict: kongToken)
            self.getProfile({ (value) in
                self.hideHUD()
                    if let data = value.Data {
                        let dict = data.asDictionary
                        UserDefaults.standard.set(dictionary: dict, forKey: Common.keyUserProfile)
                        self.registerNoti(applicationId: "\(data.ID ?? 0)")
                        
                    }
                }) { (error) in
                    self.hideHUD()
                    self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    func LikedInSignIn() {
//        let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "77tn2ar7gq6lgv", clientSecret: "iqkDGYpWdhf7WKzA", state: "DLKDJF46ikMMZADfdfds", permissions: ["r_liteprofile", "r_emailaddress"], redirectUrl: "https://github.com/tonyli508/LinkedinSwift"), nativeAppChecker: WebLoginOnly())
//        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
//            print("Login success lsToken: \(lsToken)")
//            linkedinHelper.requestURL("https://api.linkedin.com/v2/me", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
//                print("Request success with response: \(response)")
//                let fullName = (response.jsonObject["localizedFirstName"] as? String ?? "") + " " + (response.jsonObject["localizedLastName"] as? String ?? "")
//                linkedinHelper.requestURL("https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token=\(String(describing: lsToken.accessToken ?? ""))", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
//                    var email:String = ""
//                    if let res = response.jsonObject["elements"] as? [[String:Any]] , res.count > 0 {
//                        email = (res[0]["handle~"] as! [String:Any])["emailAddress"] as? String ?? ""
//                    }
//                    linkedinHelper.logout()
//                                self.loginExternal(ISCExternalLoginInput(Email: email, ExternalAccessToken: "linkedin", FullName: fullName, Provider: "linkedin", UserName: email))
//                            }) { (error) -> Void in
//                                print("Encounter error: \(error.localizedDescription)")
//                                linkedinHelper.logout()
//                            }
//            }) { (error) -> Void in
//                print("Encounter error: \(error.localizedDescription)")
//                linkedinHelper.logout()
//            }
//        }, error: {(error) -> Void in
//            print("Encounter error: \(error.localizedDescription)")
//        }, cancel: {() -> Void in
//            
//            print("User Cancelled!")
//        })
        
        // Create linkedIn Auth ViewController
        let linkedInVC = UIViewController()
        // Create WebView
        let webView = WKWebView()
        webView.navigationDelegate = self
        linkedInVC.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: linkedInVC.view.topAnchor),
            webView.leadingAnchor.constraint(equalTo: linkedInVC.view.leadingAnchor),
            webView.bottomAnchor.constraint(equalTo: linkedInVC.view.bottomAnchor),
            webView.trailingAnchor.constraint(equalTo: linkedInVC.view.trailingAnchor)
            ])

        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"

        let authURLFull = LinkedInConstants.AUTHURL + "?response_type=code&client_id=" + LinkedInConstants.CLIENT_ID + "&scope=" + LinkedInConstants.SCOPE + "&state=" + state + "&redirect_uri=" + LinkedInConstants.REDIRECT_URI


        let urlRequest = URLRequest.init(url: URL.init(string: authURLFull)!)
        webView.load(urlRequest)

        // Create Navigation Controller
        let navController = UINavigationController(rootViewController: linkedInVC)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelAction))
        linkedInVC.navigationItem.leftBarButtonItem = cancelButton
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.refreshAction))
        linkedInVC.navigationItem.rightBarButtonItem = refreshButton
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navController.navigationBar.titleTextAttributes = textAttributes
        linkedInVC.navigationItem.title = "linkedin.com"
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.tintColor = UIColor.white
        navController.navigationBar.barTintColor = UIColor.init(hexString: "#0072B1")
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        navController.modalTransitionStyle = .coverVertical

        self.present(navController, animated: true, completion: nil)
    }
    
    func registerNoti(applicationId : String) {
        self.showHUD(message: AppLanguage.commonAppName)
        loginNoti.registerNoti(deviceToken: Common.DEVICE_TOKEN, applicationId: applicationId) { (data) in
            self.hideHUD()
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            appDelegate.grabStoryboard()

        }
//        self.registerSNS(deviceToken: Common.DEVICE_TOKEN, applicationId: applicationId, { (data) in
//                    }) { (error) in
//            self.hideHUD()
//        }
    }
}

class LoginNotiVM : BaseViewModels {

    var registerNotiSuccess: (() -> ())?
    var updateProfileOTPSuccess: (() -> ())?
    var sendOTPSuccess: (() -> ())?
    let fptJobsService : FPTJobsService
    init(fptJobsService : FPTJobsService = FPTJobsService()) {
        self.fptJobsService = fptJobsService
    }
    var modelUpdate : JSON? = nil
    func registerNoti(deviceToken : String, applicationId : String , completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let param : JSON = [
            "AWSToken" : deviceToken,
            "ApplicantId" : applicationId
        ]
        fptJobsService.RegisterSNS(parameters: param) { [weak self] result in
            switch result {
            case .success(let model):
                completion(.success(payload:model))
                break
            case .error(let error):
                completion(.success(payload: Dictionary<String, Any>()))
                break
            }
        }
    }
    
    func disableEnpoint(deviceToken : String, applicationId : String , completion :  @escaping (_ result : ResultData<Dictionary<String, Any>, ErrorModel>) -> Void) {
        let param : JSON = [
            "AWSToken" : deviceToken,
            "ApplicantId" : applicationId
        ]
        fptJobsService.DisableEnpoint(parameters: param) { [weak self] result in
            switch result {
            case .success(let model):
                completion(.success(payload:model))
                break
            case .error(let error):
                completion(.success(payload: Dictionary<String, Any>()))
                break
            }
        }
    }

}
extension ISCLoginViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        RequestForCallbackURL(request: navigationAction.request)
        
        //Close the View Controller after getting the authorization code
        if let urlStr = navigationAction.request.url?.absoluteString {
            if urlStr.contains("?code=") {
                self.dismiss(animated: true, completion: nil)
            }
        }
        decisionHandler(.allow)
    }

    func RequestForCallbackURL(request: URLRequest) {
        // Get the authorization code string after the '?code=' and before '&state='
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(LinkedInConstants.REDIRECT_URI) {
            if requestURLString.contains("?code=") {
                if let range = requestURLString.range(of: "=") {
                    let linkedinCode = requestURLString[range.upperBound...]
                    if let range = linkedinCode.range(of: "&state=") {
                        let linkedinCodeFinal = linkedinCode[..<range.lowerBound]
                        handleAuth(linkedInAuthorizationCode: String(linkedinCodeFinal))
                    }
                }
            }
        }
    }

    func handleAuth(linkedInAuthorizationCode: String) {
        linkedinRequestForAccessToken(authCode: linkedInAuthorizationCode)
    }

    func linkedinRequestForAccessToken(authCode: String) {
        let grantType = "authorization_code"

        // Set the POST parameters.
        let postParams = "grant_type=" + grantType + "&code=" + authCode + "&redirect_uri=" + LinkedInConstants.REDIRECT_URI + "&client_id=" + LinkedInConstants.CLIENT_ID + "&client_secret=" + LinkedInConstants.CLIENT_SECRET
        let postData = postParams.data(using: String.Encoding.utf8)
        let request = NSMutableURLRequest(url: URL(string: LinkedInConstants.TOKENURL)!)
        request.httpMethod = "POST"
        request.httpBody = postData
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            if statusCode == 200 {
                let results = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [AnyHashable: Any]

                let accessToken = results?["access_token"] as! String
                print("accessToken is: \(accessToken)")

                let expiresIn = results?["expires_in"] as! Int
                print("expires in: \(expiresIn)")

                // Get user's id, first name, last name, profile pic url
                self.fetchLinkedInUserProfile(accessToken: accessToken)
            }
        }
        task.resume()
    }


    func fetchLinkedInUserProfile(accessToken: String) {
        let tokenURLFull = "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))&oauth2_access_token=\(accessToken)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let verify: NSURL = NSURL(string: tokenURLFull!)!
        let request: NSMutableURLRequest = NSMutableURLRequest(url: verify as URL)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if error == nil {
                let linkedInProfileModel = try? JSONDecoder().decode(LinkedInProfileModel.self, from: data!)
                
                //AccessToken
                print("LinkedIn Access Token: \(accessToken)")
                self.linkedInAccessToken = accessToken
                
                // LinkedIn Id
                let linkedinId: String! = linkedInProfileModel?.id
                print("LinkedIn Id: \(linkedinId ?? "")")
                self.linkedInId = linkedinId

                // LinkedIn First Name
                let linkedinFirstName: String! = linkedInProfileModel?.firstName.localized.enUS
                print("LinkedIn First Name: \(linkedinFirstName ?? "")")
                self.linkedInFirstName = linkedinFirstName

                // LinkedIn Last Name
                let linkedinLastName: String! = linkedInProfileModel?.lastName.localized.enUS
                print("LinkedIn Last Name: \(linkedinLastName ?? "")")
                self.linkedInLastName = linkedinLastName

                // LinkedIn Profile Picture URL
                let linkedinProfilePic: String!

                /*
                 Change row of the 'elements' array to get diffrent size of the profile url
                 elements[0] = 100x100
                 elements[1] = 200x200
                 elements[2] = 400x400
                 elements[3] = 800x800
                */
                if let pictureUrls = linkedInProfileModel?.profilePicture.displayImage.elements[2].identifiers[0].identifier {
                    linkedinProfilePic = pictureUrls
                } else {
                    linkedinProfilePic = "Not exists"
                }
                print("LinkedIn Profile Avatar URL: \(linkedinProfilePic ?? "")")
                self.linkedInProfilePicURL = linkedinProfilePic

                // Get user's email address
                self.fetchLinkedInEmailAddress(accessToken: accessToken)
            }
        }
        task.resume()
    }

    func fetchLinkedInEmailAddress(accessToken: String) {
        let tokenURLFull = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token=\(accessToken)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let verify: NSURL = NSURL(string: tokenURLFull!)!
        let request: NSMutableURLRequest = NSMutableURLRequest(url: verify as URL)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if error == nil {
                let linkedInEmailModel = try? JSONDecoder().decode(LinkedInEmailModel.self, from: data!)

                // LinkedIn Email
                let linkedinEmail: String! = linkedInEmailModel?.elements[0].elementHandle.emailAddress
                print("LinkedIn Email: \(linkedinEmail ?? "")")
                self.linkedInEmail = linkedinEmail

                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "detailseg", sender: self)
                }
            }
        }
        task.resume()
    }
    @objc func cancelAction() {
           self.dismiss(animated: true, completion: nil)
       }

       @objc func refreshAction() {
           self.webView.reload()
       }

}
@available(iOS 13.0, *)
extension ISCLoginViewController : ASAuthorizationControllerDelegate {
    
    private func setupLoginProviderView() {
        // Set button style based on device theme
        let isDarkTheme = view.traitCollection.userInterfaceStyle == .dark
        let style: ASAuthorizationAppleIDButton.Style = isDarkTheme ? .black : .whiteOutline
        
        // Create and Setup Apple ID Authorization Button
        let authorizationButton = ASAuthorizationAppleIDButton(type: .default, style: style)
        authorizationButton.addTarget(self, action: #selector(handleLogInWithAppleIDButtonPress), for: .touchUpInside)
        authorizationButton.translatesAutoresizingMaskIntoConstraints = false

        //Add Apple ID authorization button into the stack view
        self.viewLoginApple.addSubview(authorizationButton)
        NSLayoutConstraint.activate([
            authorizationButton.leadingAnchor.constraint(equalTo: viewLoginApple.leadingAnchor, constant: 0.0),
            authorizationButton.trailingAnchor.constraint(equalTo: viewLoginApple.trailingAnchor, constant: 0.0),
            authorizationButton.bottomAnchor.constraint(equalTo: viewLoginApple.bottomAnchor, constant: 0.0),
            authorizationButton.heightAnchor.constraint(equalToConstant: viewLoginApple.frame.size.height)
        ])
    }
    
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
        
        // Create an authorization controller with the given requests.
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @objc private func handleLogInWithAppleIDButtonPress() {
           let appleIDProvider = ASAuthorizationAppleIDProvider()
           let request = appleIDProvider.createRequest()
           request.requestedScopes = [.fullName, .email]
           
           let authorizationController = ASAuthorizationController(authorizationRequests: [request])
           authorizationController.delegate = self
           authorizationController.presentationContextProvider = self
           authorizationController.performRequests()
       }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            // Create an account in your system.
            // For the purpose of this demo app, store the these details in the keychain.
            KeychainItem.currentUserIdentifier = appleIDCredential.user
            KeychainItem.currentUserFirstName = appleIDCredential.fullName?.givenName
            KeychainItem.currentUserLastName = appleIDCredential.fullName?.familyName
            KeychainItem.currentUserEmail = appleIDCredential.email
            
            print("User Id - \(appleIDCredential.user)")
            print("User Name - \(appleIDCredential.fullName?.description ?? "N/A")")
            print("User Email - \(appleIDCredential.email ?? "N/A")")
            print("Real User Status - \(appleIDCredential.realUserStatus.rawValue)")
            
            if let identityTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                print("Identity Token \(identityTokenString)")
            }
            if appleIDCredential.email ?? "" == "" {
                self.loginExternal(ISCExternalLoginInput(Email: appleIDCredential.user, ExternalAccessToken: "apple", FullName: "\(appleIDCredential.user) \(appleIDCredential.user)", Provider: "apple", UserName: appleIDCredential.user))
            }else {
                self.loginExternal(ISCExternalLoginInput(Email: appleIDCredential.email ?? "", ExternalAccessToken: "apple", FullName: "\(appleIDCredential.fullName?.givenName ?? "") \(appleIDCredential.fullName?.familyName ?? "")", Provider: "apple", UserName: appleIDCredential.email ?? ""))
            }
            
            //Show Home View Controller

        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
                let alertController = UIAlertController(title: "Keychain Credential Received",
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
@available(iOS 13.0, *)
extension ISCLoginViewController : ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
