//
//  ISCResetPasswordViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 1/20/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCResetPasswordViewController: UIBaseViewController {

    @IBOutlet weak var tftOtp: UIAutoSizeTextField!
    @IBOutlet weak var tftNewPassword: UIAutoSizeTextField!
    @IBOutlet weak var tftRePassword: UIAutoSizeTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isShowChat = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func btnLogOut_click(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        _ = appDelegate.grabLogin()
    }
    
    @IBAction func btnSubmit_click(_ sender: Any) {
        if tftOtp.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullOTP)
            return
        }
        if tftNewPassword.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPassword)
            return
        }else {
            if tftNewPassword.text?.count ?? 0 < 8 {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.passwordInvalid)
                return
            }
        }
        if tftRePassword.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullRePassword)
            return
        }else {
            if tftNewPassword.text != tftRePassword.text {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.repassInvalid)
                return
            }
        }
        self.showHUD(message: AppLanguage.connecting)
        self.updatePassword(ISCUpdatePassInput(ConfirmNewPassword: self.tftRePassword.text ?? "", NewPassword: self.tftNewPassword.text ?? "", TokenCode: self.tftOtp.text ?? ""), { (value) in
            self.hideHUD()
            if value.Succeeded ?? false {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.updatePasswordSuccess)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true, completion: {
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        _ = appDelegate.grabLogin()
                    })
                }
            }
            else {
                if (value.Errors ?? []).count > 0 {
                    self.alertMessage(title: AppLanguage.commonAppName, message: value.Errors?[0]?.Description ?? AppLanguage.errorNetworkFailureConnectionApi)
                }
                else {
                    self.alertMessage(title: AppLanguage.commonAppName, message: value.error ?? AppLanguage.errorNetworkFailureConnectionApi)
                }
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
}
