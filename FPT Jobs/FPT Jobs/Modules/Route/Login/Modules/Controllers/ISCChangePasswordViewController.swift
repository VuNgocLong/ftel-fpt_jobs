//
//  ISCChangePasswordViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/17/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCChangePasswordViewController: UIBaseViewController {

    @IBOutlet weak var txtOldPass: UIAutoSizeTextField!
    @IBOutlet weak var txtNewPass: UIAutoSizeTextField!
    @IBOutlet weak var txtConfirmPass: UIAutoSizeTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isShowChat = false
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func btnBack_click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnLogOut_click(_ sender: Any) {
        let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                message: AppLanguage.confirmLogout,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .default))
        alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
            
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            _ = appDelegate.grabLogin()
        })
        
        present(alertController, animated: true)
    }
    @IBAction func btnSubmit_click(_ sender: Any) {
        if txtOldPass.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPassword)
            return
        }
        if txtNewPass.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullPassword)
            return
        }else {
            if txtNewPass.text?.count ?? 0 < 8 {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.passwordInvalid)
                return
            }
        }
        if txtConfirmPass.text == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullRePassword)
            return
        }else {
            if txtNewPass.text != txtConfirmPass.text {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.repassInvalid)
                return
            }
        }
        self.showHUD(message: AppLanguage.connecting)
        self.changePassword(ISCChangePassInput(OldPasword: self.txtOldPass.text ?? "", NewPassword: self.txtNewPass.text ?? "", ConfirmNewPassword: self.txtConfirmPass.text ?? ""), { (value) in
            self.hideHUD()
            if value.Succeeded ?? false {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.updatePasswordSuccess)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else {
                if (value.Errors ?? []).count > 0 {
                    self.alertMessage(title: AppLanguage.commonAppName, message: value.Errors?[0]?.Description ?? AppLanguage.errorNetworkFailureConnectionApi)
                }
                else {
                    self.alertMessage(title: AppLanguage.commonAppName, message: value.error ?? AppLanguage.errorNetworkFailureConnectionApi)
                }
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
}
