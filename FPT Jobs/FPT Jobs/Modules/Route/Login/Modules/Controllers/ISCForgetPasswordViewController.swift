//
//  ISCForgetPasswordViewController.swift
//  FPT Jobs
//
//  Created by Long Vu on 1/13/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCForgetPasswordViewController: UIBaseViewController {

    @IBOutlet weak var tftEmail: UIAutoSizeTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isShowChat = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        let email = tftEmail.text ?? ""
        if email == "" {
            self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.nullEmail)
            return
        }else {
            if !Common.shared.checkEmail(str: email) {
                self.alertMessage(title: AppLanguage.commonAppName, message: AppLanguage.emailInvalid)
                return
            }
        }
        self.showHUD(message: AppLanguage.connecting)
        self.forgetPassword(ISCForgetPasswordInput(Email: email), { (value) in
            self.hideHUD()
            if value.Succeeded ?? false {
                let vc:ISCResetPasswordViewController = ISCResetPasswordViewController()
                let nav:UINavigationController = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .overFullScreen
                nav.modalTransitionStyle = .crossDissolve
                self.present(nav, animated: true, completion: nil)
            }
            else {
                self.alertMessage(title: AppLanguage.commonAppName, message: value.ReturnMesage ?? AppLanguage.errorNetworkFailureConnectionApi)
            }
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.dismiss(animated: false) {
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            _ = appDelegate.grabLogin()
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
