//
//  ISCLoginConnection.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/26/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

func getKongToken(
                 _ onSuccess: @escaping (_ response: String)->(),
                 _ onFailure: @escaping (_ message: String)->()){
    ISCAlamofireManager.shared.getKongToken(ISCLoginConfig.kongToken, APIMethod.get, { (value) in
        onSuccess(value)
    }) { (error) in
        onFailure(error)
    }
}

extension ISCLoginViewController {
    func loginViaPassword(
                _ onSuccess: @escaping (_ response: APIResponse<ISCLoginOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        let email = self.tftEmail.text ?? ""
        let password = self.tftPassword.text ?? ""

        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.loginPassword,isCheckHeader: false, ISCLoginInput(username: email, password: password), .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func externalLogin(
                _ param:ISCExternalLoginInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCLoginOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.externalLogin, isCheckHeader: false, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    func getProfile(
                _ onSuccess: @escaping (_ response: APIResponse<ISCGetProfileOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.getProfile, ISCGetProfileInput(), .get, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
    
    func registerSNS(deviceToken : String, applicationId : String,
                _ onSuccess: @escaping (_ response: APIResponse<ISCRegisterSNSOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.RegisterSNS, ISCRegisterSNSInput(AWSToken: deviceToken, ApplicantId: applicationId), .post, .json, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}

extension ISCForgetPasswordViewController {
    func forgetPassword(
                _ param:ISCForgetPasswordInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCForgetPasswordOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.forgetPassword, isCheckHeader: false, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}


extension ISCRegisterViewController {
    func registerAccount(
                _ param:ISCRegisterInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCRegisterOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.register, isCheckHeader: false, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCChangePasswordViewController {
    func changePassword(
                _ param:ISCChangePassInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCChangePassOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.changePassword, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCResetPasswordViewController {
    func updatePassword(
                _ param:ISCUpdatePassInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCUpdatePassOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        ISCAlamofireManager.shared.apiConnection(ISCLoginConfig.updatePassword, isCheckHeader: false, param, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
