//
//  ISCLoginConfig.swift
//  FPT Jobs
//
//  Created by Long Vu on 12/26/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

struct ISCLoginConfig {
    static let kongToken = K.serverAPI.tokenKong + "GenerateToken"
    static let loginPassword = K.serverAPI.token + ""
    static let getProfile = K.serverAPI.api + "Applicant/GetProfile"
    static let externalLogin = K.serverAPI.api + "Applicant/ExternalLogin"
    static let forgetPassword = K.serverAPI.api + "Applicant/ForgetPassword"
    static let register = K.serverAPI.api + "Applicant/Register"
    static let changePassword = K.serverAPI.api + "Applicant/ChangePassword"
    static let updatePassword = K.serverAPI.api + "Applicant/UpdatePassword"
    static let RegisterSNS = K.serverAPI.api + "AWSSimpleNotification/RegisterSNS"
}
	
