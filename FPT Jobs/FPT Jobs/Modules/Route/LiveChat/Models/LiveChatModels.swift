//
//  LiveChatModels.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/12/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct OutsideChatboxCreateModels {
    var IsOpenChatBox : Int = 0
    var Index : Int = 0
    var Id : Int = 0
    var GroupId : String = ""
    var GroupName : String = ""
    var GroupAvatar : String = ""
    var StartTime : String = ""
    var GroupNameDisplay : String = ""
    init(dict : Dictionary<String,Any>) {
        if let IsOpenChatBox = dict["IsOpenChatBox"] as? Int {
            self.IsOpenChatBox = IsOpenChatBox
        }
        if let Index = dict["Index"] as? Int {
            self.Index = Index
        }
        if let Id = dict["Id"] as? Int {
            self.Id = Id
        }
        if let GroupId = dict["GroupId"] as? String {
            self.GroupId = GroupId
        }
        if let GroupName = dict["GroupName"] as? String {
            self.GroupName = GroupName
        }
        if let GroupAvatar = dict["GroupAvatar"] as? String {
            self.GroupAvatar = GroupAvatar
        }
        if let StartTime = dict["StartTime"] as? String {
            self.StartTime = StartTime
        }
        if let GroupNameDisplay = dict["GroupNameDisplay"] as? String {
            self.GroupNameDisplay = GroupNameDisplay
        }
    }
}

struct OnPersonalMessageMdels {
    var Content : String = ""
    var IsFromAdmin : Int = 0
    var IsOfflineMsg : Int = 0
    var SentBy : String = ""
    var MessageType : Int = 0
    var SentAt : String = ""
    var GroupId : String = ""
    init(dict : Dictionary<String,Any>) {
        if let Content = dict["Content"] as? String {
            self.Content = Content.htmlToString
        }
        if let IsFromAdmin = dict["IsFromAdmin"] as? Int {
            self.IsFromAdmin = IsFromAdmin
        }
        if let IsOfflineMsg = dict["IsOfflineMsg"] as? Int {
            self.IsOfflineMsg = IsOfflineMsg
        }
        if let GroupId = dict["GroupId"] as? String {
            self.GroupId = GroupId
        }
        if let SentBy = dict["SentBy"] as? String {
            self.SentBy = SentBy
        }
        if let MessageType = dict["MessageType"] as? Int {
            self.MessageType = MessageType
        }
        if let SentAt = dict["SentAt"] as? String {
            self.SentAt = SentAt
        }
    }
    init(Content : String, IsFromAdmin : Int, IsOfflineMsg : Int, GroupId : String, SentBy : String, MessageType : Int, SentAt : String) {
        self.Content = Content
        self.IsFromAdmin = IsFromAdmin
        self.IsOfflineMsg = IsOfflineMsg
        self.GroupId = GroupId
        self.SentBy = SentBy
        self.MessageType = MessageType
        self.SentAt = SentAt
    }
}
