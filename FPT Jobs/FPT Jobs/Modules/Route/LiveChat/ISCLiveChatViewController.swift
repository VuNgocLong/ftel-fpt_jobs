//
//  ISCLiveChatViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/10/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SDWebImage
import SignalRSwift

class ISCLiveChatViewController: UIViewController, UITextFieldDelegate {


    @IBOutlet weak var spaceTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    var chatHub: HubProxy!
    var connection: HubConnection!
    var arrayChat : [OnPersonalMessageMdels] = []
    var modelOutsideChatboxCreate : OutsideChatboxCreateModels? = nil
    var userTft:UIAutoSizeTextField!{
        didSet {
            userTft.delegate = self
            userTft.addTarget(self, action: #selector(userTftDidChanged), for: .editingChanged)
        }
    }
    var imgUser : UIImageView = UIImageView()
    var btnRegister : UIButton = UIButton()
    var isAppendHello : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SignalRManager.sharedInstance.delegateChat = self
        SignalRManager.sharedInstance.signalRConnect()
//        self.isShowChat = false
        btnRegister.isHidden = true
        txtMessage.isEnabled = false
        btnSend.isEnabled = false
        let nibTo = UINib(nibName: ISCLiveChatToCell.name, bundle: nil)
        tblList.register(nibTo, forCellReuseIdentifier: ISCLiveChatToCell.name)
        let nibFrom = UINib(nibName: ISCLiveChatFromCell.name, bundle: nil)
        tblList.register(nibFrom, forCellReuseIdentifier: ISCLiveChatFromCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 57
        let gestureRecognizer:UIGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tblList.addGestureRecognizer(gestureRecognizer)
    }
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        addHeader()
    }
    
    @objc func hideKeyboard() {
        self.userTft?.endEditing(true)
        view.endEditing(true)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    @objc
    func userTftDidChanged(textField: UITextField){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.btnRegister.isHidden = false
               self.view.layoutIfNeeded()
           }, completion: nil)
    }
    func checkRegister(){
        var GroupName : String = ""
        var GroupId : String = ""
        var isLogin : Bool = false
        let userProfile: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if userProfile != nil {
            if let InterviewerCode = userProfile?["InterviewerCode"] as? String {
                GroupId = InterviewerCode
                isLogin = true
            }
            if let Email = userProfile?["Email"] as? String {
                GroupName = Email
            }
            
            SignalRManager.sharedInstance.registerUser(groupId: GroupId, email: GroupName, isLogin: isLogin)
            if Common.shared.isKeyPresentInUserDefaults(key: Common.key_avatar){
                let dataImg = Common.shared.getImgAvatar()
                self.imgUser.image = UIImage(data: dataImg as Data)
            }else {
                if let PortraitImage = userProfile?["PortraitImage"] as? String {
                if PortraitImage != "" {
                    SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(PortraitImage)"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                            if let img = image {
                                self.imgUser.image = img
                            }
                        }
                    }
                }
            }
            
            return
        }else {
            let userLiveChat: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserLiveChat)
            if userLiveChat != nil {
                let userLiveChat = OutsideChatboxCreateModels(dict: userLiveChat!)
                GroupName = userLiveChat.GroupName
                SignalRManager.sharedInstance.registerUser(groupId: "", email: GroupName, isLogin: false)
                return
            }else {
                if !isAppendHello {
                    isAppendHello = true
                    let strHello : String = "Chào bạn! Bạn vui lòng nhập tên của mình và click \"Gửi\" để Tuyển dụng FPT Telecom - fptjobs có thể hỗ trợ và giải đáp thắc mắc giúp bạn nhé."
                    arrayChat.append(OnPersonalMessageMdels(Content: strHello, IsFromAdmin: 1, IsOfflineMsg: 0, GroupId: "0", SentBy: "", MessageType: 1, SentAt: ""))
                    tblList.reloadData()
                }
                
            }
        }
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.spaceTopConstraint.constant = -44
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func addHeader() {
        let navigationBarHeight = self.navigationController?.navigationBar.bounds.size.height ?? 10
        let navigationViewBarWidth = UIScreen.main.bounds.width - 64
        let searchView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: navigationViewBarWidth, height: navigationBarHeight - 10))
        let viewUser:UIView = UIView(frame: CGRect(x: 5, y: (navigationBarHeight - 10 - 24) / 2, width: 24, height: 24))
        imgUser.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        imgUser.image = UIImage(named: "user_avatar")
        let userProfile: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if userProfile != nil {
            if Common.shared.isKeyPresentInUserDefaults(key: Common.key_avatar){
                let dataImg = Common.shared.getImgAvatar()
                self.imgUser.image = UIImage(data: dataImg as Data)
            }else {
                if let PortraitImage = userProfile?["PortraitImage"] as? String {
                if PortraitImage != "" {
                    SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(PortraitImage)"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                            if let img = image {
                                self.imgUser.image = img
                            }
                        }
                    }
                }
            }
        }
        viewUser.addSubview(imgUser)
        viewUser.layer.cornerRadius = 12
        viewUser.layer.masksToBounds = true
        
        searchView.addSubview(viewUser)
        searchView.layer.cornerRadius = 4
        searchView.backgroundColor = .white
   
        userTft = UIAutoSizeTextField(frame: CGRect(x: viewUser.frame.size.width + 10, y: 0, width: navigationViewBarWidth - 108, height: navigationBarHeight - 10))
        searchView.addSubview(userTft!)

        userTft!.placeholder = Locale.preferredLanguages[0].contains("vi") ? "Vui lòng nhập tên của bạn":"Please enter my name"
        userTft!.borderStyle = .none
        userTft!.font = UIFont.systemFont(ofSize: 13)
        
        btnRegister.frame = CGRect(x: navigationViewBarWidth - 64, y: 3, width: 54, height: navigationBarHeight - 16)
        btnRegister.layer.cornerRadius = 6
        btnRegister.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        btnRegister.backgroundColor = UIColor.groupTableViewBackground
        btnRegister.layer.borderWidth = 1.0
        btnRegister.addTarget(self, action: #selector(self.RegisterUser), for: .touchUpInside)
        btnRegister.setTitle(Locale.preferredLanguages[0].contains("vi") ? "Gửi":"Send", for: .normal)
        btnRegister.setTitleColor(UIColor(hexString: "#F27024"), for: .normal)
        btnRegister.titleLabel?.font =  UIFont(name: "UTM-Avo", size: 15)
        searchView.addSubview(btnRegister)
        
        self.navigationItem.titleView = searchView
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#F27024")
    }
    
    @IBAction func btnClose_touch(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func RegisterUser() {
    
        if let email = userTft?.text {
//            if !email.isValidEmail() {
//                let alert = UIAlertController(title: AppLanguage.commonAppName, message: AppLanguage.emailInvalid, preferredStyle: UIAlertController.Style.alert)
//                present(alert, animated: true) {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//                        self.dismiss(animated: true, completion: nil)
//                    }
//                }
//                return
//            }
           SignalRManager.sharedInstance.registerUser(groupId: "", email: email, isLogin: false)
           self.view.endEditing(true)
       }
    }

    @IBAction func btnSendChat_touch(_ sender: Any) {
        if let message = txtMessage.text {
            SignalRManager.sharedInstance.onMessageFromOutside(groupId: modelOutsideChatboxCreate?.GroupId ?? "", message: message)
            self.view.endEditing(true)
            self.txtMessage.text = ""
       }
    }

}

extension ISCLiveChatViewController : SignalRChatDelegate {
    
    func didChangeState(_ status: SignalRManagerState) {
        switch status {
        case .disconnected:
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.spaceTopConstraint.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            break
        case .connecting:
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.spaceTopConstraint.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            break
        case .connected:
            arrayChat = []
            checkRegister()
            break
        case .reconnecting:
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.spaceTopConstraint.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
            break
        }
    }
    
    func didOutsideChatboxCreate(_ model: OutsideChatboxCreateModels) {
        modelOutsideChatboxCreate = model
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.spaceTopConstraint.constant = -44
           
            self.view.layoutIfNeeded()
        }, completion: nil)
        userTft?.text = model.GroupName
        userTft?.isEnabled = false
        userTft?.textColor = UIColor.lightGray
        self.btnRegister.isHidden = true
        txtMessage.isEnabled = true
        btnSend.isEnabled = true
        let userProfile: [String: Any]? = UserDefaults.standard.dict(forKey: Common.keyUserProfile)
        if userProfile == nil {
            if !isAppendHello {
                let strHello : String = "Chào bạn! Bạn vui lòng nhập tên của mình và click \"Gửi\" để Tuyển dụng FPT Telecom - fptjobs có thể hỗ trợ và giải đáp thắc mắc giúp bạn nhé."
                arrayChat.append(OnPersonalMessageMdels(Content: strHello, IsFromAdmin: 1, IsOfflineMsg: 0, GroupId: "0", SentBy: "", MessageType: 1, SentAt: ""))
            }
            let strHello : String = "Rất vui vì bạn đã ghé thăm! Tuyển dụng FPT Telecom - fptjobs có thể giúp được gì cho bạn?"
            arrayChat.append(OnPersonalMessageMdels(Content: strHello, IsFromAdmin: 1, IsOfflineMsg: 0, GroupId: "0", SentBy: "", MessageType: 1, SentAt: ""))
            self.tblList.reloadData()
            let ipath = IndexPath(row: self.arrayChat.count - 1, section: 0)
            self.tblList.scrollToRow(at: ipath, at: .bottom, animated: true)
        }else {
            let strHello : String = "Rất vui vì bạn đã ghé thăm! Tuyển dụng FPT Telecom - fptjobs có thể giúp được gì cho bạn?"
            arrayChat.append(OnPersonalMessageMdels(Content: strHello, IsFromAdmin: 1, IsOfflineMsg: 0, GroupId: "0", SentBy: "", MessageType: 1, SentAt: ""))
            self.tblList.reloadData()
            let ipath = IndexPath(row: self.arrayChat.count - 1, section: 0)
            self.tblList.scrollToRow(at: ipath, at: .bottom, animated: true)
        }
        SignalRManager.sharedInstance.onReadAllMessages(groupId: modelOutsideChatboxCreate?.GroupId ?? "")
    }
    
    func didLoadAllMessages(_ arrayMessage: [OnPersonalMessageMdels]) {
        for item in arrayMessage {
            arrayChat.append(item)
        }
        if arrayChat.count > 0 {
            self.tblList.reloadData()
            let ipath = IndexPath(row: self.arrayChat.count - 1, section: 0)
            self.tblList.scrollToRow(at: ipath, at: .bottom, animated: true)
        }
    }
    
    func didPersonalMessage(_ message: OnPersonalMessageMdels) {
        arrayChat.append(message)
        if arrayChat.count > 0 {
            self.tblList.reloadData()
            let ipath = IndexPath(row: self.arrayChat.count - 1, section: 0)
            self.tblList.scrollToRow(at: ipath, at: .bottom, animated: true)
        }
    }
    
    
}

extension ISCLiveChatViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChat.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let obj = arrayChat[indexPath.row]
        if obj.IsFromAdmin == 1 {
            let cell = tblList.dequeueReusableCell(withIdentifier: ISCLiveChatFromCell.name, for: indexPath) as? ISCLiveChatFromCell
            cell?.lblMessage.text = obj.Content
            let arr = obj.SentAt.components(separatedBy: "T")
            var dateMessage : String = ""
            if arr.count > 1 {
                let arrTime = arr[1].components(separatedBy: ".")
                if arrTime.count > 1 {
                    dateMessage = "\(arr[0]) \(arrTime[0])"
                }
            }
            cell?.lblTime.text = Common.shared.convertDateTimeFormaterToString(dateMessage, type: "HH:mm dd/MM/yyyy")
            cell?.selectionStyle = .none
            return cell!
        }else {
            let cell = tblList.dequeueReusableCell(withIdentifier: ISCLiveChatToCell.name, for: indexPath) as? ISCLiveChatToCell
           cell?.lblMessage.text = obj.Content
           let arr = obj.SentAt.components(separatedBy: "T")
           var dateMessage : String = ""
           if arr.count > 1 {
               let arrTime = arr[1].components(separatedBy: ".")
               if arrTime.count > 1 {
                   dateMessage = "\(arr[0]) \(arrTime[0])"
               }
           }
           cell?.lblTime.text = Common.shared.convertDateTimeFormaterToString(dateMessage, type: "HH:mm dd/MM/yyyy")
           cell?.selectionStyle = .none
           return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
    }

}

    
