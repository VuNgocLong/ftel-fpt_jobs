//
//  ISCLiveChatFromCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/12/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCLiveChatFromCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UIAutoSizeLabel!
    @IBOutlet weak var lblTime: UIAutoSizeLabel!
    static var name: String {
          return String(describing: ISCLiveChatFromCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
