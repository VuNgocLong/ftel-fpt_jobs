//
//  ISCTestingViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/21/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCTestingViewController: UIBaseViewController {

    @IBOutlet weak var clvQuestionNumber: UICollectionView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblTime: UIAutoSizeLabel!
    @IBOutlet weak var lblNumberQuestion: UIAutoSizeLabel!
    @IBOutlet weak var viewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewQuestion: UIView!
    @IBOutlet weak var btnNext: UIAutoSizeButton!
    @IBOutlet weak var btnPrevious: UIAutoSizeButton!
    
    var arrayQuestion : [ISCGetDetailExamOutput] = []
    var arrayQuestionExam : [ISCExamQuestionOutput] = []
    var ExamResultId : Int = 0
    var indexQuestion : Int = 0
    var widthTimeQuestion : CGFloat = 0
    var timeMinus : Int = 0
    var timeSecond : Int = 0
    var startDateExam : Date = Date()
    var timer = Timer()
    var objJobs : ISCJobsApplyOutput? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = AppLanguage.examTabTitle

        let cellNib = UINib(nibName: ISCQuestionNumberCollectionViewCell.name, bundle: Bundle.main)
        clvQuestionNumber.register(cellNib, forCellWithReuseIdentifier: ISCQuestionNumberCollectionViewCell.name)
        clvQuestionNumber.delegate = self
        clvQuestionNumber.dataSource = self
        
        let cellNibQuestion = UINib(nibName: ISCQuestionExamTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNibQuestion, forCellReuseIdentifier: ISCQuestionExamTableViewCell.name)
        
        let cellNibExam = UINib(nibName: ISCExamTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNibExam, forCellReuseIdentifier: ISCExamTableViewCell.name)
        
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 90
        
        getWidthQuestion()
        GetExamQuestion(OrderNo: 1, AnswerCode: "", QuestionnaireCode: "", Finish: false)
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(calcular), userInfo: nil, repeats: true)
        let date = Date()
        startDateExam = Calendar.current.date(byAdding: .minute, value: timeMinus, to: date)!
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func appBecomeActive() {
        
        calculatorHour()
    }
    
    @objc func appDidEnterBackground() {
//        startDateExam = Date()
    }
    
    @objc func calcular(){
        if timeSecond == 0 {
            if timeMinus > 0 {
                timeSecond = 59
                timeMinus -= 1
            }else {
                timer.invalidate()
                 let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.examQuestionFinish,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .cancel))
                alertController.addAction(UIAlertAction(title: AppLanguage.commonConfirmTitle, style: .default){ _ in

                 })
                 self.present(alertController, animated: true)
                 return
            }
        }else {
            timeSecond -= 1
        }
        lblTime.text = "\(timeMinus):\(timeSecond)"
    }
    
    override func backAction() {
        let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                                      message: AppLanguage.examQuestionFinish,
                                                                      preferredStyle: .alert)
              alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .cancel))
              alertController.addAction(UIAlertAction(title: AppLanguage.commonConfirmTitle, style: .default){ _ in
              
               let itemOld = self.arrayQuestion[self.indexQuestion]
               if self.indexQuestion == 0 {
                   self.indexQuestion += 1
               }else {
                   self.indexQuestion -= 1
               }
               let itemSelect = self.arrayQuestion[self.indexQuestion]
               self.GetExamQuestion(OrderNo: itemSelect.OrderNo ?? 0, AnswerCode: itemOld.AnswerCode ?? "", QuestionnaireCode: itemOld.QuestionnaireCode ?? "", Finish: true)
               })
               self.present(alertController, animated: true)
    }
    
    @objc override func backAvatarAction() {
        let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                                message: AppLanguage.examQuestionFinish,
                                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .cancel))
        alertController.addAction(UIAlertAction(title: AppLanguage.commonConfirmTitle, style: .default){ _ in
         
         
         let itemOld = self.arrayQuestion[self.indexQuestion]
         if self.indexQuestion == 0 {
             self.indexQuestion += 1
         }else {
             self.indexQuestion -= 1
         }
         let itemSelect = self.arrayQuestion[self.indexQuestion]
         self.GetExamQuestion(OrderNo: itemSelect.OrderNo ?? 0, AnswerCode: itemOld.AnswerCode ?? "", QuestionnaireCode: itemOld.QuestionnaireCode ?? "", Finish: true)
         })
         self.present(alertController, animated: true)
    }
    
    func getWidthQuestion(){
        if indexQuestion == 0 {
            btnPrevious.isHidden = true
        }else {
            btnPrevious.isHidden = false
        }
        if indexQuestion == arrayQuestion.count - 1 {
            btnNext.isHidden = true
        }else {
            btnNext.isHidden = false
        }
        let w = UIScreen.main.bounds.width - 20
        widthTimeQuestion = 0
        var numberQuestion : Int = 0
        let widthPercent : CGFloat = w / CGFloat(arrayQuestion.count)
        for item in arrayQuestion {
            if item.AnswerCode != nil {
                widthTimeQuestion += widthPercent
                numberQuestion += 1
            }
        }
        let itemSelect = arrayQuestion[indexQuestion]
        self.lblNumberQuestion.text = "\(AppLanguage.examQuestionNumber) \(itemSelect.OrderNo ?? 0)/\(arrayQuestion.count)"
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.viewWidthConstraint.constant = self.widthTimeQuestion
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    @IBAction func btnPrevious_click(_ sender: Any) {
        if indexQuestion == 0 {
            return
        }
        btnNext.isEnabled = false
        btnPrevious.isEnabled = false
        let itemOld = arrayQuestion[indexQuestion]
        indexQuestion -= 1
        let itemSelect = arrayQuestion[indexQuestion]
        GetExamQuestion(OrderNo: itemSelect.OrderNo ?? 0, AnswerCode: itemOld.AnswerCode ?? "", QuestionnaireCode: itemOld.QuestionnaireCode ?? "", Finish: false)
        getWidthQuestion()
    }
    
    @IBAction func btnNext_click(_ sender: Any) {
        
        if indexQuestion == arrayQuestion.count - 1 {
            return
        }
        btnNext.isEnabled = false
        btnPrevious.isEnabled = false
        let itemOld = arrayQuestion[indexQuestion]
        indexQuestion += 1
        let itemSelect = arrayQuestion[indexQuestion]
        GetExamQuestion(OrderNo: itemSelect.OrderNo ?? 0, AnswerCode: itemOld.AnswerCode ?? "", QuestionnaireCode: itemOld.QuestionnaireCode ?? "", Finish: false)
        getWidthQuestion()
    }
    @IBAction func btnFinish_click(_ sender: Any) {
        
        let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                               message: AppLanguage.examQuestionFinish,
                                                               preferredStyle: .alert)
       alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .cancel))
       alertController.addAction(UIAlertAction(title: AppLanguage.commonConfirmTitle, style: .default){ _ in
        
        let itemOld = self.arrayQuestion[self.indexQuestion]
        if self.indexQuestion == 0 {
            self.indexQuestion += 1
        }else {
            self.indexQuestion -= 1
        }
        let itemSelect = self.arrayQuestion[self.indexQuestion]
        self.GetExamQuestion(OrderNo: itemSelect.OrderNo ?? 0, AnswerCode: itemOld.AnswerCode ?? "", QuestionnaireCode: itemOld.QuestionnaireCode ?? "", Finish: true)
        })
        self.present(alertController, animated: true)
    }
    func GetExamQuestion(OrderNo : Int, AnswerCode : String, QuestionnaireCode: String, Finish : Bool) {
        self.showHUD(message: AppLanguage.connecting)
        self.arrayQuestionExam = []
        self.getExamQuestion(ISCGetExamQuestionInput(ExamResultId: ExamResultId, OrderNo: OrderNo, AnswerCode: AnswerCode, QuestionnaireCode: QuestionnaireCode), { (data) in
            self.hideHUD()
            if data.ErrorCode == "0" {
                if Finish {
                    self.GetFinishExam()
                }else {
                    if let array = data.ListData {
                          array.forEach { (val) in
                              if let value = val {
                                  self.arrayQuestionExam.append(value)
                              }
                          }
                    }
                    self.tblList.reloadData()
                }
            }
            self.btnNext.isEnabled = true
            self.btnPrevious.isEnabled = true
        }) { (error) in
            self.btnNext.isEnabled = true
            self.btnPrevious.isEnabled = true
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }

    }
    
    func GetFinishExam(){
        self.showHUD(message: AppLanguage.connecting)
        self.getFinishExam(ISCGetFinishExamInput(ExamResultId: ExamResultId), { (data) in
            self.hideHUD()
            if data.Result ?? false {
                if self.objJobs?.FkSurveyDataId ?? 0 > 0 {
                    let storyboard = UIStoryboard(name: "Test", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ISCTestResultSurveyViewController") as! ISCTestResultSurveyViewController
                    vc.hidesBottomBarWhenPushed = true
                    self.pushViewControler(vc, AppLanguage.jobsTabTitle)
                }else {
                    let storyboard = UIStoryboard(name: "Test", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ISCTestingResultViewController") as! ISCTestingResultViewController
                    vc.objResult = data.Data
                    vc.hidesBottomBarWhenPushed = true
                    self.pushViewControler(vc, AppLanguage.jobsTabTitle)
                }
               
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: data.MsgAll ?? "")
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
}
extension ISCTestingViewController:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, ISCQuestionNumberCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayQuestion.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: UIScreen.main.bounds.width / 10 - 2, height: 34)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ISCQuestionNumberCollectionViewCell.name, for: indexPath) as! ISCQuestionNumberCollectionViewCell
        let item = arrayQuestion[indexPath.row]
        cell.btnNumber.setTitle("\(item.OrderNo ?? 0)", for: .normal)
        cell.btnNumber.tag = indexPath.row
        if item.AnswerCode == nil || item.AnswerCode == "" {
            cell.btnNumber.setBackgroundImage(UIImage(), for: .normal)
            cell.btnNumber.backgroundColor = UIColor(hexString: "#EBEBEB")
            cell.btnNumber.setTitleColor(UIColor.black, for: .normal)
        }else {
            cell.btnNumber.setBackgroundImage(UIImage(named: "img-banner"), for: .normal)
            cell.btnNumber.backgroundColor = UIColor.white
            cell.btnNumber.setTitleColor(UIColor.white, for: .normal)
        }
        cell.delegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    func numberQuestion(index: Int) {
        let itemSelect = arrayQuestion[index]
        let itemOld = arrayQuestion[indexQuestion]
        GetExamQuestion(OrderNo: itemSelect.OrderNo ?? 0, AnswerCode: itemOld.AnswerCode ?? "", QuestionnaireCode: itemOld.QuestionnaireCode ?? "", Finish: false)
        indexQuestion = index
        getWidthQuestion()
    }
}

// MARK: - UITableView Delegate & Datasource
extension ISCTestingViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayQuestionExam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCQuestionExamTableViewCell.name, for: indexPath) as? ISCQuestionExamTableViewCell
                else {
                    return UITableViewCell()
            }
            if arrayQuestionExam.count > 0 {
                let obj = arrayQuestionExam[indexPath.row]
                cell.lblQuestiop.text = obj.Questionnaire
                cell.lblAnswer.text = "\(obj.AnswerDescription ?? "")"
                cell.btnCheck.tag = indexPath.row
                cell.btnCheck.addTarget(self, action: #selector(self.checkJobs(_:)), for: .touchUpInside)
                let itemSelect = arrayQuestion[self.indexQuestion]
                if obj.AnswerCode == itemSelect.AnswerCode {
                    cell.imgCheck.image = UIImage(named: "ic_radio")
                }else {
                    cell.imgCheck.image = UIImage(named: "ic_un_radio")
                }
            }
                    
            cell.selectionStyle = .none
            return cell
        }else {
            guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCExamTableViewCell.name, for: indexPath) as? ISCExamTableViewCell
                else {
                    return UITableViewCell()
            }
            if arrayQuestionExam.count > 0 {
                let obj = arrayQuestionExam[indexPath.row]
                cell.lblAnswer.text = "\(obj.AnswerDescription ?? "")"
                cell.btnCheck.tag = indexPath.row
                cell.btnCheck.addTarget(self, action: #selector(self.checkJobs(_:)), for: .touchUpInside)
                let itemSelect = arrayQuestion[self.indexQuestion]
                if obj.AnswerCode == itemSelect.AnswerCode {
                    cell.imgCheck.image = UIImage(named: "ic_radio")
                }else {
                    cell.imgCheck.image = UIImage(named: "ic_un_radio")
                }
            }
                            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    @objc func checkJobs(_ sender:UIButton){
        if arrayQuestionExam.count < sender.tag {
            return
        }
      
        let obj = arrayQuestionExam[sender.tag]
        var itemSelect = arrayQuestion[self.indexQuestion]
        itemSelect.AnswerCode = obj.AnswerCode
        itemSelect.QuestionnaireCode = obj.QuestionnaireCode
        itemSelect.AnswerDescription = obj.AnswerDescription
        arrayQuestion[indexQuestion] = itemSelect
        if sender.tag == 0 {
            guard let cell = tblList.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? ISCQuestionExamTableViewCell else {
                return
            }
            cell.imgCheck.image = UIImage(named: "ic_radio")
            tblList.reloadData()
        }else {
            guard let cell = tblList.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? ISCExamTableViewCell else {
                return
            }
            cell.imgCheck.image = UIImage(named: "ic_radio")
            tblList.reloadData()
        }
        
        
        guard let cell = clvQuestionNumber.cellForItem(at: IndexPath(row: indexQuestion, section: 0)) as? ISCQuestionNumberCollectionViewCell else {
           return
        }
        cell.btnNumber.setBackgroundImage(UIImage(named: "img-banner"), for: .normal)
        cell.btnNumber.backgroundColor = UIColor.white
        cell.btnNumber.setTitleColor(UIColor.white, for: .normal)
        getWidthQuestion()
    }
    
    
    func calculatorHour(){
        
        let secondDate = Date()
        let distanceBetweenDates: TimeInterval? = startDateExam.timeIntervalSince(secondDate)
        
        let secondsInAnMinuts: Double = 60
        let minutes = Int((distanceBetweenDates! / secondsInAnMinuts))

        let seconds = Int(distanceBetweenDates ?? 0) - (minutes * 60)
        timeMinus = Int(minutes)
        timeSecond = Int(seconds)
        calcular()

    }
}
