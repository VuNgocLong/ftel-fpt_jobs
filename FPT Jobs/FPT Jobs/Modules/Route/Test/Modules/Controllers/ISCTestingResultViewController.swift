//
//  ISCTestingResultViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/11/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCTestingResultViewController: UIBaseViewController {

    @IBOutlet weak var lblTitle: UIAutoSizeLabel!
    @IBOutlet weak var lbLDesc: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lbLPoint: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lbLPointPass: UIAutoSizeLabelNomarl!
    @IBOutlet weak var lbLStatus: UIAutoSizeLabelNomarl!
    @IBOutlet weak var btnInterView: UIAutoSizeButton!
    var objResult : ISCGetFinishExamOutput? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppLanguage.examTabTitle
        // Do any additional setup after loading the view.
        if CGFloat(objResult!.TotalPointArchived ?? 0.0) < CGFloat(objResult!.ThresholdPointToPass ?? 0.0) {
            self.lblTitle.text = Locale.preferredLanguages[0].contains("vi") ? "BẠN KHÔNG VƯỢT QUA BÀI THI":"YOU NOT PASS EXAM"
            self.lbLDesc.text = Locale.preferredLanguages[0].contains("vi") ? "Rất tiếc bạn chưa vượt qua bài thi. Vui lòng quay lại ứng tuyển vị trí này sau 3 tháng":"You did not pass the exam. Please come back three months later"
            lbLStatus.text = "Bạn đã không vượt qua bài thi"
            btnInterView.setTitle("KẾT THÚC BÀI THI", for: .normal)
        }else {
            self.lblTitle.text = Locale.preferredLanguages[0].contains("vi") ? "BẠN VƯỢT QUA BÀI THI":"YOU PASS EXAM"
            self.lbLDesc.text = Locale.preferredLanguages[0].contains("vi") ? "Chúc mừng bạn đã vượt qua bài thi.":"You passed the exam"
            lbLStatus.text = "Bạn đã vượt qua bài thi"
            btnInterView.setTitle("BẮT ĐẦU PHỎNG VẤN", for: .normal)
        }
        lbLPointPass.text = "\(objResult!.ThresholdPointToPass ?? 0)"
        lbLPoint.text = "\(objResult!.TotalPointArchived ?? 0)"
    }
    

    @objc override func backAvatarAction() {
        if let view = self.navigationController?.viewControllers {
            var arrViewControllerMain: [UIViewController] = []
            for n in 0...view.count - 4 {
                arrViewControllerMain.append(view[n])
            }
            
            self.navigationController?.viewControllers = arrViewControllerMain
       }
    }
    
    override func backAction() {
        if let view = self.navigationController?.viewControllers {
             var arrViewControllerMain: [UIViewController] = []
             for n in 0...view.count - 4 {
                 arrViewControllerMain.append(view[n])
             }
             
             self.navigationController?.viewControllers = arrViewControllerMain
        }
    }
    
    @IBAction func btnInterView_touch(_ sender: Any) {
        if CGFloat(objResult!.TotalPointArchived ?? 0.0) < CGFloat(objResult!.ThresholdPointToPass ?? 0.0) {
            if let view = self.navigationController?.viewControllers {
                 var arrViewControllerMain: [UIViewController] = []
                 for n in 0...view.count - 4 {
                     arrViewControllerMain.append(view[n])
                 }
                 
                 self.navigationController?.viewControllers = arrViewControllerMain
            }
        }else {

            let storyboard = UIStoryboard(name: "InterviewOnline", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCRobotInterviewGuidViewController") as! ISCRobotInterviewGuidViewController
            vc.jobcode = objResult?.JobCode ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.pushViewControler(vc, AppLanguage.jobsInterViewOnline)
        }
    }
}
