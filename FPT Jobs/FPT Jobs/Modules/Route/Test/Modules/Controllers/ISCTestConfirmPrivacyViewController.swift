//
//  ISCTestConfirmPrivacyViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/20/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import WebKit
class ISCTestConfirmPrivacyViewController: UIBaseViewController, WKNavigationDelegate {

    @IBOutlet weak var webViewContent: WKWebView!
    @IBOutlet weak var imgConfirm: UIImageView!
    var isConfirm : Bool = false
    var ExamResultId : Int = 0
    var ApplicantApplyReId : Int = 0
    var arrayQuestion : [ISCGetDetailExamOutput] = []
    var objJobs : ISCJobsApplyOutput? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.title = AppLanguage.examTabTitle
        // Do any additional setup after loading the view. examTabTitle
        webViewContent.navigationDelegate = self
        getTermsOfExam()
        
    }
    
    
    @objc override func backAvatarAction() {
        if let view = self.navigationController?.viewControllers {
            var arrViewControllerMain: [UIViewController] = []
            for n in 0...view.count - 3 {
                arrViewControllerMain.append(view[n])
            }
            
            self.navigationController?.viewControllers = arrViewControllerMain
       }
    }
    
    @IBAction func btnConfirm_click(_ sender: Any) {
        if isConfirm {
            isConfirm = false
            imgConfirm.image = UIImage(named: "ic_uncheck")
        }else {
            isConfirm = true
            imgConfirm.image = UIImage(named: "ic_check")
        }
    }
    @IBAction func btnTesting_click(_ sender: Any) {
        if isConfirm {
            self.showHUD(message: AppLanguage.connecting)
            self.getExamTime(ISCGetTimeExamInput(ApplicantApplyReId: ExamResultId, ExamResultId: 0), { (data) in
                self.hideHUD()
                if data.ErrCode == 0 {
                    self.GetDetailExam(ExamLength: data.Data?.ExamLength ?? 0, ExamVideoLength: data.Data?.ExamVideoLength ?? 0)
                }else {
                    self.alertMessage(title: AppLanguage.commonAppName, message: data.MsgAll ?? "")
                }
            }) { (error) in
                self.hideHUD()
                if error == AppLanguage.unauthorizedMessage {
                    let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                            message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                            preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        _ = appDelegate.grabLogin()
                    })
                    self.present(alertController, animated: true)
                    return
                }else {
                    self.alertMessage(title: AppLanguage.commonAppName, message: error)
                }
            }
           
        }
        
    }
    
    func getTermsOfExam(){
        self.showHUD(message: AppLanguage.connecting)
        self.getTermsOfExam({ (data) in
            self.hideHUD()
            if let obj = data.Data {
               self.webViewContent.loadHTMLString(obj.Value ?? "", baseURL: nil)
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    func GetDetailExam(ExamLength : Int,ExamVideoLength : Int ) {
        self.showHUD(message: AppLanguage.connecting)
        self.getDetailExam(ISCGetDetailExamInput(ExamResultId: ExamResultId), { (data) in
            self.hideHUD()
            
            if data.ErrorCode == "0" {
                if let array = data.ListData {
                      array.forEach { (val) in
                          if let value = val {
                              self.arrayQuestion.append(value)
                          }
                      }
                }
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.getTimeExamAlert.replacingOccurrences(of: "{0}", with: "\(ExamLength)").replacingOccurrences(of: "{1}", with: "\(ExamVideoLength)"),
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonCancelTitle, style: .cancel))
                alertController.addAction(UIAlertAction(title: AppLanguage.commonConfirmTitle, style: .default){ _ in
                    let storyboard = UIStoryboard(name: "Test", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ISCTestingViewController") as! ISCTestingViewController
                    vc.ExamResultId = self.ExamResultId
                    vc.arrayQuestion = self.arrayQuestion
                    vc.timeMinus = ExamVideoLength
                    vc.hidesBottomBarWhenPushed = true
                    self.pushViewControler(vc, "")
                })
                self.present(alertController, animated: true)
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: data.MsgAll ?? "")
            }
        }) { (error) in
            self.hideHUD()
            if error == AppLanguage.unauthorizedMessage {
                let alertController = UIAlertController(title: AppLanguage.commonAppName,
                                                        message: AppLanguage.jobsApplyFailUnauthorizedMessage,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: AppLanguage.commonOkTitle, style: .cancel){ _ in
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    _ = appDelegate.grabLogin()
                })
                self.present(alertController, animated: true)
                return
            }else {
                self.alertMessage(title: AppLanguage.commonAppName, message: error)
            }
        }
    }
    
    func parseTime(timeStr : String)-> String{
        var str : String = timeStr
        str = str.replacingOccurrences(of: "T", with: " ")
        let arrStr = str.components(separatedBy: " ")
        if arrStr.count > 1 {
            var timeStr = arrStr[1]
            let arrTimeStr = timeStr.components(separatedBy: ".")
            if arrTimeStr.count > 0 {
                timeStr = arrTimeStr[0]
                return timeStr
            }
        }
        return ""
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideHUD()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideHUD()
    }
}
