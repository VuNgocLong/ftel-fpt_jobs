//
//  ISCQuestionNumberCollectionViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/21/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
protocol ISCQuestionNumberCollectionViewCellDelegate: class {
    func numberQuestion (index : Int)
}
class ISCQuestionNumberCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var btnNumber: UIAutoSizeButton!
    weak var delegate:ISCQuestionNumberCollectionViewCellDelegate?
    static var name: String {
        return String(describing: ISCQuestionNumberCollectionViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnNumberClick(_ sender: Any) {
        let btn = sender as? UIButton
        self.delegate?.numberQuestion(index: btn?.tag ?? 0)
    }
}
