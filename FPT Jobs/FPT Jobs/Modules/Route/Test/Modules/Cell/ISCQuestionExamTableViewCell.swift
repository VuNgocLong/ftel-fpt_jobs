//
//  ISCQuestionExamTableViewCell.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/10/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCQuestionExamTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestiop: UIAutoSizeLabelNomarl!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblAnswer: UIAutoSizeLabelNomarl!
    static var name: String {
        return String(describing: ISCQuestionExamTableViewCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
