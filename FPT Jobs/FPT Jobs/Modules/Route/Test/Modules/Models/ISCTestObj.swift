//
//  ISCTestObj.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 1/21/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCExamOutput: Codable {
    var Question:String?
    var Answer:String?
}
struct ISCTermsOfExamInput: Encodable {
}
struct ISCTermsOfExamOutput: Codable {
    var Name:String?
    var Value:String?
}
struct CheckExamExistsInput: Encodable {
    var ApplicantApplyReId:Int?
}
struct CheckExamExistsOutput: Codable {
    var ExamResultId:Int?
}
struct ISCGetTimeExamInput: Encodable {
    var ApplicantApplyReId:Int?
    var ExamResultId:Int?
}
struct ISCGetTimeExamOutput: Codable {
    var ApplicantApplyReId:Int?
    var ExamResultId:Int?
    var ExamLength:Int?
    var ExamVideoLength:Int?
}
struct ISCGetDetailExamInput: Encodable {
    var ExamResultId:Int?
}
struct ISCGetDetailExamOutput: Codable {
    var ApplicantApplyReId:Int?
    var ExamResultId:Int?
    var Id:Int?
    var InterviewCode:String?
    var JobCode:String?
    var Questionnaire:String?
    var QuestionnaireEn:String?
    var QuestionnaireCode:String?
    var OrderNo:Int?
    var AnswerDescription:String?
    var AnswerPoint:Float?
    var AnswerCode:String?
    var TimeLeft:String?
    var TotalQuestionaireNumber:Int?
    var InterviewAnswer:String?
    var TotalPointArchived:Float?
    var ThresholdPointToPass:Float?
    var IsPass:Int?
    var ANSWERED:Int?
    var ExamLength:Int?
}
struct ISCGetExamQuestionInput: Encodable {
    var ExamResultId:Int?
    var OrderNo:Int?
    var AnswerCode:String?
    var QuestionnaireCode:String?
}
struct ISCExamQuestionOutput: Codable {
    var AnswerCode:String?
    var AnswerDescription:String?
    var AnswerPoint:Float?
    var Answered:Int?
    var ApplicantApplyReId:Int?
    var ExamResultId:Int?
    var ExamLength:Int?
    var ExamVideoLength:Int?
    var Id:Int?
    var OrderNo:Int?
    var Questionnaire:String?
    var QuestionnaireCode:String?
    var QuestionnaireEn:String?
    var ThresholdPointToPass:Int?
    var TimeLeft:String?
    var TotalPointArchived:Float?
    var TotalQuestionaireNumber:Int?
    
}
struct ISCGetFinishExamInput: Encodable {
    var ExamResultId:Int?
}
struct ISCGetFinishExamOutput: Codable {
    var ApplicantApplyReId:Int?
    var Id:Int?
    var JobCode:String?
    var TotalPointArchived:Float?
    var ThresholdPointToPass:Float?
}
