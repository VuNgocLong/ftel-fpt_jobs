//
//  ISCTestConfig.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCTestConfig {
    static let getTermsOfExam = K.serverAPI.api + "Catalog/GetConfig/TermsOfExam"
    static let checkExamExists = K.serverAPI.api + "Exam/CheckExamExists"
    static let getExamTime = K.serverAPI.api + "Exam/GetTime"
    static let getDetailExam = K.serverAPI.api + "Exam/GetDetailExam"
    static let getExamQuestion = K.serverAPI.api + "Exam/GetExamQuestion"
    static let getFinishExam = K.serverAPI.api + "Exam/FinishExam"
}
