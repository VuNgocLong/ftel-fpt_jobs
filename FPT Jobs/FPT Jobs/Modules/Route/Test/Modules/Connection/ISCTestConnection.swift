//
//  ISCTestConnection.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 2/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
extension ISCTestConfirmPrivacyViewController {
    func getTermsOfExam(
                   _ onSuccess: @escaping (_ response: APIResponse<ISCTermsOfExamOutput>)->(),
                   _ onFailure: @escaping (_ message: String)->()){
           ISCAlamofireManager.shared.apiConnection(ISCTestConfig.getTermsOfExam, ISCTermsOfExamInput(), .get, .application, { (value) in
               onSuccess(value)
           }) { (error) in
               onFailure(error)
           }
       }
    func getExamTime(_ param : ISCGetTimeExamInput,
                    _ onSuccess: @escaping (_ response: APIResponse<ISCGetTimeExamOutput>)->(),
                    _ onFailure: @escaping (_ message: String)->()){
            ISCAlamofireManager.shared.apiConnection(ISCTestConfig.getExamTime, param, .post, .application, { (value) in
                onSuccess(value)
            }) { (error) in
                onFailure(error)
            }
    }
    
    func getDetailExam(_ param : ISCGetDetailExamInput,
                       _ onSuccess: @escaping (_ response: APIResponse<ISCGetDetailExamOutput>)->(),
                       _ onFailure: @escaping (_ message: String)->()){
               ISCAlamofireManager.shared.apiConnection(ISCTestConfig.getDetailExam, param, .post, .application, { (value) in
                   onSuccess(value)
               }) { (error) in
                   onFailure(error)
               }
       }
    
}
extension ISCTestingViewController {
    func getExamQuestion(_ param : ISCGetExamQuestionInput,
                    _ onSuccess: @escaping (_ response: APIResponse<ISCExamQuestionOutput>)->(),
                    _ onFailure: @escaping (_ message: String)->()){
            ISCAlamofireManager.shared.apiConnection(ISCTestConfig.getExamQuestion, param, .post, .application, { (value) in
                onSuccess(value)
            }) { (error) in
                onFailure(error)
            }
    }
    func getFinishExam(_ param : ISCGetFinishExamInput,
                    _ onSuccess: @escaping (_ response: APIResponse<ISCGetFinishExamOutput>)->(),
                    _ onFailure: @escaping (_ message: String)->()){
            ISCAlamofireManager.shared.apiConnection(ISCTestConfig.getFinishExam, param, .post, .application, { (value) in
                onSuccess(value)
            }) { (error) in
                onFailure(error)
            }
    }
}


