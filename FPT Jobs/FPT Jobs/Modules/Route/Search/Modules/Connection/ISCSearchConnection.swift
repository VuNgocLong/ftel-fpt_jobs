//
//  ISCSearchConnection.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
extension ISCSearchAllViewController {
    func fullSearchForMobile(_ pram : ISCSearchForMobileInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCSearchForMobileOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCSearchConfig.fullSearchForMobile,isCheckHeader: false, pram, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCSearchRecruitmentViewController {
    func fullJobsSearch(_ pram : ISCFullJobsSearchInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCFullJobsSearchOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCSearchConfig.fullJobsSearch,isCheckHeader: false, pram, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCSearchNewsViewController {
    func fullArticleSearch(_ pram : ISCFullArticleSearchInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCFullArticleSearchOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCSearchConfig.fullArticleSearch,isCheckHeader: false, pram, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
extension ISCSearchEventViewController {
    func fullSurveyStructureSearch(_ pram : ISCFullArticleSearchInput,
                _ onSuccess: @escaping (_ response: APIResponse<ISCFullArticleSearchOutput>)->(),
                _ onFailure: @escaping (_ message: String)->()){
        
        ISCAlamofireManager.shared.apiConnection(ISCSearchConfig.fullSurveyStructureSearch,isCheckHeader: false, pram, .post, .application, { (value) in
            onSuccess(value)
        }) { (error) in
            onFailure(error)
        }
    }
}
