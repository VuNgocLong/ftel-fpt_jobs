//
//  ISCSearchConfig.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCSearchConfig {
    static let fullSearchForMobile = K.serverAPI.api + "Search/FullSearchForMobile"
    static let fullJobsSearch = K.serverAPI.api + "Search/FullJobsSearch"
    static let fullArticleSearch = K.serverAPI.api + "Search/FullArticleSearch"
    static let fullSurveyStructureSearch = K.serverAPI.api + "Search/FullSurveyStructureSearch"
}

