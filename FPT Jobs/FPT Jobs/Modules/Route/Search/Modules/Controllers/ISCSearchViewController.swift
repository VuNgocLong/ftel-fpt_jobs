//
//  ISCSearchViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCSearchViewController: UIBaseViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate{

    @IBOutlet weak var viewPage: UIView!
    
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var viewTab: UIView!
    @IBOutlet private weak var btnTab1: UIAutoSizeButton!
    @IBOutlet private weak var btnTab2: UIAutoSizeButton!
    @IBOutlet private weak var btnTab3: UIAutoSizeButton!
    @IBOutlet private weak var btnTab4: UIAutoSizeButton!
    @IBOutlet private weak var viewLine: UIView!
    @IBOutlet private weak var constantViewLeft: NSLayoutConstraint!

    var tab1VC:ISCSearchAllViewController! = nil
    var tab2VC:ISCSearchRecruitmentViewController! = nil
    var tab3VC:ISCSearchNewsViewController! = nil
    var tab4VC:ISCSearchEventViewController! = nil
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    var currentPage: Int! = 0
    var Content : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.isSearch = true
        self.isSearchViewController = true
        createPageViewController()
        
    }
    
    
    
    
    override func searchTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        tab1VC.Content = self.searchTft?.text ?? ""
        tab2VC.Content = self.searchTft?.text ?? ""
        tab3VC.Content = self.searchTft?.text ?? ""
        tab4VC.Content = self.searchTft?.text ?? ""
        switch currentPage {
           case 1:
               tab2VC.searchBar()
               break
           case 2:
               tab3VC.searchBar()
               break
           case 3:
               tab4VC.searchBar()
               break
           default:
               tab1VC.searchBar()
               break
           }
        self.searchTft?.resignFirstResponder()
        view.endEditing(true)
    }
}
//MARK : set up tab bar
extension ISCSearchViewController {
    private func selectedButton(btn: UIButton) {
        
        btn.tintColor = UIColor(hexString: "#F37320")
        btn.setTitleColor(UIColor(hexString: "#F37320"), for: .normal)
        constantViewLeft.constant = btn.frame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.tintColor = UIColor(hexString: "#999999")
        btn.setTitleColor(UIColor(hexString: "#999999"), for: UIControl.State.normal)
    }
    
    //MARK: - IBaction Methods
    
    @IBAction private func btnOptionClicked(btn: UIButton) {
        
        pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewController.NavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        
        resetTabBarForTag(tag: btn.tag-1)
    }
    
    //MARK: - CreatePagination
    
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          
            self.pageController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.pageController.view.frame = CGRect(x: 0, y: 44, width: self.view.frame.size.width, height: self.view.frame.size.height - 88)
//            self.pageController.view.frame = self.view.frame
            
        }
        
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        tab1VC = storyboard.instantiateViewController(withIdentifier: "ISCSearchAllViewController") as? ISCSearchAllViewController
        tab1VC.Content = self.Content
        tab2VC = storyboard.instantiateViewController(withIdentifier: "ISCSearchRecruitmentViewController") as? ISCSearchRecruitmentViewController
        tab2VC.Content = self.Content
        tab3VC = storyboard.instantiateViewController(withIdentifier: "ISCSearchNewsViewController") as? ISCSearchNewsViewController
        tab3VC.Content = self.Content
        tab4VC = storyboard.instantiateViewController(withIdentifier: "ISCSearchEventViewController") as? ISCSearchEventViewController
        tab4VC.Content = self.Content
        arrVC = [tab1VC, tab2VC, tab3VC, tab4VC]
        switch currentPage {
        case 1:
            pageController.setViewControllers([tab2VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
            break
        case 2:
            pageController.setViewControllers([tab3VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
            break
        case 3:
            pageController.setViewControllers([tab4VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
            break
        default:
            pageController.setViewControllers([tab1VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
            break
        }
      
        
        self.addChild(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParent: self)
        self.searchTft?.text = Content
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC.contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    
    private func resetTabBarForTag(tag: Int) {
        
        var sender: UIButton!
        
        if(tag == 0) {
            sender = btnTab1
        }
        else if(tag == 1) {
            sender = btnTab2
        }
        else if(tag == 2) {
            sender = btnTab3
        }
        else if(tag == 3) {
           sender = btnTab4
       }
        
        currentPage = tag
        
        unSelectedButton(btn: btnTab1)
        unSelectedButton(btn: btnTab2)
        unSelectedButton(btn: btnTab3)
        unSelectedButton(btn: btnTab4)
        
        selectedButton(btn: sender)
        
    }
    
    //MARK: - UIScrollView Delegate Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let xFromCenter: CGFloat = self.view.frame.size.width-scrollView.contentOffset.x
        let xCoor: CGFloat = CGFloat(viewLine.frame.size.width) * CGFloat(currentPage)
        let xPosition: CGFloat = xCoor - xFromCenter/CGFloat(arrVC.count)
        constantViewLeft.constant = xPosition
    }
    
    
}

