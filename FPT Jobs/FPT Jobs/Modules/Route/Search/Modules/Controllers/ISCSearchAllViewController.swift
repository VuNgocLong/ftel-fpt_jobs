//
//  ISCSearchAllViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SDWebImage
class ISCSearchAllViewController: UIBaseViewController, UITextFieldDelegate {

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblTotal: UIAutoSizeLabel!
    var ArticleFromRecord : Int = 0
    var ArticlePageSize : Int = 20
    var JobFromRecord : Int = 0
    var JobsPageSize : Int = 20
    var Content : String = ""
    var arrRecruitmentNews : [RecruitmentNewsModel] = []
    var arrArticle : [ArticleModel] = []
    var arrData : [ISCSearchForMobileOutput] = []
    var timer = Timer()
    var isLoadMore : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
         self.isShowChat = false
        // Do any additional setup after loading the view.
       
        let nibLoading = UINib(nibName: LoadingTableViewCell.name, bundle: nil)
        tblList.register(nibLoading, forCellReuseIdentifier: LoadingTableViewCell.name)

        let cellNib = UINib(nibName: ISCJobsTableViewCell.name, bundle: Bundle.main)
        let cellNibNew = UINib(nibName: ISCListNewsTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCJobsTableViewCell.name)
        tblList.register(cellNibNew, forCellReuseIdentifier: ISCListNewsTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 100
        
        self.searchTft?.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchTft?.resignFirstResponder()
        return false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.showHUD(message: AppLanguage.connecting)
        arrData = []
        arrArticle = []
        arrRecruitmentNews = []
        ArticleFromRecord = 0
        JobFromRecord = 0
        self.showHUD(message: AppLanguage.connecting)
        loadData()
    }
    
    func searchBar(){
        arrData = []
        arrArticle = []
        arrRecruitmentNews = []
        ArticleFromRecord = 0
        JobFromRecord = 0
        self.showHUD(message: AppLanguage.connecting)
        loadData()
    }
    
    func loadData(){
        
        self.fullSearchForMobile(ISCSearchForMobileInput(ArticleFromRecord: ArticleFromRecord, ArticlePageSize: ArticlePageSize, JobFromRecord: JobFromRecord, JobsPageSize: JobsPageSize, Content: Content), { (data) in
            self.hideHUD()
            if data.Data != nil {
                self.lblTotal.text = "\(data.Data?.TotalCount ?? 0)"
                if data.Data?.Article?.count ?? 0 > 0 || data.Data?.RecruitmentNews?.count ?? 0 > 0 {
                    self.isLoadMore = true
                   
                    for item in data.Data?.RecruitmentNews ?? []  {
                         self.arrRecruitmentNews.append(item)
                    }
                    for item in data.Data?.Article ?? [] {
                       self.arrArticle.append(item)
                   }
                    
                }
                self.arrData.append(data.Data!)
                self.tblList.reloadData()
            }
            
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }

}
// MARK: - UITableView Delegate & Datasource
extension ISCSearchAllViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrRecruitmentNews.count) + (arrArticle.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.section]
        if arrRecruitmentNews.count > 0 {
            if arrRecruitmentNews.count > indexPath.row {
                guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCJobsTableViewCell.name, for: indexPath) as? ISCJobsTableViewCell
                    else {
                        return UITableViewCell()
                }
                 let obj = arrRecruitmentNews[indexPath.row]
                 cell.lblTitle.text = obj.Title
                 if obj.MinSalary == nil && obj.MaxSalary == nil {
                     cell.lblSalary.text = AppLanguage.jobsSalary
                 }else {
                     let min = Int(obj.MinSalary ?? "0")
                     let max = Int(obj.MaxSalary ?? "0")
                     let nf = NumberFormatter()
                     nf.numberStyle = .decimal
                     cell.lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0") \(obj.CurrentUnit ?? "")"
                 }
                 cell.lblStatus.text = "HOT"
                 cell.viewStatus.isHidden = false
                 cell.viewStatus.backgroundColor = UIColor(hexString: "#F27024")
                 cell.spaceRightTitleConstraint.constant = 60
                 cell.lblNumberPosition.text = "\(obj.NumberPositions ?? 0)"
                 cell.lblAreaName.text = obj.AreaName
                 cell.lblEndDate.text = obj.EndDateStr
                cell.selectionStyle = .none
                return cell
            }else {
                guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCListNewsTableViewCell.name, for: indexPath) as? ISCListNewsTableViewCell
                      else {
                          return UITableViewCell()
                  }
                   let obj = arrArticle[indexPath.row - arrRecruitmentNews.count]
                SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(obj.ImagePath ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                      if let img = image {
                          cell.imgNews.image = img
                      }
                  }
                   cell.lblTitle.text = obj.Title
                   cell.lblDescription.text = obj.Description
                   cell.lblDate.text = obj.CreatedDateStr
                  cell.selectionStyle = .none
                  return cell
            }
            
        }else if model.Article?.count ?? 0 > 0 {
            guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCListNewsTableViewCell.name, for: indexPath) as? ISCListNewsTableViewCell
               else {
                   return UITableViewCell()
           }
            let obj = model.Article?[indexPath.row - (model.RecruitmentNews?.count ?? 0)]
            SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.baseImageURL)\(obj?.ImagePath ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
               if let img = image {
                   cell.imgNews.image = img
               }
           }
            cell.lblTitle.text = obj?.Title
            cell.lblDescription.text = obj?.Description
            cell.lblDate.text = obj?.CreatedDateStr
           cell.selectionStyle = .none
           return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrData[indexPath.section]
        if model.RecruitmentNews?.count ?? 0 > 0 {
            if model.RecruitmentNews?.count ?? 0 > indexPath.row {
                let obj = model.RecruitmentNews?[indexPath.row]
                let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
                vc.RecruitmentId = obj?.RecruitmentId ?? 0
                vc.TypePush = "MAIN"
                vc.hidesBottomBarWhenPushed = true
                self.pushViewControler(vc, AppLanguage.jobsTabTitle)
            }else {
                let obj = model.Article?[indexPath.row - (model.RecruitmentNews?.count ?? 0)]
                let storyboard = UIStoryboard(name: "News", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ISCNewsDetailViewController") as! ISCNewsDetailViewController
                vc.Title = obj?.Title ?? ""
                vc.Date = obj?.CreatedDateStr ?? ""
                vc.Description = obj?.Description ?? ""
                vc.Content = obj?.Content ?? ""
                vc.tabTitle = AppLanguage.newsShareTitle
                vc.hidesBottomBarWhenPushed = false
                self.pushViewControler(vc, "Tin tức và chia sẽ")
            }
            
        }else if model.Article?.count ?? 0 > 0 {
            let obj = model.Article?[indexPath.row - (model.RecruitmentNews?.count ?? 0)]
            let storyboard = UIStoryboard(name: "News", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ISCNewsDetailViewController") as! ISCNewsDetailViewController
            vc.Title = obj?.Title ?? ""
            vc.Date = obj?.CreatedDateStr ?? ""
            vc.Description = obj?.Description ?? ""
            vc.Content = obj?.Content ?? ""
            vc.tabTitle = AppLanguage.newsShareTitle
            vc.hidesBottomBarWhenPushed = false
            self.pushViewControler(vc, "Tin tức và chia sẽ")
        }

    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        let index = (arrRecruitmentNews.count) + (arrArticle.count)
        if indexPath.row == index - 3 {
            if isLoadMore {
                isLoadMore = false
                timer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(loadmore), userInfo: nil, repeats: true)
            }
        }
    }
    @objc func loadmore(){
        self.JobFromRecord += 20
        self.ArticleFromRecord += 20
        timer.invalidate()
        loadData()
    }
}
