//
//  ISCSearchRecruitmentViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCSearchRecruitmentViewController: UIBaseViewController {

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblTotalResult: UIAutoSizeLabelNomarl!
    var FromRecord : Int = 0
    var PageSize : Int = 20
    var Content : String = ""
    var arrData : [ISCJobsOutput] = []
    var timer = Timer()
    var isLoadMore : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
         self.isShowChat = false
        // Do any additional setup after loading the view.
        let nibLoading = UINib(nibName: LoadingTableViewCell.name, bundle: nil)
        tblList.register(nibLoading, forCellReuseIdentifier: LoadingTableViewCell.name)

        let cellNib = UINib(nibName: ISCJobsTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCJobsTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 100
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showHUD(message: AppLanguage.connecting)
        arrData = []
        loadData()
    }
    
    func searchBar(){
        FromRecord = 0
        arrData = []
        loadData()
    }

    func loadData(){
        self.fullJobsSearch(ISCFullJobsSearchInput(PageSize: PageSize, FromRecord: FromRecord, Content: Content), { (data) in
            self.hideHUD()
            if let array = data.Data?.RecruitmentNews {
                self.lblTotalResult.text = "\(data.Data?.TotalCount ?? 0)"
               array.forEach { (val) in
                   self.arrData.append(val)
               }
           }
           if data.Data?.RecruitmentNews?.count ?? 0 > 0 {
               if data.Data?.RecruitmentNews?.count ?? 0 < 20 {
                   self.isLoadMore = false
               }else {
                   self.isLoadMore = true
               }
           }else {
               self.isLoadMore = false
           }
           self.tblList.reloadData()
        }) { (error) in
            self.hideHUD()
            self.alertMessage(title: AppLanguage.commonAppName, message: error)
        }
    }
}
// MARK: - UITableView Delegate & Datasource
extension ISCSearchRecruitmentViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCJobsTableViewCell.name, for: indexPath) as? ISCJobsTableViewCell
            else {
                return UITableViewCell()
        }
        if arrData.count > 0 {
            let obj = arrData[indexPath.row]
            cell.lblTitle.text = obj.Title
            if obj.MinSalary == nil && obj.MaxSalary == nil {
                cell.lblSalary.text = AppLanguage.jobsSalary
            }else {
                let min = Int(obj.MinSalary ?? "0")
                let max = Int(obj.MaxSalary ?? "0")
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.lblSalary.text = "\(nf.string(from: NSNumber(value: min ?? 0) ) ?? "0") - \(nf.string(from: NSNumber(value: max ?? 0) ) ?? "0") \(obj.CurrentUnit ?? "")"
            }
            cell.lblStatus.text = "HOT"
            cell.viewStatus.isHidden = false
            cell.viewStatus.backgroundColor = UIColor(hexString: "#F27024")
            cell.spaceRightTitleConstraint.constant = 60
            cell.lblNumberPosition.text = "\(obj.NumberPositions ?? 0)"
            cell.lblAreaName.text = obj.AreaName
            cell.lblEndDate.text = obj.EndDateStr
            cell.selectionStyle = .none
            
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrData[indexPath.row]
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCJobsDetailViewController") as! ISCJobsDetailViewController
        vc.RecruitmentId = obj.RecruitmentId ?? 0
        vc.TypePush = "MAIN"
        vc.hidesBottomBarWhenPushed = true
        self.pushViewControler(vc, AppLanguage.jobsTabTitle)

    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if indexPath.row == arrData.count - 3 {
            if isLoadMore {
                isLoadMore = false
                timer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(loadmore), userInfo: nil, repeats: true)
            }
        }
    }
    @objc func loadmore(){
        timer.invalidate()
        FromRecord += 20
        loadData()
    }
}
