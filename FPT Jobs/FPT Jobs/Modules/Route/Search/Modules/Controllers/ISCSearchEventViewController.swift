//
//  ISCSearchEventViewController.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SDWebImage
class ISCSearchEventViewController: UIBaseViewController {

    @IBOutlet weak var lblTotal: UIAutoSizeLabel!
    @IBOutlet weak var tblList: UITableView!
       var FromRecord : Int = 0
       var PageSize : Int = 20
       var Content : String = ""
       var arrData : [ISCNewsOutput] = []
       var timer = Timer()
       var isLoadMore : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
         self.isShowChat = false
        // Do any additional setup after loading the view.
        let nibLoading = UINib(nibName: LoadingTableViewCell.name, bundle: nil)
        tblList.register(nibLoading, forCellReuseIdentifier: LoadingTableViewCell.name)

        let cellNib = UINib(nibName: ISCListNewsTableViewCell.name, bundle: Bundle.main)
        tblList.register(cellNib, forCellReuseIdentifier: ISCListNewsTableViewCell.name)
        tblList.delegate = self
        tblList.dataSource = self
        tblList.separatorStyle = .none
        tblList.showsVerticalScrollIndicator = false
        tblList.rowHeight = UITableView.automaticDimension
        tblList.estimatedRowHeight = 100
    }
    
    
       override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           self.showHUD(message: AppLanguage.connecting)
           loadData()
       }
       
       func searchBar(){
           FromRecord = 0
           arrData = []
           loadData()
       }

       func loadData(){
           self.fullSurveyStructureSearch(ISCFullArticleSearchInput(PageSize: PageSize, FromRecord: FromRecord, Content: Content), { (data) in
            self.hideHUD()
            self.lblTotal.text = "\(data.Data?.TotalCount ?? 0)"
            if let array = data.Data?.SurveyStructures {
                  array.forEach { (val) in
                     self.arrData.append(val)
                  }
              }
               if data.Data?.Article?.count ?? 0 > 0 {
                     if data.Data?.Article?.count ?? 0 < 20 {
                        self.isLoadMore = false
                    }else {
                        self.isLoadMore = true
                    }
                }else {
                    self.isLoadMore = false
                }
                self.tblList.reloadData()
           }) { (error) in
               self.hideHUD()
               self.alertMessage(title: AppLanguage.commonAppName, message: error)
           }
       }
}
// MARK: - UITableView Delegate & Datasource
extension ISCSearchEventViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         guard let cell = tblList.dequeueReusableCell(withIdentifier: ISCListNewsTableViewCell.name, for: indexPath) as? ISCListNewsTableViewCell
                   else {
                       return UITableViewCell()
               }
               if arrData.count > 0 {
                   let obj = arrData[indexPath.row]
                SDWebImageManager.shared.loadImage(with: URL(string: "\(K.serverAPI.domain)\(obj.ImagePath ?? "")"), options: .continueInBackground, context: nil, progress: nil) { (image, data, error, type, success, url) in
                       if let img = image {
                           cell.imgNews.image = img
                       }
                   }
                   cell.lblTitle.text = obj.Title
                   cell.lblDescription.text = obj.Description
                   cell.lblDate.text = obj.CreatedDateStr
               }
               cell.selectionStyle = .none
               return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrData[indexPath.row]
        let storyboard = UIStoryboard(name: "News", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ISCNewsDetailViewController") as! ISCNewsDetailViewController
        vc.Title = obj.Title ?? ""
        vc.Date = obj.CreatedDateStr ?? ""
        vc.Description = obj.Description ?? ""
        vc.Content = obj.Url ?? ""
        vc.tabTitle = AppLanguage.eventTabTitle
        vc.hidesBottomBarWhenPushed = false
        self.pushViewControler(vc, AppLanguage.eventTabTitle)

    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if indexPath.row == arrData.count - 3 {
            if isLoadMore {
                isLoadMore = false
                timer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(loadmore), userInfo: nil, repeats: true)
            }
        }
    }
    @objc func loadmore(){
        timer.invalidate()
        FromRecord += 20
        loadData()
    }
}
