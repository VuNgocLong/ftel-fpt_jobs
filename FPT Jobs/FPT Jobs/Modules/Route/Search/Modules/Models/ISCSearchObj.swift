//
//  ISCSearchObj.swift
//  FPT Jobs
//
//  Created by PHAM CHI HIEU on 3/5/20.
//  Copyright © 2020 fun.sdk.ftel.vn.su4. All rights reserved.
//

import Foundation
struct ISCSearchForMobileInput:Encodable {
    var ArticleFromRecord : Int?
    var ArticlePageSize : Int?
    var JobFromRecord : Int?
    var JobsPageSize : Int?
    var Content : String?
}
struct ISCSearchForMobileOutput: Codable {
    var RecruitmentNews:[RecruitmentNewsModel]?
    var Article:[ArticleModel]?
    var TotalCount:Int?
}
struct RecruitmentNewsModel: Codable {
    var RecruitmentId:Int?
    var BranchId:Int?
    var BranchAddressId:Int?
    var AreaId:Int?
    var AreaName:String?
    var JobTypeId:Int?
    var Title:String?
    var ContactPerson:String?
    var NumberPositions:Int?
    var JobPositionId:Int?
    var JobLevelId:Int?
    var MinSalary:String?
    var MaxSalary:String?
    var CurrentUnit:String?
    var ShowSalary:Int?
    var JobRequirement:String?
    var JobRequest:String?
    var JobBenefit:String?
    var JobInfomation:String?
    var NewsType:Int?
    var JobType:Int?
    var Gender:Int?
    var AgeFrom:Int?
    var AgeTo:Int?
    var Highlight:String?
    var JobCategoryId:Int?
    var DescriptionImage:String?
    var DescriptionVideo:String?
    var CountView:Int?
    var CountApply:Int?
    var CreatedUser:String?
    var CreatedDate:String?
    var ModifiedUser:String?
    var ModifiedDate:String?
    var StartDate:String?
    var EndDate:String?
    var Hot:Int?
    var ExamType:Int?
    var ProcessStatus:Int?
    var CouncilId:Int?
    var company_name:String?
    var company_info:String?
    var JobFullDescription:String?
    var Address:String?
    var HashTags:String?
    var BranchName:String?
    var Description:String?
    var FullName:String?
    var Email:String?
    var Telephone:String?
    var MobilePhone:String?
    var RelativeList:String?
    var BranchAddresViewModels:String?
    var EndDateStr:String?
}
struct ArticleModel: Codable {
    var ArticleID:Int?
    var ArticleType:String?
    var FullName:String?
    var DisplayOrder:Int?
    var Title:String?
    var Description:String?
    var Content:String?
    var ImagePath:String?
    var CreatedDate:String?
    var CreateBy:Int?
    var ModifiedDate:String?
    var ModifiedUser:Int?
    var Deleted:Int?
    var Pin:Int?
    var CreatedDateStr:String?
}
struct ISCFullJobsSearchInput:Encodable {
    var PageSize : Int?
    var FromRecord : Int?
    var Content : String?
}
struct ISCFullJobsSearchOutput: Codable {
    var RecruitmentNews:[ISCJobsOutput]?
    var TotalCount:Int?
//    var RecruitmentId:Int?
//    var BranchId:Int?
//    var JobCode:Int?
//    var BranchAddressId:Int?
//    var AreaId:Int?
//    var AreaName:String?
//    var JobTypeId:Int?
//    var Title:String?
//    var ContactPerson:String?
//    var NumberPositions:Int?
//    var JobPositionId:Int?
//    var JobLevelId:Int?
//    var MinSalary:String?
//    var MaxSalary:String?
//    var CurrentUnit:String?
//    var ShowSalary:Int?
//    var JobDescription:String?
//    var JobRequirement:String?
//    var JobRequest:String?
//    var JobBenefit:String?
//    var JobInfomation:String?
//    var NewsType:Int?
//    var JobType:Int?
//    var Gender:Int?
//    var AgeFrom:Int?
//    var AgeTo:Int?
//    var Highlight:Int?
//    var JobCategoryId:Int?
//    var DescriptionImage:String?
//    var DescriptionVideo:String?
//    var CountView:Int?
//    var CountApply:Int?
//    var CreatedUser:String?
//    var CreatedDate:String?
//    var ModifiedUser:String?
//    var ModifiedDate:String?
//    var StartDate:String?
//    var EndDate:String?
//    var Hot:Int?
//    var ExamType:Int?
//    var ProcessStatus:Int?
//    var CouncilId:Int?
//    var company_name:String?
//    var company_info:String?
//    var JobFullDescription:String?
//    var Address:String?
//    var HashTags:String?
//    var BranchName:String?
//    var Description:String?
//    var FullName:String?
//    var Email:String?
//    var Telephone:String?
//    var MobilePhone:String?
//    var RelativeList:String?
//    var BranchAddresViewModels:String?
//    var EndDateStr:String?
}
struct ISCFullArticleSearchInput:Encodable {
    var PageSize : Int?
    var FromRecord : Int?
    var Content : String?
}
struct ISCFullArticleSearchOutput: Codable {
    var Article:[ISCNewsOutput]?
    var SurveyStructures:[ISCNewsOutput]?
    var TotalCount:Int?
//    var ArticleId:Int?
//    var ArticleType:String?
//    var Title:String?
//    var Description:String?
//    var Url:String?
//    var Content:String?
//    var ImagePath:String?
//    var CreatedDate:String?
//    var CreatedDateStr:String?
}
